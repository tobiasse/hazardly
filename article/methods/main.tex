\section{Data and Methods}\label{sec:data_and_methods}
We build our model to estimate past and future impacts of extreme events on crop yield between 1918 and 2100 in Europe
at a regional scale (NUTS-1) of three components, as shown in Figure~\ref{fig:flowchart}.
First, we use high-resolution daily temperature data to quantify the annual number of heat and cold wave days, as
detailed in section~\ref{subsec:extreme_events}.
Second, from annual crop production and area data, we calculate the crop yield at the regional level described in
section~\ref{subsec:crop_yield}.
Third, these two datasets are combined to derive our model to estimate crop damages in response to extreme events
described in section~\ref{subsec:damage}.
\begin{figure}[htbp]
    \centering
    \includesvg[width=1.\columnwidth]{flowchart}
    \caption[Impact model in a nutshell]{
        Our model is built of three components.
        First, the magnitude of annual extreme events (blue) is combined with annual crop yield (green) to derive our
        model to estimate crop damages (red) in response to extreme events across Europe.
    }
    \label{fig:flowchart}
\end{figure}

\subsection{Extreme Events}\label{subsec:extreme_events}
Various numerical and statistical thresholds are known, in the scientific community, to quantify extreme events in
weather data records~\citep{McPhillips2018}.
The types of extreme events to be quantified are limited by the selected method and the impact under research, e.g.,
long-lasting extreme events like droughts or heat waves generally are much more likely to have a measurable impact
on crop yield compared to sudden extremes like heavy precipitation or floods which impacts can vary greatly dependent
on the regional structure~\citep{Lesk2016,Vogel2019,Sivakumar2020,Bras2021}.
For this study, we employ the methodology of percentile-based extremes described by \citet{Zhang2005} to generate a
time series of yearly summer heat waves for June, July, and August across Europe between 1981 and 2100.
Additionally, we introduce a time series of yearly cold waves as a counterfactual, analyzed for January--March and
October--December.

Percentile-based extremes compare daily threshold sequence with weather data to record exceedance.
Daily thresholds, a selected percentile of a daily weather data sample, are derived from a climatology of a selected
length in climate science commonly 30 years.
Optimally, the baseline should exceed 100 years, but such long daily weather records are often unavailable.
Therefore, it is increased by selecting weather data within an odd-sized window centered around the day in question
to remove artifacts due to the smaller sample size with shorter baselines.
An exceedance index is calculated by comparing the threshold sequence with daily weather records to represent days
with extremes according to the selected parameters.
The thresholds may be compared with in-base data, which is the time frame used for threshold generation, or with
out-of-base data.
\citet{Zhang2005} show that the exceedance index derived from the in-base shows inhomogeneities compared to the index
derived from the out-of-base.
Therefore, a bootstrapping technique, which presents a mean exceedance index as the result of repeatedly calculating
the thresholds and index, is introduced to counteract this.

For the historical period 1981--2020, we used daily minimum and maximum temperatures from the ERA5-Land
dataset~\citep{Munoz2019}.
ERA5-Land is a reanalysis dataset produced from the ERA5 data that provides atmospheric metrics for a nine-km grid.
Our daily thresholds are predicted from the ninetieth percentile of the minimum and maximum temperature for a baseline
of thirty years from 1981--2010 by applying a five days centre data window.
Additionally, we run our analysis for center data windows of nine and seventeen days.
We record the daily exceedance index by testing if the daily minimum and maximum temperature exceed the daily
thresholds for June, July, and August.
For the in-base exceedances, we applied bootstrapping as described by \citet{Zhang2005}.
Further, we removed minor heat waves by removing exceedance index sequences below three consecutive days.
This definition of a heat wave corresponds to the definition used by \citet{Sutanto2020}.
We aggregated the daily exceedance index to yearly, representing the number of heat wave days per year.
Next, we rescaled the grid-level heat waves to the NUTS-1 level using the minimum, maximum, and mean of all cells
within a NUTS unit.

For future projections for the RCP's 2.6, 4.5, and 8.5, we used EURO-CORDEX data from the following two global
circulation models ICHEC's EC-Earth and MPI-M's MPI-ESM-LR both downscaled by the regional circulation model SMHI's
RCA4.
Daily thresholds are generated from the historical experiments of both models for the baseline of 1981--2005.
After, we applied the same approach and parameter variations as described for historical data to derive yearly future
heat waves for 2021--2100.
Finally, we calculated the model average as the single data source for future heat waves.

We use the same datasets and parametrization for cold waves, except for the threshold percentiles set to the tenth
percentile of the daily minimum and maximum temperature.
Further, the exceedance index is calculated by testing if the daily values are below the selected threshold.

\subsection{Crop Yield}\label{subsec:crop_yield}
We calculate crop yield between 1975 and 2020 for 68 crop groups (Table~\ref{tab:cropids}) by dividing crop production
and area data provided by Eurostat for the NUTS 1 (Table~\ref{tab:nutsunits}) regions in Europe.
The agricultural data provided has gaps arising from unreported data or restructuring of regions.
By applying the aggregations shown in Table~\ref{tab:gapfilling} and additionally using NUTS 2 data, we filled data gaps
for the listed regions.
The final yield data coverage per region and crop group is shown in Figure~\ref{fig:coverage}, where coverage is the
number of years with data available divided by period length.

\subsection{Damage}\label{subsec:damage}
Quantifying crop yield loss by extreme events requires separating the damage from the overall increase in yield in most
regions due to advancements in agricultural practices~\citep{Arata2020}.
A viable approach for removing the trend-based signal is to find a linear trend and consider the residuals as damage.
However, values above the regression imply over-achievement, i.e., negative damage.
As an alternative, we propose an approach inspired by Data Envelopment Analysis.
We use a convex hull enveloping the yearly crop yield point cloud -- but consider only the upper segments of the hull and
do not allow decreasing segments, as shown in Figure~\ref{fig:convex_hull}(a).
\begin{figure}[htbp]
    \centering
    \includesvg[width=1.\columnwidth]{ch}
    \caption[Convex hull and damage function]{
        (A) Upper segments of the convex-hull enveloping potato (R1000) yield data in East Austria (AT1).
        The last segment of the hull is replaced with a segment representing constant yield in the dimension of
        the last vertice.
        (B) Parametrized damage function (black line) with 95\% confidence interval bands (grey) for yearly
        heat waves as exceedance days on the x-axis.
        The black-outlined dots show the relative damage calculated from the convex-hull segments' deviation.
    }
    \label{fig:convex_hull}
\end{figure}
We replace decreasing segments with a segment representing constant yield in the preceding dimension.
The resulting piecewise function represents the maximum achievable yield over time.

The damage $d_t$ in year $t$ is then defined in relative terms as the difference between the maximum achievable yield
$h$ and actual yield $y$ divided by the maximum achievable yield
\begin{equation}
    \label{eq:damage}
    d_t = \frac{h_t - y_t}{h_t}.
\end{equation}
The damage, by definition between 0 and 1, can then be compared to the magnitude of the extreme events, as shown in
Figure~\ref{fig:convex_hull}(b), where we plot damage as a function of the exceedance days.
As expected, the damage increases and years with more exceedance days tend to come with greater damage.
To interpolate and extrapolate this relationship, we employ a logistic function where the estimated mean damage $d$ is a
function of the exceedance days $i$
\begin{equation}
    \label{eq:model}
    d = \frac{2}{1 + e^{-\beta i}} - 1.
\end{equation}
We estimate the parameter $\beta$ by transforming the damages and fitting a linear model to the transformed quantity
$-\ln \left( \frac{2}{d + 1} - 1 \right) = \beta i$.
For the model, we assume that the damage is 0 if the magnitude of the extreme event is 0.

We expect that not for every region and crop type, the mean damage in response to the magnitude of the extreme event can
be predicted by our model due to regional differences in management practices (irrigation), the resilience of individual
crops against extreme events, or simply data availability~\citep{Jackson2021}.
Therefore, we employ a decision tree to filter for unreliable damage functions compared to the following four criteria.
The first criterion is that more than 18 years (coverage $\ge$ 0.4) of yield data must be available.
Second, the Spearman correlation coefficient between damage and exceedance days must be larger than 0.4 (correlation
$\ge$ 0.4); the yield decreases for an increase in the magnitude of the extreme event.
Third, the correlation's significance should at least correspond to the p-value of 0.05.

We calculate the significance by shuffling 1000 times the weather extreme time series and calculating for each the
correlation coefficient, leading to a distribution of values that can occur in the absence of correlations.
This distribution is used to calculate the significance levels, i.e. $-1.96 > z > 1.96$ (*), $-2.58 > z > 2.57$ (**),
and $-3.32 > z > 3.32$ (***), and helps to rule out that the correlation between damage and the extreme event is by
spurious correlations.
Finally, the $r^2$ of the least square regression must be above 0.5 ($r^2 \ge 0.5$).

Now, we can use the set of damages functions to quantify the impact of extreme events on crop yield at a subnational
scale for regions and individual crop types for the past and future across Europe.
We present our estimates as the mean damage between 1991--2020 and 2071--2100 for the historical and future periods,
respectively.
The estimates are weighted by the mean production between 1991 and 2020,
\begin{equation}
    \label{eq:aggregation}
    \bar{d} =
    \sum_{i=0}^{n}
    \frac{\bar{p_i}}{p}
    \bar{d_i}
    \,
\end{equation}
where $\bar{p_i}$ is the mean production of selected crop type and region, $p$ the total production, and
$\bar{d_i}$ the mean damage.
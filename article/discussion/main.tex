\section{Discussion}\label{sec:discussion}
% Summary, key messages, and selling points
Our study examined the effects of heat waves on crop yield at a sub-national level for various crop types while also
considering the impacts of cold waves as a counterfactual.
Our findings revealed that past cold waves had minimal impact on crop yield and will become even less critical with no
change for RCP2.6 and 0.4 times and 0.1 times decrease in damage for RCP4.5 and 8.5, respectively
(Figure~\ref{fig:hw_cw_nuts_damage_significant}).
Similar studies analyzing past cold wave impacts conducted globally or throughout the EU have yielded similar
results~\citep{Lesk2016,Bras2021}.
On the contrary, heat waves have had and will continue to impact crop yield, potentially putting food security
in Europe at risk, depending on the future scenario.
If global warming reaches approximately 2$^\circ$C (RCP2.6) by 2100, crop yield damage caused by heat waves could
increase by 1.7 times compared to historical damage.
The warming potential of RCP4.5 and 8.5, which predicts a global warming of around 3$^\circ$C and 4$^\circ$C by 2100,
respectively, could lead to a 3 times or 4.6 times increase in damage caused by heat extremes.

% Comparison to other studies
% TODO can be added comparison our study (13% Cereals (maize included, 12.7% Non-Cereals) with Lesk2016 (Cereals 7.6%)
% and Bras2021 (7.3% Cereals and 3.1% Non-Cereals) our
It is challenging to compare our findings to other studies due to differences in our methods.
For instance, we use statistical thresholds based on percentiles to define extremes, while similar studies often rely on
disaster databases like EM-Dat.
Additionally, we assess the impact of extremes by looking at the maximum potential yield instead of the mean yield.
Finally, our study examines a sub-national scale, as many other studies are limited to global or national scales based
on FAO and EM-Dat data.

% Sell method
Our study introduces a new non-parametric method for analyzing how weather extremes affect crop yield on a sub-national
level.
One advantage of our approach is that it does not rely on natural disaster databases like EM-Dat.
We define weather extremes statistically, using percentile-based temperature extremes such as heat and cold waves.
However, our method can be adapted to include other definitions of weather extremes or different types of extreme events
to explore the effects of compounding.
We can also study the impact of extremes on different spatial scales as long as we have crop yield data and fine-scale
extreme event data for resampling.
Our method is modular so that we can replace the detection of damage years with other methods, such as deviation from a
linear model.
Finally, we can extrapolate damage for multiple climate futures.

% Broader context
Our research indicates that it is crucial to keep climate change within 2$^\circ$C to minimize the adverse effects of
heat waves on crop production, especially considering the recent IPCC report, which suggests that is very likely that
heat extremes will become more frequent~\citep{ar6,Sivakumar2020}.
As a result, areas that are already susceptible to crop damage from heat waves, such as Northern Italy, Spain, and
Southern France, should take steps to reduce the damage caused by extreme heat
(Figure~\ref{fig:hw_cw_nuts_damage_significant}).
This may involve enhancing water management and agricultural techniques to prevent significant impacts.
However, considering that climate change and extreme events could intensify water scarcity and conflicts arising from
competing water use, e.g., energy production or irrigation.
Other measures may include early warning systems for extreme events and enhancing accessibility and usability of methods
to estimate expected damages, which is one of the advantages of our model.
Our study complements and confirms the results in the scientific literature that climate change and extreme events
jeopardize food security.
Therefore, it is pivotal to integrate adaptation measures to mitigate crop yield loss.

% Outlook
Our next step is to use our model to explore how compounding extreme events affect crop
yield~\citep{Zscheischler2020,Lesk2022}.
We are particularly interested in studying the effects of heat and dry extremes on different regions in Europe and how
temporal compounding extremes can worsen crop yield damage.
In addition, we aim to investigate whether aligning the definitions of extreme events with plant properties can improve
our predictions for certain crop types~\citep{Jackson2021}.
Furthermore, we plan to use the model to examine the impact of extreme events on other natural resources, such as
timber production.
To analyze the model's effects on predicted damage, we will change the damage-isolating convex-hull function by linear
regression and compare the results.

% Limitations
While our study is valuable to climate impact science, there are a few limitations to consider.
Firstly, the model assumes static farming practices and does not account for anthropogenic adaptation measures to
climate change.
Secondly, the definition of cold waves does not encompass crop impacts such as late spring frost.
Lastly, our study's definition of heat waves is more closely related to human health impacts rather than plant biology.
Despite these limitations, our approach contributes valuable insights, particularly regarding scale independence,
modularity, and non-parametric nature.
Additionally, our findings demonstrate that the selected heat wave definition is feasible for studying impacts.
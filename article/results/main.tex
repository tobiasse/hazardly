\section{Results}\label{sec:results}
In this study, we analyzed the impact of past and future heat and cold extremes on crop yield across Europe between
1981 and 2100 per region and crop type.
First, we present our results for the impacts of extreme events on crop yield by regions in
section~\ref{subsec:damage_region}, followed by crop types in section~\ref{subsec:damage_crop}, and finally, we
conclude with general remarks on the model parametrization in section~\ref{subsec:parametrization}.

\subsection{Damage per Region}\label{subsec:damage_region}
Our study shows that heat waves are a relevant cause of crop yield loss, and the magnitude of the damage will increase
with higher global mean temperature and, therefore, the higher frequency of this extreme, as
Figure~\ref{fig:hw_cw_nuts_damage_significant}(A1--D4) shows.
For the historical scenario, our study shows that the mean yield damage caused by heat waves for most of the regions
in Europe is below 20\%.
In comparison, only 10 NUTS regions show heat damage above 20\%, with the most severe damage in Centro Italy (39\%).
Predictions for RCP2.6, which overshoots the 1.5$^\circ$C goal, show that the number of regions with mean yield damage
exceeding 20\% increases to 19, predominantly located in the south of Europe.
An increasing temperature signal and, therefore, higher frequency of heat waves will lead to large-scale yield damage
across Europe, as our analysis for RCP4.5 and 8.5 suggests.
In more than half (30) of the regions under research (51), the mean damage is above 30\% for RCP4.5, and RCP8.5 presents
the most severe scenario, with damage above 50\% in 37 regions.
\begin{figure*}[htbp]
    \centering
    \includesvg[width=1.\textwidth]{hw_cw_damages_map_significant_landscape}
    \caption[Heat waves: Mean damage per spatial unit]{
        Aggregated predicted relative mean damage by heat waves (A--D) and cold waves (E--H) per region
        for historical (1), RCP2.6 (2), 4.5 (3), and 8.5 (4).
        For the aggregation, only the output of the damage functions that comply with the defined filter
        criteria: yield data coverage $\geq$ 0.4, extreme correlation with damage $\geq$ 0.4, z-score $\geq$
        1.96, and $r^2$ $\geq$ 0.5.
    }
    \label{fig:hw_cw_nuts_damage_significant}
\end{figure*}
In comparison, cold waves are a minor cause of damage to agriculture.
We found only 13 regions with significant crop yield damage caused by cold extremes,
Figure~\ref{fig:hw_cw_nuts_damage_significant}(E1--H4).
Further, with increasing global mean temperature, the relative damage is decreasing, related to the decreasing
frequency and magnitude of cold waves.
Our findings show that in the past, crop yield damage for most of the region (10) was well below 20\% while we measured
mean damage of approximately half of the crop yield for Noreste in Spain.
As the global mean temperature increases, the mean damage decreases to below 5\% (RCP8.5) in all regions.

\subsection{Damage per Crop Type}\label{subsec:damage_crop}
Corresponding to the regional results, the mean crop yield damage per crop type increases with the rise in the global
mean temperature.
For the crop group of cereals (7 Cxxxx crops), historical damage is between 7\% (wheat and spelt and barley) and 32\%
(triticale), while the damage increase lightly per cereal crop in the RCP2.6 scenario, as listed in
Table~\ref{tab:hw_crop_damage_significant}.
Our results for the scenarios RCP4.5 and 8.5 predict severe yield damage, accounting for 2 and 5 crops with a loss of
more than 50\% of the yield, respectively.
Oil crops (7 Ixxxx crops) show a mean yield damage between 8\% (rape and turnip rape) and 23\% (tobacco) for the
historical analysis.
Comparable to the cereals, the damage increases by applying the climate projections of RCP2.6, 4.5, and 8.5.
However, just one oil crop shows damage below 50\% (rape and turnip rape) for RCP8.5.
For green maize (G3000), we measure a mean damage of 10\% in the historical scenario, while for RCP2.6, 4.5, and 8.5,
the yield damage accounts for 16\%, 27\%, and 51\%, respectively.
Historic damage for the root crops (2 Rxxxx crops) potatoes and sugar beet accounts for 8\% and 11\% of the yield, while
future projections for RCP8.5 predict a loss of approximately half of the yield.
\begin{table*}[htbp]
    \tiny
    \centering
    \caption[Mean damage per crop type]{
        Aggregated predicted relative mean damage by heat waves per crop type for historical and future
        projections.
        For the aggregation, only the output of the damage functions that comply with the defined filter
        criteria: yield data coverage $\geq$ 0.4, extreme correlation with damage $\geq$ 0.4, z-score of the
        observed correlation with the randomized samples $\geq$ 1.96, and $r^2$ $\geq$ 0.5.
        The crop types correspond to the following crop IDs in the order of appearance: C000, C1100, C1110,
        C1111, C1120, C1300, C1600, G3000, I1100, I1110, I1120, I1130, I1110-1130, I1140, I3000, P0000,
        R1000, and R2000.
    }
    \label{tab:hw_crop_damage_significant}
    \begin{tabular}{@{}lrrrr@{}}
        \hline
        \textbf{Crop Typ}                      & \textbf{Historic} & \textbf{RCP2.6}   & \textbf{RCP4.5}   & \textbf{RCP8.5}   \\
        \hline
        Cereals                                & 0.08 (-0.09,  0.25) & 0.14 (-0.03,  0.31) & 0.27 ( 0.09,  0.43) & 0.48 ( 0.29,  0.63) \\
        Wheat and spelt                        & 0.07 (-0.05,  0.18) & 0.14 ( 0.02,  0.26) & 0.27 ( 0.12,  0.39) & 0.59 ( 0.41,  0.73) \\
        Common wheat and spelt                 & 0.08 (-0.12,  0.27) & 0.12 (-0.08,  0.31) & 0.21 ( 0.01,  0.40) & 0.42 ( 0.20,  0.60) \\
        Common winter wheat and spelt          & 0.09 (-0.10,  0.27) & 0.17 (-0.03,  0.35) & 0.37 ( 0.16,  0.54) & 0.61 ( 0.39,  0.76) \\
        Durum wheat                            & 0.23 (-0.26,  0.59) & 0.44 (-0.02,  0.69) & 0.71 ( 0.49,  0.83) & 0.84 ( 0.70,  0.92) \\
        Barley                                 & 0.07 (-0.06,  0.20) & 0.16 ( 0.03,  0.29) & 0.29 ( 0.15,  0.41) & 0.55 ( 0.41,  0.66) \\
        Triticale                              & 0.32 (-0.28,  0.74) & 0.52 (-0.04,  0.84) & 0.90 ( 0.65,  0.98) & 0.99 ( 0.92,  1.00) \\
        Green maize                            & 0.10 (-0.08,  0.28) & 0.16 (-0.03,  0.33) & 0.27 ( 0.09,  0.44) & 0.51 ( 0.33,  0.65) \\
        Oilseeds                               & 0.10 (-0.04,  0.24) & 0.32 ( 0.16,  0.46) & 0.54 ( 0.38,  0.67) & 0.89 ( 0.77,  0.95) \\
        Rape and turnip rape seeds             & 0.08 (-0.18,  0.33) & 0.11 (-0.15,  0.35) & 0.18 (-0.09,  0.42) & 0.40 ( 0.12,  0.62) \\
        Sunflower seed                         & 0.12 (-0.10,  0.32) & 0.22 ( 0.00,  0.41) & 0.42 ( 0.22,  0.59) & 0.66 ( 0.49,  0.77) \\
        Soya                                   & 0.12 (-0.08,  0.31) & 0.20 ( 0.00,  0.39) & 0.49 ( 0.30,  0.65) & 0.67 ( 0.49,  0.80) \\
        Rape, turnip, sunflower seeds and soya & 0.17 (-0.14,  0.43) & 0.30 (-0.00,  0.54) & 0.52 ( 0.23,  0.70) & 0.79 ( 0.59,  0.88) \\
        Linseed                                & 0.10 (-0.10,  0.29) & 0.17 (-0.04,  0.36) & 0.32 ( 0.11,  0.50) & 0.64 ( 0.43,  0.78) \\
        Tobacco                                & 0.23 (-0.23,  0.59) & 0.40 (-0.04,  0.71) & 0.73 ( 0.41,  0.89) & 0.94 ( 0.80,  0.98) \\
        Dry pulses and protein crops           & 0.17 (-0.23,  0.52) & 0.32 (-0.07,  0.62) & 0.57 ( 0.23,  0.78) & 0.86 ( 0.67,  0.94) \\
        Potatoes                               & 0.08 (-0.08,  0.23) & 0.13 (-0.03,  0.28) & 0.22 ( 0.06,  0.37) & 0.48 ( 0.31,  0.61) \\
        Sugar beet                             & 0.11 (-0.09,  0.30) & 0.17 (-0.03,  0.35) & 0.29 ( 0.09,  0.46) & 0.53 ( 0.35,  0.67) \\
        \hline
    \end{tabular}
\end{table*}

\subsection{Model Parametrization}\label{subsec:parametrization}
To parametrize our model, first, we determined the optimal window size and the rescaling of our measure of extremeness.
Second, we calculated the direction and magnitude of the correlation between extreme and damage; third, we filtered the
fitted damage functions.

% Extremes parameter selection
We used a center data window of 5 days and the maximum to rescale our annual sequences of heat and cold extremes to the
NUTS 1 scale.
This parameter configuration is selected based on maximizing the number of significant positive correlations
(z-score $\geq$ 1.96), which means the damage increases with an increasing number of days under extreme conditions.
For cold waves, maximum rescaling and the window sizes of 5, 9, and 17 days account for 44, 43, and 34 significant
positive correlations, respectively.
Cold waves obtained by mean rescaling and the window sizes of 5, 9, and 17 days account for 40, 43, and 38
correlations, respectively.
We measured 219, 217, and 199 significant correlations for heat waves by applying maximum rescaling and using the window
sizes of 5, 9, and 17 days.
With mean rescaling, the windows of 5, 9, and 17 days account for 202, 203, and 185 significant positive correlations,
respectively.

% Correlation details selected parameters
Figures~\ref{fig:hw_correlations} and~\ref{fig:cw_correlations} show that heat and cold waves account for 452 and 1311
negative correlations, which is the damage decrease with an increase in extreme magnitude, for our selected parameter
configuration.
Considering the number of significant positive correlations, this also shows that cold waves have a neglectable impact
on crop yield (Figures~\ref{fig:hw_correlations_significant} and~\ref{fig:cw_correlations_significant}).
However, this type of extreme acts as counterfactual proof of the relevance of heat waves and the applied methodology.

% Damage function
Building on our study's heat extremes and crop yield data, we could parametrize 3070 damage functions for the different
NUTS crop type pairs (7276, 107 NUTS-1 units x 68 crop types).
The crop yield data availability limited the parametrization to the number of damage functions.
Additionally, 2134 parametrized damage functions are filtered because yield data coverage is below 40\% (less than 18
years of data).
Furthermore, 751 are removed by a weak correlation ($<$ 0.4) between extreme weather and crop damage, while 62 and
2 are filtered due to no significance (z-score $<$ 1.96) and $r^2$ below 0.5.
After the filtering, 121 damage functions remain fulfilling our defined criteria.

% Damage function example
Figure~\ref{fig:functions} shows the predicted yearly mean damage between 1981--2100 by heat waves for potatoes in
East Austria as an example of the selected damage functions.
For the historical experiment, we observe a median damage of 5\% for potato crops.
Our analysis predicts that heat waves will increase damage to potato yield in East Austria.
Depending on the selected future climate scenario, the median damage accounts for 15\%, 27\%, and 36\% for RCP2.6, 4.5,
and 8.5, respectively.
\begin{figure}[htbp]
    \centering
    \includesvg[width=1.\columnwidth]{func}
    \caption[Historical and projected damage]{
        (A-C) Historical (black) and projected for RCP2.6 (blue), 4.5 (green), and 8.5 (orange) predicted
        yearly relative damage by heat waves for potato (R1000) yield in East Austria (AT1) with 95\%
        confidence interval bands (similar coloured shaded area enclosing the lines).
        The red line shows a 31-year centre data rolling average for the projected damage.
        The boxplot (D) shows the distribution of heat wave damage for each experiment from 1981--2100.
    }
    \label{fig:functions}
\end{figure}
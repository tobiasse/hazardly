\section{Report}\label{sec:report}

\subsection{Regional patterns of climate extremes}\label{subsec:extremes}

\subsubsection{Introduction}

% Data and methods
Additionally, we use a percentile-based approach to assess the annual exceedance index of the three weather extremes
heat waves, cold waves, and flash droughts for the past (1981–--2020) and future (2021--2100)~\citep{Zhang2005}.
We give a robust extreme event impact assessment based on this non-parametric definition of weather extremes.
For the past, we used gridded 0.1°x0.1° daily weather records from the ERA5-Land reanalysis dataset, and for future
projections, we use modelled daily weather records from EURO-CORDEX~\citep{Christensen2020,Munoz2019}.
The baseline period for the historical scenario is 1981--2010, and for future projections 1981--2005.
Daily thresholds for heat waves, cold waves, and flash droughts are estimated from the 90th percentile of the daily
minimum and maximum temperature, 10th percentile of the daily minimum and maximum temperature, and 30th percentile of
the soil volumetric water content (0--28cm), respectively~\citep{Sutanto2020}.
We use a five days centre data window for all three extreme events to estimate the thresholds from the previously listed
baseline periods.
The annual exceedance index for heat waves is calculated as the sum of days, at least for 3 consecutive days, in wich
the daily temperature values exceed the thresholds for June, July, and August.
For cold waves the annual exceedance index is the sum of days, at least for 3 consecutive days, in which the daily
temperature values are below the thresholds for January, February, October, November, and December.
Inbase exceedance is calculated by applying bootstrapping (1000x repetitions) for both extreme events.
Heat and cold wave exceedance indices are rescaled to NUTS1 regions by using a maximum resampling.
% Drought at least 5 days and threshold is target volume
We use sequent peak analysis to detect annual flash droughts, remove minor droughts, and pool interdependent droughts
for the season from June to October~\citep{Biggs2004}.
Droughts are rescaled to NUTS1 regions by using a mean resampling.

\subsubsection{Weather extremes}

We observe that the frequency of heat waves will increase in future across Europe while the frequency of cold waves
will decrease (Figures~\ref{fig:report_hw} and~\ref{fig:report_cw}).
Our study shows that the frequency of heat waves will drastically increase in the European south and north compared to
the past.
This increase in heat extremes implies that these extremes will have a socioeconomic impact in the future, which we will
discuss in the following section.
For flash droughts we find that the centre of France and Germany experienced the highest frequency while the north of
Great Britain and Ireland experienced the lowest frequency (Figure~\ref{fig:report_d}).
The alpine regions of Austria, Switzerland, and Italy also experienced a low frequency of flash droughts.

\begin{figure}[htbp]
    \centering
    \includesvg[scale=.5]{hw}
    \caption[Heat waves across Europe]{
        Frequency of heat waves for historical 1981–2020 (A) and the future 2021–2100 scenarios RCP2.6 (B), 4.5 (C), and
        8.5 (D).
        In future, the frequency of heat waves will increase across Europe.
    }
    \label{fig:report_hw}
\end{figure}
\begin{figure}[htbp]
    \centering
    \includesvg[scale=.5]{cw}
    \caption[Cold waves across Europe]{
        Frequency cold waves for historical 1981–2020 (A) and the future 2021–2100 scenarios RCP2.6 (B), 4.5 (C), and
        8.5 (D).
        In future, the frequency of cold waves will decrease across Europe.
    }
    \label{fig:report_cw}
\end{figure}
\begin{figure}[htbp]
    \centering
    \includesvg[scale=.5]{d}
    \caption[Flash droughts across Europe]{
        Historical (1981–2100) flash drought frequency across Europe.
    }
    \label{fig:report_d}
\end{figure}

\subsection{Impacts of weather extremes}\label{subsec:impacts}

\subsubsection{Introduction}

% Introduction
Climate change and the increasing frequency of specific weather extremes and their impacts are common topics in climate
science, as several studies for different ecological and economic sectors show~\citep{ar6}.
% Literature review and gaps
The inter-sectoral impact model intercomparison project (ISIMIP) provides a broad range of datasets related to the
impacts of climate change on different sectors like agriculture, biodiversity, or terrestrial biodiversity
~\citep{Frieler2017}.
Although ISIMIP covers a broad range of impacts, these impacts are driven by a changing climate and not exclusively by
weather extremes~\citep{Piontek2014}.
Therefore, this data is not suitable for the requirements of the Bioclimapaths project.
Additionally, independent studies show that past and recent weather already caused damage to bio-economical relevant
sectors such as agriculture and future scenarios suggest that this damage will increase with climate change
~\citep{Bras2021,Lesk2016,Deryng2014}.
The impact data published alongside the listed relevant scientific literature is either global or national.
Comparable to ISIMIP data, impacts are forced by a changing climate or the usage of extreme weather disasters from
EM-DAT .
Both approaches are not within the requirements of the Bioclimapaths project.
% Project requirements
For the Bioclimapaths project, we need European sub-national scale (NUTS1) impact data for relevant bioeconomic sectors
on an annual scale for the past and future scenarios.
Preferable for weather extremes analyzed by a reproducible method aligned with the current best scientific practice.
Furthermore, the impact data must be available for the past and a selection of future scenarios.
Agriculture is identified as one relevant bioeconomic sectors for the project.
We developed a novel non-parametric method to analyze the impacts of dependence on weather extremes by aligning to the
previously listed conditions.
% Data and methods
We use sub-national (NUTS1) annual (1975–2021) crop (68 groups) yield data from Eurostat and the annual exceedance
magnitudes of the weather extremes introduced in Section 2.3.
Years with substantial damage are identified by calculating the deviation of each year’s yield from the upper splines of
a convex hull enveloping each crop yield time series (Figure~\ref{fig:report_ch}).
Damage functions (3070 functions) are derived using an ordinary least squares regression to fit a linear model.
The independent variable is the magnitude of the selected weather extreme, and the dependent variable is an inverted
logistic transform of the previously identified substantial yield damage years (Figure~\ref{fig:report_ch}).
We identify significant damage functions per extreme, spatial unit and crop type using the following four descriptive
variables: yield data coverage, Spearman’s rank correlation coefficient, the significance of the correlation coefficient
derived from time series randomization, and R-squared of the regression.
The significant damage functions are used to estimate the mean impact of weather extremes on crop yield per crop group,
NUTS1 unit for the past (1981–2021) and future (2022–2100) scenarios RCP 2.6, 4.5, and 8.5 (Figure
~\ref{fig:report_functions}).
We aggregate the predicted mean damages by production-weighted mean for each NUTS1 unit and crop group.
\begin{figure}[htbp]
    \centering
    \includesvg[scale=.5]{ch}
    \caption[Convex hull and damage function]{
        (A) Upper segments of the convex-hull enveloping potato (R1000) yield data in Ostösterreich (AT1).
        (B) Parametrized damage function (black line) with 95\% confidence interval bands (grey) for yearly
        heat waves as exceedance days on the x-axis.
        The black-outlined dots show the relative damage calculated from the convex-hull segments' deviation.
    }
    \label{fig:report_ch}
\end{figure}
\begin{figure}[htbp]
    \centering
    \includesvg[scale=.5]{func}
    \caption[Historical and projected damage]{
        (A-C) Historical (black) and projected for RCP2.6 (blue), 4.5 (green), and 8.5 (orange) predicted
        yearly relative damage by heat waves for potato (R1000) yield in Ostösterreich (AT1) with 95\%
        confidence interval bands (similar coloured shaded area enclosing the lines).
        The red line shows a 31-year centre data rolling average for the projected damage.
        The boxplot (D) shows the distribution of heat wave damage for each experiment from 1981--2100.
    }
    \label{fig:report_functions}
\end{figure}

\subsubsection{Past and future crop yield damages across Europe}

% Introduction and limitations
For Bioclimapaths, we analyze the impact of three weather extremes, heat waves, cold waves, and flash droughts, on the
agricultural sector in Europe on the sub-national scale.
We use the relative damage on crop yield as a metric to measure the impact of weather extremes.
However, there needs to be more sub-national yield data coverage to provide more scientific, sound damage functions and,
therefore, a mean damage estimate for most crop groups and regions.
In this study, we reject 70\% (2134) of the functions for all extremes due to data coverage.
% Heat waves
Another 24\% of the damage functions with heat waves are weakly correlated, and we removed 2\% due to insignificant
correlation.
The remaining 4\% of the functions are significant and used to estimate the impact of heat waves on crop yield across
Europe.
For the historical scenario, our study shows that the mean yield damage caused by heat waves for most of the regions in
Europe is below 20\% (Figure~\ref{fig:report_hw_map}).
In comparison, only ten regions show heat damage above 20\%, with the most severe damage in Centro Italy (39\%).
Predictions for RCP2.6, which overshoots the 1.5$^\circ$C goal, show that the number of regions with mean yield damage
exceeding 20\% increases to 19, predominantly located in the south of Europe.
An increasing temperature signal and, therefore, higher frequency of heat waves will lead to large-scale yield damage
across Europe, as our analysis for RCP4.5 and 8.5 suggests.
In more than half (30) of the regions under research (51), the mean damage is above 30\% for RCP4.5, and RCP8.5 presents
the most severe scenario, with damage above 50\% in 37 regions.
Our estimates of large-scale yield damage by heat waves for RCP4.5 and 8.5 are especially alarming due to the high
likelihood of an increase in the frequency of heat waves across Europe~\citep{ar6}.
% Cold wave
For cold waves, only 0.6\% of the damage functions are significant, showing that this extreme has a minor impact on
European crop yield.
We rejected 29\% of the functions due to weak correlation and 0.4\% due to insignificant correlation.
Our study shows that damage in the past is below 10\% for eight regions, and for three regions, the estimated mean
damage is between 10\% and 25\% (Figure~\ref{fig:report_cw_map}).
Two regions in Spain and France show damage above 40\%; however, the 95\% confidence interval is wide (Spain -9\%–83\%
and France -39\%–87\%), limiting the mean estimates reliability.
In future, the impact of cold waves becomes neglectable, according to our analysis.
The low future impact is related to the high likelihood of a decrease in the frequency of cold waves and the already
relatively low impact of cold waves on crop yield.
% Flash drought
Our analysis of flash drought impacts is solely available for the historical scenario.
For EURO-CORDEX data, the required variable soil volumetric water content is not available as a precalculated product.
As for heat waves, 4\% of the damage functions are significant for flash droughts, and we reject 25\% of the functions
due to weak correlation and 1\% due to insignificant correlation.
In direct comparison with heat waves, droughts impact southern Spanish regions and Portugal (Figure
~\ref{fig:report_d_map}).
Central Germany, north Italy and France, and west Poland are regions that show impact by drought but not by heat waves.
Additionally, droughts impact crop yield more than heat waves in central France.
These dynamics strengthen the importance of analyzing extreme weather impacts within the framework of compounding
extremes in future research.

\begin{figure}[htbp]
    \centering
    \includesvg[scale=.5]{hw_damages_map_significant}
    \caption[Mean damage per spatial unit]{
        Aggregated predicted relative mean yield damage by heat waves per NUTS1 unit for historical (A), RCP2.6 (B),
        4.5 (C), and 8.5 (D).
        The legend highlights regions with significant damage according to the colour gradient.
    }
    \label{fig:report_hw_map}
\end{figure}
\begin{figure}[htbp]
    \centering
    \includesvg[scale=.5]{cw_damages_map_significant}
    \caption[Mean damage per spatial unit]{
        Aggregated predicted relative mean yield damage by cold waves per NUTS1 unit for historical (A), RCP2.6 (B),
        4.5 (C), and 8.5 (D).
        The legend highlights regions with significant damage according to the colour gradient.
    }
    \label{fig:report_cw_map}
\end{figure}
\begin{figure}[htbp]
    \centering
    \includesvg[scale=.5]{d_damages_map_significant}
    \caption[Mean damage per spatial unit]{
        Historical mean yield damage by flash droughts per NUTS1 unit.
        The legend highlights regions with significant damage according to the colour gradient.
    }
    \label{fig:report_d_map}
\end{figure}

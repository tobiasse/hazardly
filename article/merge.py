from re import Pattern
from re import compile
from typing import List

regex: Pattern = compile(r'\s*\\input{(.+)}\s*')

with open("article/main.tex") as f:
    lines: List[str] = f.readlines()
    to_merge: List[str] = list()

    for line in lines:
        if match := regex.match(line):
            texf: str = f"{match.group(1)}.tex" if len(match.group(1).split(".")) == 1 else match.group(1)
            with open(f"article/{texf}") as g:
                to_merge.append(g.read())
                to_merge.append("\n")
        else:
            to_merge.append(line)

with open("article/tmp.tex", "w") as f:
    f.writelines(to_merge)

\documentclass[aspectratio=169]{beamer}
\mode<presentation>

\usetheme{Singapore}
\usecolortheme{seagull}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}

\usepackage{svg}
\svgpath{{../figures/}}
\svgsetup{inkscapepath=svgsubdir}
\svgsetup{inkscapearea=page}

\usepackage{graphicx}
\graphicspath{{../figures/}}

\usepackage{adjustbox}

\usepackage{xcolor}
\usepackage{color}
\definecolor{red}{RGB}{215,25,28}
\definecolor{green}{RGB}{26,150,65}
\definecolor{blue}{RGB}{43,131,186}

\definecolor{greenish}{RGB}{27,158,119}
\definecolor{orange}{RGB}{217,95,2}
\definecolor{purple}{RGB}{117,112,179}

\setbeamertemplate{caption}{\raggedright\textnormal{\Tiny\insertcaption}\par}

\title{Percentile-based thresholds and sequent peak algorithm for the detection of flash droughts}
\author{Tobias Seydewitz}
\institute{Potsdam Institute for Climate Impact Research}
\date{\today}

\begin{document}

    \begin{frame}
        \titlepage
    \end{frame}

    % INTRODUCTION


    \section{Introduction}\label{sec:intro}
    \begin{frame}
        \frametitle{Concept}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                \begin{itemize}
                    \item Combining \textcolor{blue}{percentile-based thresholds} and
                    \textcolor{green}{sequent peak algorithm} for the detection of \textcolor{red}{flash droughts}
                    \item Identification of desired daily yield ($Q_0$) by using
                    \textcolor{blue}{percentile-based thresholds}
                    \item Detecting days ($Q_t$) with \textcolor{green}{inflow shortages by comparing} with $Q_0$
                    \item Interevent-pooling of shortages and removing minor droughts with the
                    \textcolor{green}{sequent peak algorithm}
                \end{itemize}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includegraphics[scale=.7]{concept}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}

    % METHODS


    \section{Methods}\label{sec:data_methods}
    \begin{frame}
        \frametitle{Percentile-based thresholds}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                \begin{itemize}
                    \item For each \textcolor{red}{calendar day} within the baseline the p-th percentile is calculated
                    \item Sample size is increased by a \textcolor{blue}{window} around the \textcolor{red}{calendar day}
                    \item Parameters: baseline (samples), window (smoothness and smaples), and percentile (extremeness)
                \end{itemize}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includegraphics[scale=.8]{percentile-based_thresholds}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}
    \begin{frame}
        \frametitle{Percentile-based thresholds: Brandenburg example}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includesvg[scale=.28]{1950}
                    \includesvg[scale=.28]{1980}
                \end{figure}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includesvg[scale=.28]{2003}
                    \includesvg[scale=.28]{2022}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}
    \begin{frame}
        \frametitle{Sequent peak algorithm}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                \begin{itemize}
                    \item Detection of drought events
                    \item $S_t = \begin{cases}
                                     S_{t - 1} + Q_0 - Q_t \text{ if positive} \\
                                     0 \text{ otherwise}
                    \end{cases}$
                    \item Parameters: dmin (minimum duration)
                    \item Output: duration and deficit volume
                \end{itemize}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includesvg[scale=.5]{spa}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}
    \begin{frame}
        \frametitle{Sequent peak algorithm: Brandenburg example}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includesvg[scale=.28]{1950_dmin5}
                    \includesvg[scale=.28]{1980_dmin5}
                \end{figure}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includesvg[scale=.28]{2003_dmin0}
                    \includesvg[scale=.28]{2022_dmin0}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}

    % Example


    \section{Example study}\label{sec:result}
    \begin{frame}
        \frametitle{Impact of droughts on crop yield}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                \begin{itemize}
                    \item Will not be published because future projections are not available
                    \item Number of drought days per year (exceedance days)
                    \item Damage yield deviation from the upper splines of a convex-hull enveloping the yield data
                    \item Simple model damage in response to drought days
                \end{itemize}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includesvg[scale=.4]{ch_labeled}
                    \includesvg[scale=.32]{d_damages_map_significant}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}

    % DISCUSSION


    \section{Discussion}\label{sec:discussion}
    \begin{frame}
        \frametitle{Wrap-up}
        \begin{itemize}
            \item Different measures for $Q_0$ are imaginable to circumvent the problem of no drought conditions within
            the variability of the daily climatology
            \item As far as I can see precipitation data is not feasible
            \item Ready to run analysis with to simple commands:
        \end{itemize}
        > \textbf{thresher} -percentiles "svwl<30" -years 1951 1980 -months 3-8 -window 31 -roi brbg.shp svwl.nc q0.nc \\
        > \textbf{spa} -dmin 5 -cache 184 q0.nc svwl.nc droughts.nc
    \end{frame}


\end{document}
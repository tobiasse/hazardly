\documentclass[aspectratio=169]{beamer}
\mode<presentation>

\usetheme{Singapore}
\usecolortheme{seagull}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}

\usepackage{svg}
\svgpath{{../figures/}}
\svgsetup{inkscapepath=svgsubdir}
\svgsetup{inkscapearea=page}

\usepackage{graphicx}
\graphicspath{{../figures/}}

\usepackage{adjustbox}

\usepackage{xcolor}
\definecolor{myred}{RGB}{215,25,28}
\definecolor{mygreen}{RGB}{26,150,65}
\definecolor{myblue}{RGB}{43,131,186}

\definecolor{greenish}{RGB}{27,158,119}
\definecolor{myorange}{RGB}{217,95,2}
\definecolor{mypurple}{RGB}{117,112,179}

\setbeamertemplate{caption}{\raggedright\textnormal{\Tiny\insertcaption}\par}

\title{Past and future impacts of weather extremes on crop yield across Europe}
\author{Tobias Seydewitz, Diego Rybski, Christoph Müller, and Prajal Pradhan}
\institute{Potsdam Institute for Climate Impact Research}
\date{\today}

\begin{document}

    \begin{frame}
        \titlepage
    \end{frame}

    % INTRODUCTION


    \section{Introduction}\label{sec:intro}
    \begin{frame}
        \frametitle{Extreme events}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                \begin{itemize}
                    \item \textbf{Rare} at a particular \textbf{place} and \textbf{time} of year
                    \item Considered extreme if the value of a variable \textbf{exceeds (or lies below) a threshold}
                    \item Very likely that certain types of extreme weather events will become \textbf{more frequent in
                    future}
                    \item Weather extremes causing socio-economic impacts, e.g., human health, agriculture, forestry,
                    infrastructure etc.
                \end{itemize}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includegraphics[scale=.075]{IPCC_AR6_WGI_Figure_11_4}
                    \caption{Senevirante et al. 2021}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}
    \begin{frame}
        \frametitle{Research gaps and goals}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                \begin{itemize}
                    \item Several global or country scale studies
                    \item Lacking subnational scale, future projections, and disaster database independence
                    \item \textbf{ISIMIP} provides \textbf{crop yield projections} but \textbf{not} related to
                    \textbf{extreme events}
                    \item \textbf{Past} and \textbf{future impact} of \textbf{extreme events} on crop \textbf{yield} for
                    a broad range of crop types on a European \textbf{subnational scale}, \textbf{independent} from
                    curated \textbf{disaster databases}
                \end{itemize}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includegraphics[scale=.2]{lesk_2016}
                    \caption{Lesk et al. 2016}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}

    % DATA AND METHODS


    \section{Data and Methods}\label{sec:data_methods}
    \begin{frame}
        \frametitle{Overview}
        %! suppress = MissingLabel
        \begin{figure}
            \centering
            \includegraphics[scale=.6]{flowchart}
        \end{figure}
    \end{frame}
    \begin{frame}
        \frametitle{\textcolor{greenish}{Weather extremes}}
        \begin{columns}[c]
            \begin{column}{0.6\textwidth}
                \footnotesize
                \begin{itemize}
                    \item Daily \textbf{ERA5-Land} (1981--2020) and \textbf{EURO-CORDEX} (2021--2100) data
                    \item \textbf{Percentile-based thresholds} for detection of \textbf{extremes} (baseline 1981--2010)
                    \item Counting daily exceedances at grid level afterwards rescaling to NUTS1
                    \item \textbf{Coldwave}: min and max temperature below 10th percentile at least for three consecutive days
                    (Jan, Feb, Oct--Dec)
                    \item \textbf{Heatwave}: min and max temperature exceeds 90th percentile at least for three consecutive days
                    (Jun--Aug)
                    \item \textbf{Drought}: Volumetric soil water level (0--28cm) below 30th percentile, sequent peak analysis min
                    duration of 5 days (Mar--Sep)
                \end{itemize}
            \end{column}
            \begin{column}{0.4\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includesvg[scale=.38]{spa}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}
    \begin{frame}
        \frametitle{\textcolor{myorange}{Yield damage}}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                \begin{itemize}
                    \item Yearly NUTS1 \textbf{Eurostat crop production} and production \textbf{area} data (1981--2020)
                    for 68 crop groups, e.g., grains, root crops, etc.
                    \item Deriving relative
                    $\textbf{\textcolor{myred}{damage}} = 1 - \frac{\textbf{\textcolor{myblue}{yield}}}{\textbf{\textcolor{mygreen}{expected}}}$
                    \item Estimating denominator ($\textbf{expected}$) using \textbf{upper segments} of a \textbf{convex
                    hull}
                \end{itemize}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \adjustbox{trim={0 0.1cm 6.2cm .1cm},clip}{
                        \includesvg[scale=.8]{ch_labeled}
                    }
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}
    \begin{frame}
        \frametitle{\textcolor{mypurple}{Damage function}}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                \begin{itemize}
                    \item Inverted logistic transform $damage' = -\ln \left( \frac{2}{damage + 1} - 1 \right)$
                    \item Ordinary least squares regression of $damage' = \beta \times exceedance$
                    \item Substituting the estimated regression coefficient $\beta$ to obtain
                    $damage = \frac{2}{1 + e^{-\beta \times exceedance}} - 1$
                \end{itemize}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \adjustbox{trim={5.5cm 0.1cm 0 .1cm},clip}{
                        \includesvg[scale=.7]{ch}
                    }
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}
    \begin{frame}
        \frametitle{Aggregation}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                \small
                \begin{itemize}
                    \item Individual damage functions per extreme, NUTS1 region, and crop group
                    \item Only accept damage functions: coverage $\geq$ 0.4, z-score $\leq$ -1.96 ($p \approx 0.025$),
                    correlation $\leq$ -0.4, and $r^2$ $\geq$ 0.5
                    \item \textbf{Spearman} rank \textbf{correlation} coefficient of \textbf{exceedance} and \textbf{damage}
                    \item 1000x timeseries \textbf{randomization} test to obtain \textbf{z-score}
                    \item Mean \textbf{production} (1991--2020) \textbf{weighted average damage} per NUTS1 unit and crop
                    group for predicted historical (1991--2020) and future (2071--2100) mean damage
                \end{itemize}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includesvg[scale=.4]{func}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}

    % RESULTS


    \section{Result}\label{sec:result}
    \begin{frame}
        \frametitle{Coldwave}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                \begin{itemize}
                    \item \textbf{Frequency} of cold waves is \textbf{decreasing} in all regions
                    \item \textbf{Expectation no major impact} on crop yield neither historically nor in the future
                    \item Using cold waves as \textbf{counterfactual}
                \end{itemize}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includesvg[scale=.4]{cw}
                    \caption{(A) Historical (1981--2020) and (B--D) RCP2.6, 4.5, and 8.5 (2021--2100)}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}
    \begin{frame}
        \frametitle{Heatwave}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                \begin{itemize}
                    \item \textbf{Frequency} of heat waves is \textbf{increasing} in all regions
                    \item \textbf{North and south more affected} than central Europe
                    \item Major \textbf{impact} on crop yield \textbf{expected}
                \end{itemize}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includesvg[scale=.4]{hw}
                    \caption{(A) Historical (1981--2020) and (B--D) RCP2.6, 4.5, and 8.5 (2021--2100)}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}
    \begin{frame}
        \frametitle{Correlation of exceedance and damage}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                \begin{itemize}
                    \item Cold-, heat waves and droughts account for 47, 217, and 355 significant negative correlations
                    \item Cold-, heat waves and droughts account for 79, 17, and 20 significant positive correlations
                    \item \textbf{Heat waves} and \textbf{droughts} have a \textbf{negative feedback} with
                    increasing extreme frequency
                \end{itemize}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includesvg[scale=.08]{cw_correlations_significant}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}
    \begin{frame}
        \frametitle{Coldwave damage}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                \begin{itemize}
                    \item Only in a \textbf{limited} number of regions cold waves do have \textbf{impact on yield}
                    \item Impacts is \textbf{neglectable in} the {future}
                    \item \textbf{Unclear} if the measured effect is related to \textbf{co-occurrence} with heat or
                    other extremes
                \end{itemize}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includesvg[scale=.4]{cw_damages_map_significant}
                    \caption{(A) Historical (1981--2020) and (B--D) RCP2.6, 4.5, and 8.5 (2021--2100)}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}
    \begin{frame}
        \frametitle{Heatwave damage}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                \begin{itemize}
                    \item \textbf{Historical} in most \textbf{regions (40 of 50)} predicted mean
                    \textbf{damage below 20\%}, top Centro Italy (39\%)
                    \item \textbf{RCP2.6} (GMT $\approx$ 2$^\circ$C ) number of \textbf{regions (20)} with mean yield
                    \textbf{damage exceeding 20\%} increases
                    \item \textbf{Increasing frequency} of heat waves leads to \textbf{large-scale yield damage} across
                    Europe for RCP4.5 and 8.5
                \end{itemize}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includesvg[scale=.4]{hw_damages_map_significant}
                    \caption{(A) Historical (1981--2020) and (B--D) RCP2.6, 4.5, and 8.5 (2021--2100)}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}
    \begin{frame}
        \frametitle{Drought damage}
        \begin{columns}[c]
            \begin{column}{0.5\textwidth}
                \begin{itemize}
                    \item Most \textbf{regions (34 of 47)} experience mean \textbf{damage} by droughts \textbf{below 20\%}
                    \item Central France is the most affected
                    \item Regions in \textbf{southern Spain, Portugal, Central Germany, north Italy, France, and west
                    Poland} are \textbf{damaged} by droughts but \textbf{not by heat waves}
                \end{itemize}
            \end{column}
            \begin{column}{0.5\textwidth}
                %! suppress = MissingLabel
                \begin{figure}
                    \centering
                    \includesvg[scale=.4]{d_damages_map_significant}
                    \caption{Historical (1981--2020)}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}

    % DISCUSSION


    \section{Discussion}\label{sec:discussion}
    \begin{frame}
        \frametitle{Wrap-up}
        \begin{itemize}
            \item Heat and dry \textbf{extreme events} have a \textbf{non-neglectable impact} on crop yield
            \item Even a \textbf{moderate climate future} creates \textbf{severe yield damage} in many regions
            \item Threatening \textbf{heat extremes} are \textbf{highly likely to increase} in future
            \item \textbf{No, save harbour} in Europe, neither regional nor crop types
            \item Providing a \textbf{scalable spatial} approach \textbf{independent} from curated \textbf{disaster data}
            \item \textbf{Compounding events} perspective is \textbf{required}
        \end{itemize}
    \end{frame}
    \begin{frame}
        \frametitle{Limitations}
        \begin{itemize}
            \item Crop \textbf{data availability} and \textbf{quality} on a \textbf{sub-national} level
            \item \textbf{Alignment} of \textbf{extremes} with agricultural \textbf{management} and \textbf{physiology}
            required
            \item \textbf{Projections} under the assumption of \textbf{static agricultural management}
            \item \textbf{Selection} of the \textbf{function} to estimate \textbf{expected yield}
        \end{itemize}
    \end{frame}
    \begin{frame}
        \frametitle{Future}
        \begin{itemize}
            \item \textbf{Modularity} allows \textbf{adaptation} to a \textbf{limitations} subset
            \item \textbf{Extension} to \textbf{forestry}
        \end{itemize}
    \end{frame}


\end{document}
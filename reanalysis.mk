.PHONY: reanalysis-preprocess-fires reanalysis-preprocess-floods reanalysis-preprocess-mdroughts

########################################################################################################################
################################################### GLOBAL VARIABLES ###################################################
########################################################################################################################
reanalysisId := 1981-2021

# Global percentile-based extremes analysis parameter
reanalysisBase := 1981 2010

########################################################################################################################
######################################################## FIRES #########################################################
########################################################################################################################
firePercentileDef := "fwi>${firePercentile}"

reanalysis-preprocess-fires:
	@for y in ${singleYears}; do											\
		unzip -d ${raw}												\
		${raw}${firePrefix}_$${y}.zip;										\
		cdo mergetime												\
		${raw}ECMWF*.nc												\
		${raw}${firePrefix}_$${y}.nc;										\
		rm													\
		${raw}ECMWF*.nc;											\
	done
	cdo mergetime													\
	${raw}${firePrefix}*.nc												\
	${raw}${firePrefix}_${reanalysisId}.nc
	cdo sellonlatbox,-180,180,-90,90										\
	${raw}${firePrefix}_${reanalysisId}.nc										\
	${interim}${firePrefix}_${reanalysisId}.nc
	rm														\
	${raw}${firePrefix}*.nc

reanalysis-process-fires:
	@for w in ${CD1} ${CD2} ${CD3}; do										\
		thresher -p ${firePercentileDef} -y ${reanalysisBase} -m ${fireSeason} -w $${w} -r ${eu}		\
		${interim}${firePrefix}_${reanalysisId}.nc								\
		${proc}${firePrefix}_thresholds_${firePercentileDef}_CD$${w}_${reanalysisId}.nc;			\
		apply													\
		${interim}${firePrefix}_${reanalysisId}.nc								\
		${proc}${firePrefix}_thresholds_${firePercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${proc}${firePrefix}_daily_${firePercentileDef}_CD$${w}_${reanalysisId}.nc;				\
		yearly													\
		${proc}${firePrefix}_daily_${firePercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${proc}${firePrefix}_yearly_${firePercentileDef}_CD$${w}_${reanalysisId}.nc;				\
		rescale -v exceedance -a mx --encoding utf-8								\
		${proc}${firePrefix}_yearly_${firePercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${nuts}													\
		${proc}${firePrefix}_yearly_max-rescaled_${firePercentileDef}_CD$${w}_${reanalysisId}.geojson;		\
		rescale -v exceedance -a me --encoding utf-8								\
		${proc}${firePrefix}_yearly_${firePercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${nuts}													\
		${proc}${firePrefix}_yearly_mean-rescaled_${firePercentileDef}_CD$${w}_${reanalysisId}.geojson;		\
		thresher -p ${firePercentileDef} -y ${reanalysisBase} -m ${fireSeason} -w $${w} -r ${eu}		\
		--repetitions ${repetitions} --bootstrapping								\
		${interim}${firePrefix}_${reanalysisId}.nc								\
		${proc}${firePrefix}_thresholds_${firePercentileDef}_CD$${w}_bs_${reanalysisId}.nc;			\
		apply													\
		${interim}${firePrefix}_${reanalysisId}.nc								\
		${proc}${firePrefix}_thresholds_${firePercentileDef}_CD$${w}_bs_${reanalysisId}.nc			\
		${proc}${firePrefix}_daily_${firePercentileDef}_CD$${w}_bs_${reanalysisId}.nc;				\
		yearly													\
		${proc}${firePrefix}_daily_${firePercentileDef}_CD$${w}_bs_${reanalysisId}.nc				\
		${proc}${firePrefix}_yearly_${firePercentileDef}_CD$${w}_bs_${reanalysisId}.nc;				\
		rescale -v exceedance -a mx --encoding utf-8								\
		${proc}${firePrefix}_yearly_${firePercentileDef}_CD$${w}_bs_${reanalysisId}.nc				\
		${nuts}													\
		${proc}${firePrefix}_yearly_max-rescaled_${firePercentileDef}_CD$${w}_bs_${reanalysisId}.geojson;	\
		rescale -v exceedance -a me --encoding utf-8								\
		${proc}${firePrefix}_yearly_${firePercentileDef}_CD$${w}_bs_${reanalysisId}.nc				\
		${nuts}													\
		${proc}${firePrefix}_yearly_mean-rescaled_${firePercentileDef}_CD$${w}_bs_${reanalysisId}.geojson;	\
		merge-extremes												\
		${proc}${firePrefix}_yearly_max-rescaled_${firePercentileDef}_CD$${w}_bs_${reanalysisId}.geojson	\
		${proc}${firePrefix}_yearly_max-rescaled_${firePercentileDef}_CD$${w}_${reanalysisId}.geojson		\
		${proc}${firePrefix}_yearly_max_${firePercentileDef}_CD$${w}_${reanalysisId}.csv;			\
		merge-extremes												\
		${proc}${firePrefix}_yearly_mean-rescaled_${firePercentileDef}_CD$${w}_bs_${reanalysisId}.geojson	\
		${proc}${firePrefix}_yearly_mean-rescaled_${firePercentileDef}_CD$${w}_${reanalysisId}.geojson		\
		${proc}${firePrefix}_yearly_mean_${firePercentileDef}_CD$${w}_${reanalysisId}.csv;			\
		rm													\
		${proc}${firePrefix}_daily*.nc;										\
	done

########################################################################################################################
###################################################### HEAT-WAVES ######################################################
########################################################################################################################
heatPercentileDef := "t2m_min>${heatPercentile},t2m_max>${heatPercentile}"

reanalysis-preprocess-heat:
	@for y in ${years}; do												\
		cdo -b F32 copy												\
		${raw}${temperaturePrefix}_$${y}.nc									\
		${raw}${temperaturePrefix}_$${y}_float.nc;								\
	done
	cdo mergetime													\
	${raw}${temperaturePrefix}*float.nc										\
	${raw}${temperaturePrefix}_${reanalysisId}.nc
	cdo shifttime,-1hour												\
	${raw}${temperaturePrefix}_${reanalysisId}.nc									\
	${raw}${temperaturePrefix}_${reanalysisId}_shifted.nc
	cdo --timestat_date first -chname,t2m,t2m_min -daymin								\
	${raw}${temperaturePrefix}_${reanalysisId}_shifted.nc								\
	${raw}${temperaturePrefix}_${reanalysisId}_min.nc
	cdo --timestat_date first -chname,t2m,t2m_max -daymax								\
	${raw}${temperaturePrefix}_${reanalysisId}_shifted.nc								\
	${raw}${temperaturePrefix}_${reanalysisId}_max.nc
	cdo merge													\
	${raw}${temperaturePrefix}_${reanalysisId}_min.nc ${raw}${temperaturePrefix}_${reanalysisId}_max.nc		\
	${interim}${temperaturePrefix}_${reanalysisId}.nc
	rm														\
	${raw}${temperaturePrefix}_${reanalysisId}*.nc ${raw}${temperaturePrefix}*float.nc

reanalysis-process-heat:
	@for w in ${CD1} ${CD2} ${CD3}; do										\
		thresher -p ${heatPercentileDef} -y ${reanalysisBase} -m ${heatSeason} -w $${w} -r ${eu}		\
		${interim}${temperaturePrefix}_${reanalysisId}.nc							\
		${proc}${heatPrefix}_thresholds_${heatPercentileDef}_CD$${w}_${reanalysisId}.nc;			\
		apply													\
		${interim}${temperaturePrefix}_${reanalysisId}.nc							\
		${proc}${heatPrefix}_thresholds_${heatPercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${proc}${heatPrefix}_daily_${heatPercentileDef}_CD$${w}_${reanalysisId}.nc;				\
		daily													\
		${proc}${heatPrefix}_daily_${heatPercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${proc}${heatPrefix}_days_${heatPercentileDef}_CD$${w}_${reanalysisId}.nc;				\
		setto -v exceedance -m ${heatBuffer} -c 0								\
		${proc}${heatPrefix}_days_${heatPercentileDef}_CD$${w}_${reanalysisId}.nc;				\
		consecutive -v exceedance -s ${heatStartCondition} -e ${heatEndCondition} -m disagg --mn ${heatMinDays}	\
		${proc}${heatPrefix}_days_${heatPercentileDef}_CD$${w}_${reanalysisId}.nc;				\
		cdo --timestat_date first yearsum									\
		${proc}${heatPrefix}_days_${heatPercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${proc}${heatPrefix}_yearly_${heatPercentileDef}_CD$${w}_${reanalysisId}.nc;				\
		rescale -v exceedance_consecutive -a mx --encoding utf-8						\
		${proc}${heatPrefix}_yearly_${heatPercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${nuts}													\
		${proc}${heatPrefix}_yearly_max-rescaled_${heatPercentileDef}_CD$${w}_${reanalysisId}.geojson;		\
		rescale -v exceedance_consecutive -a me --encoding utf-8						\
		${proc}${heatPrefix}_yearly_${heatPercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${nuts}													\
		${proc}${heatPrefix}_yearly_mean-rescaled_${heatPercentileDef}_CD$${w}_${reanalysisId}.geojson;		\
		thresher -p ${heatPercentileDef} -y ${reanalysisBase} -m ${heatSeason} -w $${w} -r ${eu}		\
		--repetitions ${repetitions} --bootstrapping								\
		${interim}${temperaturePrefix}_${reanalysisId}.nc							\
		${proc}${heatPrefix}_thresholds_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;			\
		apply													\
		${interim}${temperaturePrefix}_${reanalysisId}.nc							\
		${proc}${heatPrefix}_thresholds_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.nc			\
		${proc}${heatPrefix}_daily_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;				\
		daily													\
		${proc}${heatPrefix}_daily_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.nc				\
		${proc}${heatPrefix}_days_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;				\
		setto -v exceedance -m ${heatBuffer} -c 0								\
		${proc}${heatPrefix}_days_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;				\
		consecutive -v exceedance -s ${heatStartCondition} -e ${heatEndCondition} -m disagg --mn ${heatMinDays}	\
		${proc}${heatPrefix}_days_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;				\
		cdo --timestat_date first yearsum									\
		${proc}${heatPrefix}_days_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.nc				\
		${proc}${heatPrefix}_yearly_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;				\
		rescale -v exceedance_consecutive -a mx --encoding utf-8						\
		${proc}${heatPrefix}_yearly_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.nc				\
		${nuts}													\
		${proc}${heatPrefix}_yearly_max-rescaled_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.geojson;	\
		rescale -v exceedance_consecutive -a me --encoding utf-8						\
		${proc}${heatPrefix}_yearly_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.nc				\
		${nuts}													\
		${proc}${heatPrefix}_yearly_mean-rescaled_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.geojson;	\
		merge-extremes												\
		${proc}${heatPrefix}_yearly_max-rescaled_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.geojson	\
		${proc}${heatPrefix}_yearly_max-rescaled_${heatPercentileDef}_CD$${w}_${reanalysisId}.geojson		\
		${proc}${heatPrefix}_yearly_max_${heatPercentileDef}_CD$${w}_${reanalysisId}.csv;			\
		merge-extremes												\
		${proc}${heatPrefix}_yearly_mean-rescaled_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.geojson	\
		${proc}${heatPrefix}_yearly_mean-rescaled_${heatPercentileDef}_CD$${w}_${reanalysisId}.geojson		\
		${proc}${heatPrefix}_yearly_mean_${heatPercentileDef}_CD$${w}_${reanalysisId}.csv;			\
		rm													\
		${proc}${heatPrefix}_daily*.nc										\
		${proc}${heatPrefix}_days*.nc;										\
	done

########################################################################################################################
###################################################### COLD-WAVES ######################################################
########################################################################################################################
coldPercentileDef := "t2m_min<${coldPercentile},t2m_max<${coldPercentile}"

reanalysis-preprocess-cold: reanalysis-preprocess-heat

reanalysis-process-cold:
	@for w in ${CD1} ${CD2} ${CD3}; do										\
		thresher -p ${coldPercentileDef} -y ${reanalysisBase} -m ${coldSeason} -w $${w} -r ${eu}		\
		${interim}${temperaturePrefix}_${reanalysisId}.nc							\
		${proc}${coldPrefix}_thresholds_${coldPercentileDef}_CD$${w}_${reanalysisId}.nc;			\
		apply													\
		${interim}${temperaturePrefix}_${reanalysisId}.nc							\
		${proc}${coldPrefix}_thresholds_${coldPercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${proc}${coldPrefix}_daily_${coldPercentileDef}_CD$${w}_${reanalysisId}.nc;				\
		daily													\
		${proc}${coldPrefix}_daily_${coldPercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${proc}${coldPrefix}_days_${coldPercentileDef}_CD$${w}_${reanalysisId}.nc;				\
		setto -v exceedance -m ${coldBuffer} -c 0								\
		${proc}${coldPrefix}_days_${coldPercentileDef}_CD$${w}_${reanalysisId}.nc;				\
		consecutive -v exceedance -s ${coldStartCondition} -e ${coldEndCondition} -m disagg --mn ${coldMinDays}	\
		${proc}${coldPrefix}_days_${coldPercentileDef}_CD$${w}_${reanalysisId}.nc;				\
		cdo --timestat_date first yearsum									\
		${proc}${coldPrefix}_days_${coldPercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${proc}${coldPrefix}_yearly_${coldPercentileDef}_CD$${w}_${reanalysisId}.nc;				\
		rescale -v exceedance_consecutive -a mx --encoding utf-8						\
		${proc}${coldPrefix}_yearly_${coldPercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${nuts}													\
		${proc}${coldPrefix}_yearly_max-rescaled_${coldPercentileDef}_CD$${w}_${reanalysisId}.geojson;		\
		rescale -v exceedance_consecutive -a me --encoding utf-8						\
		${proc}${coldPrefix}_yearly_${coldPercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${nuts}													\
		${proc}${coldPrefix}_yearly_mean-rescaled_${coldPercentileDef}_CD$${w}_${reanalysisId}.geojson;		\
		thresher -p ${coldPercentileDef} -y ${reanalysisBase} -m ${coldSeason} -w $${w} -r ${eu}		\
		--repetitions ${repetitions} --bootstrapping								\
		${interim}${temperaturePrefix}_${reanalysisId}.nc							\
		${proc}${coldPrefix}_thresholds_${coldPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;			\
		apply													\
		${interim}${temperaturePrefix}_${reanalysisId}.nc							\
		${proc}${coldPrefix}_thresholds_${coldPercentileDef}_CD$${w}_bs_${reanalysisId}.nc			\
		${proc}${coldPrefix}_daily_${coldPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;				\
		daily													\
		${proc}${coldPrefix}_daily_${coldPercentileDef}_CD$${w}_bs_${reanalysisId}.nc				\
		${proc}${coldPrefix}_days_${coldPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;				\
		setto -v exceedance -m ${coldBuffer} -c 0								\
		${proc}${coldPrefix}_days_${coldPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;				\
		consecutive -v exceedance -s ${coldStartCondition} -e ${coldEndCondition} -m disagg --mn ${coldMinDays}	\
		${proc}${coldPrefix}_days_${coldPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;				\
		cdo --timestat_date first yearsum									\
		${proc}${coldPrefix}_days_${coldPercentileDef}_CD$${w}_bs_${reanalysisId}.nc				\
		${proc}${coldPrefix}_yearly_${coldPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;				\
		rescale -v exceedance_consecutive -a mx --encoding utf-8						\
		${proc}${coldPrefix}_yearly_${coldPercentileDef}_CD$${w}_bs_${reanalysisId}.nc				\
		${nuts}													\
		${proc}${coldPrefix}_yearly_max-rescaled_${coldPercentileDef}_CD$${w}_bs_${reanalysisId}.geojson;	\
		rescale -v exceedance_consecutive -a me --encoding utf-8						\
		${proc}${coldPrefix}_yearly_${coldPercentileDef}_CD$${w}_bs_${reanalysisId}.nc				\
		${nuts}													\
		${proc}${coldPrefix}_yearly_mean-rescaled_${coldPercentileDef}_CD$${w}_bs_${reanalysisId}.geojson;	\
		merge-extremes												\
		${proc}${coldPrefix}_yearly_max-rescaled_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.geojson	\
		${proc}${coldPrefix}_yearly_max-rescaled_${heatPercentileDef}_CD$${w}_${reanalysisId}.geojson		\
		${proc}${coldPrefix}_yearly_max_${heatPercentileDef}_CD$${w}_${reanalysisId}.csv;			\
		merge-extremes												\
		${proc}${coldPrefix}_yearly_mean-rescaled_${heatPercentileDef}_CD$${w}_bs_${reanalysisId}.geojson	\
		${proc}${coldPrefix}_yearly_mean-rescaled_${heatPercentileDef}_CD$${w}_${reanalysisId}.geojson		\
		${proc}${coldPrefix}_yearly_mean_${heatPercentileDef}_CD$${w}_${reanalysisId}.csv;			\
		rm													\
		${proc}${coldPrefix}_daily*.nc										\
		${proc}${coldPrefix}_days*.nc;										\
	done

########################################################################################################################
######################################################## FLOODS ########################################################
########################################################################################################################
floodPercentileDef := "tp>${floodPercentile}"

reanalysis-preprocess-floods:
	@for y in ${years}; do												\
		cdo -b F32 copy												\
		${raw}${floodPrefix}_$${y}.nc										\
		${raw}${floodPrefix}_$${y}_float.nc;									\
	done
	cdo mergetime													\
	${raw}${floodPrefix}*float.nc											\
	${raw}${floodPrefix}_${reanalysisId}.nc
	cdo --timestat_date first -daysum -shifttime,-1hour								\
	${raw}${floodPrefix}_${reanalysisId}.nc										\
	${interim}${floodPrefix}_${reanalysisId}.nc
	rm														\
	${raw}${floodPrefix}_${reanalysisId}.nc										\
	${raw}${floodPrefix}*float.nc

reanalysis-process-floods:
	@for w in ${CD1} ${CD2} ${CD3}; do										\
		thresher -p ${floodPercentileDef} -y ${reanalysisBase} -m ${floodSeason} -w $${w} -r ${eu}		\
		${interim}${floodPrefix}_${reanalysisId}.nc								\
		${proc}${floodPrefix}_thresholds_${floodPercentileDef}_CD$${w}_${reanalysisId}.nc;			\
		apply													\
		${interim}${floodPrefix}_${reanalysisId}.nc								\
		${proc}${floodPrefix}_thresholds_${floodPercentileDef}_CD$${w}_${reanalysisId}.nc			\
		${proc}${floodPrefix}_daily_${floodPercentileDef}_CD$${w}_${reanalysisId}.nc;				\
		yearly													\
		${proc}${floodPrefix}_daily_${floodPercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${proc}${floodPrefix}_yearly_${floodPercentileDef}_CD$${w}_${reanalysisId}.nc;				\
		rescale -v exceedance -a mx --encoding utf-8								\
		${proc}${floodPrefix}_yearly_${floodPercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${nuts}													\
		${proc}${floodPrefix}_yearly_max-rescaled_${floodPercentileDef}_CD$${w}_${reanalysisId}.geojson;	\
		rescale -v exceedance -a me --encoding utf-8								\
		${proc}${floodPrefix}_yearly_${floodPercentileDef}_CD$${w}_${reanalysisId}.nc				\
		${nuts}													\
		${proc}${floodPrefix}_yearly_mean-rescaled_${floodPercentileDef}_CD$${w}_${reanalysisId}.geojson;	\
		thresher -p ${floodPercentileDef} -y ${reanalysisBase} -m ${floodSeason} -w $${w} -r ${eu}		\
		--repetitions ${repetitions} --bootstrapping								\
		${interim}${floodPrefix}_${reanalysisId}.nc								\
		${proc}${floodPrefix}_thresholds_${floodPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;			\
		apply													\
		${interim}${floodPrefix}_${reanalysisId}.nc								\
		${proc}${floodPrefix}_thresholds_${floodPercentileDef}_CD$${w}_bs_${reanalysisId}.nc			\
		${proc}${floodPrefix}_daily_${floodPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;			\
		yearly													\
		${proc}${floodPrefix}_daily_${floodPercentileDef}_CD$${w}_bs_${reanalysisId}.nc				\
		${proc}${floodPrefix}_yearly_${floodPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;			\
		rescale -v exceedance -a mx --encoding utf-8								\
		${proc}${floodPrefix}_yearly_${floodPercentileDef}_CD$${w}_bs_${reanalysisId}.nc			\
		${nuts}													\
		${proc}${floodPrefix}_yearly_max-rescaled_${floodPercentileDef}_CD$${w}_bs_${reanalysisId}.geojson;	\
		rescale -v exceedance -a me --encoding utf-8								\
		${proc}${floodPrefix}_yearly_${floodPercentileDef}_CD$${w}_bs_${reanalysisId}.nc			\
		${nuts}													\
		${proc}${floodPrefix}_yearly_mean-rescaled_${floodPercentileDef}_CD$${w}_bs_${reanalysisId}.geojson;	\
		rm													\
		${proc}${floodPrefix}_daily*.nc;									\
	done

########################################################################################################################
############################################### METEOROLOGICAL DROUGHTS ################################################
########################################################################################################################
mdroughtPercentileDef := "tp<=${mdroughtPercentile}"

reanalysis-preprocess-mdroughts: reanalysis-preprocess-floods

reanalysis-process-mdroughts:
	@for w in ${CD1} ${CD2} ${CD3}; do										\
		thresher -p ${mdroughtPercentileDef} -y ${reanalysisBase} -m ${mdroughtSeason} -w $${w} -r ${eu}	\
		${interim}${floodPrefix}_${reanalysisId}.nc								\
		${proc}${mdroughtPrefix}_thresholds_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.nc;		\
		apply													\
		${interim}${floodPrefix}_${reanalysisId}.nc								\
		${proc}${mdroughtPrefix}_thresholds_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.nc			\
		${proc}${mdroughtPrefix}_daily_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.nc;			\
		daily													\
		${proc}${mdroughtPrefix}_daily_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.nc			\
		${proc}${mdroughtPrefix}_days_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.nc;			\
		setto -v exceedance -m ${mdroughtBuffer} -c 0								\
		${proc}${mdroughtPrefix}_days_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.nc;			\
		consecutive -v exceedance -s ${mdroughtStartCondition} -e ${mdroughtEndCondition} -m disagg		\
		--mn ${mdroughtMinDays}											\
		${proc}${mdroughtPrefix}_days_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.nc;			\
		pool -v exceedance_consecutive -e ${mdroughtEventCondition} -b ${mdroughtBetweennessCondition}		\
		--mx ${mdroughtMaxDistance} --dmin ${mdroughtDmin}							\
		${proc}${mdroughtPrefix}_days_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.nc;			\
		cdo --timestat_date first yearsum									\
		${proc}${mdroughtPrefix}_days_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.nc			\
		${proc}${mdroughtPrefix}_yearly_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.nc;			\
		rescale -v exceedance_consecutive_events -a mn --encoding utf-8						\
		${proc}${mdroughtPrefix}_yearly_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.nc			\
		${nuts}													\
		${proc}${mdroughtPrefix}_yearly_min-rescaled_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.geojson;	\
		rescale -v exceedance_consecutive_events -a mx --encoding utf-8						\
		${proc}${mdroughtPrefix}_yearly_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.nc ${nuts}		\
		${proc}${mdroughtPrefix}_yearly_max-rescaled_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.geojson;	\
		rescale -v exceedance_consecutive_events -a me --encoding utf-8						\
		${proc}${mdroughtPrefix}_yearly_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.nc			\
		${nuts}													\
		${proc}${mdroughtPrefix}_yearly_mean-rescaled_${mdroughtPercentileDef}_CD$${w}_${reanalysisId}.geojson;	\
		thresher -p ${mdroughtPercentileDef} -y ${reanalysisBase} -m ${mdroughtSeason} -w $${w} -r ${eu}	\
		--repetitions ${repetitions} --bootstrapping								\
		${interim}${floodPrefix}_${reanalysisId}.nc								\
		${proc}${mdroughtPrefix}_thresholds_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;		\
		apply													\
		${interim}${floodPrefix}_${reanalysisId}.nc								\
		${proc}${mdroughtPrefix}_thresholds_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.nc		\
		${proc}${mdroughtPrefix}_daily_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;			\
		daily													\
		${proc}${mdroughtPrefix}_daily_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.nc			\
		${proc}${mdroughtPrefix}_days_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;			\
		setto -v exceedance -m ${mdroughtBuffer} -c 0								\
		${proc}${mdroughtPrefix}_days_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;			\
		consecutive -v exceedance -s ${mdroughtStartCondition} -e ${mdroughtEndCondition} -m disagg		\
		--mn ${mdroughtMinDays}											\
		${proc}${mdroughtPrefix}_days_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;			\
		pool -v exceedance_consecutive -e ${mdroughtEventCondition} -b ${mdroughtBetweennessCondition}		\
		--mx ${mdroughtMaxDistance} --dmin ${mdroughtDmin}							\
		${proc}${mdroughtPrefix}_days_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;			\
		cdo --timestat_date first yearsum									\
		${proc}${mdroughtPrefix}_days_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.nc			\
		${proc}${mdroughtPrefix}_yearly_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.nc;			\
		rescale -v exceedance_consecutive_events -a mn --encoding utf-8						\
		${proc}${mdroughtPrefix}_yearly_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.nc			\
		${nuts}													\
		${proc}${mdroughtPrefix}_yearly_min-rescaled_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.geojson;\
		rescale -v exceedance_consecutive_events -a mx --encoding utf-8						\
		${proc}${mdroughtPrefix}_yearly_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.nc			\
		${nuts}													\
		${proc}${mdroughtPrefix}_yearly_max-rescaled_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.geojson;\
		rescale -v exceedance_consecutive_events -a me --encoding utf-8						\
		${proc}${mdroughtPrefix}_yearly_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.nc			\
		${nuts}													\
		${proc}${mdroughtPrefix}_yearly_mean-rescaled_${mdroughtPercentileDef}_CD$${w}_bs_${reanalysisId}.geojson;\
		rm													\
		${proc}${mdroughtPrefix}_daily*.nc									\
		${proc}${mdroughtPrefix}_days*.nc;									\
	done

########################################################################################################################
################################################ HYDROLOGICAL DROUGHTS #################################################
########################################################################################################################
hdroughtPercentileDef := "swvl<${hdroughtPercentile}"
soilLayer1 := 0-28cm
soilLayer2 := 28-289cm
layer := ${soilLayer1}

hdroughtPrefix := hdro

# Percentile-based extremes analysis parameter
hdroughtVar := mrro
hdroughtVar1 := ${hdroughtVar}_min
hdroughtVar2 := ${hdroughtVar}_mean
hdroughtVar3 := ${hdroughtVar}_max

# Months under analysis are 3-9, added one month as sequence break for consecutive counting
hdroughtSeason := 3-10
hdroughtBuffer := 10
hdroughtPercentile := 70

hdroughtPercentileDef1 := "${hdroughtVar1}<${hdroughtPercentile}"
hdroughtPercentileDef2 := "${hdroughtVar2}<${hdroughtPercentile}"
hdroughtPercentileDef3 := "${hdroughtVar3}<${hdroughtPercentile}"

# Consecutive counting parameter
hdroughtStartCondition := "==1"
hdroughtEndCondition := "!=1"
hdroughtMinDays := 5

# Pooling parameter
hdroughtEventCondition := "==1"
hdroughtBetweennessCondition := "!=1"
hdroughtMaxDistance := 5
hdroughtDmin := 20

reanalysis-preprocess-hdroughts:
	@for v in ${soilWaterVars}; do											\
		cdo mergetime												\
		${raw}${soilWaterPrefix}_$${v}*.nc									\
		${raw}${soilWaterPrefix}_$${v}_${reanalysisId}.nc;							\
		cdo shifttime,-1hour											\
		${raw}${soilWaterPrefix}_$${v}_${reanalysisId}.nc							\
		${raw}${soilWaterPrefix}_$${v}_${reanalysisId}_shifted.nc;						\
		cdo -b F32 --timestat_date first daymean								\
		${raw}${soilWaterPrefix}_$${v}_${reanalysisId}_shifted.nc						\
		${raw}${soilWaterPrefix}_$${v}_${reanalysisId}_mean.nc;							\
		if [ $${v} = "volumetric_soil_water_layer_1" ]; then							\
			cdo chname,swvl1,swvl										\
			${raw}${soilWaterPrefix}_$${v}_${reanalysisId}_mean.nc						\
			${raw}${soilWaterPrefix}_$${v}_${reanalysisId}_mean_norm.nc;					\
		elif [ $${v} = "volumetric_soil_water_layer_2" ]; then							\
			cdo chname,swvl2,swvl										\
			${raw}${soilWaterPrefix}_$${v}_${reanalysisId}_mean.nc						\
			${raw}${soilWaterPrefix}_$${v}_${reanalysisId}_mean_norm.nc;					\
		elif [ $${v} = "volumetric_soil_water_layer_3" ]; then							\
			cdo chname,swvl3,swvl										\
			${raw}${soilWaterPrefix}_$${v}_${reanalysisId}_mean.nc						\
			${raw}${soilWaterPrefix}_$${v}_${reanalysisId}_mean_norm.nc;					\
		elif [ $${v} = "volumetric_soil_water_layer_4" ]; then							\
			cdo chname,swvl4,swvl										\
			${raw}${soilWaterPrefix}_$${v}_${reanalysisId}_mean.nc						\
			${raw}${soilWaterPrefix}_$${v}_${reanalysisId}_mean_norm.nc;					\
		fi													\
	done
	cdo ensmean													\
	${raw}${soilWaterPrefix}_volumetric_soil_water_layer_1_${reanalysisId}_mean_norm.nc				\
	${raw}${soilWaterPrefix}_volumetric_soil_water_layer_2_${reanalysisId}_mean_norm.nc				\
	${raw}${soilWaterPrefix}_${soilLayer1}_${reanalysisId}.nc
	cdo ensmean													\
	${raw}${soilWaterPrefix}_volumetric_soil_water_layer_3_${reanalysisId}_mean_norm.nc				\
	${raw}${soilWaterPrefix}_volumetric_soil_water_layer_4_${reanalysisId}_mean_norm.nc				\
	${raw}${soilWaterPrefix}_${soilLayer2}_${reanalysisId}.nc
	cdo selyear,1981/2021												\
	${raw}${soilWaterPrefix}_${soilLayer1}_${reanalysisId}.nc							\
	${interim}${soilWaterPrefix}_${soilLayer1}_${reanalysisId}.nc
	cdo selyear,1981/2021												\
	${raw}${soilWaterPrefix}_${soilLayer2}_${reanalysisId}.nc							\
	${interim}${soilWaterPrefix}_${soilLayer2}_${reanalysisId}.nc
	rm														\
	${raw}${soilWaterPrefix}*${reanalysisId}*.nc

reanalysis-process-hdroughts:
	@for w in ${CD1} ${CD2} ${CD3}; do										\
		thresher -p ${hdroughtPercentileDef} -y ${reanalysisBase} -m ${hdroughtSeason} -w $${w} -r ${eu}	\
		${interim}${soilWaterPrefix}_${layer}_${reanalysisId}.nc						\
		${proc}${hdroughtPrefix}_thresholds_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.nc;	\
		apply													\
		${interim}${soilWaterPrefix}_${layer}_${reanalysisId}.nc						\
		${proc}${hdroughtPrefix}_thresholds_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.nc	\
		${proc}${hdroughtPrefix}_daily_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.nc;		\
		daily													\
		${proc}${hdroughtPrefix}_daily_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.nc		\
		${proc}${hdroughtPrefix}_days_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.nc;		\
		setto -v exceedance -m ${hdroughtBuffer} -c 0								\
		${proc}${hdroughtPrefix}_days_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.nc;		\
		consecutive -v exceedance -s ${hdroughtStartCondition} -e ${hdroughtEndCondition} -m disagg		\
		--mn ${hdroughtMinDays}											\
		${proc}${hdroughtPrefix}_days_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.nc;		\
		pool -v exceedance_consecutive -e ${hdroughtEventCondition} -b ${hdroughtBetweennessCondition}		\
		--mx ${hdroughtMaxDistance} --dmin ${hdroughtDmin}							\
		${proc}${hdroughtPrefix}_days_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.nc;		\
		cdo --timestat_date first yearsum									\
		${proc}${hdroughtPrefix}_days_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.nc		\
		${proc}${hdroughtPrefix}_yearly_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.nc;		\
		rescale -v exceedance_consecutive_events -a mn --encoding utf-8						\
		${proc}${hdroughtPrefix}_yearly_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.nc		\
		${nuts}													\
		${proc}${hdroughtPrefix}_yearly_min-rescaled_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.geojson;\
		rescale -v exceedance_consecutive_events -a mx --encoding utf-8						\
		${proc}${hdroughtPrefix}_yearly_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.nc		\
		${nuts}													\
		${proc}${hdroughtPrefix}_yearly_max-rescaled_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.geojson;\
		rescale -v exceedance_consecutive_events -a me --encoding utf-8						\
		${proc}${hdroughtPrefix}_yearly_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.nc		\
		${nuts}													\
		${proc}${hdroughtPrefix}_yearly_mean-rescaled_${hdroughtPercentileDef}_CD$${w}_${layer}_${reanalysisId}.geojson;\
		thresher -p ${hdroughtPercentileDef} -y ${reanalysisBase} -m ${hdroughtSeason} -w $${w} -r ${eu}	\
		--repetitions ${repetitions} --bootstrapping								\
		${interim}${soilWaterPrefix}_${layer}_${reanalysisId}.nc						\
		${proc}${hdroughtPrefix}_thresholds_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.nc;	\
		apply													\
		${interim}${soilWaterPrefix}_${layer}_${reanalysisId}.nc						\
		${proc}${hdroughtPrefix}_thresholds_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.nc	\
		${proc}${hdroughtPrefix}_daily_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.nc;		\
		daily													\
		${proc}${hdroughtPrefix}_daily_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.nc		\
		${proc}${hdroughtPrefix}_days_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.nc;		\
		setto -v exceedance -m ${hdroughtBuffer} -c 0								\
		${proc}${hdroughtPrefix}_days_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.nc;		\
		consecutive -v exceedance -s ${hdroughtStartCondition} -e ${hdroughtEndCondition} -m disagg		\
		--mn ${hdroughtMinDays}											\
		${proc}${hdroughtPrefix}_days_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.nc;		\
		pool -v exceedance_consecutive -e ${hdroughtEventCondition} -b ${hdroughtBetweennessCondition}		\
		--mx ${hdroughtMaxDistance} --dmin ${hdroughtDmin}							\
		${proc}${hdroughtPrefix}_days_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.nc;		\
		cdo --timestat_date first yearsum									\
		${proc}${hdroughtPrefix}_days_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.nc		\
		${proc}${hdroughtPrefix}_yearly_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.nc;	\
		rescale -v exceedance_consecutive_events -a mn --encoding utf-8						\
		${proc}${hdroughtPrefix}_yearly_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.nc		\
		${nuts}													\
		${proc}${hdroughtPrefix}_yearly_min-rescaled_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.geojson;\
		rescale -v exceedance_consecutive_events -a mx --encoding utf-8						\
		${proc}${hdroughtPrefix}_yearly_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.nc		\
		${nuts}													\
		${proc}${hdroughtPrefix}_yearly_max-rescaled_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.geojson;\
		rescale -v exceedance_consecutive_events -a me --encoding utf-8						\
		${proc}${hdroughtPrefix}_yearly_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.nc		\
		${nuts}													\
		${proc}${hdroughtPrefix}_yearly_mean-rescaled_${hdroughtPercentileDef}_CD$${w}_bs_${layer}_${reanalysisId}.geojson;\
		rm													\
		${proc}${hdroughtPrefix}_daily*.nc									\
		${proc}${hdroughtPrefix}_days*.nc;									\
	done

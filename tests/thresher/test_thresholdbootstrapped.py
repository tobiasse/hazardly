import os
from pathlib import Path
from unittest import TestCase

import geopandas as gpd
import numpy as np
from netCDF4 import Dataset

from hazardly.thresher.cfg import LAYER
from hazardly.thresher.netcdf import Netcdf
from hazardly.thresher.threshold import create
from tests.thresher.test_threshold import thresholds


class TestThresholdBootstrapped(TestCase):
    def setUp(self) -> None:
        self.fp = Path(__file__).parent / 'test.nc'
        self.roi = gpd.read_file(Path(__file__).parent / 'res' / 'mask.shp')
        self.netcdf = Netcdf(Path(__file__).parent / 'res' / 'realistic.nc')

    def test_repetitions(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>90', 'tasmax>90'], (1970, 1980), [2, 3], repetitions=100,
                           bootstrapped=True)
        expected = 10
        actual = threshold.repetitions

        self.assertEqual(expected, actual)

    def test_order(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>90', 'tasmax>90'], (1970, 1980), [2, 3], order=-5,
                           bootstrapped=True)
        expected = -1
        actual = threshold.order

        self.assertEqual(expected, actual)

    def test_bootstrapping(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>90', 'tasmax>90'], (1970, 1980), [2, 3],
                           bootstrapped=True)

        self.assertTrue(threshold.bootstrapping)

    def test_years(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>90', 'tasmax>90'], (1970, 1972), [2, 3],
                           bootstrapped=True)
        expected = [
            [1971, 1972],
            [1970, 1972],
            [1970, 1971],
        ]
        actual = list(threshold.sampling_years())

        self.assertListEqual(expected, actual)

    def test_sampling_year_left(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>90', 'tasmax>90'], (1970, 1973), [2, 3], repetitions=2,
                           bootstrapped=True)
        expected = [
            [1971, 1972, 1973], [1972, 1973],
            [1970, 1972, 1973], [1972, 1973],
            [1970, 1971, 1973], [1971, 1973],
            [1970, 1971, 1972], [1971, 1972],
        ]
        actual = list(threshold.sampling_years())

        self.assertListEqual(expected, actual)

    def test_sampling_years_right(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>90', 'tasmax>90'], (1970, 1973), [2, 3], order=-1,
                           repetitions=2, bootstrapped=True)
        expected = [
            [1971, 1972, 1973], [1971, 1972],
            [1970, 1972, 1973], [1970, 1972],
            [1970, 1971, 1973], [1970, 1971],
            [1970, 1971, 1972], [1970, 1971]
        ]
        actual = list(threshold.sampling_years())

        self.assertListEqual(expected, actual)

    def test_layer(self):
        create(self.fp, self.netcdf.path, ['tasmin>90', 'tasmax>90'], (1970, 1980), [2], bootstrapped=True)

        with Dataset(self.fp) as nc:
            expected = 11
            actual = nc.variables[LAYER][:]

            self.assertTrue(expected == len(actual))

    def test_layer_repetitions(self):
        create(self.fp, self.netcdf.path, ['tasmin>90', 'tasmax>90'], (1970, 1980), [2], repetitions=2,
               bootstrapped=True)

        with Dataset(self.fp) as nc:
            expected = 22
            actual = nc.variables[LAYER][:]

            self.assertTrue(expected == len(actual))

    def test_run_bootstrapping_single(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>90'], (1970, 1974), [2], window=3, seed=42,
                           bootstrapped=True)
        threshold.run()

        with Dataset(self.fp) as nc:
            expected = thresholds(0, var='tasmin', cd=3, season=[2], base=(1970, 1974), q=90., netcdf=self.netcdf,
                                  bootstrapped=True, duplicate=[1973], exclude=[1970])
            actual = nc.variables['tasmin'][0, 0, :, :]

            self.assertTrue(np.all(np.isclose(expected, actual)))

    def test_run_bootstrapping_multiple(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>80', 'tasmax>90'], (1970, 1974), [2], window=3, seed=42,
                           bootstrapped=True)
        threshold.run()

        with Dataset(self.fp) as n:
            expected = thresholds(0, var='tasmin', cd=3, season=[2], base=(1970, 1974), q=80., netcdf=self.netcdf,
                                  bootstrapped=True, duplicate=[1973], exclude=[1970])
            actual = n.variables['tasmin'][0, 0, :, :]

            self.assertTrue(np.all(np.isclose(expected, actual)))

            expected = thresholds(0, var='tasmax', cd=3, season=[2], base=(1970, 1974), q=90., netcdf=self.netcdf,
                                  bootstrapped=True, duplicate=[1973], exclude=[1970])
            actual = n.variables['tasmax'][0, 0, :, :]

            self.assertTrue(np.all(np.isclose(expected, actual)))

    def tearDown(self) -> None:
        if os.path.isfile(self.fp):
            os.remove(self.fp)

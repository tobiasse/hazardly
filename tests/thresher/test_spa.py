import os
import unittest
from pathlib import Path
from unittest import TestCase

from cftime import DatetimeGregorian
from numpy import array_equal
from numpy import full
from numpy import percentile
from numpy import zeros

from hazardly.thresher.spa import ar1
from hazardly.thresher.spa import create
from hazardly.thresher.spa import spa

no_gaps_dmin_0 = [
    1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1,
    1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1
]
no_gaps_dmin_4 = [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1,
    1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
]
gaps_dmin_0 = [
    1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1
]
gaps_dmin_4 = [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
]


class TestFunctions(TestCase):
    def setUp(self) -> None:
        self.days, self.lats, self.lons = 210, 10, 10

        self.qt = ar1((self.days, self.lats, self.lons), phi=.9, mu=60., sigma=20., seed=42).round(2)
        self.q0 = full(shape=(self.days, self.lats, self.lons), fill_value=percentile(self.qt, q=20., axis=0))
        self.events = zeros(shape=(self.days, self.lats, self.lons), dtype=int)

    def test_spa_no_gaps_dmin_0(self):
        time = list(range(self.days))
        _ = spa(self.q0, self.qt, self.events, time, self.lats, self.lons, dmin=0)
        actual = self.events[:, 2, 6]

        self.assertTrue(array_equal(no_gaps_dmin_0, actual))

    def test_spa_no_gaps_dmin_4(self):
        time = list(range(self.days))
        _ = spa(self.q0, self.qt, self.events, time, self.lats, self.lons, dmin=4)
        actual = self.events[:, 2, 6]

        self.assertTrue(array_equal(no_gaps_dmin_4, actual))

    def test_spa_gaps_dmin_0(self):
        time = list(range(0, 50)) + list(range(100, 150)) + list(range(175, self.days))
        _ = spa(self.q0, self.qt, self.events, time, self.lats, self.lons, dmin=0)
        actual = self.events[:, 2, 6]

        self.assertTrue(array_equal(gaps_dmin_0, actual))

    def test_spa_gaps_dmin_4(self):
        time = list(range(0, 50)) + list(range(100, 150)) + list(range(175, self.days))
        _ = spa(self.q0, self.qt, self.events, time, self.lats, self.lons, dmin=4)
        actual = self.events[:, 2, 6]

        self.assertTrue(array_equal(gaps_dmin_4, actual))

    def test_spa_caching(self):
        time = list(range(self.days))
        _ = spa(self.q0, self.qt, self.events, time, self.lats, self.lons, dmin=0, cache=10)
        actual = self.events[:, 2, 6]

        self.assertTrue(array_equal(no_gaps_dmin_0, actual))


class TestSPA(unittest.TestCase):
    def setUp(self) -> None:
        self.netcdf_gaps = Path(__file__).parent / 'res' / 'qt_gaps.nc'
        self.netcdf_no_gaps = Path(__file__).parent / 'res' / 'qt.nc'
        self.threshold = Path(__file__).parent / 'res' / 'q0.nc'

        self.exceedance = Path(__file__).parent / 'exceedance.nc'

    def test_spa_no_gaps_dmin_0(self):
        exceedance = create(self.exceedance, self.threshold, self.netcdf_no_gaps).run()
        actual = exceedance.read(0, (DatetimeGregorian(2021, 1, 1), DatetimeGregorian(2021, 7, 30)), 7, 6)
        actual = actual.flatten().data

        self.assertTrue(array_equal(no_gaps_dmin_0, actual))

    def test_spa_no_gaps_dmin_4(self):
        exceedance = create(self.exceedance, self.threshold, self.netcdf_no_gaps, dmin=4).run()
        actual = exceedance.read(0, (DatetimeGregorian(2021, 1, 1), DatetimeGregorian(2021, 7, 30)), 7, 6)
        actual = actual.flatten().data

        self.assertTrue(array_equal(no_gaps_dmin_4, actual))

    def test_spa_gaps_dmin_0(self):
        exceedance = create(self.exceedance, self.threshold, self.netcdf_gaps, dmin=0).run()
        actual = exceedance.read(0, (DatetimeGregorian(2021, 1, 1), DatetimeGregorian(2021, 7, 30)), 7, 6)
        actual = actual.flatten().data
        expected = [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
                    1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    1, 1, 1]

        self.assertTrue(array_equal(expected, actual))

    def test_spa_gaps_dmin_4(self):
        exceedance = create(self.exceedance, self.threshold, self.netcdf_gaps, dmin=4).run()
        actual = exceedance.read(0, (DatetimeGregorian(2021, 1, 1), DatetimeGregorian(2021, 7, 30)), 7, 6)
        actual = actual.flatten().data
        expected = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0]

        self.assertTrue(array_equal(expected, actual))

    def test_spa_caching(self):
        exceedance = create(self.exceedance, self.threshold, self.netcdf_no_gaps, cache=10).run()
        actual = exceedance.read(0, (DatetimeGregorian(2021, 1, 1), DatetimeGregorian(2021, 7, 30)), 7, 6)
        actual = actual.flatten().data

        self.assertTrue(array_equal(no_gaps_dmin_0, actual))

    def tearDown(self) -> None:
        os.remove(self.exceedance)

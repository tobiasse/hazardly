import os
from math import ceil
from pathlib import Path
from typing import List
from typing import Tuple
from unittest import TestCase

import geopandas as gpd
import numpy as np
from cftime import DatetimeGregorian
from netCDF4 import Dataset
from numpy import argwhere
from numpy import array
from numpy import percentile

from hazardly.thresher.cfg import LAT
from hazardly.thresher.cfg import LAYER
from hazardly.thresher.cfg import LON
from hazardly.thresher.cfg import TIME
from hazardly.thresher.netcdf import Netcdf
from hazardly.thresher.threshold import create


def thresholds(idx: int, var: str, cd: int, season: List[int], base: Tuple[int, int], q: float,
               netcdf: Netcdf, bootstrapped: bool = False, duplicate: List[int] = (), exclude: List[int] = ()
               ) -> array:
    """Independent threshold calculation for testing."""
    first = [t for t in netcdf.dates if t.year == base[0] and t.month in season]
    target = first[idx]
    indexes = [
        argwhere(netcdf.dates == t)[0][0] for t in netcdf.dates
        if t.day == target.day and t.month == target.month and base[0] <= t.year <= base[1]
    ]
    if bootstrapped:
        indexes += [
            argwhere(netcdf.dates == t)[0][0] for t in netcdf.dates
            if t.day == target.day and t.month == target.month and t.year in duplicate
        ]
        leave_out = [
            argwhere(netcdf.dates == t)[0][0] for t in netcdf.dates
            if t.day == target.day and t.month == target.month and t.year in exclude
        ]
        for e in leave_out:
            indexes.pop(indexes.index(e))
    dates = list()
    for i in indexes:
        dates.append(netcdf.dates[i])
        for j in range(1, ceil(cd / 2)):
            dates.append(netcdf.dates[i + j])
            dates.append(netcdf.dates[i - j])
    data = netcdf.read(var, dates, netcdf.lats, netcdf.lons, bootstrapped=bootstrapped)
    return percentile(data, q=q, axis=0)


class TestThreshold(TestCase):
    def setUp(self) -> None:
        self.fp = Path(__file__).parent / 'test.nc'
        self.roi = gpd.read_file(Path(__file__).parent / 'res' / 'mask.shp')
        self.netcdf = Netcdf(Path(__file__).parent / 'res' / 'realistic.nc')

    def test_repetitions(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>90', 'tasmax>90'], (1970, 1980), [2, 3], repetitions=100)

        expected = 1
        actual = threshold.repetitions

        self.assertEqual(expected, actual)

    def test_order(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>90', 'tasmax>90'], (1970, 1980), [2, 3], order=100)

        expected = 0
        actual = threshold.order

        self.assertEqual(expected, actual)

    def test_bootstrapping(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>90', 'tasmax>90'], (1970, 1980), [2, 3])
        self.assertFalse(threshold.bootstrapping)

    def test_years(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>90', 'tasmax>90'], (1970, 1980), [2, 3])

        expected = list(range(1970, 1981))
        actual = threshold.years

        self.assertListEqual(expected, actual)

    def test_time(self):
        create(self.fp, self.netcdf.path, ['tasmin>90', 'tasmax>90'], (1970, 1980), [2, 3], seed=42)

        with Dataset(self.fp) as nc:
            time = nc.variables[TIME]

            self.assertTrue(time.window == 5)
            self.assertTrue(time.start == 1970)
            self.assertTrue(time.end == 1980)
            self.assertTrue(time.months == '2,3')
            self.assertTrue(time.hour == 12)
            self.assertTrue(time.minute == 0)
            self.assertTrue(time.microsecond == 0)
            self.assertTrue(time.seed == 42)

    def test_time_leap_year(self):
        create(self.fp, self.netcdf.path, ['tasmin>90'], (1980, 1980), [2])

        with Dataset(self.fp) as nc:
            expected = 28
            actual = nc.variables[TIME][:]

            self.assertTrue(expected == len(actual))

    def test_layer(self):
        create(self.fp, self.netcdf.path, ['tasmin>90'], (1970, 1980), [2])

        with Dataset(self.fp) as nc:
            expected = 1
            actual = nc.variables[LAYER][:]

            self.assertTrue(expected == len(actual))

    def test_lat_lon(self):
        create(self.fp, self.netcdf.path, ['tasmin>90'], (1980, 1980), [2])

        with Dataset(self.fp) as n:
            expected_lat = 8
            expected_lon = 8
            actual_lat = n.variables[LAT][:]
            actual_lon = n.variables[LON][:]

            self.assertTrue(expected_lat == len(actual_lat))
            self.assertTrue(expected_lon == len(actual_lon))

    def test_roi(self):
        create(self.fp, self.netcdf.path, ['tasmin>90'], (1980, 1980), [2], roi=self.roi.geometry[0])

        with Dataset(self.fp) as n:
            expected_lat = 4
            expected_lon = 5
            actual_lat = n.variables[LAT][:]
            actual_lon = n.variables[LON][:]

            self.assertTrue(expected_lat == len(actual_lat))
            self.assertTrue(expected_lon == len(actual_lon))

    def test_run_p90_cd5(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>90'], (1970, 1980), [2])
        threshold.run()

        with Dataset(self.fp) as nc:
            expected = thresholds(0, var='tasmin', cd=5, season=[2], base=(1970, 1980), q=90., netcdf=self.netcdf)
            actual = nc.variables['tasmin'][0, 0, :, :]

            self.assertTrue(np.all(np.isclose(expected, actual)))

    def test_run_p80_cd5(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>80'], (1970, 1980), [2])
        threshold.run()

        with Dataset(self.fp) as nc:
            expected = thresholds(0, var='tasmin', cd=5, season=[2], base=(1970, 1980), q=80., netcdf=self.netcdf)
            actual = nc.variables['tasmin'][0, 0, :, :]

            self.assertTrue(np.all(np.isclose(expected, actual)))

    def test_run_p90_cd11(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>90'], (1970, 1980), [2], window=11)
        threshold.run()

        with Dataset(self.fp) as nc:
            expected = thresholds(0, var='tasmin', cd=11, season=[2], base=(1970, 1980), q=90., netcdf=self.netcdf)
            actual = nc.variables['tasmin'][0, 0, :, :]

            self.assertTrue(np.all(np.isclose(expected, actual)))

    def test_run_p80_cd11(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>80'], (1970, 1980), [2], window=11)
        threshold.run()

        with Dataset(self.fp) as nc:
            expected = thresholds(0, var='tasmin', cd=11, season=[2], base=(1970, 1980), q=80., netcdf=self.netcdf)
            actual = nc.variables['tasmin'][0, 0, :, :]

            self.assertTrue(np.all(np.isclose(expected, actual)))

    def test_run_multiple_variables(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>80', 'tasmax>90'], (1970, 1980), [2])
        threshold.run()

        with Dataset(self.fp) as nc:
            expected = thresholds(0, var='tasmin', cd=5, season=[2], base=(1970, 1980), q=80., netcdf=self.netcdf)
            actual = nc.variables['tasmin'][0, 0, :, :]

            self.assertTrue(np.all(np.isclose(expected, actual)))

            expected = thresholds(0, var='tasmax', cd=5, season=[2], base=(1970, 1980), q=90., netcdf=self.netcdf)
            actual = nc.variables['tasmax'][0, 0, :, :]

            self.assertTrue(np.all(np.isclose(expected, actual)))

    def test_read(self):
        threshold = create(self.fp, self.netcdf.path, ['tasmin>90'], (1970, 1980), [2])
        threshold.run()

        expected = thresholds(0, var='tasmin', cd=5, season=[2], base=(1970, 1980), q=90., netcdf=self.netcdf)[0]
        actual = threshold.read('tasmin', 0, DatetimeGregorian(1970, 2, 1), threshold.lats[0])
        self.assertTrue(np.all(np.isclose(expected, actual)))

    def tearDown(self) -> None:
        if os.path.isfile(self.fp):
            os.remove(self.fp)

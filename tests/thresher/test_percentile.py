from unittest import TestCase

from hazardly.thresher.errors import ThresherError
from hazardly.thresher.threshold import Percentile


class TestPercentile(TestCase):
    def test_from_text(self):
        p = Percentile.from_text('data > 90')
        self.assertEqual(p.var, 'data')
        self.assertEqual(p.op, '>')
        self.assertEqual(p.p, 90)

    def test_from_text_raises_error(self):
        with self.assertRaises(ThresherError):
            Percentile.from_text('data==90')

    def test_percentile_raises(self):
        with self.assertRaises(ThresherError):
            Percentile('data', '>', 101)

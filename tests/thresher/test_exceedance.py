import os
from functools import reduce
from pathlib import Path
from typing import List
from unittest import TestCase

import numpy as np
from cftime import DatetimeGregorian
from cftime import datetime
from netCDF4 import Dataset
from numpy.typing import NDArray

from hazardly.thresher.cfg import EXCEED
from hazardly.thresher.exceedance import create as ex
from hazardly.thresher.netcdf import Netcdf
from hazardly.thresher.threshold import Percentile
from hazardly.thresher.threshold import Threshold
from hazardly.thresher.threshold import create as ts


# noinspection PyTypeChecker
def validation(
        netcdf: Netcdf,
        threshold: Threshold,
        layer: int,
        date: datetime,
        percentiles: List[str]
) -> NDArray[float]:
    """Independent exceedance calculation for testing"""
    results = list()

    for p in percentiles:
        percentile = Percentile.from_text(p)
        responses = netcdf.read(percentile.var, date, threshold.lats, threshold.lons)
        thresholds = threshold.read(percentile.var, layer, date, threshold.lats)

        if percentile.op == '>':
            results.append(responses > thresholds)
        elif percentile.op == '<':
            results.append(responses < thresholds)
        elif percentile.op == '>=':
            results.append(responses >= thresholds)
        elif percentile.op == '<=':
            results.append(responses <= thresholds)

    return reduce(lambda previous, current: previous & current, results)


# noinspection DuplicatedCode
class TestExceedance(TestCase):
    def setUp(self) -> None:
        self.netcdf = Netcdf(Path(__file__).parent / 'res' / 'realistic.nc')
        self.threshold = Path(__file__).parent / 'threshold.nc'
        self.exceedance = Path(__file__).parent / 'exceedance.nc'

        self.base = (1970, 1974)
        self.months = [8]
        self.percentile = ['tasmin>90']
        self.percentiles = ['tasmin>90', 'tasmax>90']

    def test_run_single_var(self):
        threshold = ts(self.threshold, self.netcdf.path, self.percentile, self.base, self.months).run()
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()

        expected = validation(self.netcdf, threshold, 0, DatetimeGregorian(1971, 8, 1, 12), self.percentile)
        actual = exceedance.read(0, (DatetimeGregorian(1971, 8, 1), DatetimeGregorian(1971, 8, 2)), exceedance.lats)

        self.assertTrue(np.array_equal(expected, actual))

    def test_run_multiple_var(self):
        threshold = ts(self.threshold, self.netcdf.path, self.percentiles, self.base, self.months).run()
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()

        expected = validation(self.netcdf, threshold, 0, DatetimeGregorian(1971, 8, 1, 12), self.percentiles)
        actual = exceedance.read(0, (DatetimeGregorian(1971, 8, 1), DatetimeGregorian(1971, 8, 2)), exceedance.lats)

        self.assertTrue(np.array_equal(expected, actual))

    def test_read(self):
        ts(self.threshold, self.netcdf.path, self.percentile, self.base, self.months).run()
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()

        with Dataset(self.exceedance) as nc:
            expected = nc.variables[EXCEED][0, 0:1, :, :]
        actual = exceedance.read(0, (DatetimeGregorian(1970, 8, 1), DatetimeGregorian(1970, 8, 2)), exceedance.lats)

        self.assertTrue(np.array_equal(expected, actual))

    def test_days(self):
        ts(self.threshold, self.netcdf.path, self.percentile, self.base, self.months).run()
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()

        expected = 341
        actual = len(exceedance.days)

        self.assertEqual(expected, actual)

    def test_months(self):
        ts(self.threshold, self.netcdf.path, self.percentile, self.base, self.months).run()
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()

        expected = 11
        actual = len(exceedance.months)

        self.assertEqual(expected, actual)

    def test_years(self):
        ts(self.threshold, self.netcdf.path, self.percentile, self.base, self.months).run()
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()

        expected = 11
        actual = len(exceedance.years)

        self.assertEqual(expected, actual)

    def tearDown(self) -> None:
        os.remove(self.threshold)
        os.remove(self.exceedance)

import os
from pathlib import Path
from unittest import TestCase

import numpy as np
from cftime import DatetimeGregorian

from hazardly.thresher.exceedance import create as ex
from hazardly.thresher.netcdf import Netcdf
from hazardly.thresher.threshold import create as ts
from tests.thresher.test_exceedance import validation


# noinspection DuplicatedCode
class TestExceedanceBootstrapped(TestCase):
    def setUp(self) -> None:
        self.netcdf = Netcdf(Path(__file__).parent / 'res' / 'realistic.nc')
        self.threshold = Path(__file__).parent / 'threshold.nc'
        self.exceedance = Path(__file__).parent / 'exceedance.nc'

        self.base = (1970, 1974)
        self.months = [8]
        self.percentile = ['tasmin>90']
        self.percentiles = ['tasmin>90', 'tasmax>90']

    def test_run_only_base(self):
        threshold = ts(self.threshold, self.netcdf.path, self.percentile, self.base, self.months, bootstrapped=True)
        threshold.run()
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()

        self.assertTrue(exceedance.dates[0].year == self.base[0])
        self.assertTrue(exceedance.dates[-1].year == self.base[-1])

    def test_run_only_bootstrapping_year(self):
        threshold = ts(self.threshold, self.netcdf.path, self.percentile, self.base, self.months, bootstrapped=True)
        threshold.run()
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()

        for lay in range(5):
            if lay == 1:
                expected = validation(self.netcdf, threshold, lay, DatetimeGregorian(1971, 8, 1, 12), self.percentile)
            else:
                expected = np.zeros((1, len(exceedance.lats), len(exceedance.lons)))
            actual = exceedance.read(lay, (DatetimeGregorian(1971, 8, 1), DatetimeGregorian(1971, 8, 2)),
                                     exceedance.lats)

            self.assertTrue(np.array_equal(expected, actual.data))

    def test_run_single_var(self):
        threshold = ts(self.threshold, self.netcdf.path, self.percentile, self.base, self.months, bootstrapped=True)
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()

        expected = validation(self.netcdf, threshold, 1, DatetimeGregorian(1971, 8, 1, 12), self.percentile)
        actual = exceedance.read(1, (DatetimeGregorian(1971, 8, 1), DatetimeGregorian(1971, 8, 2)), exceedance.lats)

        self.assertTrue(np.array_equal(expected, actual))

    def test_run_multiple_var(self):
        threshold = ts(self.threshold, self.netcdf.path, self.percentiles, self.base, self.months, bootstrapped=True)
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()

        expected = validation(self.netcdf, threshold, 1, DatetimeGregorian(1971, 8, 1, 12), self.percentiles)
        actual = exceedance.read(1, (DatetimeGregorian(1971, 8, 1), DatetimeGregorian(1971, 8, 2)), exceedance.lats)

        self.assertTrue(np.array_equal(expected, actual))

    def tearDown(self) -> None:
        os.remove(self.threshold)
        os.remove(self.exceedance)

from pathlib import Path
from unittest import TestCase

from cftime import DatetimeGregorian
from cftime import DatetimeNoLeap

from hazardly.thresher.errors import ThresherError
from hazardly.thresher.netcdf import Netcdf
from hazardly.thresher.threshold import Time

times = list()
for y in [2019, 2020]:
    for m in range(1, 13):
        for d in range(1, 32):
            try:
                t = DatetimeGregorian(y, m, d)
                times.append(t)
            except ValueError:
                pass


class TestTime(TestCase):
    def test_init_raises_on_months(self):
        with self.assertRaises(ThresherError):
            Time(base=(2019, 2020), months=[13], window=5)

    def test_init_raises_on_window(self):
        with self.assertRaises(ThresherError):
            Time(base=(2019, 2020), months=[1, 2], window=6)

    def test_base_length(self):
        obj = Time(base=(2019, 2020), months=[12], window=5)

        expected = 2
        actual = obj.base_length

        self.assertEqual(expected, actual)

    # noinspection PyTypeChecker
    def test_samples_keep_not_set(self):
        obj = Time(base=(2020, 2025), months=[2], window=3)
        sink = list()
        for current, samples in obj.samples():
            sink.append((current, samples))
            self.assertEqual(18, len(samples))
            for i in samples:
                self.assertTrue(isinstance(i, DatetimeNoLeap))

        expected_current, expected_samples = DatetimeNoLeap(2020, 2, 1), [
            DatetimeNoLeap(2020, 2, 1), DatetimeNoLeap(2020, 1, 31), DatetimeNoLeap(2020, 2, 2),
            DatetimeNoLeap(2021, 2, 1), DatetimeNoLeap(2021, 1, 31), DatetimeNoLeap(2021, 2, 2),
            DatetimeNoLeap(2022, 2, 1), DatetimeNoLeap(2022, 1, 31), DatetimeNoLeap(2022, 2, 2),
            DatetimeNoLeap(2023, 2, 1), DatetimeNoLeap(2023, 1, 31), DatetimeNoLeap(2023, 2, 2),
            DatetimeNoLeap(2024, 2, 1), DatetimeNoLeap(2024, 1, 31), DatetimeNoLeap(2024, 2, 2),
            DatetimeNoLeap(2025, 2, 1), DatetimeNoLeap(2025, 1, 31), DatetimeNoLeap(2025, 2, 2),
        ]
        actual_current, actual_samples = sink[0]

        self.assertEqual(expected_current, actual_current)
        self.assertListEqual(expected_samples, actual_samples)

    # noinspection PyTypeChecker
    def test_samples_keep_set(self):
        obj = Time(base=(2020, 2025), months=[2], window=3, seed=42)
        sink = list()
        for current, samples in obj.samples(keep=[2022, 2023, 2024, 2025]):
            sink.append((current, samples))
            self.assertEqual(18, len(samples))
            for i in samples:
                self.assertTrue(isinstance(i, DatetimeNoLeap))

        expected_current, expected_samples = DatetimeNoLeap(2022, 2, 1), [
            DatetimeNoLeap(2022, 2, 1), DatetimeNoLeap(2022, 1, 31), DatetimeNoLeap(2022, 2, 2),
            DatetimeNoLeap(2023, 2, 1), DatetimeNoLeap(2023, 1, 31), DatetimeNoLeap(2023, 2, 2),
            DatetimeNoLeap(2024, 2, 1), DatetimeNoLeap(2024, 1, 31), DatetimeNoLeap(2024, 2, 2),
            DatetimeNoLeap(2025, 2, 1), DatetimeNoLeap(2025, 1, 31), DatetimeNoLeap(2025, 2, 2),
            DatetimeNoLeap(2024, 2, 1), DatetimeNoLeap(2024, 1, 31), DatetimeNoLeap(2024, 2, 2),
            DatetimeNoLeap(2025, 2, 1), DatetimeNoLeap(2025, 1, 31), DatetimeNoLeap(2025, 2, 2),
        ]
        actual_current, actual_samples = sink[0]

        self.assertEqual(expected_current, actual_current)
        self.assertListEqual(expected_samples, actual_samples)

    def test_indices_leap_year(self):
        obj = Time(base=(2019, 2020), months=[2, 3], window=5)

        expected = list(range(32, 91))  # calendar days for February till March in leap year
        actual = obj.indices

        self.assertListEqual(expected, actual)

    def test_monthly_indices_leap_year(self):
        obj = Time(base=(2019, 2020), months=[2, 3], window=5)

        expected = (32, 59)
        actual = obj.monthly_indices[2]

        self.assertEqual(expected, actual)

    def test_monthly_indices(self):
        obj = Time(base=(2019, 2020), months=[2, 3], window=5)

        expected = (32, 59)
        actual = obj.monthly_indices[2]

        self.assertEqual(expected, actual)

    def test_sampling_range(self):
        obj = Time((1980, 1990), [1, 2, 3], 5)

        actual = obj.sampling_range

        self.assertEqual(DatetimeNoLeap(1980, 1, 1), actual[0])
        self.assertEqual(DatetimeNoLeap(1980, 3, 31), actual[-1])

    def test_len(self):
        obj = Time(base=(2019, 2020), months=[1, 2], window=5)

        expected = 59
        actual = len(obj)

        self.assertEqual(expected, actual)

    def test_randomize(self):
        obj = Time(base=(2020, 2025), months=[1, 2], window=5, seed=42)

        expected = [2024, 2025]
        actual = obj.randomize(keep=[2022, 2023, 2024, 2025])

        self.assertListEqual(expected, actual)

    def test_timestamp_index_raises(self):
        obj = Time(base=(2019, 2020), months=[1, 2], window=5)
        with self.assertRaises(ThresherError):
            obj.timestamp_index(DatetimeGregorian(2020, 3, 1))

    def test_timestamp_index(self):
        obj = Time(base=(2019, 2020), months=[1, 2], window=5)
        expected = 32
        actual = obj.timestamp_index(DatetimeGregorian(2020, 2, 1))

        self.assertEqual(expected, actual)

    def test_index_raises(self):
        obj = Time(base=(2019, 2020), months=[1, 2], window=5)
        with self.assertRaises(ThresherError):
            obj.index(365)

    def test_index(self):
        obj = Time(base=(2019, 2020), months=[1, 2], window=5)
        expected = 31
        actual = obj.index(32)

        self.assertEqual(expected, actual)

    def test_within(self):
        obj = Time((1981, 1985), [2], 5)
        nc = Netcdf(Path(__file__).parent / 'res/test.nc')

        actual = obj.within(nc, bootstrapped=False)

        self.assertEqual(31, actual[0])
        self.assertEqual(3711, actual[-1])

    def test_within_bootstrapped(self):
        obj = Time((1981, 1985), [2], 5)
        nc = Netcdf(Path(__file__).parent / 'res/test.nc')

        actual = obj.within(nc, bootstrapped=True)

        self.assertEqual(397, actual[0])
        self.assertEqual(1885, actual[-1])

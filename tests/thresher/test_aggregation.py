import os
from pathlib import Path
from typing import List
from typing import Tuple
from unittest import TestCase

from cftime import DatetimeGregorian
from cftime import datetime
from numpy import array
from numpy import array_equal
from numpy.typing import NDArray

from hazardly.thresher.aggregation import Aggregation
from hazardly.thresher.aggregation import create as ag
from hazardly.thresher.exceedance import Exceedance
from hazardly.thresher.exceedance import create as ex
from hazardly.thresher.netcdf import Netcdf
from hazardly.thresher.threshold import create as ts


def aggregate(
        exceedance: Exceedance,
        span: Tuple[datetime, datetime],
        steps: str,
        layers: List[int] = None,
        confidence: float = .75
) -> NDArray[float]:
    """Independent aggregation for testing"""
    if not layers:
        layers = exceedance.layer
    data = array([
        exceedance.read(lay, span, exceedance.lats).sum(axis=0)
        for lay in layers
    ])
    if steps == Aggregation.daily:
        data = data.mean(axis=0)
        data[data <= confidence] = 0
        data[data > confidence] = 1
        return data
    else:
        return data.mean(axis=0)


class TestAggregation(TestCase):
    def setUp(self) -> None:
        self.netcdf = Netcdf(Path(__file__).parent / 'res' / 'realistic.nc')
        self.threshold = Path(__file__).parent / 'threshold.nc'
        self.exceedance = Path(__file__).parent / 'exceedance.nc'
        self.aggregation = Path(__file__).parent / 'aggregation.nc'

        self.base = (1970, 1974)
        self.months = [8]
        self.percentile = ['tasmin>90']

    def test_aggregation_daily(self):
        ts(self.threshold, self.netcdf.path, self.percentile, self.base, self.months).run()
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()
        aggregation = ag(self.aggregation, self.exceedance, steps=Aggregation.daily).run()

        expected = aggregate(exceedance, (DatetimeGregorian(1971, 8, 1), DatetimeGregorian(1971, 8, 2)),
                             aggregation.steps)
        actual = aggregation.read(DatetimeGregorian(1971, 8, 1, 12), aggregation.lats)[0]

        self.assertTrue(array_equal(expected, actual))

    def test_aggregation_monthly(self):
        ts(self.threshold, self.netcdf.path, self.percentile, self.base, self.months).run()
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()
        aggregation = ag(self.aggregation, self.exceedance, steps=Aggregation.monthly).run()

        expected = aggregate(exceedance, (DatetimeGregorian(1971, 8, 1), DatetimeGregorian(1971, 9, 1)),
                             aggregation.steps)
        actual = aggregation.read(DatetimeGregorian(1971, 8, 1, 12), aggregation.lats)[0]

        self.assertTrue(array_equal(expected, actual))

    def test_aggregation_yearly(self):
        ts(self.threshold, self.netcdf.path, self.percentile, self.base, self.months).run()
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()
        aggregation = ag(self.aggregation, self.exceedance, steps=Aggregation.yearly).run()

        expected = aggregate(exceedance, (DatetimeGregorian(1971, 8, 1), DatetimeGregorian(1972, 1, 1)),
                             aggregation.steps)
        actual = aggregation.read(DatetimeGregorian(1971, 8, 1, 12), aggregation.lats)[0]

        self.assertTrue(array_equal(expected, actual))

    def tearDown(self) -> None:
        os.remove(self.threshold)
        os.remove(self.exceedance)
        os.remove(self.aggregation)

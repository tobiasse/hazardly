import os
from pathlib import Path
from shutil import copyfile
from tempfile import TemporaryDirectory
from unittest import TestCase

import numpy as np
from cftime import DatetimeGregorian
from cftime import DatetimeNoLeap
from netCDF4 import Dataset
from numpy import array_equal

from hazardly.thresher.errors import ThresherError
from hazardly.thresher.netcdf import Netcdf
from hazardly.thresher.netcdf import copy_to
from hazardly.thresher.netcdf import fill_value
from hazardly.thresher.netcdf import get_attr
from hazardly.thresher.netcdf import set_to

src = Path(os.path.join(os.path.dirname(__file__), 'res/test.nc'))
first = Path(os.path.join(os.path.dirname(__file__), 'res/first.nc'))
second = Path(os.path.join(os.path.dirname(__file__), 'res/second.nc'))


class TestNetcdf(TestCase):
    def test_init_path_is_not_file_raises_error(self):
        with self.assertRaises(ThresherError):
            Netcdf(Path('/home'))

    def test_init_path_is_not_netcdf_raises_error(self):
        with self.assertRaises(OSError):
            Netcdf(Path(__file__))

    def test_init_with_netcdf(self):
        obj = Netcdf(src)

        self.assertTrue(len(obj.vars) == 4)
        self.assertTrue(len(obj.lats) == 13)
        self.assertTrue(len(obj.lons) == 21)
        self.assertTrue(len(obj.dates) == 418)

    def test_read_with_missing_var_raises_error(self):
        with self.assertRaises(ThresherError):
            obj = Netcdf(src)
            obj.read('foo', DatetimeNoLeap(1, 1, 1), 0, 0)

    def test_read_with_var_returns_data(self):
        obj = Netcdf(src)

        expected = [[[119]]]
        actual = obj.read('data', DatetimeNoLeap(1980, 1, 27), 52., 2.)

        self.assertTrue(array_equal(expected, actual))

    def test_read_bootstrapped(self):
        dates = [DatetimeNoLeap(1980, 2, 1), DatetimeNoLeap(1980, 2, 2), DatetimeNoLeap(1980, 2, 3),
                 DatetimeNoLeap(1980, 2, 2), DatetimeNoLeap(1980, 2, 2), DatetimeNoLeap(1980, 2, 3),
                 DatetimeNoLeap(1980, 2, 1), DatetimeNoLeap(1980, 2, 3), DatetimeNoLeap(1980, 2, 3)]
        obj = Netcdf(src)

        expected = [[[60]], [[31]], [[32]], [[60]], [[31]], [[32]], [[31]], [[32]], [[32]]]
        actual = obj.read('data', dates, 52., 2., bootstrapped=True)

        self.assertTrue(len(dates) == len(actual))
        self.assertTrue(np.array_equal(expected, actual))

    def test_read_bootstrapped_with_date_overflow(self):
        dates = [DatetimeNoLeap(1979, 2, 1), DatetimeNoLeap(1979, 1, 31), DatetimeNoLeap(1979, 2, 2),
                 DatetimeNoLeap(1980, 2, 1), DatetimeNoLeap(1980, 1, 31), DatetimeNoLeap(1980, 2, 2),
                 DatetimeNoLeap(1981, 2, 1), DatetimeNoLeap(1981, 1, 31), DatetimeNoLeap(1981, 2, 2),
                 DatetimeNoLeap(1982, 2, 1), DatetimeNoLeap(1982, 1, 31), DatetimeNoLeap(1982, 2, 2),
                 DatetimeNoLeap(1984, 2, 1), DatetimeNoLeap(1984, 1, 31), DatetimeNoLeap(1984, 2, 2),
                 DatetimeNoLeap(1990, 2, 1), DatetimeNoLeap(1990, 1, 31), DatetimeNoLeap(1990, 2, 2),
                 # Start of repetitions
                 DatetimeNoLeap(1990, 2, 1), DatetimeNoLeap(1990, 1, 31), DatetimeNoLeap(1990, 2, 2),
                 DatetimeNoLeap(1990, 2, 1), DatetimeNoLeap(1990, 1, 31), DatetimeNoLeap(1990, 2, 2),
                 DatetimeNoLeap(1979, 2, 1), DatetimeNoLeap(1979, 1, 31), DatetimeNoLeap(1979, 2, 2),
                 DatetimeNoLeap(1980, 2, 1), DatetimeNoLeap(1980, 1, 31), DatetimeNoLeap(1980, 2, 2),
                 DatetimeNoLeap(1980, 2, 1), DatetimeNoLeap(1980, 1, 31), DatetimeNoLeap(1980, 2, 2),
                 DatetimeNoLeap(1984, 2, 1), DatetimeNoLeap(1984, 1, 31), DatetimeNoLeap(1984, 2, 2),
                 DatetimeNoLeap(1990, 2, 1), DatetimeNoLeap(1990, 1, 31), DatetimeNoLeap(1990, 2, 2),
                 DatetimeNoLeap(1990, 2, 1), DatetimeNoLeap(1990, 1, 31), DatetimeNoLeap(1990, 2, 2),
                 DatetimeNoLeap(1990, 2, 1), DatetimeNoLeap(1990, 1, 31), DatetimeNoLeap(1990, 2, 2),
                 DatetimeNoLeap(1984, 2, 1), DatetimeNoLeap(1984, 1, 31), DatetimeNoLeap(1984, 2, 2)]
        obj = Netcdf(src)

        expected = [[[36]], [[60]], [[31]],
                    [[96]], [[34]], [[52]],
                    [[105]], [[118]], [[85]],
                    [[32]], [[16]], [[75]],
                    [[79]], [[91]], [[83]],
                    # Start of repetitions
                    [[36]], [[60]], [[31]],
                    [[32]], [[16]], [[75]],
                    [[79]], [[91]], [[83]],
                    [[36]], [[60]], [[31]],
                    [[32]], [[16]], [[75]],
                    [[79]], [[91]], [[83]],
                    [[79]], [[91]], [[83]],
                    [[79]], [[91]], [[83]],
                    [[79]], [[91]], [[83]],
                    ]

        actual = obj.read('data', dates, 52., 2., bootstrapped=True)
        self.assertTrue(42 == len(actual))
        self.assertTrue(np.array_equal(expected, actual))

    def test_cast_with_list(self):
        obj = Netcdf(src)
        dates = [DatetimeNoLeap(1980, 1, 1), DatetimeNoLeap(1980, 1, 1)]
        actual = obj._cast(dates)
        self.assertTrue(type(actual[0]) == DatetimeGregorian)
        self.assertTrue(type(actual[1]) == DatetimeGregorian)

    def test_cast_with_list_of_same_type(self):
        obj = Netcdf(src)
        dates = [DatetimeGregorian(1980, 1, 1), DatetimeGregorian(1980, 1, 1)]
        actual = obj._cast(dates)
        self.assertTrue(type(actual[0]) == DatetimeGregorian)
        self.assertTrue(type(actual[1]) == DatetimeGregorian)

    def test_cast_with_single(self):
        obj = Netcdf(src)
        date = DatetimeNoLeap(1980, 1, 1)
        actual = obj._cast(date)
        self.assertTrue(type(actual) == DatetimeGregorian)

    def test_cast_with_single_of_same_type(self):
        obj = Netcdf(src)
        date = DatetimeNoLeap(1980, 1, 1)
        actual = obj._cast(date)
        self.assertTrue(type(actual) == DatetimeGregorian)

    def test_units_with_existing_var(self):
        obj = Netcdf(src)

        actual = obj.units('data')
        expected = '-'

        self.assertTrue(actual == expected)

    def test_units_raises_error_if_var_not_exist(self):
        obj = Netcdf(src)

        with self.assertRaises(ThresherError):
            obj.units('foo')

    def test_fill_value(self):
        obj = Netcdf(src)

        actual = obj.fill_value('data')
        expected = 0

        self.assertTrue(actual == expected)

    def test_fill_value_raises_error_if_var_not_exist(self):
        obj = Netcdf(src)

        with self.assertRaises(ThresherError):
            obj.fill_value('foo')


class TestFunctions(TestCase):
    def test_fill_value_set(self):
        obj = Dataset(src)

        expected = 2
        actual = fill_value(obj.variables['data3'])

        obj.close()

        self.assertEqual(expected, actual)

    def test_fill_value_not_set(self):
        obj = Dataset(src)

        expected = -127
        actual = fill_value(obj.variables['data4'])

        obj.close()

        self.assertEqual(expected, actual)

    def test_get_attr_not_set(self):
        obj = Dataset(src)

        expected = None
        actual = get_attr('foo', obj.variables['data3'])

        obj.close()

        self.assertEqual(expected, actual)

    def test_get_attr_default(self):
        obj = Dataset(src)

        expected = 'bar'
        actual = get_attr('foo', obj.variables['data3'], default='bar')

        obj.close()

        self.assertEqual(expected, actual)

    def test_set_to(self):
        with TemporaryDirectory() as d:
            dst = Path(d) / 'test.nc'
            copyfile(src, dst)

            set_to(dst, 'data', [2], 1)

            with Dataset(dst) as nc:
                data = nc.variables['data'][5, :, :]
                self.assertTrue(np.all(data == 1))

    def test_copy_to(self):
        with TemporaryDirectory() as d:
            path = Path(d)
            copyfile(second, path / second.name)

            copy_to(first, path / second.name, 'data')

            with Dataset(first) as src1, Dataset(second) as src2, Dataset(path / second.name) as tar:
                expected1 = src1.variables['data'][0]
                expected2 = src2.variables['data'][-2]
                actual1 = tar.variables['data'][0]
                actual2 = tar.variables['data'][-2]

                self.assertTrue(np.all(expected1 == actual1))
                self.assertTrue(np.all(expected2 == actual2))

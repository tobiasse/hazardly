import os
from pathlib import Path
from unittest import TestCase

from cftime import DatetimeGregorian
from numpy import array_equal

from hazardly.thresher.aggregation import Aggregation
from hazardly.thresher.aggregation import create as ag
from hazardly.thresher.exceedance import create as ex
from hazardly.thresher.netcdf import Netcdf
from hazardly.thresher.threshold import create as ts
from tests.thresher.test_aggregation import aggregate


class TestAggregationBootstrapped(TestCase):
    def setUp(self) -> None:
        self.netcdf = Netcdf(Path(__file__).parent / 'res' / 'realistic.nc')
        self.threshold = Path(__file__).parent / 'threshold.nc'
        self.exceedance = Path(__file__).parent / 'exceedance.nc'
        self.aggregation = Path(__file__).parent / 'aggregation.nc'

        self.base = (1970, 1974)
        self.months = [8]
        self.percentile = ['tasmin>90']

    def test_aggregation_daily(self):
        ts(self.threshold, self.netcdf.path, self.percentile, self.base, self.months, bootstrapped=True).run()
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()
        aggregation = ag(self.aggregation, self.exceedance, steps=Aggregation.daily).run()

        expected = aggregate(exceedance, (DatetimeGregorian(1971, 8, 1), DatetimeGregorian(1971, 8, 2)),
                             aggregation.steps, layers=[1])
        actual = aggregation.read(DatetimeGregorian(1971, 8, 1, 12), aggregation.lats)

        self.assertTrue(array_equal(expected, actual[0]))

    def test_aggregation_monthly(self):
        ts(self.threshold, self.netcdf.path, self.percentile, self.base, self.months, bootstrapped=True).run()
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()
        aggregation = ag(self.aggregation, self.exceedance, steps=Aggregation.monthly).run()

        expected = aggregate(exceedance, (DatetimeGregorian(1971, 8, 1), DatetimeGregorian(1971, 9, 1)),
                             aggregation.steps, layers=[1])
        actual = aggregation.read(DatetimeGregorian(1971, 8, 1, 12), aggregation.lats)[0]

        self.assertTrue(array_equal(expected, actual))

    def test_aggregation_yearly(self):
        ts(self.threshold, self.netcdf.path, self.percentile, self.base, self.months, bootstrapped=True).run()
        exceedance = ex(self.exceedance, self.threshold, self.netcdf.path).run()
        aggregation = ag(self.aggregation, self.exceedance, steps=Aggregation.yearly).run()

        expected = aggregate(exceedance, (DatetimeGregorian(1971, 8, 1), DatetimeGregorian(1972, 1, 1)),
                             aggregation.steps, layers=[1])
        actual = aggregation.read(DatetimeGregorian(1971, 8, 1, 12), aggregation.lats)[0]

        self.assertTrue(array_equal(expected, actual))

    def tearDown(self) -> None:
        os.remove(self.threshold)
        os.remove(self.exceedance)
        os.remove(self.aggregation)

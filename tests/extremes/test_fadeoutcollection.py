from unittest import TestCase
from unittest.mock import Mock

from matplotlib.cm import get_cmap

from hazardly.extremes.fadeout import FadeoutCollection

reds = get_cmap('Reds')

feature = Mock()
feature._kwargs = dict()


class TestFadeoutCollection(TestCase):
    def test_is_keyframe(self):
        fc = FadeoutCollection(10, reds, 'white')

        self.assertTrue(fc.is_keyframe(0))
        self.assertTrue(fc.is_keyframe(10))

        self.assertFalse(fc.is_keyframe(5))

    def test_notify_removes_ele(self):
        fadeouts = FadeoutCollection(10, reds, 'white')
        fadeouts.add('test', feature, 1.)

        fadeouts.update(9)

        self.assertTrue('test' not in fadeouts._fadeouts)

    def test_add_adds_new_fadeout(self):
        fadeouts = FadeoutCollection(10, reds, 'white')
        fadeouts.add('test', feature, 1.)

        self.assertTrue('test' in fadeouts._fadeouts)

    def test_add_replaces_already_existing_fadeout(self):
        fadeouts = FadeoutCollection(10, reds, 'white')
        fadeouts.add('test', feature, 1.)

        current1 = fadeouts._fadeouts['test'].func.intercept

        fadeouts.add('test', feature, .7)
        current2 = fadeouts._fadeouts['test'].func.intercept

        self.assertTrue(current1 != current2)

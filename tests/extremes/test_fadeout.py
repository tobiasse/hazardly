from unittest import TestCase
from unittest.mock import Mock

from matplotlib.cm import get_cmap

from hazardly.extremes.fadeout import Fadeout
from hazardly.extremes.fadeout import LinearDecrement

reds = get_cmap('Reds')

feature = Mock()
feature._kwargs = dict()


class TestFadeout(TestCase):
    def test_update_calls_notify(self):
        func = LinearDecrement(9, .7)
        fadeout = Fadeout('test', func, feature, lambda idx: self.assertTrue(idx == 'test'), reds, 'white')

        fadeout.update(9)

    def test_update_sets_end_color(self):
        func = LinearDecrement(9, .7)
        fadeout = Fadeout('test', func, feature, lambda idx: idx, reds, 'white')

        fadeout.update(9)
        self.assertTrue(feature._kwargs[Fadeout.face] == 'white')

    def test_update_fades_out_color(self):
        func = LinearDecrement(9, .7)
        fadeout = Fadeout('test', func, feature, lambda idx: idx, reds, 'white')

        fadeout.update(0)
        self.assertEqual(reds(.7), feature._kwargs[Fadeout.face])
        fadeout.update(5)
        self.assertEqual(reds(func(5)), feature._kwargs[Fadeout.face])

from unittest import TestCase

from hazardly.extremes.fadeout import LinearDecrement


class TestLinearDecrement(TestCase):
    def test_intercept(self):
        x, y = 9, .7
        norm = LinearDecrement(x, y)

        expected = .7
        actual = norm.intercept

        self.assertEqual(expected, actual)

    def test_slope(self):
        x, y = 9, .7
        norm = LinearDecrement(x, y)

        expected = y / (-1 * x)
        actual = norm.slope

        self.assertEqual(expected, actual)

    def test_linear_decrement(self):
        x, y = 9, .7
        norm = LinearDecrement(x, y)

        self.assertEqual(.7, norm(0))
        self.assertEqual(0, norm(9))
        self.assertEqual(.7, norm(-1))
        self.assertEqual(0, norm(10))

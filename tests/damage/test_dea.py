from unittest import TestCase

from numpy import array
from numpy import array_equal
from numpy import nan
from scipy.stats import pearsonr

from hazardly.damage.dea import Ring
from hazardly.damage.dea import YieldDamage
from hazardly.damage.dea import _samples
from hazardly.damage.dea import dea
from hazardly.damage.dea import make_points
from hazardly.damage.dea import pearsoncor
from hazardly.damage.dea import spearmancor
from hazardly.damage.errors import DamageError


class TestFunctions(TestCase):
    def test_make_points_keep_zeros_false(self):
        actual = make_points(array([1, 0, -1]), False)
        expected = array([
            [0, 1],
            [2, -1]
        ])

        self.assertTrue(array_equal(actual, expected))

    def test_make_points_keep_zeros_true(self):
        actual = make_points(array([1, 0, -1]), True)
        expected = array([
            [0, 1],
            [1, 0],
            [2, -1]
        ])

        self.assertTrue(array_equal(actual, expected))

    def test_make_points_exclude_nan(self):
        actual = make_points(array([1, 0, -1, nan]), True)
        expected = array([
            [0, 1],
            [1, 0],
            [2, -1]
        ])

        self.assertTrue(array_equal(actual, expected))

    def test_samples_1d(self):
        x1, y1 = (array([2, 3, 4, 5, 6]),
                  array([1, 2, 3, 4, 5]))
        x2, y2 = _samples(
            array([1, 2, 3, 4, 5, 6, 7]),
            array([1, 1, 0, 1, 2, 3, 4, 5, nan]),
            offset=2,
            keep_zeros=False
        )

        self.assertTrue(array_equal(x1, x2))
        self.assertTrue(array_equal(y1, y2))

    def test_samples_2d(self):
        x1, y1 = (array([2, 3, 4, 5, 6, 9, 3, 5]),
                  array([1, 2, 3, 4, 5, 2, 9, 7]))
        x2, y2 = _samples(
            array([[1, 2, 3, 4, 5, 6, 7],
                   [8, 9, 1, 2, 3, 4, 5]]),  # 9 3 5
            array([[1, 1, 0, 1, 2, 3, 4, 5, nan],
                   [0, 0, 0, 2, nan, 0, 9, 0, 7]]),  # 2 9 7
            offset=2,
            keep_zeros=False
        )

        self.assertTrue(array_equal(x1, x2))
        self.assertTrue(array_equal(y1, y2))

    def test_dea_raises(self):
        with self.assertRaises(DamageError):
            dea(array([1, 1, 1]))

    def test_dea_lower(self):
        expected = array([[0, 1], [6, 3], [10, 17]])
        dea(
            array([1, 2, 3, 6, 7, 4, 3, 18, 21, 15, 17]),
            lambda r, actual, u: self.assertTrue(array_equal(expected, actual))
        )

    def test_dea_upper(self):
        expected = array([[0, 1], [8, 21], [10, 21]])
        dea(
            array([1, 2, 3, 6, 7, 4, 3, 18, 21, 15, 17]),
            lambda r, l, actual: self.assertTrue(array_equal(expected, actual))
        )

    def test_dea_upper_decreasing(self):
        expected = array([[0, 4], [28, 3.5], [44, 1.556]])
        dea(
            array([4., 2., 2.333, 3., 3., 2.5, 1.5, 2., 2., 2., 1.667, 2., 1.25, 1.5, 3., 2., 2.5, 2., 2., 1.667, 2.,
                   2.5, 2.5, 3., 3.5, 3., 3., 3.5, 3.5, 3., 3., 0.019, 1.792, 2.261, 2.48, 2.333, 1.917, 2.25, 2.,
                   1.333, 1.5, 1.615, 1.667, 1.545, 1.556, ]),
            lambda r, l, actual: self.assertTrue(array_equal(expected, actual))
        )

    def test_spearman_cor_filters_nan_triggers(self):
        expected = [[1, 2], [2, 4], [3, 6], [6, 12], [7, 14], [8, 16]]
        actual, spearman, p_value = spearmancor(
            array([1, 2, 3, nan, nan, 6, 7, 8, nan]),
            array([2, 4, 6, 8, 10, 12, 14, 16, 18])
        )

        self.assertTrue(array_equal(expected, actual))

    def test_spearman_cor_filters_zero_triggers(self):
        expected = [[1, 2], [2, 4], [3, 6], [6, 12], [7, 14], [8, 16]]
        actual, spearman, p_value = spearmancor(
            array([1, 2, 3, 0, 0, 6, 7, 8, 0]),
            array([2, 4, 6, 8, 10, 12, 14, 16, 18])
        )

        self.assertTrue(array_equal(expected, actual))

    def test_spearman_cor_filters_nan_responses(self):
        expected = [[1, 2], [2, 4], [3, 6], [6, 12], [7, 14], [8, 16]]
        actual, spearman, p_value = spearmancor(
            array([1, 2, 3, 4, 5, 6, 7, 8, 9]),
            array([2, 4, 6, nan, nan, 12, 14, 16, nan])
        )

        self.assertTrue(array_equal(expected, actual))

    def test_spearman_cor_filters_zero_responses(self):
        expected = [[1, 2], [2, 4], [3, 6], [6, 12], [7, 14], [8, 16]]
        actual, spearman, p_value = spearmancor(
            array([1, 2, 3, 4, 5, 6, 7, 8, 9]),
            array([2, 4, 6, 0, 0, 12, 14, 16, 0])
        )

        self.assertTrue(array_equal(expected, actual))

    def test_spearman_cor_filters_keeps_zero_responses(self):
        expected = [[1, 2], [2, 4], [3, 6], [4, 0], [5, 0], [6, 12], [7, 14], [8, 16], [9, 0]]
        actual, spearman, p_value = spearmancor(
            array([1, 2, 3, 4, 5, 6, 7, 8, 9]),
            array([2, 4, 6, 0, 0, 12, 14, 16, 0]),
            keep_zeros=True
        )

        self.assertTrue(array_equal(expected, actual))

    def test_spearman_cor_offset(self):
        expected_samples = [[1, 2], [2, 4], [3, 6], [4, 8], [5, 10], [6, 12], [7, 14], [8, 16], [9, 18]]
        expected_spearman = 1.
        expected_p_value = 0.

        samples, spearman, p_value = spearmancor(
            array([1, 2, 3, 4, 5, 6, 7, 8, 9]),
            array([1, 1, 1, 1, 1, 1, 2, 4, 6, 8, 10, 12, 14, 16, 18]),
            offset=6,
            keep_zeros=True
        )

        self.assertTrue(array_equal(expected_samples, samples))
        self.assertTrue(expected_spearman == spearman)
        self.assertTrue(expected_p_value == p_value)

    def test_spearmancor(self):
        expected_samples = [[1, 2], [2, 4], [3, 6], [4, 8], [5, 10], [6, 12], [7, 14], [8, 16], [9, 18]]
        expected_spearman = 1.
        expected_p_value = 0.

        samples, spearman, p_value = spearmancor(
            array([1, 2, 3, 4, 5, 6, 7, 8, 9]),
            array([2, 4, 6, 8, 10, 12, 14, 16, 18]),
            keep_zeros=True
        )

        self.assertTrue(array_equal(expected_samples, samples))
        self.assertTrue(expected_spearman == spearman)
        self.assertTrue(expected_p_value == p_value)

    def test_pearsoncor_1d(self):
        expected_samples = [[1, 2], [2, 4], [3, 6], [4, 8], [5, 10], [6, 12], [7, 14], [8, 16], [9, 18], [10, 20]]
        expected_pearson = 1.
        expected_p_value = 0.

        samples, pearson, p_value = pearsoncor(
            array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
            array([2, 4, 6, 8, 10, 12, 14, 16, 18, 20]),
            keep_zeros=True
        )

        self.assertTrue(array_equal(expected_samples, samples))
        self.assertTrue(expected_pearson == pearson)
        self.assertTrue(expected_p_value == p_value)

    def test_pearsoncor_2d(self):
        expected_samples = [[1, 2], [2, 4], [3, 6], [4, 8], [5, 10], [6, 12], [7, 14], [8, 16], [9, 18], [10, 20],
                            [1, 3], [2, 6], [3, 9], [4, 12], [5, 15], [6, 18], [7, 21], [8, 24], [9, 27], [10, 30]]
        expected_pearson, expected_p_value = pearsonr(*list(zip(*expected_samples)))

        samples, pearson, p_value = pearsoncor(
            array([[1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                   [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]]),
            array([[2, 4, 6, 8, 10, 12, 14, 16, 18, 20],
                   [3, 6, 9, 12, 15, 18, 21, 24, 27, 30]]),
            keep_zeros=True
        )

        self.assertTrue(array_equal(expected_samples, samples))
        self.assertTrue(expected_pearson == pearson)
        self.assertTrue(expected_p_value == p_value)


class TestRing(TestCase):
    def test_is_ring(self):
        ring = Ring(make_points(array([1, 3, 3, 4, 5, 6, 7, 8, 16, 15, 19]), False))
        self.assertTrue(array_equal(ring.ring[0], ring.ring[-1]))


class TestYieldDamage(TestCase):
    def setUp(self) -> None:
        self.damage = YieldDamage(
            responses=array([1, 2, 3, 6, 7, 4, 3, 18, 21, 15, 17]),
            lower=array([[0, 1], [6, 3], [10, 17]]),
            upper=array([[0, 1], [8, 21], [10, 21]])
        )

    def test_lower_absolute_deviation(self):
        expected = [0.0, 0.7, 1.3, 4.0, 4.7, 1.3, 0.0, 11.5, 11.0, 1.5, 0.0]
        actual = list(map(lambda x: round(x, 1), self.damage.lower_absolute_deviation))

        self.assertListEqual(expected, actual)

    def test_lower_relative_deviation(self):
        expected = [0.0, 0.5, 0.8, 2.0, 2.0, 0.5, 0.0, 1.77, 1.1, 0.11, 0.0]
        actual = list(map(lambda x: round(x, 2), self.damage.lower_relative_deviation))

        self.assertListEqual(expected, actual)

    def test_upper_absolute_deviation(self):
        expected = [0.0, -1.5, -3.0, -2.5, -4.0, -9.5, -13.0, -0.5, 0.0, -6.0, -4.0]
        actual = list(map(lambda x: round(x, 1), self.damage.upper_absolute_deviation))

        self.assertListEqual(expected, actual)

    def test_upper_relative_deviation(self):
        expected = [0.0, -0.43, -0.5, -0.29, -0.36, -0.7, -0.81, -0.03, 0.0, -0.29, -0.19]
        actual = list(map(lambda x: round(x, 2), self.damage.upper_relative_deviation))

        self.assertListEqual(expected, actual)

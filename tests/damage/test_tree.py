from unittest import TestCase

from hazardly.damage.errors import DamageError
from hazardly.damage.tree import DecisionTree
from hazardly.damage.tree import _DecisionNode


class TestDecisionTree(TestCase):
    def test_gt(self):
        self.assertTrue(DecisionTree._gt(1, 2))
        self.assertFalse(DecisionTree._gt(2, 1))

    def test_lt(self):
        self.assertTrue(DecisionTree._lt(1, 0))
        self.assertFalse(DecisionTree._lt(0, 1))

    def test_ge(self):
        self.assertTrue(DecisionTree._ge(1, 1))
        self.assertTrue(DecisionTree._ge(1, 2))
        self.assertFalse(DecisionTree._ge(2, 1))

    def test_le(self):
        self.assertTrue(DecisionTree._le(1, 1))
        self.assertTrue(DecisionTree._le(1, 0))
        self.assertFalse(DecisionTree._le(1, 2))

    def test_eq(self):
        self.assertTrue(DecisionTree._eq(1, 1))
        self.assertFalse(DecisionTree._eq(2, 1))

    def test_ne(self):
        self.assertTrue(DecisionTree._ne(1, 2))
        self.assertFalse(DecisionTree._ne(1, 1))

    def test_add_node(self):
        tree = DecisionTree()
        tree.add_node('>', 1)

        self.assertTrue(tree._depth == 1)
        self.assertTrue(isinstance(tree._root, _DecisionNode))

    def test_compare_raises_error_on_values(self):
        tree = DecisionTree().add_node('>', 1)

        with self.assertRaises(DamageError):
            tree.compare([1, 1])

    def test_compare_raises_error_on_node(self):
        tree = DecisionTree()

        with self.assertRaises(DamageError):
            tree.compare([])

    def test_compare_one_node_returns_true(self):
        tree = DecisionTree().add_node('>', 1)

        result, msg = tree.compare([2])

        self.assertTrue(result)
        self.assertEqual(msg, '')

    def test_compare_one_node_returns_false(self):
        tree = DecisionTree().add_node('>', 1)

        result, msg = tree.compare([0])

        self.assertFalse(result)
        self.assertEqual(msg, '0')

    def test_compare_nodes_returns_true(self):
        tree = DecisionTree().add_node('>', 1).add_node('<', 1)

        result, msg = tree.compare([2, 0])

        self.assertTrue(result)
        self.assertEqual(msg, '')

    def test_compare_nodes_returns_false(self):
        tree = DecisionTree().add_node('>', 1).add_node('<', 1).add_node('>', 1)

        result, msg = tree.compare([2, 2, 2])

        self.assertFalse(result)
        self.assertEqual(msg, '1')

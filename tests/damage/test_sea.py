from unittest import TestCase

from numpy import array
from numpy import array_equal
from numpy.ma import append
from numpy.ma import masked_array
from numpy.random import randint

from hazardly.damage.errors import DamageError
from hazardly.damage.sea import YieldDamage
from hazardly.damage.sea import align
from hazardly.damage.sea import distance
from hazardly.damage.sea import lookahead
from hazardly.damage.sea import sea


class TestFunctions(TestCase):
    def test_align(self):
        expected = array([0, 1, 2, 3])
        actual = align(array([-3, -2, -1, 0, 1, 2, 3, 4, 5]), mn=0, mx=4)

        self.assertTrue(array_equal(expected, actual))

    def test_lookahead_start(self):
        expected = 2
        actual = lookahead(0, array([1, 2, 3, 5, 7, 8, 9]))

        self.assertEqual(expected, actual)

    def test_lookahead_center(self):
        expected = 3
        actual = lookahead(3, array([1, 2, 3, 5, 7, 8, 9]))

        self.assertEqual(expected, actual)

    def test_lookahead_end(self):
        expected = 6
        actual = lookahead(4, array([1, 2, 3, 5, 7, 8, 9]))

        self.assertEqual(expected, actual)

    def test_distance(self):
        expected = [3, 2, 5]
        actual = distance(array([1, 2, 3, 6, 7, 9, 10, 15]))

        self.assertListEqual(expected, actual)

    def test_sea_raises_if_no_disturbances_within_responses(self):
        with self.assertRaises(DamageError):
            sea(triggers=array([0, 0, 1]), responses=array([1, 2]))

    def test_sea_raises_if_offset_forces_no_disturbances_within_responses(self):
        with self.assertRaises(DamageError):
            sea(triggers=array([0, 1, 0]), responses=array([1, 2]), offset=1)

    def test_sea_even_widow(self):
        with self.assertRaises(DamageError):
            sea(triggers=array([0, 1, 0]), responses=array([1, 2, 3]), window=2)

    def test_sea_window_one(self):
        with self.assertRaises(DamageError):
            sea(triggers=array([0, 1, 0]), responses=array([1, 2, 3]), window=1)

    def test_sea_negative_window(self):
        with self.assertRaises(DamageError):
            sea(triggers=array([0, 1, 0]), responses=array([1, 2, 3]), window=-1)

    def test_sea_adaptive_even_window(self):
        with self.assertRaises(DamageError):
            sea(triggers=array([]), responses=array([]), adaptive=lambda x, y: 4)

    def test_sea_window(self):
        expected = masked_array([[2, 3, 4, 5, 6]])
        sea(
            array([0, 0, 0, 1, 0, 0, 0]),
            array([1, 2, 3, 4, 5, 6, 7]),
            window=5,
            result=lambda actual: self.assertTrue(array_equal(expected, actual))
        )

    def test_sea_composites_simple(self):
        expected = masked_array([[1, 2, 3, 4, 5, 6, 7]])
        sea(
            array([0, 0, 0, 1, 0, 0, 0]),
            array([1, 2, 3, 4, 5, 6, 7]),
            result=lambda actual: self.assertTrue(array_equal(expected, actual))
        )

    def test_sea_composites_multiple(self):
        expected = masked_array([[1, 2, 3, 4, 5, 6, 7], [5, 6, 7, 8, 9, 10, 11]])
        sea(
            array([0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0]),
            array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]),
            result=lambda actual: self.assertTrue(array_equal(expected, actual))
        )

    def test_sea_trigger_pooling(self):
        expected = masked_array([[1, 2, 3, 4.5, 6, 7, 8]])
        sea(
            array([0, 0, 0, 1, 1, 0, 0, 0]),
            array([1, 2, 3, 4, 5, 6, 7, 8]),
            result=lambda actual: self.assertTrue(array_equal(expected, actual))
        )

    def test_sea_trigger_pooling_with_na(self):
        expected = masked_array([[1, 2, 3, 4, 6, 7, 8]])
        sea(
            array([0, 0, 0, 1, 1, 0, 0, 0]),
            array([1, 2, 3, 4, 0, 6, 7, 8]),
            result=lambda actual: self.assertTrue(array_equal(expected, actual))
        )

    def test_sea_sample_bound_left_out_of_bounds(self):
        expected = masked_array([[0, 0, 1, 2, 3, 4, 5]])
        sea(
            array([0, 1, 0, 0, 0]),
            array([1, 2, 3, 4, 5]),
            result=lambda actual: self.assertTrue(array_equal(expected, actual))
        )

    def test_sea_sample_bounds_right_out_of_bounds(self):
        expected = masked_array([[1, 2, 3, 4, 5, 0, 0]])
        sea(
            array([0, 0, 0, 1, 0]),
            array([1, 2, 3, 4, 5]),
            result=lambda actual: self.assertTrue(array_equal(expected, actual))
        )

    def test_sea_sample_bounds_out_of_bounds(self):
        expected = masked_array([[0, 0, 0, 1, 0, 0, 0]])
        sea(
            array([1]),
            array([1]),
            result=lambda actual: self.assertTrue(array_equal(expected, actual))
        )

    def test_sea_sample_bounds_false(self):
        expected = masked_array([])
        sea(
            array([1]),
            array([1]),
            sample_bounds=False,
            result=lambda actual: self.assertTrue(array_equal(expected, actual))
        )

    def test_sea_offset(self):
        expected = masked_array([[0, 1, 2, 3, 4, 0, 0]])
        sea(
            array([1]),
            array([1, 2, 3, 4]),
            offset=2,
            result=lambda actual: self.assertTrue(array_equal(expected, actual))
        )


class TestYieldDamage(TestCase):
    def setUp(self) -> None:
        self.composites = randint(0, 20, size=(4, 7))
        self.masked = masked_array(self.composites, self.composites == 0)
        self.sea = YieldDamage(composites=masked_array(self.composites, self.composites == 0))

    def test_init_unmasked_array(self):
        with self.assertRaises(DamageError):
            YieldDamage(composites=self.composites)

    def test_norm(self):
        window = self.composites.shape[1] // 2

        expected = append(self.masked[:, :window], self.masked[:, window + 1:]).mean()
        actual = self.sea.norm

        self.assertEqual(expected, actual)

    def test_means(self):
        window = self.composites.shape[1] // 2
        norm = append(self.masked[:, :window], self.masked[:, window + 1:]).mean()
        norm_composites = self.masked / norm

        expected = norm_composites.mean(0)
        actual = self.sea.means

        self.assertTrue(array_equal(expected, actual))

    def test_damage(self):
        window = self.composites.shape[1] // 2
        norm = append(self.masked[:, :window], self.masked[:, window + 1:]).mean()
        norm_composites = self.masked / norm

        expected = norm_composites.mean(axis=0)[window]
        actual = self.sea.damage

        self.assertEqual(expected, actual)

    def test_window(self):
        _, expected = self.composites.shape
        actual = self.sea.window

        self.assertEqual(expected, actual)

    def test_half_window(self):
        expected = self.composites.shape[1] // 2
        actual = self.sea.half_window

        self.assertEqual(expected, actual)

    def test_norm_composites(self):
        window = self.composites.shape[1] // 2
        norm = append(self.masked[:, :window], self.masked[:, window + 1:]).mean()

        expected = self.masked / norm
        actual = self.sea.norm_composites

        self.assertTrue(array_equal(expected, actual))

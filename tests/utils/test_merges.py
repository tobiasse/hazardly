import os
from pathlib import Path
from unittest import TestCase

from cftime import DatetimeGregorian
from netCDF4 import Dataset
from netCDF4 import num2date
from numpy import append

from hazardly.utils.merges import create

filepath = Path(__file__).parent / 'test.nc'
template = Path(__file__).parent / 'res' / 'test.nc'

times = list()
for y in [1979, 1991]:
    for m in range(1, 13):
        for d in range(1, 32):
            try:
                times.append(DatetimeGregorian(year=y, month=m, day=d))
            except ValueError:
                pass


class TestMerges(TestCase):
    def setUp(self) -> None:
        with Dataset(template) as nc:
            self.times = list(append(times, num2date(
                nc.variables['time'][:], units=nc.variables['time'].units, calendar=nc.variables['time'].calendar
            )))
            self.lat_len = len(nc.variables['latitude'][:])
            self.lon_len = len(nc.variables['longitude'][:])
            self.time_len = len(self.times)

    def tearDown(self) -> None:
        if filepath.is_file():
            os.remove(filepath)

    def test_create_dimensions(self):
        create(filepath, template, self.times, 'data3')

        with Dataset(filepath) as nc:
            self.assertTrue(len(nc.dimensions) == 3)

            for k, v in nc.dimensions.items():
                if k == 'time':
                    self.assertTrue(v.size == self.time_len)
                elif k == 'latitude':
                    self.assertTrue(v.size == self.lat_len)
                elif k == 'longitude':
                    self.assertTrue(v.size == self.lon_len)
                else:
                    self.fail('Unknown dimension')

    def test_create_fill_value_set(self):
        create(filepath, template, self.times, 'data3')

        with Dataset(filepath) as nc:
            expected = 2
            actual = nc.variables['data3']._FillValue

            self.assertEqual(expected, actual)

    def test_create_fill_value_not_set(self):
        create(filepath, template, self.times, 'data4')

        with Dataset(filepath) as nc:
            expected = -127
            actual = nc.variables['data4']._FillValue

        self.assertEqual(expected, actual)

    def test_create_rename_variable(self):
        create(filepath, template, self.times, 'data3', 'data')

        with Dataset(filepath) as nc:
            self.assertTrue('data' in list(nc.variables.keys()))

    def test_create_multiple_variables(self):
        pass

    def test_create_multiple_renames(self):
        pass

from collections import namedtuple
from unittest import TestCase

from click import BadParameter

from hazardly.utils.parse import parse_decision
from hazardly.utils.parse import parse_ranges


class TestParse(TestCase):
    def test_parse_ranges_returns_parsed_list_of_strings(self):
        expected = ['01', '02', '03', '05', '07', '12']
        actual = parse_ranges()(None, None, '1-3, 5, 7, 12\n')

        self.assertListEqual(expected, actual)

    def test_parse_ranges_parses_single_item(self):
        expected = ['01', '02', '03']
        actual = parse_ranges()(None, None, '1-3\n')

        self.assertListEqual(expected, actual)

    def test_parse_ranges_raises_error_on_unknown_component(self):
        with self.assertRaises(BadParameter):
            parse_ranges()(None, None, '1-3, a, b, c')

    def test_parse_decision(self):
        Case = namedtuple('Case', ['arg', 'expected'])
        cases = [
            Case(arg='\n>\t\t- . 9 9   9', expected=('>', -0.999)),

            Case(arg='<=-.999', expected=('<=', -0.999)),
            Case(arg='>=-.999', expected=('>=', -0.999)),
            Case(arg='!=-.999', expected=('!=', -0.999)),
            Case(arg='==-.999', expected=('==', -0.999)),
            Case(arg='<-.999', expected=('<', -0.999)),
            Case(arg='>-.999', expected=('>', -0.999)),

            Case(arg='<-.999', expected=('<', -0.999)),
            Case(arg='<.999', expected=('<', 0.999)),

            Case(arg='<-999.', expected=('<', -999.0)),
            Case(arg='<999.', expected=('<', 999.0)),

            Case(arg='<-999.999', expected=('<', -999.999)),
            Case(arg='<999.999', expected=('<', 999.999)),

            Case(arg='<-999', expected=('<', -999)),
            Case(arg='<999', expected=('<', 999)),
        ]

        for c in cases:
            with self.subTest(msg=f'{c.arg}'):
                try:
                    actual = parse_decision(None, None, c.arg)
                    self.assertTupleEqual(c.expected, actual, f'expected={c.expected}, actual={actual}')
                except (SyntaxError, StopIteration, ValueError):
                    self.fail(f'{c}')

    def test_parse_decision_raises(self):
        cases = [
            '',
            '<=',
            '!.999',
            '=-.999',
            'aaaa',
            '=>',
            '>-.99a9a',
            '<aaa.999',
            '>=.',
            '<-.',
            '<9.99.',
            '<9<9',
        ]

        for c in cases:
            with self.subTest(msg=f'{c}'):
                with self.assertRaises(expected_exception=Exception):
                    parse_decision(None, None, c)

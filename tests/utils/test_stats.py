import os
import re
from pathlib import Path
from unittest import TestCase

from netCDF4 import Dataset
from numpy import array_equal
from numpy import float32
from numpy.ma import masked

from hazardly.utils.stats import DIMENSIONS
from hazardly.utils.stats import STATS
from hazardly.utils.stats import _LAT
from hazardly.utils.stats import agg
from hazardly.utils.stats import coord_cov
from hazardly.utils.stats import create
from hazardly.utils.stats import is_valid
# from hazardly.methods.stats import time_cov
from hazardly.utils.stats import var_cov

filepath = Path(__file__).parent / 'test.nc'
template = Path(__file__).parent / 'res' / 'test.nc'


class TestStats(TestCase):
    def tearDown(self) -> None:
        if filepath.is_file():
            os.remove(filepath)

    def test_create_dimensions(self):
        create(filepath, template, [], False, False, False, False)

        with Dataset(template) as expected, Dataset(filepath) as actual:
            self.assertTrue(len(expected.dimensions) == len(actual.dimensions))

            for k, v in expected.dimensions.items():
                dim = actual.dimensions[k]
                self.assertTrue(dim.size == v.size)

            # get vars from actual bc expected comprises dimension vars and data vars
            for k, v in actual.variables.items():
                var = expected.variables[k]
                self.assertTrue(v.dtype == var.dtype)
                self.assertTrue(array_equal(v[:], var[:]))

    def test_create_variables_single(self):
        var = ['data']
        create(filepath, template, var, True, True, True, True)

        with Dataset(filepath) as actual:
            self.assertTrue(len(actual.variables) == 7)

            for n in ['%s_%s' % (v, p) for v in var for p in STATS]:
                v = actual.variables[n]
                p = n.split('_')[-1]
                regex = re.compile(r'%s of (.+)?' % p)

                self.assertTrue(v.dtype == float32)
                self.assertTrue(v.dimensions == DIMENSIONS)
                self.assertTrue(regex.match(v.units))

    def test_create_variables_multi(self):
        var = ['data', 'data2', 'data3', 'data4']
        create(filepath, template, var, True, True, True, True)

        with Dataset(filepath) as actual:
            self.assertTrue(len(actual.variables) == 19)

            for n in ['%s_%s' % (v, p) for v in var for p in STATS]:
                v = actual.variables[n]
                self.assertTrue(v.dtype == float32)
                self.assertTrue(v.dimensions == DIMENSIONS)

    def test_var_cov_returns_true_if_vars_present(self):
        expected = True
        actual = var_cov([template, template, template], ['data', 'data2', 'data3', 'data4'])

        self.assertTrue(expected == actual)

    def test_var_cov_returns_false_if_vars_not_present(self):
        expected = False
        actual = var_cov([template, template, template], ['data', 'foo'])

        self.assertTrue(expected == actual)

    def test_coord_cov_returns_true_if_full_coverage(self):
        expected = True
        actual = coord_cov([template, template, template], _LAT)

        self.assertTrue(expected == actual)

    def test_coord_cov_returns_false_if_no_coverage(self):
        print('Not implemented, requires additional test netCDf with diverging coordinates')

    def test_time_cov_returns_template(self):
        print('Not implemented, requires additional test netCDf with shorter time dimension')

    def test_is_valid_return_true_template_if_coverage(self):
        expected = True, template
        actual = is_valid([template, template, template], ['data'])

        self.assertTrue(expected[0] == actual[0])
        self.assertTrue(expected[-1] == actual[-1])

    def test_is_valid_return_false_template_if_no_coverage(self):
        print('Not implemented, requires additional test netCDf with diverging properties')

    # TODO bad test, requires a second dataset with different values
    def test_agg(self):
        _ = create(filepath, template, ['data', 'data4'], True, True, True, True)
        _ = agg([template, template, template], filepath, template, ['data', 'data4'], True, True, True, True)

        data_expected = 119
        data4_expected = masked

        with Dataset(filepath) as src:
            self.assertTrue(src.variables['data_mean'][0, 0, 0] == data_expected)
            self.assertTrue(src.variables['data_median'][0, 0, 0] == data_expected)
            self.assertTrue(src.variables['data_min'][0, 0, 0] == data_expected)
            self.assertTrue(src.variables['data_max'][0, 0, 0] == data_expected)

            self.assertTrue(type(src.variables['data4_mean'][0, 0, 0]) == type(data4_expected))
            self.assertTrue(type(src.variables['data4_median'][0, 0, 0]) == type(data4_expected))
            self.assertTrue(type(src.variables['data4_min'][0, 0, 0]) == type(data4_expected))
            self.assertTrue(type(src.variables['data4_max'][0, 0, 0]) == type(data4_expected))

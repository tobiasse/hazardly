from pathlib import Path
from tempfile import TemporaryDirectory
from unittest import TestCase

import geopandas as gpd
import pandas as pd
from cftime import DatetimeGregorian
from netCDF4 import Dataset
from numpy.ma import masked_array

from hazardly.thresher.cfg import LAT
from hazardly.thresher.cfg import LON
from hazardly.threshertools.rescales import CSV
from hazardly.threshertools.rescales import GeoJSON
from hazardly.threshertools.rescales import Masking
from hazardly.threshertools.rescales import SHP
from hazardly.threshertools.rescales import _Base
from hazardly.threshertools.rescales import md
from hazardly.threshertools.rescales import me
from hazardly.threshertools.rescales import mn
from hazardly.threshertools.rescales import mx
from hazardly.threshertools.rescales import output_factory
from hazardly.threshertools.rescales import sm


class BaseTest(TestCase):
    def setUp(self) -> None:
        self.var = 'foo'
        self.gdf = gpd.read_file(Path(__file__).parent / 'res' / 'belgium.shp', encoding='utf-8')
        self.times = [DatetimeGregorian(year=2021, day=1, month=1), DatetimeGregorian(year=2021, day=2, month=1)]


class TestOutputFactory(BaseTest):
    def test_raises(self):
        with self.assertRaises(AttributeError):
            output_factory('foo', self.gdf, self.var, self.times)

    def test_shp(self):
        expected = SHP
        actual = output_factory('SHP', self.gdf, self.var, self.times)

        self.assertTrue(isinstance(actual, expected))

    def test_csv(self):
        expected = CSV
        actual = output_factory('CSV', self.gdf, self.var, self.times)

        self.assertTrue(isinstance(actual, expected))

    def test_geojson(self):
        expected = GeoJSON
        actual = output_factory('JSON', self.gdf, self.var, self.times)

        self.assertTrue(isinstance(actual, expected))


class TestOutput(BaseTest):
    def test_init_adds_cols(self):
        obj = _Base(self.gdf, self.var, self.times)
        for t in self.times:
            cname = f'{self.var}_{t.year}-{t.day:02d}-{t.month:02d}_{t.hour:02d}:{t.minute:02d}'
            self.assertTrue(cname in obj.table.columns)

    def test_write_writes_cell_values(self):
        obj = _Base(self.gdf, self.var, self.times)
        obj.write(0, [1, 2])
        t1, t2 = self.times

        self.assertEqual(
            obj.table.loc[0, f'{self.var}_{t1.year}-{t1.day:02d}-{t1.month:02d}_{t1.hour:02d}:{t1.minute:02d}'], 1)
        self.assertEqual(
            obj.table.loc[0, f'{self.var}_{t2.year}-{t2.day:02d}-{t2.month:02d}_{t2.hour:02d}:{t2.minute:02d}'], 2)


class TestSHP(BaseTest):
    def test_save(self):
        with TemporaryDirectory() as d:
            obj = SHP(self.gdf, self.var, self.times)
            obj.write(0, [1, 2])
            obj.save(str(Path(d) / 'tmp.shp'))

            new = gpd.read_file(Path(d) / 'tmp.shp')
            self.assertEqual(new.loc[0, '0'], 1)
            self.assertEqual(new.loc[0, '1'], 2)
            self.assertIn('geometry', new.columns)


class TestCSV(BaseTest):
    def test_save(self):
        with TemporaryDirectory() as d:
            obj = CSV(self.gdf, self.var, self.times)
            obj.write(0, [1, 2])
            obj.save(str(Path(d) / 'tmp.csv'))

            new = pd.read_csv(Path(d) / 'tmp.csv')
            t1, t2 = self.times
            self.assertEqual(
                new.loc[0, f'{self.var}_{t1.year}-{t1.day:02d}-{t1.month:02d}_{t1.hour:02d}:{t1.minute:02d}'], 1)
            self.assertEqual(
                new.loc[0, f'{self.var}_{t2.year}-{t2.day:02d}-{t2.month:02d}_{t2.hour:02d}:{t2.minute:02d}'], 2)
            self.assertNotIn('geometry', new.columns)


class TestGeoJSON(BaseTest):
    def test_save(self):
        with TemporaryDirectory() as d:
            obj = GeoJSON(self.gdf, self.var, self.times)
            obj.write(0, [1, 2])
            obj.save(str(Path(d) / 'tmp.json'))

            new = gpd.read_file(Path(d) / 'tmp.json')
            t1, t2 = self.times
            self.assertEqual(
                new.loc[0, f'{self.var}_{t1.year}-{t1.day:02d}-{t1.month:02d}_{t1.hour:02d}:{t1.minute:02d}'], 1)
            self.assertEqual(
                new.loc[0, f'{self.var}_{t2.year}-{t2.day:02d}-{t2.month:02d}_{t2.hour:02d}:{t2.minute:02d}'], 2)
            self.assertIn('geometry', new.columns)


class TestFunction(TestCase):
    def setUp(self) -> None:
        self.data = masked_array([[1, 2, 3], [4, 5, 6]], [[0, 0, 0], [0, 0, 1]])

    def test_mn(self):
        expected = 1
        actual = mn(self.data)

        self.assertEqual(expected, actual)

    def test_mx(self):
        expected = 5
        actual = mx(self.data)

        self.assertEqual(expected, actual)

    def test_me(self):
        expected = 3
        actual = me(self.data)

        self.assertEqual(expected, actual)

    def test_md(self):
        expected = 3
        actual = md(self.data)

        self.assertEqual(expected, actual)

    def test_sm(self):
        expected = 15
        actual = sm(self.data)

        self.assertEqual(expected, actual)


class TestMasking(TestCase):
    def setUp(self) -> None:
        self.nc = Path(__file__).parent / 'res' / 'yearly_1980_1990_m2_w11_dataq9.nc'
        self.mask = Path(__file__).parent / 'res' / 'belgium.shp'

    def test_init(self):
        expected_shape = (13, 21)  # y, x
        expected_res = (.25, -.25)  # x, y
        expected_coords = (2., 52.)  # x, y

        with Dataset(self.nc) as nc:
            masker = Masking(nc.variables[LAT], nc.variables[LON])
            actual_shape = masker._out_shape
            actual_res = (masker._transform.a, masker._transform.e)
            actual_coords = (masker._transform.c, masker._transform.f)

            self.assertEqual(expected_shape, actual_shape)
            self.assertEqual(expected_res, actual_res)
            self.assertEqual(expected_coords, actual_coords)

    def test_get_mask(self):
        gdf = gpd.read_file(self.mask)
        geometry = gdf.geometry[0]
        expected_shape = (13, 21)  # y, x

        with Dataset(self.nc) as nc:
            masker = Masking(nc.variables[LAT], nc.variables[LON])
            mask = masker.get_mask(geometry)

            self.assertEqual(expected_shape, mask.shape)

from unittest import TestCase

import numpy as np

from hazardly.threshertools.consecutive import condition
from hazardly.threshertools.pooling import pooling


# noinspection DuplicatedCode
class TestPooling(TestCase):
    def test_pooling_list(self):
        expected = np.array([
            [0, 1, 1, 1, 0, 0],
            [1, 0, 1, 0, 0, 0],
            [1, 0, 1, 0, 0, 1],
            [1, 0, 1, 0, 0, 0],
            [1, 1, 1, 0, 0, 0],
            [1, 1, 0, 0, 1, 0],
        ])
        actual = np.array([
            [0, 1, 1, 1, 0, 0],
            [1, 0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0, 1],
            [0, 0, 0, 0, 0, 0],
            [1, 1, 1, 0, 0, 0],
            [1, 1, 0, 0, 1, 0],
        ])
        expected_real = np.array([
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 3, 0, 0, 0],
            [3, 0, 0, 0, 0, 0],
        ])
        expected_full = np.array([
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 5, 0, 0, 0],
            [5, 0, 0, 0, 0, 0],
        ])
        expected_events = np.array([
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0, 0],
            [1, 0, 0, 0, 0, 0],
        ])
        real, full, events = np.zeros(actual.shape), np.zeros(actual.shape), np.zeros(actual.shape)

        pooling(actual, actual, real, full, events, condition('==', 1), condition('==', 0), dmin=3, mx=3, set_to=1)

        self.assertTrue(np.array_equal(expected, actual))
        self.assertTrue(np.array_equal(expected_real, real))
        self.assertTrue(np.array_equal(expected_full, full))
        self.assertTrue(np.array_equal(expected_events, events))

    def test_pooling_set_to(self):
        expected = np.array([
            [0, 1, 1, 1, 0, 1],
            [1, 0, 2, 0, 0, 2],
            [2, 0, 1, 0, 0, 1],
            [2, 0, 2, 0, 0, 0],
            [1, 1, 1, 0, 0, 0],
            [1, 1, 0, 0, 1, 0],
        ])
        actual = np.array([
            [0, 1, 1, 1, 0, 1],
            [1, 0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0, 1],
            [0, 0, 0, 0, 0, 0],
            [1, 1, 1, 0, 0, 0],
            [1, 1, 0, 0, 1, 0],
        ])
        expected_real = np.array([
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 3, 0, 0, 0],
            [3, 0, 0, 0, 0, 0],
        ])
        expected_full = np.array([
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 5, 0, 0, 0],
            [5, 0, 0, 0, 0, 0],
        ])
        expected_events = np.array([
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0, 0],
            [1, 0, 0, 0, 0, 0],
        ])
        real, full, events = np.zeros(actual.shape), np.zeros(actual.shape), np.zeros(actual.shape)

        pooling(actual, actual, real, full, events, condition('==', 1), condition('==', 0), dmin=3, mx=3, set_to=2)

        self.assertTrue(np.array_equal(expected, actual))
        self.assertTrue(np.array_equal(expected_real, real))
        self.assertTrue(np.array_equal(expected_full, full))
        self.assertTrue(np.array_equal(expected_events, events))

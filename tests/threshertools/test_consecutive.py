from pathlib import Path
from unittest import TestCase

from netCDF4 import Dataset
from numpy import array
from numpy import array_equal
from numpy import zeros

from hazardly.threshertools.consecutive import cc
from hazardly.threshertools.consecutive import condition
from hazardly.threshertools.consecutive import cumulative
from hazardly.threshertools.consecutive import disaggregated


# noinspection DuplicatedCode
class TestFunctions(TestCase):
    def test_cc_cumulative_with_array(self):
        mn = 3
        threshold = 1
        src = array([[1, 1, 1, 1, 1],
                     [1, 1, 1, 1, 0],
                     [1, 1, 1, 0, 1],
                     [1, 1, 0, 0, 1],
                     [1, 0, 0, 0, 1]])
        expected = array([[0, 0, 0, 0, 0],
                          [0, 0, 0, 0, 0],
                          [0, 0, 3, 0, 0],
                          [0, 4, 0, 0, 0],
                          [5, 0, 0, 0, 3]])
        actual = zeros(shape=src.shape)

        cc(src, actual, lambda x: x == threshold, lambda x: x != threshold, cumulative, mn, set_to=1)
        self.assertTrue(array_equal(expected, actual))

    def test_cc_cumulative_with_netcdf(self):
        mn = 3
        threshold = 3
        with Dataset(Path(__file__).parent / 'res/yearly_1980_1990_m2_w11_dataq9.nc') as src:
            var = src.variables['exceedance']
            expected = array([
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            ])
            actual = zeros(var.shape)

            cc(var, actual, lambda x: x > threshold, lambda x: x <= threshold, cumulative, mn, set_to=1)
            self.assertTrue(array_equal(expected, actual[:, 0]))

    def test_cc_disaggregated_with_array(self):
        mn = 3
        threshold = 1
        src = array([[1, 1, 1, 1, 1],
                     [1, 1, 1, 1, 0],
                     [1, 1, 1, 0, 1],
                     [1, 1, 0, 0, 1],
                     [1, 0, 0, 0, 1]])
        expected = array([[1, 1, 1, 0, 0],
                          [1, 1, 1, 0, 0],
                          [1, 1, 1, 0, 1],
                          [1, 1, 0, 0, 1],
                          [1, 0, 0, 0, 1]])
        actual = zeros(shape=src.shape)

        cc(src, actual, lambda x: x == threshold, lambda x: x != threshold, disaggregated, mn, set_to=1)
        self.assertTrue(array_equal(expected, actual))

    def test_cc_disaggregated_with_set_to(self):
        mn = 3
        threshold = 1
        src = array([[1, 1, 1, 1, 1],
                     [1, 1, 1, 1, 0],
                     [1, 1, 1, 0, 1],
                     [1, 1, 0, 0, 1],
                     [1, 0, 0, 0, 1]])
        expected = array([[5, 5, 5, 0, 0],
                          [5, 5, 5, 0, 0],
                          [5, 5, 5, 0, 5],
                          [5, 5, 0, 0, 5],
                          [5, 0, 0, 0, 5]])
        actual = zeros(shape=src.shape)

        cc(src, actual, lambda x: x == threshold, lambda x: x != threshold, disaggregated, mn, set_to=5)
        self.assertTrue(array_equal(expected, actual))

    def test_disaggregated(self):
        data = array([[0, 1, 2],
                      [0, 0, 0]])
        expected = array([
            [[0, 0, 1],
             [0, 0, 0]],
            [[0, 1, 1],
             [0, 0, 0]]
        ])
        actual = disaggregated(data, set_to=1)

        self.assertEqual(2, len(actual))
        self.assertTrue(array_equal(expected, actual))

    def test_cc_cumulative_end_condition(self):
        mn = 3
        start_threshold = 1
        end_threshold = 2
        src = array([
            [1, 1, 1, 2, 2],
            [1, 1, 1, 1, 2],
            [1, 1, 2, 1, 2],
            [1, 2, 2, 1, 2],
            [2, 2, 2, 2, 2],
        ])
        expected = array([
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 3, 0, 0, 0],
            [4, 0, 0, 3, 0],
            [0, 0, 0, 0, 0],
        ])
        actual = zeros(shape=src.shape)

        cc(src, actual, lambda x: x == start_threshold, lambda x: x == end_threshold, cumulative, mn, set_to=1)
        self.assertTrue(array_equal(expected, actual))

    def test_cc_disaggregated_end_condition(self):
        mn = 3
        start_threshold = 1
        end_threshold = 2
        src = array([
            [1, 1, 1, 2, 2],
            [1, 1, 1, 1, 2],
            [1, 1, 2, 1, 2],
            [1, 2, 2, 1, 2],
            [2, 2, 2, 2, 2],
        ])
        expected = array([
            [1, 1, 0, 0, 0],
            [1, 1, 0, 1, 0],
            [1, 1, 0, 1, 0],
            [1, 0, 0, 1, 0],
            [0, 0, 0, 0, 0],
        ])
        actual = zeros(shape=src.shape)

        cc(src, actual, lambda x: x == start_threshold, lambda x: x == end_threshold, disaggregated, mn, set_to=1)
        self.assertTrue(array_equal(expected, actual))

    def test_condition(self):
        mn = 3
        threshold = 1
        src = array([[1, 1, 1, 1, 1],
                     [1, 1, 1, 1, 0],
                     [1, 1, 1, 0, 1],
                     [1, 1, 0, 0, 1],
                     [1, 0, 0, 0, 1]])
        expected = array([[0, 0, 0, 0, 0],
                          [0, 0, 0, 0, 0],
                          [0, 0, 3, 0, 0],
                          [0, 4, 0, 0, 0],
                          [5, 0, 0, 0, 3]])
        actual = zeros(shape=src.shape)

        cc(src, actual, condition('==', threshold), condition('!=', threshold), cumulative, mn, set_to=1)
        self.assertTrue(array_equal(expected, actual))

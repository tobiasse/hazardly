#!/bin/bash

# This script contains all processing steps which must be executed to obtain the yield datasets used for this project.
# Please call run to execute the pipeline.
# Call suggestion:
# source yield.sh; run

# Download agricultural production and area used for production for all crop types from EUROSTAT as CSV files.
# $1 Is the path where the downloaded files are stored, e.g., data/raw/.
# $2 Is the geo unit, e.g., country, nuts1, etc.
function download {
  local path=${1:-}
  local g=${2:-country}

  local present="apro_cpnhr;2000-2020"
  local historical="apro_cpnhr_h;1975-1999"
  local datasets=("$present" "$historical")

  local area="AR"
  local production="PR"
  local structuralProperties=("$area" "$production")

  # EUROSTAT limits number of crops that can be downloaded
  local crops1="C0000,C1000,C1100,C1110,C1111,C1112,C1120,C1200,C1210,C1220,C1300,C1310,C1320,C1400,C1410,C1420,C1500,C1600,C1700"
  local crops2="C1900,C2000,C2100,C2200,P0000,P1100,P1200,P1300,P9000,R0000,R1000,R2000,R9000,I0000,I1100,I1110-1130,I1110,I1111"
  local crops3="I1112,I1120,I1130,I1140,I1150,I1190,I2000,I2100,I2200,I2300,I2900,I3000,I4000,I5000,I6000,I9000,G0000,G1000,G2000"
  local crops4="G2100,G2900,G2910,G3000,G9100,G9900,V0000_S0000,N0000,E0000,ARA99,Q0000,J0000,F0000,T0000,W1000,O1000,H9000,L0000"
  local crops=("${crops1}" "${crops2}" "${crops3}" "${crops4}")

  for c in "${crops[@]}"; do
    for s in "${structuralProperties[@]}"; do
      for dataset in "${datasets[@]}"; do
        IFS=";" read -r -a arr <<<"${dataset}"
        d=${arr[0]}
        y=${arr[1]}
        echo "Downloading... ${d}_${y}_${g}_${c}_${s}.csv to ${path}"
        download-crops -d "$d" -y "$y" -g "$g" -c "$c" -s "$s" "${path}${d}_${y}_${g}_${c}_${s}.csv"
      done
    done
  done
}

# Merge EUROSTAT crop production or area CSV files to one single CSV file.
# $1 Is the path of the EUROSTAT data files.
# $2 Is the geo unit, e.g., country, nuts1, etc.
# $3 Is the structural property, e.g., AR, PR.
# $4 Is the output path of the merged file.
function merge {
  local opts=()

  echo "Merging..."

  while read -r line; do
    echo "$line"
    opts+=(--reshape)
    opts+=("$line")
  done < <(find "$1" -name "*_$2_*_$3.csv")

  echo "to $4$2_$3.csv"
  reshape "${opts[@]}" "$4$2_$3.csv"
}

# Run NUTS 1 gap-filling.
# $1 Is the location of the NUTS 1 and 2 crop file.
# $2 Is the structural property.
function gapFilling {
  # TODO (MINOR) generalize function, report current state to user
  # France
  merge-nuts -c "geo_id" -l "FRK" -r "FR7" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "FRY" -r "FRA,FR9" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  sum-nuts -g "crops_id" -s "geo_id" -v "FR24" "$1nuts2_$2.csv" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "FRB" -r "foo" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  sum-nuts -g "crops_id" -s "geo_id" -v "FR26,FR43" "$1nuts2_$2.csv" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "FRC" -r "foo" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  sum-nuts -g "crops_id" -s "geo_id" -v "FR23,FR25" "$1nuts2_$2.csv" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "FRD" -r "foo" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  sum-nuts -g "crops_id" -s "geo_id" -v "FR22,FR30" "$1nuts2_$2.csv" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "FRE" -r "foo" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  sum-nuts -g "crops_id" -s "geo_id" -v "FR21,FR41,FR42" "$1nuts2_$2.csv" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "FRF" -r "foo" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  sum-nuts -g "crops_id" -s "geo_id" -v "FR51" "$1nuts2_$2.csv" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "FRG" -r "foo" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  sum-nuts -g "crops_id" -s "geo_id" -v "FR52" "$1nuts2_$2.csv" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "FRH" -r "foo" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  sum-nuts -g "crops_id" -s "geo_id" -v "FR53,FR61,FR63" "$1nuts2_$2.csv" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "FRI" -r "foo" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  sum-nuts -g "crops_id" -s "geo_id" -v "FR62,FR81" "$1nuts2_$2.csv" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "FRJ" -r "foo" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  sum-nuts -g "crops_id" -s "geo_id" -v "FR82" "$1nuts2_$2.csv" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "FRL" -r "foo" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  sum-nuts -g "crops_id" -s "geo_id" -v "FR83" "$1nuts2_$2.csv" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "FRM" -r "foo" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  # Greece
  merge-nuts -c "geo_id" -l "EL5" -r "EL1" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "EL6" -r "EL2" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  # Italy
  merge-nuts -c "geo_id" -l "ITH" -r "ITD" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "ITI" -r "ITE" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  # Poland
  sum-nuts -g "crops_id" -s "geo_id" -v "PL11,PL33" "$1nuts2_$2.csv" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "PL7" -r "foo" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  sum-nuts -g "crops_id" -s "geo_id" -v "PL12" "$1nuts2_$2.csv" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
  merge-nuts -c "geo_id" -l "PL9" -r "foo" -s "crops_id,geo_id" "$1nuts1_$2.csv" "$1nuts1_$2.csv"
}

# Run all required processing steps to reproduce the yield datasets used in this project.
function run {
  local raw="data/raw/"
  local proc="data/proc/"
  local interim="data/interim/"

  local masks=("${interim}nuts0.shp" "${interim}nuts1.shp" "${interim}nuts2.shp")
  local geoUnits=("country" "nuts1" "nuts2")

  local area="AR"
  local yield="YI"
  local production="PR"
  local structuralProperties=("$area" "$production")

  local years="1975-2020"

  for g in "${geoUnits[@]}"; do
    download $raw "$g"
  done
  for g in "${geoUnits[@]}"; do
    for s in "${structuralProperties[@]}"; do
      merge $raw "$g" "$s" $interim
    done
  done
  for s in "${structuralProperties[@]}"; do
    gapFilling $interim "$s"
  done
  for i in {0..2}; do
    g="${geoUnits[$i]}"
    m="${masks[$i]}"

    echo "Calculating yield... from $interim${g}_$production.csv $interim${g}_$area.csv to $interim${g}_$yield.csv"
    yield "$interim${g}_$production.csv" "$interim${g}_$area.csv" "$interim${g}_$yield.csv"

    echo "Filtering... $interim${g}_$yield.csv to $proc${g}_$yield.csv"
    filter -y "$years" "$interim${g}_$yield.csv" "$m" "$proc${g}_$yield.csv"

    echo "Plotting... $proc${g}_${yield}_coverage.svg"
    coverage "$proc${g}_$yield.csv" "$m" "$proc${g}_${yield}_coverage.svg" "$proc${g}_${yield}_coverage.csv"
  done
}

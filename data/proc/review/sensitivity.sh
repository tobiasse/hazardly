#!/usr/bin/env bash

#SBATCH --account=nsp
#SBATCH --job-name=hw-sensi

#SBATCH --error=err_hw-sensi_%j.log
#SBATCH --output=out_hw-sensi_%j.log

#SBATCH --qos=short

#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1

i=0

for coverage in {.2,.3,.4,.5,.6,.7,.8}; do
  for correlation in {.2,.3,.4,.5,.6,.7,.8}; do
    for r in {.2,.3,.4,.5,.6,.7,.8}; do
      decisions \
      --coverage ">=$coverage" \
      --correlation "<=-$correlation" \
      --r2 ">=$r" \
      ../coverages.csv \
      ../hw_correlations.csv \
      ../hw_functions.csv \
      >hw_decisions.csv
      python sensitivity.py hw_sensitivity.csv hw_decisions.csv
      rm hw_decisions.csv
      ((i += 1))
    done
  done
done

echo "Tests=$i"

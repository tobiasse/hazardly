from sys import argv

from pandas import DataFrame
from pandas import Series
from pandas import concat
from pandas import read_csv

survival: DataFrame = read_csv(argv[1])
decisions: DataFrame = read_csv(argv[2])

for _, row in decisions.loc[decisions.decision].iterrows():
    frame: DataFrame = survival.loc[(survival["NUTS_ID"] == row["NUTS_ID"]) & (survival["CROP_ID"] == row["CROP_ID"])]

    if len(frame) == 1:
        survival.loc[frame.index[0], "survival"] += 1

    else:
        survival = concat(
            (
                survival,
                DataFrame([Series({"NUTS_ID": row["NUTS_ID"], "CROP_ID": row["CROP_ID"], "survival": 1})])
            ),
            ignore_index=True,
            verify_integrity=True,
        )

survival.sort_values(by=["NUTS_ID", "CROP_ID"], inplace=True, ignore_index=True)
survival.to_csv(argv[1], index=False)

include reanalysis.mk
.PHONY: install clean test
.PHONY: download-era5-fwi download-era5-temperature download-era5-precipitation download-era5-soil-water

install:
	python setup.py sdist
	pip install -e .

clean:
	python setup.py clean --all
	pip uninstall -y hazardly
	rm -r dist/
	rm -r hazardly.egg-info

test:
	python -m unittest

########################################################################################################################
################################################### GLOBAL VARIABLES ###################################################
########################################################################################################################
# Data directories
raw := ${PWD}/data/raw/
proc := ${PWD}/data/proc/
interim := ${PWD}/data/interim/

# Auxiliary layers for processing
eu := ${interim}eu.shp
nuts0 := ${interim}eu_countries.shp
nuts1 := ${interim}nuts1.shp
nuts2 := ${interim}eu_regions.shp

nuts := ${nuts1}
spatialScale := nuts1

grid := ${PWD}/grid
remap := remapbil

# Global download parameter
exp1 := historical
exp2 := rcp_2_6
exp3 := rcp_8_5

temporal1 := daily_mean
temporal2 := 6_hours

gcm1 := ichec_ec_earth
gcm2 := mpi_m_mpi_esm_lr
gcm3 := ncc_noresm1_m
gcm4 := mohc_hadgem2_es
gcm5 := cnrm_cerfacs_cm5

rcm1 := smhi_rca4
rcm2 := ictp_regcm4_6
rcm3 := cnrm_aladin63

ensemble1 := r1i1p1
ensemble2 := r12i1p1

hours := 0-23
days := 1-31
months := 1-12
years := 1981-1990 1991-2000 2001-2010 2011-2020 2021

singleYears1 := 1981 1982 1983 1984 1985 1986 1987 1988 1989 1990 1991 1992 1993 1994 1995 1996 1997 1998 1999 2000
singleYears2 := 2001 2002 2003 2004 2005 2006 2007 2008 2009 2010 2011 2012 2013 2014 2015 2016 2017 2018 2019 2020 2021
singleYears := ${singleYears1} ${singleYears2}

########################################################################################################################
######################################################### FWI ##########################################################
########################################################################################################################
firePrefix := fwi
fwiVar := fire_weather_index

download-era5-fwi:
	@for y in ${singleYears}; do											\
		download-era5 -v ${fwiVar} -d ${days} -m ${months} -y $${y} ${raw}${firePrefix}_$${y}.zip;		\
	done

########################################################################################################################
###################################################### TEMPERATURE #####################################################
########################################################################################################################
temperaturePrefix := tmp

# Download parameter
startHist := 1970,1971,1976,1981,1986,1991,1996,2001
endHist := 1970,1975,1980,1985,1990,1995,2000,2005
startRCP := 2006,2011,2016,2021,2026,2031,2036,2041,2046,2051,2056,2061,2066,2071,2076,2081,2086,2091,2096
endRCP := 2010,2015,2020,2025,2030,2035,2040,2045,2050,2055,2060,2065,2070,2075,2080,2085,2090,2095,2100

regex := EUR-11_[0-9A-Za-z-]+_(?:${exp1}|${subst _,,${exp2}}|${subst _,,${exp3}})_r\d+i\d+p\d+_SMHI-RCA4_v\d+(?:[a-zA-Z]+)?_day_\d{8}-\d{8}\.nc
tasminRegex := tasmin_${regex}
tasmaxRegex := tasmax_${regex}

temperatureVar = 2m_temperature
temperatureVars := maximum_2m_temperature_in_the_last_24_hours,minimum_2m_temperature_in_the_last_24_hours

download-era5-temperature:
	@for y in ${years}; do												\
		download-era5 -v ${temperatureVar} -h ${hours} -d ${days} -m ${months} -y $${y}				\
		${raw}${temperaturePrefix}_$${y}.nc;									\
	done

########################################################################################################################
##################################################### PRECIPITATION ####################################################
########################################################################################################################
floodPrefix := prc
floodDownloadVar := mean_precipitation_flux
precipitationVar := total_precipitation

download-era5-precipitation:
	@for y in ${years}; do												\
		download-era5 -v ${precipitationVar} -h ${hours} -d ${days} -m ${months} -y $${y}			\
		${raw}${floodPrefix}_$${y}.nc;										\
	done

########################################################################################################################
##################################################### SOIL WATER #######################################################
########################################################################################################################
soilWaterPrefix := sw
soilWaterVars := volumetric_soil_water_layer_1 volumetric_soil_water_layer_2 volumetric_soil_water_layer_3 volumetric_soil_water_layer_4

download-era5-soil-water:
	@for y in ${years}; do												\
		for v in ${soilWaterVars}; do										\
			download-era5 -v $${v} -h ${hours} -d ${days} -m ${months} -y $${y}				\
			${raw}${soilWaterPrefix}_$${v}_$${y}.nc;							\
		done													\
	done

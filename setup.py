import re

from setuptools import find_packages
from setuptools import setup

with open('hazardly/__init__.py') as src:
    for line in src:
        if line.startswith('__version__'):
            version = re.sub(r'[ \n]', '', line)
            version = version.split('=')[1]
            version = version.strip("'")
            break

with open('README.md') as src:
    long_description = src.read()

with open('requirements.txt') as src:
    requirements = src.read()

# noinspection SpellCheckingInspection
setup(
    name='hazardly',
    version=version,
    description='Hazardly past and future climate hazard hotspots in Europe.',
    long_description=long_description,
    author='Tobias Seydewitz',
    author_email='seydewitz@pik-potsdam.de',
    url='',
    packages=find_packages(),
    install_requires=requirements,
    license='MIT',
    entry_points={
        'console_scripts': [
            # cds
            'download-fwi = hazardly.cds.cli_download_fwi:download_fwi',
            'download-era5 = hazardly.cds.cli_download_era5:download_era5',
            'download-cordex = hazardly.cds.cli_download_cordex:download_cordex',
            # damage
            'means = hazardly.damage.cli_means:means',
            'damages = hazardly.damage.cli_damages:damages',
            'decisions = hazardly.damage.cli_decisions:decisions',
            'functions = hazardly.damage.cli_functions:functions',
            'correlations = hazardly.damage.cli_correlations:correlations',
            'corr-plot = hazardly.damage.cli_plot_correlations:plot_correlations',
            'panel-one = hazardly.damage.cli_plot_panels:panel_one',
            'panel-two = hazardly.damage.cli_plot_panels:panel_two',
            'panel-three = hazardly.damage.cli_plot_panels:panel_three',
            # eurostat
            'download-crops = hazardly.eurostat.cli_download_crops:download_crops',
            'sum-nuts = hazardly.eurostat.cli_sum_nuts:sum_nuts',
            'merge-nuts = hazardly.eurostat.cli_merge_nuts:merge_nuts',
            'filter = hazardly.eurostat.cli_filter_crops:filter_crops',
            'reshape = hazardly.eurostat.cli_reshape_crops:reshape_crops',
            'yield = hazardly.eurostat.cli_crop_yield:crop_yield',
            'coverage = hazardly.eurostat.cli_plot_coverage:plot_coverage',
            # extremes
            'merge-extremes = hazardly.extremes.cli_merge:merge',
            # thresher
            'spa = hazardly.thresher.cli_spa:spa',
            'apply = hazardly.thresher.cli_apply:apply',
            'daily = hazardly.thresher.cli_daily:daily',
            'yearly = hazardly.thresher.cli_yearly:yearly',
            'monthly = hazardly.thresher.cli_monthly:monthly',
            'thresher = hazardly.thresher.cli_thresher:thresher',
            # threshertools
            'pool = hazardly.threshertools.cli_pool:pool',
            'setto = hazardly.threshertools.cli_setto:setto',
            'copyto = hazardly.threshertools.cli_copyto:copyto',
            'rescale = hazardly.threshertools.cli_rescale:rescale',
            'consecutive = hazardly.threshertools.cli_consecutive:consecutive',
            # utils
            'doc = hazardly.utils.cli_doc:doc',
            'merge = hazardly.utils.cli_merge:merge',
            'stats = hazardly.utils.cli_stats:stats',
        ]
    }
)

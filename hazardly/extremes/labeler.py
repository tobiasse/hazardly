from typing import Any
from typing import Callable
from typing import Dict
from typing import List

from cartopy.crs import CRS
from matplotlib.artist import Artist
from matplotlib.axes import Axes
from pandas import DataFrame

from hazardly.extremes.geofeature import GeoFeature


class Labeler:
    """A simple class that wraps a styler function that is called through the animation runner on each new frame."""

    def __init__(
            self,
            ax: Axes,
            crs: CRS,
            responses: DataFrame,
            geofeatures: Dict[Any, GeoFeature],
            styler: Callable[[Axes, CRS, str, DataFrame, Dict[Any, GeoFeature]], List[Artist]],
    ) -> None:
        self.ax = ax
        self.crs = crs
        self.responses = responses
        self.geofeatures = geofeatures

        if not styler:
            self.styler = self.__class__._null_styler
        else:
            self.styler = styler

        self._artists = list()

    def update(self, col: str) -> None:
        """Calls the provided styler function.

        Before calling the styler function all artists added by the styler in the last are removed.

        Args:
            col: The current active animation data column.
        """
        for a in self._artists:
            a.remove()

        self._artists = self.styler(self.ax, self.crs, col, self.responses, self.geofeatures)

    @staticmethod
    def _null_styler(*args, **kwargs) -> List[Artist]:
        return list()

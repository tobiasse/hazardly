from functools import cached_property
from typing import Callable
from typing import Tuple
from typing import Union

# noinspection PyPackageRequirements
from cartopy.mpl.feature_artist import FeatureArtist
from matplotlib.colors import Colormap


class LinearDecrement:
    """Create a linear decrement function.

    Slope of the function is always negative.

    Attributes:
        x: End x value of the linear decrement function. Y is always zero at this point (intercept with x-axis).
        y: Start y value of the linear decrement function. Y is always the intercept with y-axis of the function.
        clip: Sets value below zero to zero or values above x to x if set to true.
    """

    def __init__(self, x: float, y: float, clip: bool = True) -> None:
        self.clip = clip
        self._x, self._y = x, y

    @cached_property
    def intercept(self) -> float:
        """Get the intercept of the linear decrement function."""
        return self._y

    @cached_property
    def slope(self) -> float:
        """Get the slope of the lineare decrement function."""
        return self._y / (-1 * self._x)

    def __call__(self, value: float) -> float:
        """Get f(value) = value * slope + intercept"""
        if self.clip and value < 0:
            value = 0

        if self.clip and value > self._x:
            value = self._x

        return value * self.slope + self.intercept


class Fadeout:
    """Class may be used for fading out the face color frame by frame of a feature during an animation.

    Attributes:
        idx: The name of the managed feature. This name is reported through notify when end of fade out is reached.
        func: An instance of linear configured for color fadeout per frame.
        feature: The managed feature
        notify: A function that is called with the feature name when fade out end is reached. Function signature should
            be def(idx: Union[int, str]) -> None:
        color: This colormap is used to determine the colors of the fade out.
        end_color: This color will be used at the end of the fade out.
    """
    face = 'facecolor'

    def __init__(
            self,
            idx: Union[int, str],
            func: LinearDecrement,
            feature: FeatureArtist,
            notify: Callable[[Union[int, str]], None],
            color: Colormap,
            end_color: Union[str, Tuple[float, float, float, float]],
    ) -> None:
        self.idx = idx
        self.func = func
        self.feature = feature
        self.notify = notify
        self.color = color
        self.end_color = end_color

    def update(self, frame: int) -> None:
        """Updates the face color of the managed feature.

        Args:
            frame: Current frame of the fadeout effect.
        """
        value = self.func(frame)

        if value <= 0.:
            col = self.end_color
            self.notify(self.idx)
        else:
            col = self.color(value)

        # noinspection PyProtectedMember
        self.feature._kwargs[Fadeout.face] = col


class FadeoutCollection:
    """Class manages a collection of fade out effects.

    Attributes:
        frames: The length of the fadeout effect in frames.
        color: The colormap that is used for the fade out effect.
        end_color: This color will be used at the end of the fade out.
    """

    def __init__(
            self,
            frames: int,
            color: Colormap,
            end_color: Union[str, Tuple[float, float, float, float]]
    ) -> None:
        self.frames = frames
        self.color = color
        self.end_color = end_color

        self._fadeouts = dict()

    def _notify(self, idx: Union[int, str]) -> None:
        """Removes a finished fade out from the collection."""
        self._fadeouts.pop(idx)

    def is_keyframe(self, frame: int) -> bool:
        """Get if frame is a keyframe where a new fadeout effect starts."""
        return frame % self.frames == 0

    def add(
            self,
            idx: Union[int, str],
            feature: FeatureArtist,
            start: float,
    ) -> None:
        """Add a fade out effect to the provided feature.

        Args:
            idx: Name/identification of the managed feature (different feature should have different names).
            feature: The feature to fade out.
            start: The feature will colored with this value (uses the color from the colormap which is assigned to this
                numeric value) during the keyframe of the animation, value should be between 0 and 1.
        """
        func = LinearDecrement(self.frames - 1, start)
        self._fadeouts[idx] = Fadeout(idx, func, feature, self._notify, self.color, self.end_color)

    def update(self, frame: int) -> None:
        """Update the color of all managed features.

        Args:
            frame: Current frame of the fadeout effect.
        """
        # Save keys in list because the length of the dictionary may change during the iteration (update may trigger
        # notify which removes an element from the dictionary)
        keys = list(self._fadeouts.keys())
        frame = frame % self.frames

        for k in keys:
            self._fadeouts[k].update(frame)

from typing import Any

from cartopy.mpl.feature_artist import FeatureArtist
from shapely.geometry.base import BaseGeometry


class GeoFeature:
    """A simple data class to wrap a feature and its corresponding geometry."""

    def __init__(self, idx: Any, feature: FeatureArtist, geometry: BaseGeometry) -> None:
        self.idx = idx
        self.feature = feature
        self.geometry = geometry

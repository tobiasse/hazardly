import re

import click
import geopandas as gpd


@click.command()
@click.option('-on', type=str, default='NUTS_ID', required=True,
              help='Column that should be used as data identifier for the merge')
@click.argument('inbase', type=str, required=True)
@click.argument('outbase', type=str, required=True)
@click.argument('path', type=str, required=True)
def merge(on: str, outbase: str, inbase: str, path: str) -> None:
    """Merges a in- and out-of-base weather extremes data set to one dataset.

    INBASE and OUTBASE are the in- and out-of-base weather extremes datasets either as CSV or vector files. PATH is
    the respective filepath of the merged file.
    """
    gdf1 = gpd.read_file(outbase)
    gdf2 = gpd.read_file(inbase)

    regex = re.compile(r'.*(\d{4})-\d{2}-\d{2}_\d{2}:\d{2}.*')
    gdf1.columns = [m.group(1) if (m := regex.match(col)) else col for col in gdf1.columns]
    gdf2.columns = [m.group(1) if (m := regex.match(col)) else col for col in gdf2.columns]

    gdf1.set_index(on, inplace=True)
    gdf2.set_index(on, inplace=True)

    gdf1.update(gdf2)
    gdf1.drop(labels='id', axis=1, inplace=True)

    ext = path.split('.')[-1].lower()
    if ext in ['geojson', 'json']:
        with open(path, 'w') as dst:
            dst.write(gdf1.to_json())
    elif ext == 'csv':
        gdf1.drop(labels='geometry', axis=1, inplace=True)
        gdf1.to_csv(path)
    else:
        gdf1.to_file(path)

from typing import Any
from typing import Callable
from typing import Dict
from typing import List
from typing import Tuple
from typing import Union

import pandas as pd
from cartopy.crs import CRS
from matplotlib.artist import Artist
from matplotlib.axes import Axes
# noinspection PyPackageRequirements
from matplotlib.cm import get_cmap
from matplotlib.colors import Colormap
from matplotlib.colors import Normalize

from hazardly.extremes.fadeout import FadeoutCollection
from hazardly.extremes.geofeature import GeoFeature
from hazardly.extremes.labeler import Labeler


class AnimationRunner:
    """Runner for a fadeout effect map.

    Attributes:
        ax: Animation is drawn on this axes.
        crs: An instance of cartopy.crs.CRS that represents the projection of the ax.
        on: This column is used to identify the geofeatures related data in responses.
        cols: Columns in responses that should be used as animation data.
        responses: Data that should be animated.
        geofeatures: A dictionary of GeoFeature instances. The key of the dictionaries should relate to the values found
            in the on column of the responses.
        frames: The length of the fadeout effects in frames.
        title: Set an additional title per animation. On default the current active animation data column is displayed.
            If a title is set it is the prefix of the current active animation data column.
        mode: Set a normalization mode, available options are None, cols, and col. If mode is None on each run of an
            animation all objects will have a similar starting color. If mode is set to cols a linear normalization is
            applied on all animation data columns from responses. If mode is set to col a linear normalization is
            applied on the current active animation data column.
        color: The colormap to use for the animations.
        end_color: The end color of each animation.
        styler: A function that may be used to apply additional styling of the animation like labeling geometries or
            else. The styler function is called on each new frame of the animation by providing the ax, crs, current
            active animation data column, responses, and the geofeatures as arguments.
    """

    def __init__(
            self,
            ax: Axes,
            crs: CRS,
            on: str,
            cols: List[str],
            responses: pd.DataFrame,
            geofeatures: Dict[Any, GeoFeature],
            frames: int = 10,
            title: str = None,
            mode: str = None,
            color: Colormap = get_cmap('Reds'),
            end_color: Union[str, Tuple[float, float, float, float]] = 'white',
            styler: Callable[[Axes, CRS, str, pd.DataFrame, Dict[Any, GeoFeature]], List[Artist]] = None,
    ) -> None:
        missing = (set(cols) ^ set(responses.columns.tolist())) & set(cols)
        if len(missing) > 0:
            raise ValueError(f'Columns ({missing}) in cols provided that are not present in responses')

        self.ax = ax
        self.crs = crs
        self.on = on
        self.responses = responses
        self.geofeatures = geofeatures

        self.fc = FadeoutCollection(frames, color, end_color)
        self.la = Labeler(ax, crs, responses, geofeatures, styler)
        self.idx = 0
        self.col = cols[self.idx]
        self.cols = cols

        self._title = title

        self._norm = None
        self._mode = mode
        if self._mode == 'cols':
            self._norm = Normalize(vmin=0, vmax=responses[cols].values.max())

    def _set_title(self) -> None:
        self.ax.set_title(f'{self._title} {self.col}' if self._title else f'{self.col}')

    def update(self, frame: int) -> None:
        """Updates the animation

        Args:
            frame: Current animation frame.
        """
        if self.idx >= len(self.cols):
            self.idx = 0

        if self.fc.is_keyframe(frame):
            self.col = self.cols[self.idx]
            selection = self.responses[(self.responses[self.col] != 0) & (pd.notna(self.responses[self.col]))]

            if self._mode == 'col':
                self._norm = Normalize(vmin=0, vmax=selection[self.col].max())

            for _, row in selection.iterrows():
                key, value = row[self.on], row[self.cols[self.idx]]
                start = self._norm(value) if self._norm else 1.
                self.fc.add(key, self.geofeatures[key].feature, start)

            self._set_title()
            self.idx += 1

        self.fc.update(frame)
        self.la.update(self.col)

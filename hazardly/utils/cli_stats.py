from typing import List

import click

from hazardly.utils.stats import agg
from hazardly.utils.stats import create
from hazardly.utils.stats import is_valid


@click.command()
@click.option('--ncs', multiple=True, required=True, help='Filepath to netCDF file that should be aggregated.')
@click.option('-v', '--variables', type=str, required=str,
              help='Aggregation variable or variables, e.g., var or var1,var2 ')
@click.option('-p', '--filepath', type=str, required=True, help='Name of the output NetCDF.')
@click.option('--me', is_flag=True, help='Aggregate by mean')
@click.option('--md', is_flag=True, help='Aggregate by median')
@click.option('--mn', is_flag=True, help='Aggregate by minimum')
@click.option('--mx', is_flag=True, help='Aggregate by maximum')
def stats(ncs: List[str], variables: str, filepath: str, me: bool, md: bool, mn: bool, mx: bool) -> None:
    """Aggregates a data variable or variables of multiple netCDFs by applying the mean, median, minimum, or maximum."""
    variables = variables.split(',')
    valid, template = is_valid(ncs, variables)
    if not valid:
        click.echo('NetCDF stack not valid', err=True)
        raise click.ClickException('NetCDF stack not valid')
    filepath = create(filepath, template, variables, me, md, mn, mx)
    _ = agg(ncs, filepath, template, variables, me, md, mn, mx)

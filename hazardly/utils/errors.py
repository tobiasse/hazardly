class ParseError(Exception):
    """Raise if parsing fails."""

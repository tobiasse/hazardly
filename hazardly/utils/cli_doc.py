from os import walk
from os.path import join

import click


@click.command()
@click.argument('start', type=str, required=True)
@click.argument('result', type=str, required=True)
def doc(start: str, result: str) -> None:
    """Extracts the docstrings from each *.py file under the given path (START) and its subdirectories. Writes the
    collected docstrings to an out file (RESULT).
    """
    docs = list()

    for dirpath, _, filenames in walk(start):
        for f in filenames:
            ext = f.split('.')[-1]
            if ext != 'py':
                continue

            docs.append(f + '\n')

            with open(join(dirpath, f)) as src:
                idx = 0
                lines = src.readlines()

                while idx < len(lines):
                    line = lines[idx].strip()

                    if line.startswith('"""') and line.endswith('"""'):
                        docs.append(lines[idx] + '\n')
                        idx += 1

                    elif line.startswith('"""'):
                        multiline = lines[idx]
                        while True:
                            idx += 1
                            line = lines[idx].strip()
                            if line.startswith('"""') and line.endswith('"""'):
                                multiline += lines[idx]
                                idx += 1
                                break
                            else:
                                multiline += lines[idx]
                        docs.append(multiline + '\n')

                    else:
                        idx += 1

            docs.append('\n')

    with open(result, mode='w') as dst:
        dst.write(''.join(docs))

from itertools import chain
from os import remove

import click

from hazardly.utils.merges import copy
from hazardly.utils.merges import create
from hazardly.utils.merges import remap
from hazardly.utils.merges import flip
from hazardly.utils.merges import time_per_nc
from hazardly.utils.merges import unzip
import re


@click.command()
@click.option('-a', '--archive', type=str, required=True,
              help='Filepath pointing to a ZIP archive of netCDFs that should be merged.')
@click.option('--pattern', type=str, help='If set only the files matching the regex will be extracted and merged.')
@click.option('-n', '--name', type=str, required=True,
              help='Filepath of the new netCDF that contains the merged data.')
@click.option('--variable', type=str, required=True,
              help='The target variable that should be merged from the source netCDFs')
@click.option('--rename', type=str, help='If set the target variable will be renamed to this ion the merged dataset.')
@click.option('-m', '--method', type=click.Choice(['remapbil', 'remapnn']), required=True,
              help='The remapping method CDO should apply.')
@click.option('-g', '--grid', type=str, required=True,
              help='Filepath to CDO grid definition. Required for the remapping.')
def merge(archive: str, name: str, method: str, grid: str, variable: str, rename: str, pattern: str) -> None:
    """Merges a CORDEX dataset distributed over several netCDF files into one single netCDF. Pleas note, CDO is
    required.

    Step one extract all selected files from a ZIP archive. Next, remap rotated latlon grid to regular latlon grid.
    After, create a new netCDf covering the period of all selected source netCDFs. Finally, copy data from each netCDF
    into our target netCDF.
    """
    if pattern:
        pattern = re.compile(r'%s' % pattern)
    captures = time_per_nc(unzip(archive, pattern=pattern))
    ncs, time = list(zip(*captures))
    remapped_ncs, time_flat = remap(ncs, method, grid), list(chain(*time))
    create(name, remapped_ncs[0], time_flat, variable, rename)
    copy(list(zip(remapped_ncs, time)), name, variable, rename)
    if rename:
        flip(name, rename)
    else:
        flip(name, variable)

    with click.progressbar(list(ncs) + remapped_ncs, label='Removing extracted') as bar:
        for p in bar:
            remove(p)

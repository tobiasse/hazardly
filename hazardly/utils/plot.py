from numbers import Number
from typing import Any
from typing import Callable
from typing import Dict
from typing import List
from typing import Sequence
from typing import Tuple
from typing import Union

import geopandas as gpd
from cartopy.crs import CRS
from cartopy.mpl.feature_artist import FeatureArtist
from cartopy.mpl.gridliner import Gridliner
from matplotlib.axes import Axes
from matplotlib.axes import BarContainer
from matplotlib.cm import ScalarMappable
from matplotlib.cm import get_cmap
from matplotlib.collections import QuadMesh
from matplotlib.colorbar import Colorbar
from matplotlib.colors import Colormap
from matplotlib.colors import LogNorm
from matplotlib.colors import Normalize
from matplotlib.colors import TwoSlopeNorm
from matplotlib.patches import Polygon
from matplotlib.pyplot import colorbar
from numpy import percentile
from numpy.ma import MaskedArray
from shapely.geometry.base import BaseGeometry

Bu = get_cmap('Blues')
Re = get_cmap('Reds')
YlOrRd = get_cmap('YlOrRd')
BuYlRd = get_cmap('RdYlBu').reversed()


def hist(
        data: Sequence[Number],
        bins: int,
        ax: Axes,
        fc: Tuple[float, float, float] = (.19, .5, .74, 1.),
        ec: Tuple[float, float, float] = (0., 0., 0., 1.),
        width: float = 0.1,
        axis: Tuple[str, str] = None,
        title: str = None,
        minor: bool = True
) -> Tuple[
    Union[Sequence[float], Sequence[Sequence[float]]],
    Sequence[float],
    Union[BarContainer, Sequence[Polygon], Sequence[Union[BarContainer, Polygon]]]
]:
    """Draw a histogram for exceedance data.

    Function defines some useful defaults for the plotting of a exceedance histogram.

    Args:
        data: A sequence of pixel values
        bins: The number of bins
        ax: Axes to use for drawing a histogram
        fc: Face color
        ec: Edged color
        width: Line width of the bar edges
        axis: X- and Y-axis labels
        title: The title of the histogram
        minor: Draw minor ticks

    Returns:
        Please, read the matplotlib documentation on `matplotlib.axes.Axes.hist` for details.
    """
    n, edges, patches = ax.hist(data, bins=bins, fc=fc, ec=ec, linewidth=width)
    if axis:
        ax.set_xlabel(axis[0])
        ax.set_ylabel(axis[1])
    if title:
        ax.set_title(title)
    if minor:
        ax.minorticks_on()
    return n, edges, patches


def map_(
        lats: Sequence[Number],
        lons: Sequence[Number],
        data: Sequence[Number],
        ax: Axes, norm: Normalize,
        cmap: Colormap = None,
        title: str = None,
        grid: bool = False,
        legend: bool = True,
        legend_style: Dict[str, Any] = None,
        geometries: Sequence[Tuple[Sequence[BaseGeometry], CRS, Dict[str, Any]]] = None
) -> Tuple[QuadMesh, Union[None, Gridliner], Union[None, Colorbar], Sequence[FeatureArtist]]:
    """Private function called by historical and projected.

    Args:
        lats: Latitude coordinates of the data to map
        lons: Longitude coordinates of the data to map
        data: The data that should be mapped
        ax: Axes to use for drawing the map
        norm: Color normalization
        cmap: Color map
        title: The title
        grid: If set to true a grid will be displayed
        legend: If true a legend will be displayed
        legend_style: Styling of the legend
        geometries: Additional geometry to display on the map
    """
    mesh = ax.pcolormesh(lons, lats, data, shading='nearest', norm=norm, cmap=cmap)
    gl = None
    cb = None
    artists = list()
    if title:
        ax.set_title(title)
    if grid:
        gl = ax.gridlines(draw_labels=True)
    if legend:
        cb = colorbar(mesh, ax=ax, **legend_style)
    if geometries:
        for geo, prj, kw in geometries:
            a = ax.add_geometries(geo, prj, **kw)
            artists.append(a)
    return mesh, gl, cb, artists


def historical(
        lats: Sequence[Number],
        lons: Sequence[Number],
        data: MaskedArray,
        ax: Axes,
        q: float = 50.,
        cmap: Colormap = BuYlRd,
        title: str = None,
        grid: bool = False,
        legend: bool = True,
        legend_style: Dict[str, Any] = None,
        geometries: Sequence[Tuple[Sequence[BaseGeometry], CRS, Dict[str, Any]]] = None
) -> Tuple[QuadMesh, Union[None, Gridliner], Union[None, Colorbar], Sequence[FeatureArtist]]:
    """Draw a map for historical exceedance data.

    Defines some defaults for historical exceedance maps. Applies a two slope color normalization with center set to
    the 50th percentile (``q``) of mapped data. Will draw a horizontal legend if legend is set and no legend_style is
    set.
    """
    norm = TwoSlopeNorm(vmin=data.min(), vmax=data.max(), vcenter=percentile(data[~data.mask], q=q))
    if not legend_style:
        legend_style = {'orientation': 'horizontal', 'fraction': .06, 'pad': .04, 'label': 'edays / #days'}
    return map_(lats, lons, data, ax, norm, cmap, title, grid, legend, legend_style, geometries)


def projected(
        lats: Sequence[Number],
        lons: Sequence[Number],
        data: MaskedArray,
        ax: Axes,
        cmap: Colormap = BuYlRd,
        title: str = None,
        grid: bool = False,
        legend: bool = True,
        legend_style: Dict[str, Any] = None,
        geometries: Sequence[Tuple[Sequence[BaseGeometry], CRS, Dict[str, Any]]] = None
) -> Tuple[QuadMesh, Union[None, Gridliner], Union[None, Colorbar], Sequence[FeatureArtist]]:
    """Draw a map for projected exceedance data.

    Defines some defaults for exceedance exceedance maps. Applies a two slope color normalization with center set to
    zero. Will draw a horizontal legend if legend is set and no legend_style is set.
    """
    norm = TwoSlopeNorm(vmin=data.min(), vmax=data.max(), vcenter=0)
    if not legend_style:
        legend_style = {'orientation': 'horizontal', 'fraction': .06, 'pad': .04, 'label': 'Proj(e%) - Hist(e%)'}
    return map_(lats, lons, data, ax, norm, cmap, title, grid, legend, legend_style, geometries)


# TODO add attr exceedance style, allows the user to modify the style of the exceedance map
#  (disallow change of facecolor)
# TODO after adding exceedance style, remove ec
def styler(
        gdf: gpd.GeoDataFrame,
        attr: str,
        cmap: Colormap,
        ec: Tuple[float, float, float, float] = (0., 0., 0., 1.),
        vmin: Union[int, float] = None,
        vmax: Union[int, float] = None,
        vcenter: Union[int, float] = None,
        norm: str = None
) -> Tuple[Callable[[BaseGeometry], Dict[str, Tuple[float, float, float, float]]], Normalize]:
    """Maps a colormap against a selected feature attribute by using the selected normalization.

    Args:
        gdf: A geopandas GeoDataFrame.
        attr: The attribute that should be used for the colorization of the features.
        cmap: A colormap that is mapped against the selected feature attributes/
        ec: The selected edge color will be used for all geometries.
        vmin: If set this min value is used to initialize the normalization, otherwise the min value is derived from the
            selected feature attribute.
        vmax: If set this max value is used to initialize the normalization, otherwise the max value is derived from the
            selected feature attribute.
        vcenter: If set this center value is used to initialize the normalization, otherwise the center value is derived
            from the selected feature attribute.
        norm: The normalization function possible values are: norm (linear function), twoslope (two linear functions),
            and lognorm (log10 of the selected feature attributes). Default is the simple single linear function
            normalization.

    Returns:
        A function that returns the face- and edge-color for a geometry based on the selected attribute of the features.
        The normalization used to map colors to selected feature attributes.
    """
    gdf = gdf.copy()
    mn, mx, md = gdf.min()[attr], gdf.max()[attr], gdf.median()[attr]

    if vmin:
        mn = vmin
    if vmax:
        mx = vmax
    if vcenter:
        md = vcenter

    if norm == 'norm':
        norm = Normalize(vmin=mn, vmax=mx)
    elif norm == 'twoslope':
        norm = TwoSlopeNorm(vmin=mn, vmax=mx, vcenter=md)
    elif norm == 'lognorm':
        norm = LogNorm(vmin=mn, vmax=mx)
    else:
        norm = Normalize(vmin=mn, vmax=mx)

    def wrapped(geometry: BaseGeometry) -> Dict[str, Tuple[float, float, float, float]]:
        idx = gdf.geom_equals(geometry)
        selected = gdf.loc[idx][attr].values[0]
        fc = cmap(norm(selected))

        return {'fc': fc, 'ec': ec, 'linewidth': .6}  # TODO remove hard coded ec and linewidth

    return wrapped, norm


def exceedance_map(
        gdf: gpd.GeoDataFrame,
        ax: Axes,
        crs: CRS,
        styler: Callable[[BaseGeometry], Dict[str, Tuple[float, float, float, float]]],
        cmap: Colormap,
        norm: Normalize,
        legend: bool = True,
        legend_style: Dict[str, Any] = None,
        extra: List[Tuple[List[BaseGeometry], Dict[str, Any]]] = None
) -> None:
    """Draws a weather extremes map from vector data.

    Args:
        gdf: A geopandas GeoDataFrame.
        ax: Axes to use for drawing the map.
        crs: The projection that should be used for the feature geometries.
        styler: Used for the colorization of the features.
        cmap: The color map that is used to colorize the features.
        norm: The normalization class, e.g. TwoSlopeNorm, etc., that is used to map colors against feature attributes.
        legend: If set to true a color-bar legend will be added to the ax by using cmap and norm.
        legend_style: Styling of the legend, please consult the matplotlib documentation for details.
        extra: A list of extra geometries that is added to the exceedance map.
    """
    mns, mxs = gdf.bounds.min(), gdf.bounds.max()
    xmn, xmx, ymn, ymx = mns['minx'], mxs['maxx'], mns['miny'], mxs['maxy']

    ax.set_extent([xmn, xmx, ymx, ymn], crs)
    ax.add_geometries(gdf.geometry.to_list(), crs, styler=styler)

    if extra:
        for geo, sty in extra:
            if 'fc' in sty:
                sty.pop('fc')
            if 'facecolor' in sty:
                sty.pop('facecolor')

            ax.add_geometries(geo, crs, facecolor='none', **sty)

    if legend:
        legend_style = legend_style if legend_style else dict()
        colorbar(ScalarMappable(norm=norm, cmap=cmap), **legend_style)

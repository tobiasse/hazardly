from pathlib import Path
from subprocess import run
from typing import List
from typing import Pattern
from typing import Tuple
from zipfile import ZipFile

import click
import numpy as np
from cftime import DatetimeGregorian
from netCDF4 import Dataset
from netCDF4 import date2num
from netCDF4 import num2date

from hazardly.thresher.netcdf import fill_value
from hazardly.thresher.netcdf import get_attr


def unzip(archive: str, pattern: Pattern = None) -> List[Path]:
    """Extracts all or only by a regex pattern selected files from a ZIP archive.

    Args:
        archive: Filepath pointing to the ZIP archive.
        pattern: If regex pattern is set only matching files will be extracted from the ZIP archive.

    Returns:
        A list of filepaths pointing to each extracted file.
    """
    dir_ = Path(archive).parent
    click.echo('Extracting {}'.format(archive))

    with ZipFile(archive) as zip_:
        if pattern:
            ncs = list()
            for f in zip_.namelist():
                if pattern.match(f):
                    zip_.extract(f, dir_)
                    ncs.append(dir_ / f)
        else:
            ncs = [dir_ / f for f in zip_.namelist()]
            zip_.extractall(dir_)

    return ncs


def time_per_nc(ncs: List[Path]) -> List[Tuple[Path, List[DatetimeGregorian]]]:
    """Get the time objects for each netCDF file.

    Args:
        ncs: A list of filepaths pointing to netCDF files.

    Returns:
        A list of tuples where each tuple consist of the netCDF filepath and the corresponding time dimension.
    """
    sink = list()

    for n in ncs:
        with Dataset(n) as nc:
            time = nc.variables['time']
            obj = num2date(time[:], calendar=time.calendar, units=time.units)
            sink.append((n, obj))

    return sink


def remap(
        ncs: List[Path],
        method: str,
        grid: str
) -> List[Path]:
    """Convert CORDEX rotated latlon grid to a regular latlon grid.

    Creates for each netCDF a remapped netCDF identified by the prefix ``remapped_``. Please note, CDO (climate data
    operators) must be installed.

    Args:
        ncs: A list of netCDF files that should be remapped.
        method: The remapping method that should be applied, see CDO documentation for further details.
        grid: Filepath pointing to a CDO grid definition.

    Returns:
        A list of filepaths pointing to the remapped files.
    """
    click.echo('Applying remapping')

    option = '{},{}'.format(method, grid)
    remapped_ncs = list()

    for nc in ncs:
        remapped = nc.parent / 'remapped_{}.nc'.format(nc.name)
        run(["cdo", option, nc, remapped])
        remapped_ncs.append(remapped)

    return remapped_ncs


# TODO (merging multiple variables) create a new variable for each variable with dimensions (time, lat, lon)
# TODO get variable datatype from template dataset
def create(name: str, template: Path, times: List[DatetimeGregorian], variable: str, rename: str = None) -> None:
    """Creates a new netCDF that is used as data sink during the merge process.

    Args:
        name: Filepath of the new netCDF.
        template: Filepath to a template netCDF. The latitude and longitude coordinates are copied from this file.
            Further, some variable attributes like standard names, units, etc. are copied.
        times: Ideally the netCDFs that should be merged cover multiple periods. Therefore, times is a list of time
            objects fetched from each netCDF.
        variable: The name of the data variable (the target variable of the merging process).
        rename: If set the target variable will be identified by this name in the new dataset.
    """
    click.echo('Creating netCDF {}'.format(name))

    with Dataset(template) as temp, Dataset(name, mode='w') as nc:
        times = sorted(times)
        calendar = get_attr('calendar', temp.variables['time'], default='standard')
        units = 'days since {}-{:02d}-{:02d} {:02d}:{:02d} UTC'.format(times[0].year, times[0].month, times[0].day,
                                                                       times[0].hour, times[0].minute)
        vlat, vlon, vvar = temp.variables['latitude'], temp.variables['longitude'], temp.variables[variable]
        lats, lons = vlat[:], vlon[:]

        nc.createDimension('time', len(times))
        time = nc.createVariable('time', 'f4', ('time',), fill_value=False)
        time[:] = date2num(times, units=units, calendar=calendar)
        time.units, time.calendar = units, calendar
        nc.createDimension('latitude', len(lats))
        lat = nc.createVariable('latitude', 'f4', ('latitude',), fill_value=False)
        lat[:] = lats
        lat.units = get_attr('units', vlat, default='Degrees north')
        nc.createDimension('longitude', len(lons))
        lon = nc.createVariable('longitude', 'f4', ('longitude',), fill_value=False)
        lon[:] = lons
        lon.units = get_attr('units', vlon, default='Degrees west')

        if rename:
            target = rename
        else:
            target = variable

        fill = fill_value(vvar)
        if fill:
            var = nc.createVariable(target, 'f4', ('time', 'latitude', 'longitude'), fill_value=fill)
        else:
            var = nc.createVariable(target, 'f4', ('time', 'latitude', 'longitude'))

        var.units = get_attr('units', vvar, default='')
        var.long_name = get_attr('long_name', vvar, default='')
        var.standard_name = get_attr('standard_name', vvar, default='')


def copy(srcs: List[Tuple[Path, List[DatetimeGregorian]]], target: str, variable: str, rename: str = None) -> None:
    """Merge content of multiple netCDFs into on single target netCDF.

    Args:
        srcs: The netCDFs that should be merged.
        target: The target netCDF.
        variable: The source variable to merge, will be copied from the source netCDFs.
        rename: If set the target variable will be identified by this name in the target dataset.
    """
    with Dataset(target, mode='a') as dst:
        var = dst.variables[rename] if rename else dst.variables[variable]
        time = dst.variables['time']
        times = num2date(time[:], units=time.units, calendar=time.calendar)

        with click.progressbar(srcs, label='Copying netCDFs') as bar:
            for nc, t in bar:
                src = Dataset(nc, mode='r')
                var[np.isin(times, t), :, :] = src.variables[variable][:, :, :]
                src.close()
                dst.sync()


def flip(name: str, variable: str) -> None:
    """CORDEX latitude coordinates are sorted ascending, this function reverts the sorting to descending and applies
    the same to all data variables.

    Args:
        name: Filepath to netCDF with flipped latitude coordinates
        variable: Data variable to flip.
    """
    with Dataset(name, mode='a') as nc:
        time, lats = nc.variables['time'], nc.variables['latitude']

        lats[:] = np.flip(lats[:])

        with click.progressbar(time, label='Correcting latitude coords') as bar:
            for i, t in enumerate(bar):
                data = nc.variables[variable][i, :, :]
                data = np.flipud(data)
                nc.variables[variable][i, :, :] = data

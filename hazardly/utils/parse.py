import re
from typing import Callable
from typing import Generator
from typing import List
from typing import Set
from typing import Tuple
from typing import Union

from click import BadParameter
from click import Context
from click import Parameter
from click import UsageError
from pandas import DataFrame
from pandas import read_csv
from pandas._typing import FilePathOrBuffer

DECISION_COL_NAME = 'decision'


def parse_ranges(sep: str = ',', formatting: str = '{:0>2}') -> Callable[[Context, Parameter, str], List[str]]:
    """Parses a string of numerical ranges to a list of numerical values by applying the provided
    formatting pattern. By keeping the default values the string "1-3,4,7,8" is parsed to 01,02,03,04,07,08.
    Grammar is:

    range_string = number | range {sep, number | range};
    range = number, range_sep, number;

    number = digit, {digit};
    digit = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9";

    sep = ","; (* can be altered *)
    range_sep = "-";

    Args:
        sep: The item separator.
        formatting: Formatting pattern (should use Pythons formatting mini language) to apply to the parsed values.

    Returns:
        The parsed values a list of strings.
    """

    def wrapped(ctx: Context, para: Parameter, value: str) -> List[str]:
        sink = list()
        for c in value.split(sep):
            c = c.strip()
            if re.match(r'\d+ ?- ?\d+', c):
                s, e = c.split('-')
                c = list(map(formatting.format, map(str, range(int(s), int(e) + 1))))
            elif re.match(r'\d+', c):
                c = [formatting.format(c)]
            else:
                raise BadParameter(f'Can not parse {c}', ctx=ctx, param=para)
            sink += c
        return sink

    return wrapped


def operator(scan: Generator[str, None, str], lookahead: Callable[[], str]) -> str:
    """Parses operators, e.g., '<', '>', '<=', '>=', '!=', '==' and returns them."""
    op: str = ''
    char: str = next(scan)

    if char in ['<', '>', '!', '=']:
        op += char
        char = lookahead()

        if char == '=':
            op += char
            next(scan)

        elif op in ['!', '='] and char != '=':
            raise SyntaxError('= expected')

        elif re.match(r'\d|-|\.', char):
            pass

        else:
            raise SyntaxError('Unknown char')

        return op

    raise SyntaxError('Comparison operator expected')


def number(scan: Generator[str, None, str], lookahead: Callable[[], str]) -> Union[int, float]:
    """Parses integer of floating point numbers and returns them."""
    num: str = ''
    char: str = lookahead()
    integer: bool = True

    def digits() -> None:
        nonlocal num, integer
        char: str = next(scan)

        if char == '.':
            num += char
            integer = False
            while (char := next(scan, 'EOL')) != 'EOL':
                if re.match(r'\d', char):
                    num += char
                else:
                    raise SyntaxError('Digit expected')

        elif re.match(r'\d', char):
            num += char
            while (char := next(scan, 'EOL')) != 'EOL':
                if re.match(r'\d', char):
                    num += char
                elif char == '.' and integer:
                    num += char
                    integer = False
                else:
                    raise SyntaxError('Digit or dot expected')

        else:
            raise SyntaxError('Digit or dot expected')

    if char == '-':
        num += char
        next(scan)
        digits()

    elif re.match(r'\d|\.', char):
        digits()

    else:
        raise SyntaxError('Integer or float expected')

    return int(num) if integer else float(num)


def scanner(text: str) -> Tuple[Generator[str, None, str], Callable[[], str]]:
    """String scanner ignores whitespace and emits character by character of the provided text till end of line.

    Args:
        text: Text to scan.

    Returns:
        A character generator that emits each character (except whitespace) of the string till end of line is reached,
        and a lookahead function that emits the character that follows the current generator character.
    """
    idx: int = 0
    text = re.sub(r'\s', '', text)

    def scan() -> Generator[str, None, str]:
        """Emit char by char, whitespace is ignored."""
        nonlocal idx
        for idx, char in enumerate(text):
            yield char
        return 'EOL'

    def lookahead() -> str:
        """Emit scanner char + one."""
        if idx + 1 < len(text):
            return text[idx + 1]
        raise StopIteration('EOL')

    return scan(), lookahead


def parse_decision(ctx: Context, para: Parameter, value: str) -> Tuple[str, Union[int, float]]:
    """Parses decision strings.

    Parses decision string with the following structure:

    decision = operator, number ;
    operator = lg | neq ;
    lg = "<" | ">", [ "=" ] ;
    neq = "!" | "=", "=" ;
    number = integer | float ;
    integer = [ "-" ], digit, { digit } ;
    float = [ "-" ], digit, { digit }, ".", [ digit, { digit } ] ;
    digit = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" ;

    Args:
        ctx: See click documentation.
        para: See click documentation.
        value: A decision string, e.g., '<=1.5'.

    Returns:
        A string-number tuple, first element is the operator and the second is an integer or floating point number.
    """
    scanners: Tuple[Generator[str, None, str], Callable[[], str]] = scanner(value)
    return operator(*scanners), number(*scanners)  # Order matters!


def validate_labels(
        paras: List[str] = (),
        labels: List[str] = (),
        reader=read_csv
) -> Callable[[Context, Parameter, FilePathOrBuffer], DataFrame]:
    """Checks availability of column labels

    Function intended to be used as callback (validator) of click parameters. It returns a function that expects a file
    type as argument. The file type is read/opened by the provided pandas reader function. To extract column labels
    from other click parameters (user input for options), these parameters must be set to eager (`is_eager=True`) to
    allow the function to find them in the provided click context. Allowed value types for parameters are string and
    list of strings.

    Args:
        paras: List of context parameter keys where to fetch from column labels
        labels: List of column labels
        reader: A pandas data reader function, e.g. read_csv, read_json, etc.

    Returns:
        Validator function
    """

    def wrapped(
            ctx: Context,
            param: Parameter,
            val: Union[Tuple[FilePathOrBuffer, ...], FilePathOrBuffer, None]
    ) -> Union[Tuple[DataFrame, ...], DataFrame, None]:
        if not val:  # Handle called with val=None if `required=False`
            return val

        cols: List[str] = list(labels)

        for p in paras:
            label: Union[str, List[str]] = ctx.params.get(p, None)
            if not label:
                raise UsageError(f'Unknown parameter {p}', ctx=ctx)
            if isinstance(label, str):
                cols.append(label)
            elif isinstance(label, list):
                cols += label
            else:
                raise UsageError(f'Unknown value type assigned to {p}', ctx=ctx)

        if isinstance(val, tuple):
            dfs: Tuple[DataFrame, ...] = tuple(reader(v) for v in val)
            buffs: Tuple[FilePathOrBuffer, ...] = val
        else:
            dfs: Tuple[DataFrame, ...] = (reader(val),)
            buffs: Tuple[FilePathOrBuffer, ...] = (val,)

        for buf, df in zip(buffs, dfs):
            df_cols: Set[str] = set(df.columns)
            unique_cols: Set[str] = set(cols)
            intersection: Set[str] = unique_cols & df_cols

            if len(intersection) != len(unique_cols):
                missing: Set[str] = intersection ^ unique_cols
                raise BadParameter(f'Missing {missing} in {buf.name}', ctx=ctx, param=param)

        return dfs[-1] if len(dfs) == 1 else dfs

    return wrapped

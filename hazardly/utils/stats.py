from math import inf
from typing import List
from typing import Tuple

from click import echo
from click import progressbar
from netCDF4 import Dataset
from netCDF4 import num2date
from numpy import array_equal
from numpy.ma import concatenate
from numpy.ma import median

from hazardly.thresher.netcdf import fill_value
from hazardly.thresher.netcdf import get_attr

_MEAN = 'mean'
_MEDIAN = 'median'
_MIN = 'min'
_MAX = 'max'

STATS = [_MEAN, _MEDIAN, _MIN, _MAX]

_LAT = 'latitude'
_LON = 'longitude'
_TIME = 'time'

DIMENSIONS = (_TIME, _LAT, _LON)


def var_cov(ncs: List[str], variables: List[str]) -> bool:
    """Test if a selection of variables is present in each give netCDF.

    Args:
        ncs: Filepath to netCDF files.
        variables: A list of variables that should be present in each netCDF

    Returns:
        True if each netCDF has the requested variables.
    """
    valid = True
    for v in variables:
        for nc in ncs:
            with Dataset(nc) as src:
                if v not in src.variables:
                    echo('%s not found in %s' % (v, nc))
                    valid = False
    return valid


def coord_cov(ncs: List[str], var: str) -> bool:
    """Test coordinates are covered by all netCDFs.

    Args:
        ncs: File path to netCDF files.
        var: The coordinate variable to test (e.g. latitude).

    Returns:
        True if the selected coordinate variable of the first netCDF is covered by all provided netCFDFs.
    """
    valid = True
    with Dataset(ncs[0]) as temp:
        coords = temp.variables[var][:]
    for nc in ncs:
        with Dataset(nc) as src:
            c = src.variables[var][:]
            if not array_equal(coords, c):
                echo('%s coords of %s are not covered' % (var, nc))
                valid = False
    return valid


def time_cov(ncs: List[str]) -> str:
    """Get the min time dimension from a selection of netCDFs.

    Args:
        ncs: Filepath to netCDF files.

    Returns:
        Filepath of the netCDF with min timestamps in time dimension.
    """
    min_ = inf
    template = None
    for nc in ncs:
        with Dataset(nc) as src:
            t = src.variables[_TIME]
            if len(t) < min_:
                min_ = len(t)
                template = nc
    return template


# TODO better approach is to determine per dimension the max overlap of all netCDFs, after create netCDF from
#  overlapping dimensions
def is_valid(ncs: List[str], variables: List[str]) -> Tuple[bool, str]:
    """Test if a stack of netCDFs have equal properties (variables, coordinate coverage, and min time dimension).

    Args:
        ncs: Filepath to netCDF files.
        variables: A list of variables that should be present in each netCDF

    Returns:
        True if full property coverage of each netCDF and the template netCDF (is netCDF with min time dimension)
    """
    valid = var_cov(ncs, variables)
    valid = coord_cov(ncs, _LAT)
    valid = coord_cov(ncs, _LON)
    template = time_cov(ncs)

    return valid, template


def create(filepath: str, template: str, variables: List[str], me: bool, md: bool, mn: bool, mx: bool) -> str:
    """Creates a new netCDF that can be used to merge variables from different netCDfs by applying a statistical
    aggregation.

    Args:
        filepath: File path of the new netCDF
        template: Coordinates and time dimension are fetched from this netCDF
        variables: The variables to aggregate.
        me: If true creates a mean variable.
        md: If true creates a median variable.
        mn: If true creates a min variable.
        mx: If true creates a max variable.

    Returns:
        Filepath to the new dataset
    """
    with Dataset(template) as temp, Dataset(filepath, mode='w') as new:
        # Get dimensions from template
        tv = temp.variables[_TIME]
        tt, tu, tc = tv[:], get_attr('units', tv), get_attr('calendar', tv)
        latv = temp.variables[_LAT]
        lat, latu = latv[:], get_attr('units', latv, 'Degrees north')
        lonv = temp.variables[_LON]
        lon, lonu = lonv[:], get_attr('units', lonv, 'Degrees west')

        # Create dimensions for new netCDF
        new.createDimension(_TIME, len(tt))
        tv = new.createVariable(_TIME, tv.dtype, (_TIME,), fill_value=False)
        tv[:], tv.units, tt.calendar = tt, tu, tc
        new.createDimension(_LAT, len(lat))
        latv = new.createVariable(_LAT, latv.dtype, (_LAT,), fill_value=False)
        latv[:], latv.units = lat, latu
        new.createDimension(_LON, len(lon))
        lonv = new.createVariable(_LON, lonv.dtype, (_LON,), fill_value=False)
        lonv[:], lonv.units = lon, lonu

        for var in variables:
            v = temp.variables[var]
            fill = fill_value(v)
            units = get_attr('units', v, default='')
            long_name = get_attr('long_name', v, default='')
            standard_name = get_attr('standard_name', v, '')

            for b, prop in zip([me, md, mn, mx], STATS):
                if not b:
                    continue

                name = '%s_%s' % (var, prop)
                if fill:
                    v = new.createVariable(name, 'f4', DIMENSIONS, fill_value=fill)
                else:
                    v = new.createVariable(name, 'f4', DIMENSIONS)

                v.units = '%s of %s' % (prop, units)
                v.long_name = long_name
                v.standard_name = standard_name

    return filepath


def agg(ncs: List[str], filepath: str, template: str, variables: List[str],
        me: bool, md: bool, mn: bool, mx: bool) -> str:
    """Aggregates netCDFs by a selected statistical method.

    Args:
        ncs: Filepath to the netCDF files that should be aggregated.
        filepath: Filepath to aggregation data sink.
        template: Time dimension is fetched from this netCDF.
        variables: The variables that should be aggregated.
        me: If true perform mean aggregation.
        md: If true perform median aggregation.
        mn: If true perform min aggregation.
        mx: If true perform max aggregation.

    Returns:
        Filepath to tha dataset that contains the aggregated data.
    """
    with Dataset(template) as temp:
        times = num2date(temp.variables[_TIME][:], units=temp.variables[_TIME].units,
                         calendar=temp.variables[_TIME].calendar)

    ncs_time = list()
    for nc in ncs:
        with Dataset(nc) as src:
            time = num2date(src.variables[_TIME][:], units=src.variables[_TIME].units,
                            calendar=src.variables[_TIME].calendar)
            ncs_time.append((nc, time))

    for v in variables:
        with progressbar(times, label='Aggregating %s' % v) as tbar:
            for t in tbar:
                data = list()
                for nc, time in ncs_time:
                    with Dataset(nc) as src:
                        casted = time[0].__class__(t.year, t.month, t.day, t.hour, t.minute, t.second)
                        d = src.variables[v][time == casted, :, :]
                        data.append(d)

                with Dataset(filepath, mode='a') as tar:
                    data = concatenate(data, axis=0)
                    data = data.astype('float32')
                    if me:
                        tar.variables['%s_%s' % (v, _MEAN)][times == t, :, :] = data.mean(axis=0)
                    if md:
                        tar.variables['%s_%s' % (v, _MEDIAN)][times == t, :, :] = median(data, axis=0)
                    if mn:
                        tar.variables['%s_%s' % (v, _MIN)][times == t, :, :] = data.min(axis=0)
                    if mx:
                        tar.variables['%s_%s' % (v, _MAX)][times == t, :, :] = data.max(axis=0)

    return filepath

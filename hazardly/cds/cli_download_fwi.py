from typing import List

import click

from hazardly.cds.cfg import cds_experiments
from hazardly.cds.cfg import cds_gcms
from hazardly.cds.downloads import fwi
from hazardly.utils.parse import parse_ranges

gcms = {
    'ncc_noresm1_m': 'noresm1_m',
    'mpi_m_mpi_esm_lr': 'mpi_esm_lr',
    'ichec_ec_earth': 'ec_earth',
    'mohc_hadgem2_es': 'hadgem2_es',
    'ipsl_cm5a_mr': 'ipsl_cm5a_mr',
}
experiments = {
    'historical': 'historical',
    'rcp_2_6': 'rcp2_6',
    'rcp_4_5': 'rcp4_5',
    'rcp_8_5': 'rcp8_5',
}


@click.command()
@click.option('--gcm', help='Select global climate model',
              type=click.Choice(cds_gcms))
@click.option('-e', '--experiment', type=click.Choice(cds_experiments),
              help='Select experiment hypothesis.')
@click.option('-y', '--years', type=str, callback=parse_ranges(),
              help='The years to download as a range string, e.g., 2004-2007,2012,2019.')
@click.option('-p', '--path', type=str, help='Storage location, e.g., ~/data.zip')
def download_fwi(gcm: str, experiment: str, years: List[str], path: str):
    """Downloader for fire danger indicators for Europe from 1970 to 2098 derived from climate projections
    from climate data store (CDS).
    (https://cds.climate.copernicus.eu/cdsapp#!/dataset/sis-tourism-fire-danger-indicators?tab=overview)
    """
    fwi(gcms[gcm], experiments[experiment], years, path)

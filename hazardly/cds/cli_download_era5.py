from typing import List
from typing import Tuple

import click

from hazardly.cds.cfg import era5_fire_variables
from hazardly.cds.cfg import era5_variables
from hazardly.cds.downloads import era5_fire
from hazardly.cds.downloads import era5_land
from hazardly.utils.parse import click_parse_ranges


@click.command()
@click.option('-v', '--variable', type=click.Choice(era5_variables + era5_fire_variables), required=True,
              help='The variable to download.')
@click.option('-h', '--hours', type=str, default='0-23', show_default=True,
              callback=click_parse_ranges(formatting='{:0>2}:00'),
              help="""The hours to download, you may provide ranges 0-2,6,13-15 translates to 00:00,
              01:00, 02:00, 06:00, 13:00, 14:00, 15:00""")
@click.option('-d', '--days', type=str, required=True, callback=click_parse_ranges(),
              help='The days to download, you may provide ranges (see hours)')
@click.option('-m', '--months', type=str, required=True, callback=click_parse_ranges(),
              help='The months to download, you may provide ranges')
@click.option('-y', '--years', type=str, required=True, callback=click_parse_ranges(),
              help='Obvious or not?')
@click.option('-a', '--area', type=float, nargs=4, default=(72., -12., 34., 33.), show_default=True,
              help='The area to download.')
@click.argument('path', type=str, required=True)
def download_era5(
        variable: str,
        hours: List[str],
        days: List[str],
        months: List[str],
        years: List[str],
        area: Tuple[float, float, float, float],
        path: str
) -> None:
    """Downloader for hourly ERA5-Land data from climate data store (CDS).
    (https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-land?tab=overview)

    Please note the following pre-selections for downloads: format is NetCDF.
    """
    if variable in era5_variables:
        era5_land(path, variable, hours, days, months, years, area)
    else:
        era5_fire(path, variable, days, months, years)

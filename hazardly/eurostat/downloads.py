from collections import defaultdict
from functools import reduce
from typing import List

import pandas as pd
from requests import get

from hazardly.eurostat.cfg import url
from hazardly.eurostat.errors import EurostatError


def api(
        dataset: str,
        geo: str,
        time: List[str],
        crops: List[str],
        strucpro: List[str],
        precision: int = 3
) -> dict:
    """Queries Eurostat REST-API and returns the JSON response as a Python dictionary.

    Args:
        dataset: The dataset to query.
        geo: Geo units selection to query (e.g., nuts1, nuts2, etc.).
        time: Years to query.
        crops: Crops to query (e.g., C0000, C1000, etc.).
        strucpro: Values to query (e.g., AR, Ma, etc.).
        precision: Number of decimals.

    Returns:
        The API response as a dictionary.
    """
    if len(crops) * len(strucpro) > 50:
        raise EurostatError('To many categories selected, max is 50')

    payload = {
        'geoLevel': geo,
        'filterNonGeo': 1,
        'precision': precision,
        'crops': crops,
        'strucpro': strucpro,
        'time': time
    }

    req = get(url + dataset, params=payload)
    req.raise_for_status()

    return req.json()


def resolve(rows: int, sizes: List[int], dimension: dict) -> dict:
    """Function resolves Eurostat REST-API JSON response to column-wise data.

    Args:
        rows: Total number of rows.
        sizes: A list of unique elements per column (count). The list should only include the current column to resolve
            and the following columns.
        dimension: The dimension data for the current column to resolve.

    Returns:
        The resolved column as a dictionary with two key-value pairs where each of the two entries represents a column.
    """
    label = dimension['label']
    labels = dimension['category']['label']

    # Convert index dictionary to a list of label keys increasing ordered (order given by the value of each key)
    keys, *_ = list(zip(*sorted(dimension['category']['index'].items(), key=lambda x: x[1])))

    if len(sizes) > 1 and label != 'time':
        length = sizes.pop(0)
        factor = reduce(lambda x, y: x * y, sizes)
        repeat = rows / (factor * length)

    elif len(sizes) == 1 and label == 'time':
        factor = 1
        repeat = rows / sizes.pop()
        labels = {'time': list(map(int, keys))}
        keys = ['time']

    else:
        raise EurostatError('Can not resolve dimension')

    i = 0
    data = defaultdict(list)

    while True:
        i += 1

        for k in keys:
            if isinstance(labels[k], list):
                data[f'{label}_value'] += labels[k] * factor
                data[f'{label}_id'] += [k] * len(labels[k])
            else:
                data[f'{label}_id'] += [k] * factor
                data[f'{label}_value'] += [labels[k]] * factor

        if not i < repeat:
            break

    return data


def as_table(data: dict) -> pd.DataFrame:
    """Converts a Eurostat REST-API JSON response to a pd.Dataframe.

    Args:
        data: Parsed Eurostat REST-API JSON response.

    Returns:
        A pandas dataframe
    """
    dims = data['dimension']
    vals = data['value']
    attrs = data['id']
    sizes = data['size']

    cols = dict()
    rows = reduce(lambda x, y: x * y, sizes)

    for i, attr in enumerate(attrs):
        cols.update(resolve(rows, sizes[i:], dims[attr]))

    df = pd.DataFrame.from_records(cols)
    values = list()

    for i in df.index:
        values.append(vals.get(str(i), pd.NA))

    df['values'] = values

    return df

from typing import List

import click
from numpy import all

# noinspection PyPep8Naming
from hazardly.eurostat.cfg import crops as CROPS
# noinspection PyPep8Naming
from hazardly.eurostat.cfg import dataset as DATASET
from hazardly.eurostat.cfg import geoLevel
# noinspection PyPep8Naming
from hazardly.eurostat.cfg import strucpro as STRUCPRO
from hazardly.eurostat.downloads import api
from hazardly.eurostat.downloads import as_table
from hazardly.eurostat.errors import EurostatError
from hazardly.utils.parse import parse_ranges


@click.command()
@click.option('-d', '--dataset', required=True, type=click.Choice(DATASET),
              help='The Eurostat crop dataset to download; please select one of the available choices')
@click.option('-y', '--years', required=True, type=str, callback=parse_ranges(),
              help='The years to download')
@click.option('-g', '--geo', required=True, type=click.Choice(geoLevel),
              help='The geo units to download; please select one of the available choices')
@click.option('-c', '--crops', required=True, type=str,
              help=f'The crop type to download; acceptable parameters are one or multiple of {CROPS}')
@click.option('-s', '--strucpro', required=True, type=str,
              help=f'The structural properties to download; acceptable parameters are one or multiple of {STRUCPRO}')
@click.option('-p', '--precision', type=int, default=3, show_default=True,
              help='The number of decimals')
@click.argument('path', required=True, type=str)
def download_crops(
        dataset: str,
        years: List[str],
        geo: str,
        crops: str,
        strucpro: str,
        precision: int,
        path: str
) -> None:
    """Queries the Eurostat REST-API for downloading European crop data.

    The downloader supports the download of the following three crop datasets (-d, --dataset): crop production in
    national humidity (apro_cpnhr), crop production in standard humidity (apro_cpshr), and crop production - historical
    data (apro_cpnhr_h). You may define year ranges and lists of years to download your required time frame (-y,
    --years) by applying the following syntax: 1975-1980,1990,1995,1999 (downloads all years between 1975-1980 and the
    listed years). The geo option (-g, --geo) helps select your region of interest; please use one of the available
    choices (request is sent with filter parameter set). Crops (-c, --crops) help you select the crops you want to
    download; please select one or multiple crops from the list shown in the option documentation by applying the
    following syntax: C1100,C2000 (downloads data for wheat and spelt, and rice). By selecting one or multiple of the
    structural properties listed in the option documentation (-s, --strucpro), you define which of them you want to
    download (parameter definition syntax follows the approach described for crops). PATH sets the location and filename
    where to store the downloaded file.
    """
    crops = crops.split(',')
    strucpro = strucpro.split(',')

    if not all(list(map(lambda x: x in CROPS, crops))):
        raise EurostatError('Unknown value for option crops')

    if not all(list(map(lambda x: x in STRUCPRO, strucpro))):
        raise EurostatError('Unknown value for option strucpro')

    data = api(dataset, geo, years, crops, strucpro, precision)
    df = as_table(data)
    df.to_csv(path, index=False)

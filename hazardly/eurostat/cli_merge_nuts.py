import click
import pandas as pd

from hazardly.eurostat.utils import merge as MERGE


@click.command()
@click.option('-c', '--col', required=True, type=str, help='Column to use for row selection')
@click.option('-l', '--left', required=True, type=str, help='Value to identify the row for merging')
@click.option('-r', '--right', required=True, type=str,
              help='Rows will be selected by the given value or values and merged into left')
@click.option('-s', '--sortby', default='crops_id,geo_id', type=str, show_default=True,
              help='Sort result table by the give columns')
@click.argument('merge', required=True, type=click.Path(exists=True))
@click.argument('merged', required=True, type=click.Path())
def merge_nuts(col: str, left: str, right: str, sortby: str, merge: str, merged: str) -> None:
    """Merge rows by column values.

    Uses a selected column (-c, --col) and the give parameter (-l , --left) to identify the row for merging. The
    selected row is merged with the a selection of rows identified by the right argument (-r, --right). Only NaN values
    of the left are replaced with the values of the right rows. Right row row will be merge into left row by keeping the
    given list order. You may sort (-s, --sortby) the result by the given columns.
    """
    df = pd.read_csv(merge)

    sortby = sortby.split(',')
    right = right.split(',')

    m = MERGE(df, col, left, right)
    m.sort_values(by=sortby, ignore_index=True, inplace=True)
    m.to_csv(merged, index=False)

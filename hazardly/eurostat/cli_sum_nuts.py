import click
import pandas as pd

from hazardly.eurostat.utils import sum as SUM


@click.command()
@click.option('-g', '--groupby', required=True, type=str,
              help='Group the rows for summation by the given value or values')
@click.option('-s', '--sumby', required=True, type=str, help='The column to use for row selection')
@click.option('-v', '--values', required=True, type=str, help='The column values for the row selection')
@click.option('-l', '--label', default='foo', type=str,
              help='This value is for all aggregated rows by adding it to the sumby column')
@click.option('--sort', default='crops_id,geo_id', type=str, help='Sort result table by the give columns')
@click.argument('sum', required=True, type=click.Path(exists=True))
@click.argument('append', required=False, type=click.Path(exists=True))
@click.argument('summed', required=True, type=click.Path())
def sum_nuts(groupby: str, sumby: str, values: str, label: str, sort: str, sum: str, append: str, summed: str) -> None:
    """Aggregates a set of selected rows by performing a sum over them."""
    df = pd.read_csv(sum)

    sort = sort.split(',')
    values = values.split(',')
    groupby = groupby.split(',')

    s = SUM(df, groupby, sumby, values, label)

    if append:
        append = pd.read_csv(append)
        s = s.append(append, ignore_index=True)
        s.sort_values(by=sort, ignore_index=True, inplace=True)

    s.to_csv(summed, index=False)

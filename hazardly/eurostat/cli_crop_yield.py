import click
import pandas as pd

from hazardly.eurostat.utils import cropyield


@click.command()
@click.option('--drop', type=str, default='strucpro_id,strucpro_value', show_default=True,
              help='Columns to drop')
@click.option('--rename', type=str, default='YI,Yield (production/area) (t/ha)', show_default=True,
              help='Added dropped columns to yield, set to give values')
@click.option('-d', '--decimals', type=int, default=3, show_default=True, help='Number of decimals')
@click.argument('production', type=click.Path(exists=True), required=True)
@click.argument('area', type=click.Path(exists=True), required=True)
@click.argument('yield_', type=click.Path(), required=False)
def crop_yield(drop: str, rename: str, decimals: int, production: str, area: str, yield_: str) -> None:
    """Calculates crop yield (YIELD_) by dividing production (PRODUCTION) and area (AREA).

    The result is rounded to the indicated number of decimals (-d, --decimals).
    """
    pr, ar = pd.read_csv(production), pd.read_csv(area)

    drop = drop.split(',')
    rename = rename.split(',')

    yi = cropyield(pr, ar, drop, rename, decimals)
    yi.to_csv(yield_)

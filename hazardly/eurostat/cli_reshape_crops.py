from typing import List

import click
import pandas as pd


@click.command()
@click.option('-r', '--reshape', required=True, type=click.Path(exists=True), multiple=True, help='Input files')
@click.argument('reshaped', required=True, type=click.Path())
def reshape_crops(reshape: List[str], reshaped: str) -> None:
    """Method reshapes raw tabular Eurostat crop data to a more convenient format.

    After reshaping, the unique values of the time_value are column headers, while the corresponding entries of the
    values column are copied to the corresponding time_value column.
    """
    dfs = [pd.read_csv(r) for r in reshape]
    df = dfs[0]

    for d in dfs[1:]:
        df = df.append(d, ignore_index=True)

    df = df.pivot(
        index=['strucpro_id', 'strucpro_value', 'crops_id', 'crops_value', 'geo_id', 'geo_value'],
        columns='time_value',
        values='values'
    )

    df.to_csv(reshaped)

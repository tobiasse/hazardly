class EurostatError(Exception):
    """Raise if an error occurs within this package."""

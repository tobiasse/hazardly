import click
import geopandas as gpd
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import FuncFormatter
from numpy import linspace
from numpy import nan
from numpy import nanmax
from numpy.ma import isMA
from pandas import DataFrame

from hazardly.eurostat.plot import annotate
from hazardly.eurostat.plot import coverage
from hazardly.eurostat.plot import heatmap


@click.command()
@click.option('--crops_id', default='crops_id', type=str, required=True, show_default=True,
              help='Column label of crop identifiers for CROPS table')
@click.option('--geo_id', default='geo_id', type=str, required=True, show_default=True,
              help='Column label of geo identifiers for CROPS table')
@click.option('--nuts_id', default='NUTS_ID', type=str, required=True, show_default=True,
              help='Column label of nuts identifiers for NUTS table')
@click.option('--decimals', default=3, type=int, required=True, show_default=True,
              help='Number of decimals (coverage rounding)')
@click.option('--groups', default=11, type=int, required=True, show_default=True,
              help='Number of coloring groups')
@click.option('--label', default='Relative data coverage between 1975-2020', required=True, show_default=True,
              help='Label of the legend')
@click.option('--width', default=38, required=True, show_default=True,
              help='Width of the figure')
@click.option('--height', default=38, required=True, show_default=True,
              help='Height of the figure')
@click.option('--dpi', default=200, type=int, required=True, show_default=True,
              help='DPI of the figure')
@click.argument('crops', required=True, type=click.Path(exists=True))
@click.argument('nuts', required=True, type=click.Path(exists=True))
@click.argument('figure', required=True, type=click.Path())
@click.argument('covs', required=True, type=click.Path())
def plot_coverage(
        groups: int,
        label: str,
        width: int,
        height: int,
        dpi: int,
        crops_id: str,
        geo_id: str,
        nuts_id: str,
        decimals: int,
        crops: str,
        nuts: str,
        figure: str,
        covs: str,
) -> None:
    """Plot a heatmap of crop data coverage for each crop type in CROPS table and NUTS unit in NUTS vector data."""
    crops = pd.read_csv(crops)
    nuts = gpd.read_file(nuts)

    coverages: DataFrame = pd.DataFrame(
        index=nuts[nuts_id].unique().tolist(), columns=crops[crops_id].unique().tolist()
    )
    coverages.sort_index(inplace=True)
    coverages = coverages.apply(coverage, args=(crops, decimals, geo_id, crops_id))

    median = coverages.median()
    median.name = 'Median'
    coverages = coverages.append(median)
    coverages.replace(to_replace=0, value=nan, inplace=True)

    rows = coverages.index.tolist()
    cols = coverages.columns.tolist()
    values = coverages.values.astype(float)

    coverages.reset_index(inplace=True)
    coverages.rename(columns={"index": geo_id}, inplace=True)

    RdYlGn = plt.get_cmap('RdYlGn', groups)
    labels = linspace(0, nanmax(values), groups)
    norm = BoundaryNorm(labels, ncolors=groups)

    fig, ax = plt.subplots(figsize=(width, height))

    im, cbar = heatmap(values, rows, cols, ax, cmap=RdYlGn, norm=norm, cbarlabel=label, cbar_kw={'ticks': labels})
    annotate(im, size=6, formatter=FuncFormatter(lambda x, pos: '-' if isMA(x) else "{:.2f}".format(x)))

    plt.tight_layout()
    fig.savefig(figure, dpi=dpi)
    coverages.to_csv(covs, index=False)

from typing import Any
from typing import Dict
from typing import List
from typing import Tuple
from typing import Union

import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.axes import Axes
from matplotlib.colorbar import Colorbar
from matplotlib.image import AxesImage
from matplotlib.ticker import Formatter
from matplotlib.ticker import StrMethodFormatter
from numpy import arange
from numpy import count_nonzero
from numpy import isfinite
from numpy import logical_and
from numpy import ndarray
from numpy.typing import NDArray


def heatmap(
        data: NDArray[float],
        row_labels: List[str],
        col_labels: List[str],
        ax: Axes,
        cbarlabel: str = '',
        cbar_kw: Dict[str, Any] = None,
        **kwargs
) -> Tuple[AxesImage, Colorbar]:
    """Create a heatmap from a numpy array and two lists of labels.

    Pre-edit code copied from https://matplotlib.org/stable/gallery/images_contours_and_fields/image_annotated_heatmap
    and https://stackoverflow.com/questions/55289921/matplotlib-matshow-xtick-labels-on-top-and-bottom

    Args:
        data: A two-dimensional numpy array of shape (N, M).
        row_labels: A list or array of length N with the labels for the rows.
        col_labels: A list or array of length M with the labels for the columns.
        ax: A matplotlib axes instance.
        cbar_kw: A dictionary with arguments to matplotlib colorbar.
        cbarlabel: The label for the colorbar.
        **kwargs: All other arguments are forwarded to imshow.

    Returns:
        AxesImage and ColorBar
    """
    im = ax.imshow(data, **kwargs)

    if cbar_kw:
        cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    else:
        cbar = ax.figure.colorbar(im, ax=ax)

    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    ax.set_xticks(arange(data.shape[1]))
    ax.set_yticks(arange(data.shape[0]))

    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    ax.tick_params(axis='x', top=True, labeltop=True, bottom=False, labelbottom=False)
    ax.tick_params(axis='y', left=True, labelleft=True, right=False, labelright=False)

    plt.setp(
        [tick.label1 for tick in ax.xaxis.get_major_ticks()],
        rotation=45, ha='right', va='center', rotation_mode='anchor'
    )
    plt.setp(
        [tick.label2 for tick in ax.xaxis.get_major_ticks()],
        rotation=45, ha='left', va='center', rotation_mode='anchor'
    )

    ax.set_xticks(arange(data.shape[1] + 1) - .5, minor=True)
    ax.set_yticks(arange(data.shape[0] + 1) - .5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", left=False, bottom=False)
    ax.spines[:].set_visible(False)

    return im, cbar


def annotate(
        im: AxesImage,
        data: NDArray[float] = None,
        formatter: Union[str, Formatter] = "{x:.2f}",
        color: str = 'white',
        **kwargs
):
    """A function to annotate a heatmap.

    Pre-edit code copied from https://matplotlib.org/stable/gallery/images_contours_and_fields/image_annotated_heatmap

    Args:
        im: The AxesImage to be labeled.
        data: Data used to annotate. If None, the image's data is used.
        formatter: The format of the annotations inside the heatmap. This should either use the string format method,
            e.g. "$ {x:.2f}", or be a `matplotlib.ticker.Formatter`.
        color: The annotation color.
        **kwargs: All other arguments are forwarded to each call to text used to create the text labels.
    """

    if not isinstance(data, (list, ndarray)):
        data = im.get_array()

    kw = dict(horizontalalignment="center", verticalalignment="center")
    kw.update(kwargs)

    if isinstance(formatter, str):
        formatter = StrMethodFormatter(formatter)

    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=color)
            text = im.axes.text(j, i, formatter(data[i, j], None), **kw)
            texts.append(text)

    return texts


def coverage(
        row: pd.Series,
        yields: pd.DataFrame,
        decimals: int = 3,
        nuts_id_col: str = 'geo_id',
        crops_id_col: str = 'crops_id'
) -> pd.Series:
    """Calculate the relative data coverage for a crop type and each corresponding NUTS unit.

    Function expects a row where the row name is the the crop id and the index is a set of NUTS IDs. The crop ID
    and NUTS IDs of that row is matched with corresponding columns of a yield table to calculate the data coverage.

    Args:
        row: A row where the name is a crops ID and the index is a set of NUTS IDs.
        yields: A dataframe that stores a yield time-series per crop type and NUTS unit.
        decimals: The number of decimals.
        nuts_id_col: The name of the NUTS ID column in the yield table that is used for matching against the row indices.
        crops_id_col: The name of the crops ID column in the yield table that is used for matching against the row name.

    Returns:
        A pandas series
    """
    crops_id = row.name

    for nuts_id in row.index:
        frame = yields.loc[(yields[nuts_id_col] == nuts_id) & (yields[crops_id_col] == crops_id)].select_dtypes(
            include='number')

        if len(frame) > 0:
            vals = frame.iloc[0].values
            cov = count_nonzero(logical_and(isfinite(vals), vals != 0)) / len(vals)
            row[nuts_id] = round(cov, ndigits=decimals)

    return row

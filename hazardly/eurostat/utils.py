from typing import Any
from typing import List
from typing import Union

from numpy import all
from numpy import inf
from numpy import isin
from numpy import logical_not
from numpy import nan
from numpy import where
from pandas import DataFrame

from hazardly.eurostat.errors import EurostatError


def merge(df: DataFrame, col: str, left: str, right: Union[str, List[str]]) -> DataFrame:
    """Merges a selected row with other selected rows.

    Uses a selected column and the give parameter (left) to identify the row for merging. The selected row is merged
    with the a selection of rows identified by the right argument. Only NaN values of the left are replaced with the
    values of the right rows. Right row row will be merge into left row by keeping the given list order.

    Args:
        df: A pandas dataframe.
        col: The column to use for row selection.
        left: The value to identify the row for merging.
        right: Rows will be selected by the given value or values and merged into left.

    Returns:
        The merged rows as a dataframe.
    """
    if not isinstance(right, list):
        ids = [left] + [right]
    else:
        ids = [left] + right

    frames = list()
    for i in ids:
        frame = df[df[col] == i].copy()
        df.drop(index=frame.index, inplace=True)
        frame.reset_index(drop=True, inplace=True)
        frames.append(frame)

    for f in frames[1:]:
        frames[0].update(f, overwrite=False)

    return df.append(frames[0], ignore_index=True)


def sum(
        df: DataFrame,
        groupby_cols: Union[str, List[str]],
        sumby_col: str,
        sum_values: List[Any],
        relabel: str = ''
) -> DataFrame:
    """Aggregates a set of selected rows by performing a sum over them.

    Args:
        df: A pandas dataframe.
        groupby_cols: Group the rows for summation by the given value or values.
        sumby_col: The column to use for row selection.
        sum_values: The column values for the row selection.
        relabel: This value is for all aggregated rows by adding it to the sumby_col.

    Returns:
        The aggregated rows.
    """
    sink = DataFrame(columns=df.columns)

    for _, frame in df.groupby(by=groupby_cols):
        selection = DataFrame()
        for v in sum_values:
            selection = selection.append(frame[frame[sumby_col] == v], ignore_index=True)

        row = selection.sum(axis=0, numeric_only=True)

        # Copy content of non-numeric columns to row (use the first row of the selected rows as source values)
        cols = row.index.to_list()
        selection = selection.iloc[0]
        for col in selection.index.to_list():
            if col not in cols:
                row[col] = selection[col]

        sink = sink.append(row, ignore_index=True)

    sink[sumby_col] = relabel

    return sink


def filter(df: DataFrame, value_cols: List[str], nuts_ids: List[str], nuts_col: str) -> DataFrame:
    """Filter crop yield by time-series and NUTS units.

    Drop rows where the time-series has no values for crop yield and rows with superfluous NUTS units.

    Args:
        df: The crop yield table.
        value_cols: The time-series years to consider for dropping rows.
        nuts_ids: The NUTS units you want to keep.
        nuts_col: The crop yield table column where the NUTS ids are stored.

    Returns:
        A pandas dataframe without empty crop yield time-series rows and deleted NUTS units.
    """
    # Filter rows with no time-series
    df.replace(to_replace=[-inf, 0, inf], value=nan, inplace=True)
    df.dropna(axis=0, thresh=1, subset=value_cols, inplace=True)
    df.reset_index(drop=True, inplace=True)

    # Filter by nuts ids
    idx = df[nuts_col].to_list()
    available = list(set(idx) & set(nuts_ids))
    idx_known = where(logical_not(isin(idx, available)))[0]

    return df.drop(index=idx_known)


def cropyield(
        production: DataFrame,
        area: DataFrame,
        drop: List[str] = ('strucpro_id', 'strucpro_value'),
        rename: List[str] = ('YI', 'Yield (production/area) (t/ha)'),
        decimals: int = 3
) -> DataFrame:
    """Calculate crop yield from production and area.

    Args:
        production: The crop production
        area: The area used for crop production
        drop: Columns to drop
        rename: Added dropped columns to yield, set to give values
        decimals: Number of decimals

    Returns:
        The crop yield
    """
    if not all(isin(production.columns.tolist(), area.columns.tolist())):
        raise EurostatError('Different columns in tables')

    production.drop(labels=drop, axis=1, inplace=True)
    area.drop(labels=drop, axis=1, inplace=True)

    idx = production.select_dtypes(exclude='number').columns.tolist()
    production.set_index(keys=idx, inplace=True)
    area.set_index(keys=idx, inplace=True)

    yi = production / area

    for c, v in zip(drop, rename):
        yi[c] = v

    return yi.round(decimals=decimals)

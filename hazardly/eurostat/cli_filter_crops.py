from typing import List

import click
import geopandas as gpd
import pandas as pd

from hazardly.eurostat.utils import filter
from hazardly.utils.parse import parse_ranges


@click.command()
@click.option('-y', '--years', type=str, show_default=True, default='1975-2020', callback=parse_ranges(),
              help='Years to keep')
@click.option('-c', '--col', type=str, required=True, default='geo_id', show_default=True,
              help='Name of the CROPS column that stores the NUTS ids')
@click.argument('crop', type=click.Path(exists=True), required=True)
@click.argument('nuts', type=click.Path(exists=True), required=True)
@click.argument('filtered', type=click.Path(), required=True)
def filter_crops(years: List[str], col: str, crop: str, nuts: str, filtered: str) -> None:
    """Filters a CROP dataset by nuts units available in a NUTS dataset, years of interest (-y, --years).

    Rows where the time-series does not contain any data are filtered too.
    """
    crop = pd.read_csv(crop)

    nuts = gpd.read_file(nuts)
    nuts_ids = nuts.NUTS_ID.unique().tolist()

    f = filter(crop, years, nuts_ids, col)

    f.to_csv(filtered, index=False)

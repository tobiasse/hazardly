from pathlib import Path

import click

from hazardly.thresher.aggregation import create


@click.command()
@click.argument('exceedance', required=True, type=click.Path(exists=True))
@click.argument('months', required=True, type=click.Path())
def monthly(exceedance: str, months: str) -> None:
    """Aggregates daily exceedance index (EXCEEDANCE) to monthly exceedance index (MONTHS) by summing each grid cell
    calculates the statistical mean for bootstrapped daily exceedance.
    """
    create(Path(months), Path(exceedance), steps='monthly').run()

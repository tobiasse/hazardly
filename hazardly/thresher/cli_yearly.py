from pathlib import Path

import click

from hazardly.thresher.aggregation import create


@click.command()
@click.argument('exceedance', required=True, type=click.Path(exists=True))
@click.argument('years', required=True, type=click.Path())
def yearly(exceedance: str, years: str) -> None:
    """Aggregates daily exceedance (EXCEEDANCE) to yearly exceedance index (YEARS) by summing each grid cell for each
    year available. In the second step, if the source dataset (EXCEEDANCE) is bootstrapped, the statistical mean is
    calculated for each grid cell's yearly exceedance.
    """
    create(Path(years), Path(exceedance), steps='yearly').run()

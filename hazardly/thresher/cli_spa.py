from pathlib import Path

import click

from hazardly.thresher.spa import create


@click.command()
@click.option('--dmin', type=int, default=0, required=True,
              help='Droughts with a duration below (exclusive) this threshold are not considered as drought events.')
@click.option('--cache', type=int, default=100, required=True,
              help='Limit the maximum size of the Storage memoization.')
@click.argument('netcdf', required=True, type=click.Path(exists=True))
@click.argument('threshold', required=True, type=click.Path(exists=True))
@click.argument('exceedance', required=True, type=click.Path())
def spa(dmin: int, cache: int, netcdf: str, threshold: str, exceedance: str) -> None:
    """Sequent peak analysis (SPA) computes the presence of a daily drought event for a qt-NetCDF (NETCDF) and the
    corresponding q0-thresholds (THRESHOLD).

    Implementation corresponds to the algorithm described in: Vogel, R. M., & Stedinger, J. R. (1987). Generalized
    storage-reliability-yield relationships. Journal of Hydrology, 89(3–4), 303–327.
    https://doi.org/10.1016/0022-1694(87)90184-3 and Biggs, B. J. F., Clausen, B., Demuth, S., Fendeková,
    M., Gottschalk, L., Gustard, A., Hisdal, H., Holmes, M. G. R., Jowett, I. G., Kašpárek, L., Kasprzyk, A.,
    Kupczyk, E., Van Lanen, H. A. J., Madsen, H., Marsh, T. J., Moeslund, B., Novický, O., Peters, E., Pokojski, W., …
    Young, A. R. (2004). Hydrological Drought: Processes and Estimation Methods for Streamflow and Groundwater
    (L. M. Tallaksen & H. A. J. Van Lanen (eds.); 1st ed.). Elsevier.

    Additionally, dmin is introduced to exclude droughts with duration below this threshold. Further the algorithm is
    time-step sequence-breaks aware (a gap between two time-steps, e.g. 1. day, 2. day, 5 day) which means droughts
    will not span time gaps

    The provided NetCDF should comprise all data variables that are present in the threshold dataset. Please note, yet
    yet SPA is only possible for one single variable. Further, the NetCDF must fully cover the time dimension of the
    threshold data, i.e., if the threshold dataset comprises thresholds for May, the NetCDF should provide at least data
    for the same month. Dates without the threshold time coverage will be ignored and not considered for the resulting
    exceedance dataset.
    """
    create(Path(exceedance), Path(threshold), Path(netcdf), dmin=dmin, cache=cache).run()

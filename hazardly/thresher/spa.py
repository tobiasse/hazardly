from pathlib import Path
from typing import Sequence
from typing import Union

from cftime import datetime
from click import progressbar
from netCDF4 import Dataset
from numpy import any
from numpy import append
from numpy import copy
from numpy import expand_dims
from numpy import float64
from numpy import full
from numpy import logical_and as AND
from numpy import logical_not as NOT
from numpy import max
from numpy import putmask
from numpy import sqrt
from numpy import uint16
from numpy import uint8
from numpy import where
from numpy import zeros
from numpy.random import default_rng
from numpy.typing import NDArray

from hazardly.thresher.cfg import EXCEED
from hazardly.thresher.cfg import LAT
from hazardly.thresher.cfg import LAYER
from hazardly.thresher.cfg import LON
from hazardly.thresher.cfg import TIME
from hazardly.thresher.errors import ThresherError
from hazardly.thresher.exceedance import Exceedance
from hazardly.thresher.netcdf import Netcdf
from hazardly.thresher.threshold import get as get_threshold
from hazardly.threshertools.consecutive import disaggregated


class SPA(Exceedance):
    """Sequent peak algorithm (SPA) calculation for a NetCDF.

    Implementation corresponds to the algorithm described in: Vogel, R. M., & Stedinger, J. R. (1987). Generalized
    storage-reliability-yield relationships. Journal of Hydrology, 89(3–4), 303–327.
    https://doi.org/10.1016/0022-1694(87)90184-3 and Biggs, B. J. F., Clausen, B., Demuth, S., Fendeková,
    M., Gottschalk, L., Gustard, A., Hisdal, H., Holmes, M. G. R., Jowett, I. G., Kašpárek, L., Kasprzyk, A.,
    Kupczyk, E., Van Lanen, H. A. J., Madsen, H., Marsh, T. J., Moeslund, B., Novický, O., Peters, E., Pokojski, W., …
    Young, A. R. (2004). Hydrological Drought: Processes and Estimation Methods for Streamflow and Groundwater
    (L. M. Tallaksen & H. A. J. Van Lanen (eds.); 1st ed.). Elsevier.

    Additionally, dmin is introduced to exclude droughts with duration below this threshold. Further the algorithm is
    time-step sequence-breaks aware (a gap between two time-steps, e.g. 1. day, 2. day, 5 day) which means droughts
    will not span time gaps.
    """

    def __init__(self, path: Path) -> None:
        super().__init__(path)

        with Dataset(self.path) as nc:
            exceed = nc.variables[EXCEED]
            self.dmin = exceed.dmin

            if exceed.cache <= 0:
                self.cache = None
            else:
                self.cache = exceed.cache

    # noinspection DuplicatedCode
    def run(self) -> 'SPA':
        """Execute sequent peak analysis."""
        if self.created:
            return self

        with progressbar(self.dates, label='Computing SPA') as p:
            lats, lons, var = len(self.lats), len(self.lons), self._thresholds.percentiles[0].var
            s = zeros(shape=(1, lats, lons), dtype=float)
            duration = zeros(shape=(lats, lons), dtype=int)

            i = 0
            days = len(self.dates)
            previous = self.dates[0]

            while i <= days:
                if i < days:
                    t = self.dates[i]
                    if (t - previous).days <= 1:
                        # print(self.dates[i])
                        q0 = self._thresholds.read(var, 0, t, self.lats, self.lons)
                        qt = self._responses.read(var, t, self.lats, self.lons)[0]
                        st = s[-1] + q0 - qt
                        p.update(1)  # Hack, for updating progress
                        previous = t
                        n_plus_one = False
                        i += 1
                    else:
                        st = zeros(shape=(lats, lons), dtype=float)
                        t, previous = previous, t
                        n_plus_one = True
                else:
                    st = zeros(shape=(lats, lons), dtype=float)
                    t = previous
                    n_plus_one = True
                    i += 1

                is_filled = st <= 0
                st[is_filled] = 0
                duration[NOT(is_filled)] += 1

                s = append(s, expand_dims(st, axis=0), axis=0)

                is_drought = AND(is_filled, duration > 0)
                if any(is_drought):
                    depletion_period = copy(duration)
                    depletion_period[NOT(is_drought)] = 0
                    v_i = copy(s[-1 - max(depletion_period):-1])
                    putmask(v_i, NOT(disaggregated(depletion_period, set_to=1)), 0)
                    self._write(0, t, d_i(max(v_i, axis=0), v_i, self.dmin), n_plus_one)
                    duration[depletion_period > 0] = 0

                if self.cache and len(s) > self.cache:
                    d_i_max = max(duration)
                    s = s[-1 - d_i_max:]
                    if d_i_max >= self.cache:
                        self.cache = d_i_max ** 2

        return self

    def _write(self, layer: int, timestamp: datetime, events: NDArray[int], n_plus_one: bool = False) -> None:
        """Write drought event data to dataset.

        Args:
            n_plus_one: If true one is added to the write index (required in case of a time gap or sequence end write).
        """
        with Dataset(self.path, mode='a') as nc:
            var = nc.variables[EXCEED]
            idx = where(self.dates == timestamp)[0][-1]
            if n_plus_one:
                idx += 1
            var[layer, idx - len(events):idx] = where(events > 0, events, var[layer, idx - len(events):idx])


class SPABootstrapped(SPA):
    # noinspection DuplicatedCode
    def run(self) -> 'SPABootstrapped':
        """Execute bootstrapped sequent peak analysis."""
        if self.created:
            return self

        with progressbar(self.layer, label='Computing bootstrapped SPA') as layer:
            for lay in layer:
                lats, lons, var = len(self.lats), len(self.lons), self._thresholds.percentiles[0].var
                s = zeros(shape=(1, lats, lons), dtype=float)
                duration = zeros(shape=(lats, lons), dtype=int)

                i = 0
                days = len(self.dates)
                previous = self.dates[0]

                while i <= days:
                    if i < days:
                        t = self.dates[i]
                        if (t - previous).days <= 1:
                            q0 = self._thresholds.read(var, lay, t, self.lats, self.lons)
                            qt = self._responses.read(var, t, self.lats, self.lons)[0]
                            st = s[-1] + q0 - qt
                            previous = t
                            n_plus_one = False
                            i += 1
                        else:
                            st = zeros(shape=(lats, lons), dtype=float)
                            t, previous = previous, t
                            n_plus_one = True
                    else:
                        st = zeros(shape=(lats, lons), dtype=float)
                        t = previous
                        n_plus_one = True
                        i += 1

                    is_filled = st <= 0
                    st[is_filled] = 0
                    duration[NOT(is_filled)] += 1

                    s = append(s, expand_dims(st, axis=0), axis=0)

                    is_drought = AND(is_filled, duration > 0)
                    if any(is_drought):
                        depletion_period = copy(duration)
                        depletion_period[NOT(is_drought)] = 0
                        v_i = copy(s[-1 - max(depletion_period):-1])
                        putmask(v_i, NOT(disaggregated(depletion_period, set_to=1)), 0)
                        self._write(lay, t, d_i(max(v_i, axis=0), v_i, self.dmin), n_plus_one)
                        duration[depletion_period > 0] = 0

                    if self.cache and len(s) > self.cache:
                        d_i_max = max(duration)
                        s = s[-1 - d_i_max:]
                        if d_i_max >= self.cache:
                            self.cache = d_i_max ** 2

        return self


def get(path: Path) -> Union[SPA, SPABootstrapped]:
    """Get a SPA instance, the type of the instance depends on the provided dataset.

    Args:
        path: Path to the SPA dataset.

    Returns:
        A SPA instance, either SPA or SPABootstrapped.
    """
    with Dataset(path) as nc:
        if nc.variables[LAYER].bootstrapped:
            instance = SPABootstrapped
        else:
            instance = SPA

    return instance(path)


# noinspection DuplicatedCode
def create(path: Path, threshold: Path, netcdf: Path, dmin: int = 0, cache: int = 0) -> Union[SPA, SPABootstrapped]:
    """Creates a new SPA dataset and returns a SPA or SPABootstrapped instance.

    Args:
        path: Path where the new dataset should be created.
        threshold: Threshold dataset (q0), required for deriving drought events.
        netcdf: Source data (qt), represents the daily inflow.
        dmin: Minimal duration, droughts with a duration below this threshold will be excluded.
        cache: Limit the maximum size of S, this helps to prevent memory overflow if you want analyze large sequences
            of q0 and qt. Ideally, set to value equal the longest drought + 1. On default the size of S is unlimited
            and will therefore grow till the length of q0 ant qt.

    Returns:
        A SPA instance, either SPA or SPABootstrapped.
    """
    if path.is_file():
        return get(path)

    threshold = get_threshold(threshold)
    layers, lats, lons = threshold.layer, threshold.lats, threshold.lons

    if len(threshold.percentiles) > 1:
        raise ThresherError('SPA only possible for single variable datasets')

    netcdf = Netcdf(netcdf)
    times, units, calendar = threshold.within(netcdf), netcdf.tunits, netcdf.tcalendar

    if threshold.bootstrapping:
        instance = SPABootstrapped
    else:
        instance = SPA

    with Dataset(path, mode='w') as exceedance:
        exceedance.createDimension(LAYER, len(layers))
        layer = exceedance.createVariable(LAYER, uint16, (LAYER,), fill_value=False)
        layer[:] = layers
        layer.bootstrapped = int(threshold.bootstrapping)
        layer.repetitions = threshold.repetitions
        layer.order = threshold.order

        exceedance.createDimension(TIME, len(times))
        time = exceedance.createVariable(TIME, float64, (TIME,), fill_value=False)
        time[:] = times
        time.units, time.calendar = units, calendar
        time.base = threshold.time_str()

        exceedance.createDimension(LAT, len(lats))
        latitude = exceedance.createVariable(LAT, float64, (LAT,), fill_value=False)
        latitude[:] = lats

        exceedance.createDimension(LON, len(lons))
        longitude = exceedance.createVariable(LON, float64, (LON,), fill_value=False)
        longitude[:] = lons

        exceedance = exceedance.createVariable(EXCEED, uint8, (LAYER, TIME, LAT, LON), fill_value=0)
        exceedance.dmin = dmin
        exceedance.cache = cache
        exceedance.created = int(False)
        exceedance.responses = str(netcdf.path)
        exceedance.thresholds = str(threshold.path)
        exceedance.percentiles = threshold.percentiles_str()

    return instance(path)


def spa(
        q0: NDArray[float],
        qt: NDArray[float],
        events: NDArray[float],
        time: Sequence[int],
        lats: int,
        lons: int,
        dmin: int = 0,
        cache: int = None,
) -> NDArray[float]:
    """Reference implementation of the sequent peak algorithm (SPA) for a three-dimensional lattice.

    Implementation corresponds to the algorithm described in: Vogel, R. M., & Stedinger, J. R. (1987). Generalized
    storage-reliability-yield relationships. Journal of Hydrology, 89(3–4), 303–327.
    https://doi.org/10.1016/0022-1694(87)90184-3 and Biggs, B. J. F., Clausen, B., Demuth, S., Fendeková,
    M., Gottschalk, L., Gustard, A., Hisdal, H., Holmes, M. G. R., Jowett, I. G., Kašpárek, L., Kasprzyk, A.,
    Kupczyk, E., Van Lanen, H. A. J., Madsen, H., Marsh, T. J., Moeslund, B., Novický, O., Peters, E., Pokojski, W., …
    Young, A. R. (2004). Hydrological Drought: Processes and Estimation Methods for Streamflow and Groundwater
    (L. M. Tallaksen & H. A. J. Van Lanen (eds.); 1st ed.). Elsevier.

    Additionally, dmin is introduced to exclude droughts with duration below this threshold. Further the algorithm is
    time-step sequence-breaks aware (a gap between two time-steps, e.g. 1. day, 2. day, 5 day) which means droughts
    will not span time gaps.

    Args:
        q0: A three-dimensional matrix containing the desired yield or any other predefined flow.
        qt: A three-dimensional matrix describing the daily inflow to a reservoir.
        events: A three-dimensional matrix of zeros. Drought events will be stored in this matrix by setting the cells
            to one.
        time: A one-dimensional list of timestamps that represent the time dimension of q0, qt, and events.
        lats: The number of latitude coordinates.
        lons: The number of longitude coordinates.
        dmin: Droughts with a duration below (exclusive) this threshold are not considered as drought events.
        cache: Limit the maximum size of S, this helps to prevent memory overflow if you want analyze large sequences
            of q0 and qt. Ideally, set to value equal the longest drought + 1. On default the size of S is unlimited
            and will therefore grow till the length of q0 ant qt. Please note, if set the returned S is never bigger
            than cache.

    Returns:
        S, the storage volume deficit per grid cell. Please note, S will either be equal or smaller than cache if cache
        is set, or it will be greater then the length of time at least time + 2 (first element is S₀ a zero matrix and
        and last element is S₋₁ a zero matrix as well). If time contains sequence breaks the length of S corresponds to
        time + 2 + number-of-sequence-breaks.
    """
    # TODO time must be in q0, qt, and events
    # TODO shape[1:] == q0, qt, and events
    s = zeros(shape=(1, lats, lons), dtype=float)  # Init S₀
    duration = zeros(shape=(lats, lons), dtype=int)

    i = 0
    previous = time[0]
    while i <= len(time):
        if i < len(time):
            t = time[i]
            if t - previous <= 1:
                st = s[-1] + q0[t] - qt[t]
                previous = t
                i += 1
            else:  # Catch time sequence breaks (distance between previous and current time-step is greater than one)
                # Run the algorithm to evaluate all unfinished droughts before the sequence break
                st = zeros(shape=(lats, lons), dtype=float)  # Set St to zeros forces evaluation of unfinished droughts
                # No increment of i bc the current time-step is the time-step after the sequence break, therefore we
                # want to run the algorithm another times with the current values
                t, previous = previous + 1, t  # Set t to the previous time-step to write events to correct time span
        else:  # End of q0 and qt sequence, i is out-of-bounds
            # Run the algorithm another time to evaluate unfinished drought events
            st = zeros(shape=(lats, lons), dtype=float)  # Setting St to zeros forces evaluation of unfinished droughts
            t = previous + 1
            i += 1

        is_filled = st <= 0
        st[is_filled] = 0
        duration[NOT(is_filled)] += 1

        s = append(s, expand_dims(st, axis=0), axis=0)

        is_drought = AND(is_filled, duration > 0)
        if any(is_drought):
            depletion_period = copy(duration)
            # Excluding still running droughts
            depletion_period[NOT(is_drought)] = 0

            # The drought deficit volume, vᵢ
            v_i = copy(s[-1 - max(depletion_period):-1])
            putmask(v_i, NOT(disaggregated(depletion_period, set_to=1)), 0)

            drought_events = d_i(max(v_i, axis=0), v_i, dmin)

            events[t - len(drought_events):t] = where(drought_events > 0, drought_events,
                                                      events[t - len(drought_events):t])
            duration[depletion_period > 0] = 0

        if cache and len(s) > cache:
            d_i_max = max(duration)
            s = s[-1 - d_i_max:]
            # Persistent expansion of max cache size if longest drought exceeds cache size
            if d_i_max >= cache:
                cache = d_i_max ** 2

    return s


def d_i(tau_max: NDArray[float], v_i: NDArray[float], dmin: int) -> NDArray[int]:
    """Disaggregates drought deficit volume to the drought time interval.

    Disaggregates the drought deficit volume, vᵢ, to the time interval, dᵢ, from the beginning of the depletion
    period, τ₀, to the time of the maximum depletion, τₘₐₓ. The time interval between τ₀ <= t <= τₘₐₓ is set to one.

    Args:
        tau_max: The maximum depletion volume, τₘₐₓ, per grid cell.
        v_i: The drought deficit volume matrix.
        dmin: Droughts with a duration below (exclusive) this threshold are not considered as drought events.

    Returns:
        The drought time interval.
    """
    peaks = zeros(shape=v_i.shape[1:], dtype=int)  # Store cells for which the position of τₘₐₓ is known
    # Prevent in-place modification, we will abuse the drought deficit volume matrix, vᵢ, and
    # convert it to the drought time interval, dᵢ
    v_i = v_i.copy()

    # Iter over the reversed depletion volumes to replicate (τₘₐₓ - τ₀ + 1 = dᵢ)
    for tau in v_i[-1::-1]:
        # Identify where τₜ = τₘₐₓ
        peaks[AND(tau == tau_max, tau > 0)] = 1
        tau[AND(tau == tau_max, tau > 0)] = 1

        # Remove filling-up events
        tau[AND(NOT(peaks), tau > 0)] = 0
        # If found τₘₐₓ set the subsequent τₜ to one to indicate a drought event, dᵢ
        tau[AND(peaks, tau > 0)] = 1

    # Remove events below dmin
    v_i = v_i.astype(int)
    mask = v_i.sum(axis=0) < dmin
    mask = full(v_i.shape, mask)
    putmask(v_i, mask, 0)

    return v_i


def ar1(
        shape: Union[int, Sequence[int]],
        phi: float = .5,
        mu: float = 0.,
        sigma: float = 1.,
        seed: int = None
) -> Union[float, NDArray[float]]:
    """Create random data with an auto-regressive model (AR(1)).

    Noise is generated by a normal distribution with mean 0 and standard deviation corresponding to the equations
    below.

    Xₜ = c + φXₜ₋₁ + εₜ

    εₜ := white noise process with μ=0 and constant variance σε² or standard deviation σε
    φ := Model parameter, if close to 0 process looks like white noise, but as close to 1 the outputs gets a larger
         contribution from the previous term relative to the noise.

    μ = c / (1 - φ)
    c = μ(1 - φ)

    σ² = φ²σ² + σε²
    σε = σ * sqrt(1 - φ²)

    X₀ = c + N(0, σε²)
    X₁ = c + φX₀ + N(0, σε²)
    Xₜ = c + φXₜ₋₁ + N(0, σε²)

    Code copy edit from https://stackoverflow.com/questions/33898665/python-generate-array-of-specific-autocorrelation.

    Args:
        shape: Shape of the output data.
        phi: Model parameter, if close to 0 process looks like white noise, but as close to 1 the outputs gets a larger
            contribution from the previous term relative to the noise.
        mu: Mean of the auto-regressive model.
        sigma: Standard deviation of the aut-regressive model.
        seed: Seed for the random number generator.

    Returns:
        An array with data generated by an AR(1) model, shape corresponds to the give parameter.
    """
    if not (0. < phi < 1.):
        raise ThresherError()

    if isinstance(shape, int):
        shape = [shape]

    rng = default_rng(seed)

    c = mu * (1 - phi)
    sigma_epsilon = sigma * sqrt(1 - phi ** 2)

    x = zeros(shape=shape, dtype=float)
    x[0] = c + rng.normal(loc=0, scale=sigma_epsilon, size=shape[1:])

    for t in range(1, shape[0]):
        x[t] = c + phi * x[t - 1] + rng.normal(loc=0, scale=sigma_epsilon, size=shape[1:])

    return x

class DamageError(Exception):
    """Raise if some errors occur during the execution of damage analysis."""

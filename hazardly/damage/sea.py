from functools import cached_property
from typing import Any
from typing import Callable
from typing import List
from typing import NewType
from typing import Union

from numpy import array
from numpy import isnan
from numpy import logical_and
from numpy import logical_not
from numpy import where
from numpy.ma import append
from numpy.ma import masked_array
from numpy.typing import NDArray

from hazardly.damage.errors import DamageError

SEAResult = NewType('SEAResult', NDArray[float])


class YieldDamage:
    # noinspection PyUnresolvedReferences
    """The class encapsulates the result (composites) of a superposed epoch analysis run. An instance of this class
    provides methods to calculate crop yield reduction corresponding to the methodology of the following publication:

    Lesk, C., Rowhani, P., & Ramankutty, N. (2016). Influence of extreme weather disasters on global crop production.
    Nature, 529(7584), 84–87. https://doi.org/10.1038/nature16467

    Args:
        composites: The composite matrix with disturbed and undisturbed samples; please provide a masked array to
            exclude NaN samples from further calculations.

    Attributes:
        norm: The divisor used for normalization.
        means: Column-wise means of the normalized samples.
        damage: Mean of the normalized disturbed samples.
        window: Size of the window that is used to select the samples.
        composites: Two-dimensional sample matrix, the first dimension corresponds to the window size, and the second
            dimension corresponds to the number of windowed samples drawn from the source data.
        half_window: Size of half of the window minus one (center position of the disturbance).
        norm_composites: The composite matrix divided by norm coefficient.
    """

    def __init__(self, composites: NDArray[float]) -> None:
        if not isinstance(composites, masked_array):
            raise DamageError('Composites isn\'t a numpy.masked_array')

        if len(composites.shape) != 2:
            raise DamageError('Composites isn\'t a two-dimensional matrix')

        self.composites = composites

    @cached_property
    def norm(self) -> float:
        """Get the divisor for normalization, mean of all undisturbed samples."""
        return append(
            self.composites[:, :self.half_window],
            self.composites[:, self.half_window + 1:]
        ).mean()

    @cached_property
    def means(self) -> NDArray[float]:
        """Get column-wise means of the normalized composites."""
        return self.norm_composites.mean(axis=0)

    @cached_property
    def damage(self) -> float:
        """Get mean of the disturbed samples after normalization."""
        return self.means[self.half_window]

    @cached_property
    def window(self) -> int:
        """Get window size."""
        _, window = self.composites.shape
        return window

    @cached_property
    def half_window(self) -> int:
        """Get half window size."""
        return self.window // 2

    @cached_property
    def norm_composites(self) -> NDArray[float]:
        """Get normalized composite matrix, composites are normalized by dividing all samples with norm."""
        return self.composites / self.norm


def align(indexes: NDArray[int], mn: int, mx: int) -> NDArray[int]:
    """Get all triggers indexes that greater or equal to a minimum index (inclusive) and smaller to a maximum index
    (exclusive).

    Args:
        indexes: A list of trigger indexes (not the triggers themselves, only their position/index).
        mn: The minimum index.
        mx: The maximum index.

    Returns:
        The trigger indexes within the described bounds.
    """
    return indexes[logical_and(indexes >= mn, indexes < mx)]


def lookahead(idx: int, indexes: NDArray[int]) -> int:
    """Pools neighboring (distance between two triggers is one) triggers till the distance is greater than one and
    returns the end position of this pool.

    Args:
        idx: Current starting position for pooling.
        indexes: A list of trigger indexes (not the triggers themselves, only their position/index).

    Returns:
        The end position of the pooled triggers.
    """
    if idx + 1 >= len(indexes) or indexes[idx] + 1 != indexes[idx + 1]:
        return idx
    return lookahead(idx + 1, indexes)


def distance(indexes: NDArray[int]) -> List[int]:
    """Calculates the distance between triggers and returns them as a list. Returns only the spaces greater than one;
    distances equal to one are interpreted as neighboring.

    Args:
        indexes: A list of trigger indexes (not the triggers themselves, only their position/index).

    Returns:
        A list of distances between triggers returns only distances greater than one.
    """
    distances = list()

    if len(indexes) < 2:
        return distances

    for idx in range(1, len(indexes)):
        if (dist := indexes[idx] - indexes[idx - 1]) > 1:
            distances.append(dist)

    return distances


def sea(
        triggers: NDArray[float],
        responses: NDArray[float],
        offset: int = 0,
        window: int = 7,
        adaptive: Callable[[int, List[int]], int] = None,
        sample_bounds: bool = True,
        result: Union[Callable[[NDArray[float]], Any], SEAResult] = YieldDamage,
) -> Any:
    """Superposed epoch analysis creates the two-dimensional (window * number of composites) composite matrix for
    deriving your or default results (see argument documentation).

    Requires a list of triggers (all values not equal to zero or NaN are interpreted as disturbance) that force
    subsampling of the provided responses in the defined window size. Neighboring triggers (distance between them is
    one) force an aggregation of the related responses by applying the statistical mean. This aggregation excludes
    responses that equal to zero (zeros are interpreted as NaN).

    The default configuration of this function follows the approach described in:
    Lesk, C., Rowhani, P., & Ramankutty, N. (2016). Influence of extreme weather disasters on global crop production.
    Nature, 529(7584), 84–87. https://doi.org/10.1038/nature16467

    Args:
        triggers: A 1-D list of triggers, all values not equal to zero or NaN will be interpreted as disturbance and
            trigger the subsampling for the composite matrix. Please note, there should be at least one disturbance
            within the provided responses; otherwise, the function raises an exception.
        responses: A 1-D list of responses.
        offset: The offset between the triggers and responses should be a negative or positive integer. If triggers and
            responses are not aligned, you may shift triggers to align with responses by providing this argument. All
            disturbances that are not within the responses are excluded from the composite sampling.
        window: Size of the subsampling window for data selection when a trigger indicates a disturbance. Please note
            the window should be an odd positive integer. Further, the window is always centered at the disturbance
            position.
        adaptive: Define your function for selecting the optimal window size. The function must accept as the first
            argument an integer that relates to the value of the argument window. As the second argument, the function
            must take a list of integers representing different options for window sizes calculated from the distances
            between triggers. As a result, the function should return an odd integer that represents the selected window
            size. If adaptive is not set, the parameter of the window argument will be applied.
        sample_bounds: Set it to false if you want to suppress subsampling when the bounds of responses are exceeded.
            The default behavior is to append zeros as a replacement in the size of the bound exceedance.
        result: Define your own function or class that calculates your required results from the composites. As an
            argument, the provided context must accept a two-dimensional numpy.masked_array (all values equal zero are
            masked). If you provide your function or class, either it calls the provided function with the described
            parameter and returns the result, or it creates an instance of your class with the related parameter and
            returns it. On default, an instance of YieldDamages is created and returned.

    Returns:
        On default, an instance of YieldDamage is returned. If you provide your function or class, either it returns the
        result of the function call, or it returns an instance of your class.

    Raises:
        HazardlyError: If the window is not an odd positive integer or no disturbances from triggers within the
            responses are found.
    """
    # TODO (IMPROVEMENT, MINOR IMPORTANCE) co-occurring disasters may be aggregated by a user defined function
    indexes = where(logical_and(triggers != 0, logical_not(isnan(triggers))))[0] + offset
    indexes = align(indexes, mn=0, mx=len(responses))
    composites = list()

    if len(indexes) < 1:
        raise DamageError('No disturbances within responses')

    if adaptive:
        windows = list((array(distance(indexes)) * 2) + 1)  # Convert distances to full window
        window = adaptive(window, windows)

    if window % 2 == 0 or window <= 1:
        raise DamageError('Window isn\'t an odd positive integer')

    i = 0
    window = window // 2  # Convert to half window for convenient slicing

    while i < len(indexes):
        j = lookahead(i, indexes)

        start = indexes[i]
        end = indexes[j] + 1

        disturbance = responses[start:end]
        disturbance = [masked_array(disturbance, disturbance == 0).mean()]

        below_bounds = start - window < 0
        above_bounds = end + window > len(responses)
        out_of_bounds = below_bounds or above_bounds

        if sample_bounds and out_of_bounds:
            if below_bounds and above_bounds:
                left = list(responses[:start])
                left = [0.] * (window - len(left)) + left
                right = list(responses[end:])
                right = right + [0.] * (window - len(right))
            elif below_bounds:
                left = list(responses[:start])
                left = [0.] * (window - len(left)) + left
                right = list(responses[end:end + window])
            else:
                left = list(responses[start - window:start])
                right = list(responses[end:])
                right = right + [0.] * (window - len(right))

            composites.append(left + disturbance + right)

        elif not out_of_bounds:
            left = list(responses[start - window:start])
            right = list(responses[end:end + window])
            composites.append(left + disturbance + right)

        i = j + 1

    composites = array(composites)
    return result(masked_array(composites, composites == 0))


def prefer_window_use_min_if_smaller(window: int, windows: List[int]) -> int:
    """Prefer the initial window but set it to the minimum window when the initial window would sample disturbances."""
    if (w := min(windows)) < window:
        return w
    return window

from sys import stdout
from typing import List
from typing import Tuple
from typing import Union

from click import File
from click import argument
from click import command
from click import option
from pandas import DataFrame
from pandas import concat

from hazardly.damage.cli_damages import CI_LOWER
from hazardly.damage.cli_damages import CI_UPPER
from hazardly.damage.cli_damages import MEAN
from hazardly.damage.cli_damages import METRIC_COL_NAME
from hazardly.damage.cli_decisions import DECISION_COL_NAME
from hazardly.utils.parse import parse_ranges
from hazardly.utils.parse import validate_labels


@command()
@option('--geoid', type=str, default='NUTS_ID', show_default=True, is_eager=True,
        help='Column label of the geo unit, used for selecting data from `AREAS` and `DAMAGES`')
@option('--cropid', type=str, default='CROP_ID', show_default=True, is_eager=True,
        help='Column label of the crop type, used for selecting data from `AREAS` and `DAMAGES`')
@option('--ay', type=str, default='1991-2020', show_default=True, is_eager=True, callback=parse_ranges(),
        help='Years (columns) to select form `AREAS` for mean area aggregation')
@option('--dy', type=str, default='1991-2020', show_default=True, is_eager=True, callback=parse_ranges(),
        help='Years (columns) to select from `DAMAGES` for mean damage aggregation')
@option('--decisions', type=File(lazy=False),
        callback=validate_labels(paras=['geoid', 'cropid'], labels=[DECISION_COL_NAME]),
        help='Optional decisions CSV, aggregates only data from `AREAS` and `DAMAGES` that evaluate to `TRUE`')
@option('--oncrops', is_flag=True,
        help='Switches between geo- or crop-wise aggregation, if set aggregates crop-wise otherwise geo-wise')
@argument('weighting', type=File(lazy=False), required=True, callback=validate_labels(paras=['geoid', 'cropid', 'ay']))
@argument('damages', type=File(lazy=False), required=True,
          callback=validate_labels(paras=['geoid', 'cropid', 'dy'], labels=[METRIC_COL_NAME]))
def means(
        geoid: str,
        cropid: str,
        ay: List[str],
        dy: List[str],
        decisions: Union[DataFrame, None],
        oncrops: bool,
        weighting: DataFrame,
        damages: DataFrame,
) -> None:
    """
    Calculates weighted mean of crop damages either per `geoid` or `cropid` and prints the result formatted as a CSV to
    STDOUT

    Calculates the weights from `WEIGHTING` that is either area used for agricultural production or the agricultural
    production. First, the mean weighting per `geoid` and `cropid` for the selected years (`ay`) is calculated. Next, it
    determines the total mean weighting either by summing the mean weighting per `geoid` or per `cropid`. The weights
    are the quotient of the mean weight and total mean weight, and the aggregation result is the weighted sum of the
    mean damages per `geoid` or `cropid`. Mean damages are calculated per `geoid` and `cropid` for selected years (`dy`).

    The options `ay` and `dy` accept range definition strings, e.g. "1991,1993-1995,2000" converts to
    `[1991, 1993, 1994, 1995, 2000]`, interprets range as a closed interval.

    `AREAS`, `DAMAGES`, and `DECISIONS` may be a path to a file or an input stream, e.g.,
    `means --decisions decisions.csv -- <(xsv search -i -s "NUTS_ID" "AT1" "areas.csv") damages.csv`.

    Input data must comply with CSV.
    """
    idx, cols = geoid, cropid
    if oncrops:
        idx, cols = cropid, geoid

    if isinstance(decisions, DataFrame):
        items: List[Tuple[str, str]] = [
            (row[geoid], row[cropid])
            for _, row in decisions.iterrows() if row[DECISION_COL_NAME]
        ]
        ex_items: List[Tuple[str, str, str]] = [(g, c, metric) for g, c in items for metric in
                                                [MEAN, CI_LOWER, CI_UPPER]]
        weighting, damages = weighting.set_index([geoid, cropid]), damages.set_index([geoid, cropid, METRIC_COL_NAME])
        weighting, damages = weighting.filter(items=items, axis=0), damages.filter(items=ex_items, axis=0)
        weighting, damages = weighting.reset_index(), damages.reset_index()

    weights: DataFrame = DataFrame()
    weights[geoid], weights[cropid] = weighting[geoid], weighting[cropid]
    weights['mean'] = weighting.loc[:, ay].mean(axis=1)  # Mean weighting for selected yrs
    sums: DataFrame = weights.pivot(index=idx, columns=cols, values='mean').sum(axis=1)  # Mean total weighting
    weights['total'] = weights.apply(lambda row: sums[row[idx]], axis=1)  # Link mean total weighting with mean weight
    weights['weight'] = weights['mean'] / weights['total']  # Calculate individual weights
    weights = weights.pivot(index=idx, columns=cols, values='weight')

    result: DataFrame = DataFrame()
    for metric in [MEAN, CI_LOWER, CI_UPPER]:
        mean_da: DataFrame = damages.loc[damages[METRIC_COL_NAME] == metric].copy()
        mean_da['mean'] = mean_da.loc[:, dy].mean(axis=1)  # Mean damage for selected yrs
        mean_da = mean_da.pivot(index=idx, columns=cols, values='mean')

        mcols: List[str] = sorted(set(weights.columns) & set(mean_da.columns))
        midxs: List[str] = sorted(set(weights.index) & set(mean_da.index))

        partial: DataFrame = DataFrame()
        partial[metric] = (weights[mcols].loc[midxs] * mean_da[mcols].loc[midxs]).sum(axis=1)  # Aggregated mean damage

        result = concat([result, partial], axis=1)

    result.reset_index(inplace=True)
    result.to_csv(stdout, index=False)

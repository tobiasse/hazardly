from __future__ import annotations

from functools import cached_property
from typing import Any
from typing import Callable
from typing import Dict
from typing import List
from typing import NewType
from typing import Tuple

import numpy as np
from numpy import append
from numpy import array
from numpy import exp
from numpy import isnan
from numpy import log
from numpy import logical_and
from numpy import logical_not
from numpy import mean
from numpy import nan
from numpy import reshape
from numpy import std
from numpy import unique
from numpy import where
from numpy import zeros
from numpy.random import default_rng
from numpy.typing import NDArray
from scipy.spatial import ConvexHull
from scipy.stats import pearsonr
from scipy.stats import spearmanr
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import LeaveOneOut

from hazardly.damage.errors import DamageError
from hazardly.damage.sea import align

DEAResult = NewType('DEAResult', Tuple[NDArray[float], NDArray[float], NDArray[float]])


class YieldDamage:
    # noinspection PyUnresolvedReferences
    """The class encapsulates the results (yield, min and max yield) of a data envelopment analysis run. This class
    provides methods to calculate absolute and relative crop yield deviations from the minimum and maximum yield
    splines.

    Args:
        responses: A one-dimensional list of yields.
        lower: Vertices of minimum yield splines.
        upper: Vertices of maximum yield splines.

    Attributes:
        lower_absolute_deviation: The absolute deviation of the yield from the minimum yield splines.
        lower_relative_deviation: The relative deviation of the yield from the minimum yield splines.
        upper_absolute_deviation: The absolute deviation of the yield from the maximum yield splines.
        upper_relative_deviation: The relative deviation of the yield from the maximum yield splines.
    """

    def __init__(self, responses: NDArray[float], lower: NDArray[float], upper: NDArray[float]) -> None:
        self.responses: NDArray[float] = responses
        self.lower: NDArray[float] = lower
        self.upper: NDArray[float] = upper

    @cached_property
    def lower_absolute_deviation(self) -> NDArray[float]:
        """Get the absolute yield deviations from the minimum yield splines."""
        return self._deviations()

    @cached_property
    def lower_relative_deviation(self) -> NDArray[float]:
        """Get the relative yield deviations from the minimum yield splines."""
        return self._deviations(relative=True)

    @cached_property
    def upper_absolute_deviation(self) -> NDArray[float]:
        """Get the absolute yield deviations from the maximum yield splines."""
        return self._deviations(lower=False)

    @cached_property
    def upper_relative_deviation(self) -> NDArray[float]:
        """Get the relative yield deviations from the maximum yield splines."""
        return self._deviations(lower=False, relative=True)

    def _deviations(self, lower: bool = True, relative: bool = False) -> NDArray[float]:
        """Calculates the absolute or relative yield deviations from the min or max yield splines. Responses with a
        value of zero will be excluded from deviation calculation.

        Args:
            lower: Either vertices of the min splines (true) or vertices of the max splines (false) are used.
            relative: Either relative (true) or absolute (false) deviation is calculated.

        Returns:
            The calculated deviations.
        """
        deviations: NDArray[float] = zeros(shape=len(self.responses))
        deviations[isnan(self.responses)] = nan

        if lower:
            vertices: NDArray[float] = self.lower
        else:
            vertices: NDArray[float] = self.upper

        for i in range(1, len(vertices)):
            p1x: float
            p1y: float
            p2x: float
            p2y: float
            p1x, p1y = vertices[i - 1]
            p2x, p2y = vertices[i]

            slope: float = (p2y - p1y) / (p2x - p1x)
            intercept: float = p1y - slope * p1x

            for j in range(int(p1x), int(p2x) + 1):
                if actual := self.responses[j]:
                    expected: float = slope * j + intercept

                    if relative:
                        deviation = (actual / expected) - 1
                    else:
                        deviation = actual - expected

                    deviations[j] = deviation

        return deviations


class Ring(ConvexHull):
    """The class extends scipy.spatial.ConvexHull class by adding the property ring."""

    @cached_property
    def ring(self) -> NDArray[int]:
        """Returns the vertices coordinates as a closed ring (first coordinates are equal to last coordinates).

        Returns:
            The vertices of the convex hull while the first coordinates are equal to the last coordinates (a closed
            ring).
        """
        return append(self.vertices, self.vertices[0])


def make_points(responses: NDArray[float], keep_zeros: bool) -> NDArray[float]:
    """Converts responses to a two-dimensional matrix (2 x samples). Each row represents a coordinate tuple where the
    first element is x (the index of the response), and the second value is the actual value of the response. Responses
    with a value of NaN are not included in the matrix.

    Args:
        responses: A one-dimensional list of responses.
        keep_zeros: If true keeps zero values for the convex hull, otherwise zero values are excluded.

    Returns:
        A two-dimensional matrix (2 x samples).
    """
    if keep_zeros:
        x = where(logical_not(isnan(responses)))[0]
    else:
        x = where(logical_and(responses != 0, logical_not(isnan(responses))))[0]
    y = responses[x]
    return append(
        reshape(x, newshape=(*x.shape, 1)),
        reshape(y, newshape=(*y.shape, 1)),
        axis=1
    )


def dea(
        responses: NDArray[float],
        result: Callable[[NDArray[float], NDArray[float], NDArray[float]], Any] | DEAResult = YieldDamage,
        keep_zeros: bool = False
) -> Any:
    """Data envelopment analysis applies a convex hull to derive the vertices of the minimum and maximum response DMUs,
    which may be used for deriving your or default results (see argument documentation).

    Requires a one-dimensional list of responses while NaN values are excluded from the convex hull. The vertices of the
    convex hull are split into lower and upper parts. The lower part is defined as the vertices where response values
    are greater than the vertices. The upper part is defined as the vertices where the response values are smaller than
    the vertices. Further, all splines between the upper vertices are increasing; if a spline decreases, it is replaced
    by a flat spline.

    Args:
        responses: A one-dimensional list of responses.
        result: Define your own function or class that calculates your required results from the vertices of the minimum
            and maximum response DMUs. As arguments, the provided context must accept a one-dimensional list (responses)
            and two two-dimensional lists (lower and upper vertices, 2 x number of vertices). If you provide your
            function or class, either it calls the provided function with the described parameters and returns the
            result, or it creates an instance of your class with the related parameter and returns it. On default, an
            instance of YieldDamages is created and returned.
        keep_zeros: If true keeps zero values for the convex hull, otherwise zero values are excluded.
    Returns:
        On default, an instance of YieldDamage is returned. If you provide your function or class, either it returns the
        result of the function call, or it returns an instance of your class.

    Raises:
        DamageError: If no valid convex hull can be derived.
    """
    points = make_points(responses, keep_zeros)

    try:
        ring = Ring(points)
    except Exception as err:
        raise DamageError(str(err))

    upper_inc = list()
    upper_dec = list()
    lower = list()

    cx = np.mean(ring.points[ring.vertices, 0])
    cy = np.mean(ring.points[ring.vertices, 1])

    for idx in range(1, len(ring.ring)):
        p1 = points[ring.ring[idx - 1]]
        p2 = points[ring.ring[idx]]

        if p1[0] > p2[0]:
            p1, p2 = p2, p1

        p1x, p1y = p1
        p2x, p2y = p2

        slope = (p2y - p1y) / (p2x - p1x)
        intercept = p1y - slope * p1x
        y = slope * cx + intercept

        if cy < y and slope >= 0:
            upper_inc.append(p1)
            upper_inc.append(p2)
        elif cy > y:
            lower.append(p1)
            lower.append(p2)
        else:
            upper_dec.append(p1)
            upper_dec.append(p2)

    upper_inc = array(unique(upper_inc, axis=0))
    upper_dec = array(unique(upper_dec, axis=0))
    lower = array(unique(lower, axis=0))

    if len(upper_inc) == 0:
        upper = upper_dec
    elif upper_inc[-1, 0] < len(responses) - 1:
        x, y = len(responses) - 1, upper_inc[-1, 1]
        upper_inc = append(upper_inc, [[x, y]], axis=0)
        upper = upper_inc
    else:
        upper = upper_inc

    return result(responses, lower, upper)


def _samples(
        triggers: NDArray[float],
        responses: NDArray[float],
        offset: int,
        keep_zeros: bool
) -> Tuple[NDArray[float], NDArray[float]]:
    """Get one- dimensional lists of triggers and responses from the provided arguments.

    Args:
        triggers: A one- or two-dimensional list of triggers used to select responses. Uses only non NaN values and
            values that are not equal to zero.
        responses: A one- or two dimensional list of responses, NaN responses are excluded from the sampling.
        offset: The offset between the triggers and responses should be a negative or positive integer. If triggers and
            responses are not aligned, you may shift triggers to align with responses by providing this argument. All
            trigger values that are not within the responses are excluded from sample selection.
        keep_zeros: If true will keep responses with a value of zero for the sampling (if selected by a trigger),
            otherwise zero responses are excluded.

    Returns:
        Two one-dimensional list where the first are all valid triggers and the second the corresponding valid
        responses.
    """
    if len(triggers.shape) == 1 and len(responses.shape) == 1:
        indexes = where(logical_and(triggers != 0, logical_not(isnan(triggers))))[0] + offset
        indexes = align(indexes, mn=0, mx=len(responses))
        x, y = triggers[indexes - offset], responses[indexes]

    elif len(triggers.shape) == 2 and len(responses.shape) == 2:
        x, y = array([]), array([])
        for row in range(triggers.shape[0]):
            indexes = where(logical_and(triggers[row] != 0, logical_not(isnan(triggers[row]))))[0] + offset
            indexes = align(indexes, mn=0, mx=len(responses[row]))
            x, y = append(x, triggers[row][indexes - offset]), append(y, responses[row][indexes])

    else:
        raise DamageError('Only 1D or 2D arrays allowed')

    x, y = x[logical_not(isnan(y))], y[logical_not(isnan(y))]

    if not keep_zeros:
        x, y = x[y != 0], y[y != 0]

    return x, y


def spearmancor(
        triggers: NDArray[float],
        responses: NDArray[float],
        offset: int = 0,
        keep_zeros: bool = False,
        **kwargs
) -> Tuple[NDArray[float], float, float]:
    """Calculate a Spearman correlation coefficient for the provided triggers and responses.

    Requires a list of triggers, triggers that are not NaN or zero are used to select responses for the calculation of
    correlation coefficient.

    Args:
        triggers: A one or two-dimensional list of triggers used to select responses for the calculation of the
            correlation coefficient. Uses only non NaN values and values that are not equal to zero.
        responses: A one- or two-dimensional list of responses, NaN responses are excluded from the sampling for
            correlation.
        offset: The offset between the triggers and responses should be a negative or positive integer. If triggers and
            responses are not aligned, you may shift triggers to align with responses by providing this argument. All
            trigger values that are not within the responses are excluded from sample selection for the correlation.
        keep_zeros: If true will keep responses with a value of zero for the correlation calculation (if selected by
            a trigger), otherwise zero responses are excluded.
        **kwargs: Arguments for scipy.stats.spearmanr

    Returns:
        The samples used as parameter for spearmanr, Spearman rank-order correlation coefficient, and p-value.
    """
    x, y = _samples(triggers, responses, offset, keep_zeros)

    kwargs.update({'nan_policy': 'omit'})
    spearman, p_value = spearmanr(x, y, **kwargs)
    samples = append(
        reshape(x, newshape=(*x.shape, 1)),
        reshape(y, newshape=(*y.shape, 1)),
        axis=1
    )

    return samples, spearman, p_value


def pearsoncor(
        triggers: NDArray[float],
        responses: NDArray[float],
        offset: int = 0,
        keep_zeros: bool = False
) -> Tuple[NDArray[float], float, float]:
    """Calculate a Pearson correlation coefficient for the provided triggers and responses.

    Requires a list of triggers, triggers that are not NaN or zero are used to select responses for the calculation of
    correlation coefficient.

    Args:
        triggers: A one- or two-dimensional list of triggers used to select responses for the calculation of the
            correlation coefficient. Uses only non NaN values and values that are not equal to zero.
        responses: A one- or two list of responses, NaN responses are excluded from the sampling for correlation.
        offset: The offset between the triggers and responses should be a negative or positive integer. If triggers and
            responses are not aligned, you may shift triggers to align with responses by providing this argument. All
            trigger values that are not within the responses are excluded from sample selection for the correlation.
        keep_zeros: If true will keep responses with a value of zero for the correlation calculation (if selected by
            a trigger), otherwise zero responses are excluded.

    Returns:
        The samples used as parameter for pearsonr, pearson correlation coefficient, and p-value.
    """
    x, y = _samples(triggers, responses, offset, keep_zeros)
    pearson, p_value = pearsonr(x, y)
    samples = append(
        reshape(x, newshape=(*x.shape, 1)),
        reshape(y, newshape=(*y.shape, 1)),
        axis=1
    )

    return samples, pearson, p_value


class CorrelationResult:
    # noinspection PyUnresolvedReferences
    """This class envelops the results of a randomization run called either with spearmancor or pearsoncor.

    Attention a normal distribution is assumed therefore you must check if your results of the randomization fulfill
    the criteria.

    Attributes:
        result: The results of a randomization run either called with spearmancor or pearsoncor.
        mu: Statistical mean
        sd: Standard deviation
    """

    def __init__(self, result: List[Tuple[NDArray[float], float, float]]) -> None:
        self.samples: NDArray[float] = array([r[1] for r in result])

    @cached_property
    def mu(self) -> float:
        """Get the statistical mean."""
        return mean(self.samples)

    @cached_property
    def sd(self) -> float:
        """Get the standard deviation."""
        return std(self.samples)

    def z_score(self, x: float) -> float:
        """Calculate the z-score a sample.

        Returns:
            The z-score
        """
        return (x - self.mu) / self.sd


def randomization(
        x: NDArray[float],
        y: NDArray[float],
        func: Callable[[NDArray[float], NDArray[float], Any], Any],
        result: Callable[[List[Any]], Any] = CorrelationResult,
        repetitions: int = 1000,
        seed: int = None,
        **kwargs
) -> Any:
    """Performs significance test by sample randomization.

    Shuffles the x samples and runs the provided analysis function with the shuffled x samples.

    Args:
        x: A one- or two-dimensional list of samples. If x is one-dimensional the shuffling is performed over axis 0,
            otherwise over axis 1.
        y: A one- or two-dimensional list of samples unaltered forwarded to y.
        func: The analysis function to run with the shuffled x samples and un-shuffled y samples. The function should
            have the following signature func(x, y, **kwargs).
        result: Define your own function or class that calculates your required results from the randomized samples. As
            arguments, the provided context must accept a one-dimensional list with the returned values of func. If you
            provide your function or class, either it calls the provided function with the described parameters and
            returns the result, or it creates an instance of your class with the related parameter and returns it. On
            default, an instance of CorrelationResult is created and returned.
        repetitions: How often the shuffling should be repeated.
        seed: Seed for the random number generator.
        **kwargs: Forwarded to func.

    Returns:
        On default, an instance of CorrelationResult is returned. If you provide your function or class, either it
        returns the result of the function call, or it returns an instance of your class.

    Raises:
        DamageError: If dimensionality of x is greater than 2.
    """
    if len(x.shape) > 2:
        raise DamageError('Only 1D or 2D arrays allowed')

    if seed:
        mix = default_rng(seed).shuffle
    else:
        mix = default_rng().shuffle

    i = 0
    x = x.copy()
    results = list()

    while i < repetitions:
        if len(x.shape) == 2:
            for i in range(len(x)):
                mix(x[i])
        else:
            mix(x)

        results.append(func(x, y, **kwargs))
        i += 1

    return result(results)


def cor_reduction(
        func: spearmancor | pearsoncor
) -> Callable[[NDArray[float], NDArray[float], Dict[str, Any]], Any]:
    """A simple decorator to reduce the size of the data returned by the correlation functions."""

    def wrapped(triggers: NDArray[float], responses: NDArray[float], **kwargs) -> Tuple[None, float, None]:
        _, correlation, _ = func(triggers, responses, **kwargs)
        return None, correlation, None

    return wrapped


def transform(data: NDArray[float]) -> NDArray[float]:
    """Apply logistic transformation to the provided data.

    The data is transformed by applying the following function:

    d' = ln( 1 / ( -( 2 / ( d - 1 ) ) - 1 ) )

    Please note, the function has a sigmoid shape, midpoint (x₀) is centred at <0, 0>, growth rate k is 1,
    min is -1, and max is 1. Therefore, the function is undef for all values -1 >= d >= 1.

    Args:
        data: Transformation is applied on each value of the given numpy array.

    Returns:
        The transformed data.
    """
    return log(1 / (-(2 / (data - 1)) - 1))


def inverse(data: NDArray[float]) -> NDArray[float]:
    return (-2 / (1 + exp(-1 * data))) + 1


class DamageModel:
    def __init__(self, fit_intercept: bool = False) -> None:
        self.fit_intercept: bool = fit_intercept

        self._coef: None | NDArray[float] = None
        self._intercept: None | float = None

    @property
    def coef_(self) -> NDArray[float]:
        if self._coef is None:
            raise AttributeError("Model has no attribute coef_")
        return self._coef

    @coef_.setter
    def coef_(self, value: NDArray[float]) -> None:
        self._coef = value

    @property
    def intercept_(self) -> float:
        if self._intercept is None:
            raise AttributeError("Model has no attribute intercept_")
        return self._intercept

    @intercept_.setter
    def intercept_(self, value: float) -> None:
        self._intercept = value

    def fit(self, x: NDArray[int | float], y: NDArray[float]) -> DamageModel:
        x = x.copy()
        y = transform(y.copy())
        reg: LinearRegression = LinearRegression(fit_intercept=self.fit_intercept)
        reg.fit(x, y)

        obj: DamageModel = DamageModel(self.fit_intercept)
        obj._coef = reg.coef_
        obj._intercept = reg.intercept_

        return obj

    def predict(self, x: NDArray[float]) -> NDArray[float]:
        x = x * self.coef_ + self.intercept_
        return inverse(x)


def cross_validate_mae(estimator: DamageModel, x: NDArray[float], y: NDArray[float], cv: LeaveOneOut) -> float:
    """Calculate mean absolute error for cross validation."""
    n: int = cv.get_n_splits(x)
    mae: float = 0

    for train, test in cv.split(x, y):
        estimator = estimator.fit(x[train], y[train])
        predictions: NDArray[float] = estimator.predict(x[test])
        mae += abs(predictions - y[test]).sum()

    return mae / n

from sys import stderr
from sys import stdout
from typing import Any
from typing import List
from warnings import simplefilter

from click import File
from click import argument
from click import command
from click import option
from click import progressbar
from numpy import nan
from numpy.typing import NDArray
from pandas import DataFrame
from pandas import Series
from pandas import concat
from scipy.stats import PearsonRConstantInputWarning
from scipy.stats import SpearmanRConstantInputWarning
from sklearn.model_selection import LeaveOneOut
from statsmodels.api import OLS
from statsmodels.api import add_constant
from statsmodels.regression.linear_model import RegressionResults

from hazardly.damage.dea import DamageModel
from hazardly.damage.dea import cross_validate_mae
from hazardly.damage.dea import dea
from hazardly.damage.dea import spearmancor
from hazardly.damage.dea import transform
from hazardly.damage.errors import DamageError
from hazardly.utils.parse import parse_ranges
from hazardly.utils.parse import validate_labels

simplefilter('error', RuntimeWarning)
simplefilter('error', PearsonRConstantInputWarning)
simplefilter('error', SpearmanRConstantInputWarning)

X_MEAN = 'x_mean'
SS_xx = 'ss_xx'
DF = 'df'
S2 = 's2'
R2 = 'r2'
BETA0 = 'beta0'
SE_BETA0 = 'se_beta0'
T_BETA0 = 't_beta0'
P_BETA0 = 'p_beta0'
BETA1 = 'beta1'
SE_BETA1 = 'se_beta1'
T_BETA1 = 't_beta1'
P_BETA1 = 'p_beta1'
MAE = 'mae'

COLS = [SS_xx, DF, S2, R2, BETA0, SE_BETA0, T_BETA0, P_BETA0, MAE]
CONST_COLS = [X_MEAN, SS_xx, DF, S2, R2, BETA0, SE_BETA0, T_BETA0, P_BETA0, BETA1, SE_BETA1, T_BETA1, P_BETA1, MAE]


@command()
@option('--geoid', type=str, default='NUTS_ID', show_default=True, is_eager=True,
        help='Column label of the geo unit, used for matching entries between `YIELDS` and `EXTREMES`')
@option('--cropid', type=str, default='CROP_ID', show_default=True, is_eager=True,
        help='Column label of the  crop type, used for selecting data from `YIELDS`')
@option('--yy', type=str, default='1975-2020', show_default=True, is_eager=True, callback=parse_ranges(),
        help='Years (columns) to select from `YIELDS` for parameter estimation')
@option('--ey', type=str, default='1981-2020', show_default=True, is_eager=True, callback=parse_ranges(),
        help='Years (columns) to select from `EXTREMES` for parameter estimation')
@option('--const', is_flag=True,
        help='Run OLS with y = β₀ + β₁x')
@option('--cross_validate', is_flag=True,
        help='Calculate mean absolute error with one-leave-out cross validation')
@argument('yields', type=File(lazy=False), required=True, callback=validate_labels(['geoid', 'cropid', 'yy']))
@argument('extremes', type=File(lazy=False), required=True, callback=validate_labels(['geoid', 'ey']))
def functions(
        geoid: str,
        cropid: str,
        yy: List[str],
        ey: List[str],
        const: bool,
        cross_validate: bool,
        yields: DataFrame,
        extremes: DataFrame,
) -> None:
    """
    Estimates damage function parameters per `geoid` and `cropid` from the samples given in `YIELDS` and `EXTREMES`
    and print the result formatted as a CSV to STDOUT

    Parameters are estimated by applying an ordinary least squares regression, calculating the spearman regression
    coefficient before and excluding samples from estimation where the coefficient is greater than zero.

    The options `yy` and `ey` accept range definition strings, e.g. "1991,1993-1995,2000" converts to
    `[1991, 1993, 1994, 1995, 2000]`, interprets range as a closed interval.

    `YIELDS` and `EXTREMES` may be a path to a file or an input stream, e.g.,
    `functions <(xsv search -i -s "NUTS_ID" "AT1" "yields.csv") extremes.csv`.

    Input data must comply with CSV.
    """

    def row(values: List[Any]) -> Series:
        if const:
            return Series({k: v for k, v in zip([geoid, cropid] + CONST_COLS, values)})
        return Series({k: v for k, v in zip([geoid, cropid] + COLS, values)})

    result: DataFrame = DataFrame()
    offset: int = int(ey[0]) - int(yy[0])

    with progressbar(list(extremes.iterrows()), label='Regressing...', file=stderr) as bar:
        for _, extreme in bar:
            ets: NDArray[float] = extreme[ey].values.astype(float)
            nuts: str = extreme[geoid]

            rows: List[Series] = list()
            for _, yi in yields.loc[yields[geoid] == nuts, [cropid] + yy].iterrows():
                yts: NDArray[float] = yi[yy].values.astype(float)
                crop: str = yi[cropid]

                try:
                    damages: NDArray[float] = dea(yts).upper_relative_deviation
                    samples: NDArray[float]
                    coeff: float
                    samples, coeff, _ = spearmancor(ets, damages, offset=offset, keep_zeros=True)
                    x: NDArray[float] = samples[:, 0]
                    y: NDArray[float] = samples[:, 1]

                    if coeff < 0 and const:
                        m: RegressionResults = OLS(transform(y), add_constant(x)).fit()
                        mae: float = cross_validate_mae(
                            DamageModel(fit_intercept=True),
                            samples[:, 0].reshape((samples.shape[0], 1)),
                            samples[:, 1].reshape((samples.shape[0], 1)),
                            LeaveOneOut()
                        ) if cross_validate else nan
                        rows.append(row([
                            nuts, crop, x.mean(), ((x - x.mean()) ** 2).sum(), m.df_resid, m.mse_resid,
                            m.rsquared, m.params[0], m.bse[0], m.tvalues[0], m.pvalues[0], m.params[1],
                            m.bse[1], m.tvalues[1], m.pvalues[1], mae
                        ]))

                    elif coeff < 0:
                        m: RegressionResults = OLS(transform(y), x).fit()
                        mae: float = cross_validate_mae(
                            DamageModel(),
                            samples[:, 0].reshape((samples.shape[0], 1)),
                            samples[:, 1].reshape((samples.shape[0], 1)),
                            LeaveOneOut()
                        ) if cross_validate else nan
                        rows.append(row([
                            nuts, crop, (x ** 2).sum(), m.df_resid, m.mse_resid, m.rsquared, m.params[0],
                            m.bse[0], m.tvalues[0], m.pvalues[0], mae
                        ]))

                    else:
                        rows.append(row([nuts, crop] + [nan] * len(CONST_COLS)))

                except (DamageError, PearsonRConstantInputWarning, SpearmanRConstantInputWarning, RuntimeWarning):
                    rows.append(row([nuts, crop] + [nan] * len(CONST_COLS)))

            result = concat([result, DataFrame(rows)], ignore_index=True)

    result.to_csv(stdout, index=False)

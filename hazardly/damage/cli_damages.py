from sys import stderr
from sys import stdout
from typing import Any
from typing import Callable
from typing import List
from typing import Tuple

from click import File
from click import argument
from click import command
from click import option
from click import progressbar
from numpy import sqrt
from numpy.typing import NDArray
from pandas import DataFrame
from pandas import Series
from pandas import concat
from scipy.stats import t

from hazardly.damage.cli_functions import BETA0
from hazardly.damage.cli_functions import BETA1
from hazardly.damage.cli_functions import COLS
from hazardly.damage.cli_functions import CONST_COLS
from hazardly.damage.cli_functions import DF
from hazardly.damage.cli_functions import S2
from hazardly.damage.cli_functions import SS_xx
from hazardly.damage.cli_functions import X_MEAN
from hazardly.damage.dea import inverse
from hazardly.utils.parse import parse_ranges
from hazardly.utils.parse import validate_labels

METRIC_COL_NAME: str = 'METRIC'
MEAN: str = 'me'
CI_LOWER: str = '-ci'
CI_UPPER: str = '+ci'


@command()
@option('--geoid', type=str, default='NUTS_ID', show_default=True, is_eager=True,
        help='Column label of the geo unit, used for matching entries between `EXTREMES` and `FUNCTIONS`')
@option('--cropid', type=str, default='CROP_ID', show_default=True, is_eager=True,
        help='Column label of the crop type, used for selecting data from `FUNCTIONS`')
@option('--ey', type=str, default='1981-2020', show_default=True, is_eager=True, callback=parse_ranges(),
        help='Calculates damages for the selected years (columns) in `EXTREMES`')
@option('--q', type=float, default=.975, show_default=True,
        help='Width of the confidence interval around the predicted mean damage')
@argument('extremes', type=File(lazy=False), required=True, callback=validate_labels(['geoid', 'ey']))
@argument('functions', type=File(lazy=False), required=True, callback=validate_labels(['geoid', 'cropid'], COLS))
def damages(
        geoid: str,
        cropid: str,
        ey: List[str],
        q: float,
        extremes: DataFrame,
        functions: DataFrame,
) -> None:
    """
    Calculates yield damages of `EXTREMES` and parametrized damage functions in `FUNCTIONS` per `geoid` and `cropid` and
    prints the result formatted as a CSV to STDOUT

    The option `ey` accepts range definition strings, e.g. "1991,1993-1995,2000" converts to
    `[1991, 1993, 1994, 1995, 2000]`, interprets range as a closed interval.

    `EXTREMES` and `FUNCTIONS` may be a path to a file or an input stream, e.g.,
    `damages <(xsv search -i -s "NUTS_ID" "AT1" "extremes.csv") functions.csv`.

    Input data must comply with CSV.
    """
    const: bool = len(functions.columns) > len(CONST_COLS)

    def damagefunc(
            parameter: Series
    ) -> Callable[[Series], Tuple[List[str], NDArray[float], NDArray[float], NDArray[float]]]:
        beta0: float = parameter[BETA0]
        df: float = parameter[DF]
        s2: float = parameter[S2]
        SSxx: float = parameter[SS_xx]

        if const:
            n: float = df + 2
            beta1: float = parameter[BETA1]
            xmean: float = parameter[X_MEAN]
            f: Callable[[NDArray[float]], NDArray[float]] = lambda x: beta0 + beta1 * x
            lower: Callable[[NDArray[float]], NDArray[float]] = lambda x, y: y - t.ppf(q=q, df=df) * sqrt(s2) * sqrt(
                1 + 1 / n + (x - xmean) ** 2 / SSxx)
            upper: Callable[[NDArray[float]], NDArray[float]] = lambda x, y: y + t.ppf(q=q, df=df) * sqrt(s2) * sqrt(
                1 + 1 / n + (x - xmean) ** 2 / SSxx)

        else:
            f: Callable[[NDArray[float]], NDArray[float]] = lambda x: beta0 * x
            lower: Callable[[NDArray[float]], NDArray[float]] = lambda x, y: y - t.ppf(q=q, df=df) * sqrt(s2) * sqrt(
                1 + x ** 2 / SSxx)
            upper: Callable[[NDArray[float]], NDArray[float]] = lambda x, y: y + t.ppf(q=q, df=df) * sqrt(s2) * sqrt(
                1 + x ** 2 / SSxx)

        def wrapped(
                extreme_row: Series
        ) -> Tuple[List[str], NDArray[float], NDArray[float], NDArray[float]]:
            x: NDArray[float] = extreme_row[ey].values.astype(float)
            y: NDArray[float] = f(x)

            l: NDArray[float] = lower(x, y)
            u: NDArray[float] = upper(x, y)

            return ey, inverse(y) * -1, inverse(l) * -1, inverse(u) * -1

        return wrapped

    def row(values: List[Any]) -> Series:
        return Series({k: v for k, v in zip([geoid, cropid, METRIC_COL_NAME], values)})

    result: DataFrame = DataFrame()
    with progressbar(list(extremes.iterrows()), label='Calculating...', file=stderr) as bar:
        for _, extreme in bar:
            nuts: str = extreme[geoid]

            rows: List[Series] = list()
            for _, paras in functions.loc[functions[geoid] == nuts].iterrows():
                crop: str = paras[cropid]
                year: List[str]
                damage: NDArray[float]
                ci_lower: NDArray[float]
                ci_upper: NDArray[float]
                year, damage, ci_lower, ci_upper = damagefunc(paras)(extreme)

                rows.append(row([nuts, crop, CI_LOWER]).append(Series(ci_lower, index=year)))
                rows.append(row([nuts, crop, MEAN]).append(Series(damage, index=year)))
                rows.append(row([nuts, crop, CI_UPPER]).append(Series(ci_upper, index=year)))

            result = concat([result, DataFrame(rows)])

    result.to_csv(stdout, index=False)

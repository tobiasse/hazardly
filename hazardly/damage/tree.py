from __future__ import annotations

from typing import Callable
from typing import List
from typing import Tuple

from hazardly.damage.errors import DamageError


class _DecisionNode:
    def __init__(
            self,
            threshold: int | float,
            comparison: Callable[[int | float, int | float], bool],
            name: int | str,
    ) -> None:
        self.name: int | str = name
        self.threshold: int | float = threshold

        self._next: _DecisionNode | None = None
        self._comparison: Callable[[int | float, int | float], bool] = comparison

    def compare(self, values: List[int | float], idx: int) -> Tuple[bool, str]:
        result: bool = self._comparison(self.threshold, values[idx])

        if result and self._next:
            return self._next.compare(values, idx + 1)
        elif result:
            return result, ''
        else:
            return result, str(self.name)

    def add_node(self, node: '_DecisionNode') -> None:
        if self._next:
            self._next.add_node(node)
        else:
            self._next = node


class DecisionTree:
    """Class may be used to create binary decision trees.

    Use this class to evaluate variable sized binary decisions (yes/no) of the form 'if x1 > t1 and x2 < t2 and ...'.
    x1 > t1
    |   |
    f   x2 < t2
        |   |
        f   x2 > t3
            |   |
            f   t
    """

    def __init__(self) -> None:
        self._depth: int = 0
        self._root: _DecisionNode | None = None

    def compare(self, values: List[int | float]) -> Tuple[bool, str]:
        """Evaluates the decision tree against a list of values.

        The tree is evaluated in FIFO order, e.g., first node added is evaluated
        with the first value in the list and so on.
        >>> tree = DecisionTree().add_node('>', 1).add_node('<', 1)
        >>> result, msg = tree.compare([2, 0])
        >>> assert result

        Args:
            values: A list of values that should be evaluated against the decision tree.
                The number of values must match the depth of the tree (number of nodes in the tree).

        Returns:
            A boolean and a string, either true and an empty string if all nodes evaluate to true or false and the name
            from the first node that evaluates to false.

        Raises:
            DamageError, if the number of values does not match the depth of the tree or the tree
            was never initialiazed.
        """
        if len(values) != self._depth:
            raise DamageError('Not enough values to evaluate')

        if self._root:
            return self._root.compare(values, 0)
        else:
            raise DamageError('Missing node')

    def add_node(self, op: str, threshold: int | float, name: None | str | int = None) -> 'DecisionTree':
        """Adds a new decision node to the decision tree.

        You may build a decision tree by chained method calls:
        >>> tree = DecisionTree().add_node('>', 1).add_node('<', 1)
        or by separate method calls:
        >>> tree = DecisionTree()
        >>> tree.add_node('>', 1)  # Represents the decision x > threshold, x > 1
        >>> tree.add_node('<', 1)  # Represents the decision x < threshold, x < 1

        Args:
            op: The comparison operation of the node, one of '>', '<', '>=', '<=', '==', or '!='.
            threshold: The threshold used in the comparison.
            name: Identification of the node.

        Returns:
            DecisionTree instance
        """
        if not name:
            name: int = self._depth

        if op == '>':
            node = _DecisionNode(threshold, DecisionTree._gt, name)
        elif op == '<':
            node = _DecisionNode(threshold, DecisionTree._lt, name)
        elif op == '>=':
            node = _DecisionNode(threshold, DecisionTree._ge, name)
        elif op == '<=':
            node = _DecisionNode(threshold, DecisionTree._le, name)
        elif op == '==':
            node = _DecisionNode(threshold, DecisionTree._eq, name)
        elif op == '!=':
            node = _DecisionNode(threshold, DecisionTree._ne, name)
        else:
            raise DamageError('Illegal operator!')

        if self._root:
            self._root.add_node(node)
        else:
            self._root = node

        self._depth += 1
        return self

    @staticmethod
    def _gt(threshold: int | float, value: int | float) -> bool:
        return threshold < value

    @staticmethod
    def _lt(threshold: int | float, value: int | float) -> bool:
        return threshold > value

    @staticmethod
    def _ge(threshold: int | float, value: int | float) -> bool:
        return threshold <= value

    @staticmethod
    def _le(threshold: int | float, value: int | float) -> bool:
        return threshold >= value

    @staticmethod
    def _eq(threshold: int | float, value: int | float) -> bool:
        return threshold == value

    @staticmethod
    def _ne(threshold: int | float, value: int | float) -> bool:
        return threshold != value

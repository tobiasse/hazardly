from sys import stderr
from sys import stdout
from typing import Any
from typing import Callable
from typing import List
from typing import Tuple
from typing import Union
from warnings import simplefilter

from click import Choice
from click import File
from click import argument
from click import command
from click import option
from click import progressbar
from numpy import nan
from numpy.typing import NDArray
from pandas import DataFrame
from pandas import Series
from pandas import concat
from scipy.stats import PearsonRConstantInputWarning
from scipy.stats import SpearmanRConstantInputWarning

from hazardly.damage.dea import cor_reduction
from hazardly.damage.dea import dea
from hazardly.damage.dea import pearsoncor
from hazardly.damage.dea import randomization
from hazardly.damage.dea import spearmancor
from hazardly.damage.errors import DamageError
from hazardly.utils.parse import parse_ranges
from hazardly.utils.parse import validate_labels

simplefilter('error', PearsonRConstantInputWarning)
simplefilter('error', SpearmanRConstantInputWarning)

CORR_COL_NAME: str = 'coeff'
ZSCORE_COL_NAME: str = 'zscore'

CorrFunc = Callable[[NDArray[float], NDArray[float], int, bool], Tuple[NDArray[float], float, float]]


@command()
@option('--geoid', type=str, default='NUTS_ID', show_default=True, is_eager=True,
        help='Column label of the geo unit, used for matching entries between `EXTREMES` and `YIELDS`')
@option('--cropid', type=str, default='CROP_ID', show_default=True, is_eager=True,
        help='Column label of the crop type, used for selecting data from `YIELDS`')
@option('--yy', type=str, default='1975-2020', show_default=True, is_eager=True, callback=parse_ranges(),
        help='Years (columns) to select from `YIELDS` for correlation analysis')
@option('--ey', type=str, default='1981-2020', show_default=True, is_eager=True, callback=parse_ranges(),
        help='Years (columns) to select from `EXTREMES` for correlation analysis')
@option('--method', type=Choice(['spearman', 'pearson']), default='spearman', show_default=True,
        help='Correlation method')
@option('--zscore', is_flag=True,
        help='Calculate Z-score')
@option('--repetitions', type=int, default=1000, show_default=True,
        help='Shuffling repetitions')
@option('--seed', type=int,
        help='Shuffling seed')
@argument('yields', type=File(lazy=False), required=True, callback=validate_labels(paras=['geoid', 'cropid', 'yy']))
@argument('extremes', type=File(lazy=False), required=True, callback=validate_labels(paras=['geoid', 'ey']))
def correlations(
        geoid: str,
        cropid: str,
        yy: List[str],
        ey: List[str],
        method: str,
        zscore: bool,
        repetitions: int,
        seed: int,
        yields: DataFrame,
        extremes: DataFrame,
) -> None:
    """
    Calculates correlations between `EXTREMES` and `YIELDS` per `geoid` and `cropid` and prints the result formatted as
    a CSV to STDOUT

    Yield is normalized by calculating yearly relative damage, while relative damage is the deviation from the
    convex-hull enclosing the data. If `zscore`  is set, the score is calculated by repeatedly (`repetitions`)
    shuffling the extremes and calculating the correlation coefficient.  Next, it uses the mean and standard deviation
    of the randomized population and the predicted correlation coefficient to derive the standard score.

    The options `yy` and `ey` accept range definition strings, e.g. "1991,1993-1995,2000" converts to
    `[1991, 1993, 1994, 1995, 2000]`, interprets range as a closed interval.

    `YIELDS` and `EXTREMES` may be a path to a file or an input stream, e.g.,
    `correlations <(xsv search -i -s "NUTS_ID" "AT1" "yields.csv") extremes.csv`.

    Input data must comply with CSV.
    """

    def row(values: List[Any]) -> Series:
        if zscore:
            return Series({k: v for k, v in zip([geoid, cropid, CORR_COL_NAME, ZSCORE_COL_NAME], values)})
        return Series({k: v for k, v in zip([geoid, cropid, CORR_COL_NAME], values)})

    result: DataFrame = DataFrame()
    offset: int = int(ey[0]) - int(yy[0])
    corrfunc: CorrFunc = spearmancor if method == 'spearman' else pearsoncor

    with progressbar(list(extremes.iterrows()), label='Correlating...', file=stderr) as bar:
        for _, extreme in bar:
            ets: NDArray[float] = extreme[ey].values.astype(float)
            nuts: str = extreme[geoid]

            rows: List[Series] = list()
            for _, yi in yields.loc[yields[geoid] == nuts, [cropid] + yy].iterrows():
                yts: NDArray[float] = yi[yy].values.astype(float)
                crop: str = yi[cropid]

                try:
                    damages: NDArray[float] = dea(yts).upper_relative_deviation
                    coeff: float
                    _, coeff, _ = corrfunc(ets, damages, offset, True)
                    z: Union[float, nan] = randomization(ets, damages, func=cor_reduction(corrfunc),
                                                         repetitions=repetitions, seed=seed, offset=offset,
                                                         keep_zeros=True).z_score(coeff) if zscore else nan
                    rows.append(row([nuts, crop, coeff, z]))

                except (DamageError, PearsonRConstantInputWarning, SpearmanRConstantInputWarning):
                    rows.append(row([nuts, crop, nan, nan]))

            result = concat([result, DataFrame(rows)], ignore_index=True)

    result.to_csv(stdout, index=False)

from __future__ import annotations

from sys import stderr
from sys import stdout
from typing import Any
from typing import List
from typing import Tuple

from click import File
from click import argument
from click import command
from click import option
from click import progressbar
from pandas import DataFrame
from pandas import Series
from pandas import concat

from hazardly.damage.cli_correlations import CORR_COL_NAME
from hazardly.damage.cli_correlations import ZSCORE_COL_NAME
from hazardly.damage.cli_functions import R2
from hazardly.damage.tree import DecisionTree
from hazardly.utils.parse import parse_decision
from hazardly.utils.parse import validate_labels

DECISION_COL_NAME: str = 'decision'
COLS: List[str] = [CORR_COL_NAME, ZSCORE_COL_NAME]


@command()
@option('--geoid', type=str, default='NUTS_ID', show_default=True, is_eager=True,
        help='Column label of the geo unit, used for matching entries of `COVERAGES`, `CORRELATIONS`, and `FUNCTIONS`')
@option('--cropid', type=str, default='CROP_ID', show_default=True, is_eager=True,
        help='Column label of the crop type, used for matching entries between `CORRELATIONS` and `FUNCTIONS`')
@option('--coverage', type=str, default='>=.4', show_default=True, callback=parse_decision,
        help='Coverage threshold')
@option('--correlation', type=str, default='<=-.4', show_default=True, callback=parse_decision,
        help='Correlation coefficient threshold')
@option('--significance', type=str, default='<=-1.96', show_default=True, callback=parse_decision,
        help='Correlation z-score threshold')
@option('--r2', type=str, default='>=.5', show_default=True, callback=parse_decision,
        help='R2 threshold of the OLS')
@option('--reporting', is_flag=True, help='Prints to STDERR a detailed report of the decisions')
@argument('coverages', type=File(lazy=False), required=True, callback=validate_labels(['geoid']))
@argument('correlations', type=File(lazy=False), required=True, callback=validate_labels(['geoid', 'cropid'], COLS))
@argument('functions', type=File(lazy=False), required=True, callback=validate_labels(['geoid', 'cropid'], [R2]))
def decisions(
        geoid: str,
        cropid: str,
        coverage: Tuple[str, int | float],
        correlation: Tuple[str, int | float],
        significance: Tuple[str, int | float],
        r2: Tuple[str, int | float],
        reporting: bool,
        coverages: DataFrame,
        correlations: DataFrame,
        functions: DataFrame,
) -> None:
    """
    Decides if each data record per `geoid` and `cropid` is within the defined thresholds and prints the result
    formatted as a CSV to STDOUT

    The options `coverage`, `correlation`, `significance`, and `r2` accept threshold definitions in the following
    format: `OPERATOR,NUMBER` where `OPERATOR` is one of <, <=, >, >=, !=, == and `NUMBER` is any integer of real
    number.

    `COVERAGES`, `CORRELATIONS`, and `FUNCTIONS` may be a path to a file or an input stream, e.g.,
    `decisions <(xsv search -i -s "NUTS_ID" "AT1" "coverages.csv") correlations.csv functions.csv`.

    Input data must comply with CSV.
    """

    def row(values: List[Any]) -> Series:
        return Series({k: v for k, v in zip([geoid, cropid, DECISION_COL_NAME], values)})

    tree: DecisionTree = DecisionTree().add_node(*coverage, 'Coverage').add_node(*correlation, 'Correlation').add_node(
        *significance, 'Z-score').add_node(*r2, 'R2')
    report: DataFrame = DataFrame({'Coverage': 0, 'Correlation': 0, 'Z-score': 0, 'R2': 0, '': 0}, dtype=int, index=[0])

    coverages.set_index(geoid, inplace=True)

    result: DataFrame = DataFrame()
    with progressbar(list(coverages.iterrows()), label='Deciding...', file=stderr) as bar:
        for nuts, series in bar:
            rows: List[Series] = list()
            for crop, cov in series.items():
                try:
                    coeff: int | float = correlations.loc[
                        (correlations[geoid] == nuts) & (correlations[cropid] == crop), CORR_COL_NAME].values[0]
                    z: int | float = correlations.loc[
                        (correlations[geoid] == nuts) & (correlations[cropid] == crop), ZSCORE_COL_NAME].values[0]
                    r2: int | float = functions.loc[
                        (functions[geoid] == nuts) & (functions[cropid] == crop), R2].values[0]

                    msg: str
                    res: bool
                    res, msg = tree.compare([cov, coeff, z, r2])

                    rows.append(row([nuts, crop, res]))
                    report[msg] += 1

                except IndexError:
                    rows.append(row([nuts, crop, False]))

            result = concat([result, DataFrame(rows)], ignore_index=True)

    result.to_csv(stdout, index=False)

    if reporting:
        report.rename(columns={'': 'Success'}, inplace=True)
        report.to_csv(stderr, index=False)

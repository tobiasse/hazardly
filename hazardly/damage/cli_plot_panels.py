from __future__ import annotations

import matplotlib

matplotlib.use('Agg')

from collections import namedtuple
from os.path import join
from sys import exit
from sys import stderr
from typing import Callable
from typing import List
from typing import Tuple
from typing import Union

import matplotlib.pyplot as plt
import pandas as pd
from cartopy.crs import PlateCarree
from cartopy.feature import LAND
from click import File
from click import Path
from click import argument
from click import command
from click import option
from click import progressbar
from geopandas import GeoDataFrame
from geopandas import read_file
from matplotlib.axes import Axes
from matplotlib.cm import ScalarMappable
from matplotlib.colors import BoundaryNorm
from matplotlib.figure import Figure
from matplotlib.gridspec import GridSpec
from matplotlib.pyplot import get_cmap
from matplotlib.transforms import ScaledTranslation
from numpy import arange
from numpy import array
from numpy import append
from numpy import convolve
from numpy import ones
from numpy import sqrt
from numpy.typing import NDArray
from pandas import DataFrame
from pandas import Series
from scipy.stats import t
from shapely.geometry import MultiPolygon
from shapely.geometry import Point
from shapely.geometry import Polygon

from hazardly.damage.cli_damages import MEAN
from hazardly.damage.cli_functions import BETA0
from hazardly.damage.cli_functions import BETA1
from hazardly.damage.cli_functions import COLS
from hazardly.damage.cli_functions import CONST_COLS
from hazardly.damage.cli_functions import DF
from hazardly.damage.cli_functions import S2
from hazardly.damage.cli_functions import SS_xx
from hazardly.damage.cli_functions import X_MEAN
from hazardly.damage.dea import YieldDamage
from hazardly.damage.dea import _samples
from hazardly.damage.dea import dea
from hazardly.damage.dea import inverse
from hazardly.damage.errors import DamageError
from hazardly.utils.parse import parse_ranges
from hazardly.utils.parse import validate_labels

Options = namedtuple('Options', ['color', 'linestyle', 'alpha', 'zorder', 'label', 'median', 'text'])
base = Options(color='black', linestyle='-', alpha=.3, zorder=2, label='Historic', median='white', text='')
opts = [
    Options(color='#2b83ba', linestyle='-', alpha=.3, zorder=1, label='RCP2.6', median='black', text='A'),
    Options(color='#abdda4', linestyle='-', alpha=.3, zorder=1, label='RCP4.5', median='black', text='B'),
    Options(color='#fdae61', linestyle='-', alpha=.3, zorder=1, label='RCP8.5', median='black', text='C'),
]

PlottingFunc = Callable[[Axes, str, str], None]


def plot_convex(
        yields: pd.DataFrame,
        geoid: str,
        cropid: str,
        yy: List[str]
) -> Callable[[Axes, str, str], None]:
    """Plot

    Args:
        yields: Yield dataframe.
        geoid: Column name of the geo ids.
        cropid: Column name of the crop ids.
        yy: Years to consider for convex-hull analysis and subsequent plotting.

    Returns:
        Convex-hull plotting function.
    """
    years: NDArray[int] = array(yy, dtype=int)

    def wrapped(ax: Axes, nuts: str, crop: str) -> None:
        """Convex-hull plotting function.

        Args:
            ax: Plot is drawn on the provided ax.
            nuts: Geoid of the current row to plot.
            crop: Cropid of the current row to plot.
        """
        yi: NDArray[float] = yields.loc[(yields[geoid] == nuts) & (yields[cropid] == crop), yy].values[0].astype(float)

        try:
            damages: YieldDamage = dea(yi)
            spines_x: NDArray[int] = years[damages.upper[:, 0].astype(int)]
            spines_y: NDArray[float] = damages.upper[:, 1]

            ax.plot(spines_x, spines_y, linestyle='--', color='black')
            ax.scatter(years, yi, fc='white', ec='black', marker='o', zorder=5, s=15)

            ax.set_ylim(ymin=0)

            ax.set_xlabel('Year')
            ax.set_ylabel('Yield [t/ha]')

        except DamageError:
            raise DamageError

    return wrapped


def plot_function(
        functions: pd.DataFrame,
        geoid: str,
        cropid: str,
        dy: int,
        q: float,
        const: bool,
) -> Callable[[Axes, str, str], None]:
    """

    Args:
        functions: Damage function parameters dataframe.
        geoid: Column name of the geo ids.
        cropid: Columns name of the crop ids.
        dy: X-axis length (number of exceedance days).
        q: Width of the confidence interval around the predicted damage.
        const: If true an OLS with constant is assumed.

    Returns:
        Damage function plotting function.
    """
    days: NDArray[float] = arange(0, dy)

    def wrapped(ax: Axes, nuts: str, crop: str) -> None:
        """Damage function plotting function.

        Args:
            ax: Plot is drawn on the provided ax.
            nuts: Geoid of the current row to plot.
            crop: Cropid of the current row to plot.
        """
        func: Series = functions.loc[(functions[geoid] == nuts) & (functions[cropid] == crop)].iloc[0]

        df: float
        s2: float
        SSxx: float
        xmean: float
        beta0: float
        beta1: float

        y: NDArray[float]
        lower: NDArray[float]
        upper: NDArray[float]

        if const:
            beta0, beta1, df, s2, SSxx, xmean = func[BETA0], func[BETA1], func[DF], func[S2], func[SS_xx], func[X_MEAN]
            y = beta0 + beta1 * days
            lower = y - t.ppf(q=q, df=df) * sqrt(s2) * sqrt(1 + 1 / (df + 2) + (days - xmean) ** 2 / SSxx)
            upper = y + t.ppf(q=q, df=df) * sqrt(s2) * sqrt(1 + 1 / (df + 2) + (days - xmean) ** 2 / SSxx)

        else:
            beta0, df, s2, SSxx = func[BETA0], func[DF], func[S2], func[SS_xx]
            y = beta0 * days
            lower = y - t.ppf(q=q, df=df) * sqrt(s2) * sqrt(1 + days ** 2 / SSxx)
            upper = y + t.ppf(q=q, df=df) * sqrt(s2) * sqrt(1 + days ** 2 / SSxx)

        ax.plot(days, -1 * inverse(y), color='black', zorder=1)
        ax.fill_between(days, -1 * inverse(lower), -1 * inverse(upper), alpha=.3, color='black', ec='none')

        ax.set_xlim(xmin=0, xmax=dy)
        ax.set_ylim(ymin=0, ymax=1)

        ax.yaxis.tick_right()
        ax.yaxis.set_label_position('right')

        ax.set_xlabel('Exceedance days')
        ax.set_ylabel('Damage')

    return wrapped


def plot_scatter(
        extremes: pd.DataFrame,
        yields: pd.DataFrame,
        geoid: str,
        cropid: str,
        yy: List[str],
        ey: List[str]
) -> Callable[[Axes, str, str], None]:
    """

    Args:
        extremes: Yearly weather extremes dataframe.
        yields: Yield dataframe.
        geoid: Column name of the geo ids.
        cropid: Column name of the crop ids.
        yy: Years to consider for convex-hull analysis and subsequent plotting.
        ey: Years to consider for weather-extremes data selection and subsequent plotting.

    Returns:
        Yield damages plotting function.
    """
    offset: int = int(ey[0]) - int(yy[0])

    def wrapped(ax: Axes, nuts: str, crop: str) -> None:
        """Yield damages plotting function.

        Args:
            ax: Plot is drawn on the provided ax.
            nuts: Geoid of the current row to plot.
            crop: Cropid of the current row to plot.
        """
        yi: NDArray[float] = yields.loc[(yields[geoid] == nuts) & (yields[cropid] == crop), yy].values[0].astype(float)
        ex: NDArray[float] = extremes.loc[extremes[geoid] == nuts, ey].values[0].astype(float)

        damages: NDArray[float] = dea(yi).upper_relative_deviation

        x: NDArray[float]
        y: NDArray[float]
        x, y = _samples(ex, damages, offset, keep_zeros=True)

        ax.scatter(x, -1 * y, fc='white', ec='black', marker='o', zorder=2, s=15)

        ax.set_xlim(xmax=x.max() + 1)

    return wrapped


@command()
@option('--geoid', default='NUTS_ID', show_default=True, type=str, is_eager=True,
        help='Column used for matching data rows of yields, functions and extremes.')
@option('--cropid', default='CROP_ID', show_default=True, type=str, is_eager=True,
        help='Columns used for matching different crop rows in yields and functions.')
@option('--yy', default='1975-2020', show_default=True, type=str, callback=parse_ranges(), is_eager=True,
        help='Yield years, values from these columns are selected for convex-hull analysis.')
@option('--ey', default='1981-2020', show_default=True, type=str, callback=parse_ranges(), is_eager=True,
        help='Extreme years, values from the these columns are selected for correlation analysis.')
@option('--dy', default=365, show_default=True, type=int,
        help='X-axis length of the damage function plot (no effect when extremes are available).')
@option('--q', default=.975, show_default=True, type=float,
        help='Width of the confidence interval around the predicted damage.')
@option('--figsize', default=(6, 3), nargs=2, show_default=True, type=float,
        help='Size of the figure.')
@option('--yields', required=False, type=File(lazy=False), callback=validate_labels(['geoid', 'cropid', 'yy']),
        help='A yield data CSV file, stores a timeseries of yield per geoid and cropid.')
@option('--functions', required=False, type=File(lazy=False), callback=validate_labels(['geoid', 'cropid'], COLS),
        help='A functions CSV file, stores function parameters per geoid and cropid.')
@option('--extremes', required=False, type=File(lazy=False), callback=validate_labels(['geoid', 'ey']),
        help='A extremes CSV file, stores exceedance days per year and geoid of one weather extreme.')
@argument('path', required=True, type=Path(exists=True, file_okay=False))
def panel_one(
        geoid: str,
        cropid: str,
        yy: List[str],
        ey: List[str],
        dy: int,
        q: float,
        figsize: Tuple[float, float],
        yields: Union[DataFrame, None],
        functions: Union[DataFrame, None],
        extremes: Union[DataFrame, None],
        path: str
) -> None:
    """Plot panel one convex-hull and damage function."""
    const: bool

    keys: DataFrame = DataFrame()
    plots: List[PlottingFunc] = list()
    scatter: Union[PlottingFunc, None] = None

    if isinstance(yields, DataFrame) and isinstance(functions, DataFrame) and isinstance(extremes, DataFrame):
        const = len(functions.columns) > len(CONST_COLS)
        plots = [
            plot_convex(yields, geoid, cropid, yy),
            plot_function(functions, geoid, cropid, dy, q, const)
        ]
        scatter = plot_scatter(extremes, yields, geoid, cropid, yy, ey)
        keys: DataFrame = yields

    elif isinstance(yields, DataFrame) and isinstance(functions, DataFrame):
        const = len(functions.columns) > len(CONST_COLS)
        plots = [
            plot_convex(yields, geoid, cropid, yy),
            plot_function(functions, geoid, cropid, dy, q, const)
        ]
        keys = yields

    elif isinstance(yields, DataFrame):
        plots = [plot_convex(yields, geoid, cropid, yy)]
        keys = yields

    elif isinstance(functions, DataFrame):
        const = len(functions.columns) > len(CONST_COLS)
        plots = [plot_function(functions, geoid, cropid, dy, q, const)]
        keys = functions

    else:
        exit(0)

    with progressbar(list(keys.iterrows()), label='Plotting...', file=stderr) as bar:
        for _, row in bar:
            nuts: str = row[geoid]
            crop: str = row[cropid]

            fig: Figure
            axes: NDArray[Axes]
            fig, axes = plt.subplots(ncols=len(plots), squeeze=False, figsize=figsize)
            dx: float = 5 / fig.dpi
            dy: float = -15 / fig.dpi
            offset: ScaledTranslation = ScaledTranslation(dx, dy, fig.dpi_scale_trans)

            try:
                for ax, func, label in zip(axes[0], plots, ['A', 'B']):
                    func(ax, nuts, crop)
                    ax.text(0, 1, label, transform=ax.transAxes + offset)

                if scatter:
                    scatter(axes[0][-1], nuts, crop)

                plt.tight_layout()
                fig.savefig(join(path, f'{nuts}_{crop}.svg'))

            except DamageError:
                pass

            plt.close(fig=fig)


@command()
@option('--geoid', default='NUTS_ID', show_default=True, type=str, is_eager=True,
        help='Column used for matching data rows of functions and extremes.')
@option('--cropid', default='CROP_ID', show_default=True, type=str, is_eager=True,
        help='Columns used for matching different crop rows in yields and functions.')
@option('--ey', default='1981-2100', show_default=True, type=str, callback=parse_ranges(),
        help='Extreme years, values from the these columns are selected for correlation analysis.')
@option('--q', default=.975, show_default=True, type=float,
        help='Width of the confidence interval around the predicted damage.')
@option('--window', default=31, show_default=True, type=int,
        help='Size of the window for calculating the rolling mean.')
@option('--functions', required=True, type=File(lazy=False), callback=validate_labels(['geoid', 'cropid'], COLS),
        help='A functions CSV file, stores function parameters per geoid and cropid.')
@option('--historic', required=True, type=File(lazy=False), callback=validate_labels(['geoid']),
        help='Historic weather extremes CSV file.')
@option('--projections', required=True, type=File(lazy=False), multiple=True, callback=validate_labels(['geoid']),
        help='Projected weather extremes CSV file.')
@argument('path', required=True, type=Path(exists=True, file_okay=False))
def panel_two(
        geoid: str,
        cropid: str,
        ey: List[str],
        q: int,
        window: int,
        functions: DataFrame,
        historic: DataFrame,
        projections: Tuple[DataFrame, ...],
        path: str
) -> None:
    """Plot panel two damage time-series and boxplot."""
    const: bool = len(functions.columns) > len(CONST_COLS)

    def damages(
            parameter: Series
    ) -> Callable[
        [Series], Tuple[NDArray[int], NDArray[float], NDArray[float], NDArray[float], NDArray[int], NDArray[float]]]:
        df: float = parameter[DF]
        s2: float = parameter[S2]
        SSxx: float = parameter[SS_xx]
        beta0: float = parameter[BETA0]

        if const:
            n: float = df + 2
            xmean: float = parameter[X_MEAN]
            beta1: float = parameter[BETA1]
            f: Callable[[NDArray[float]], NDArray[float]] = \
                lambda x: beta0 + beta1 * x
            lower: Callable[[NDArray[float], NDArray[float]], NDArray[float]] = \
                lambda x, y: y - t.ppf(q=q, df=df) * sqrt(s2) * sqrt(1 + 1 / n + (x - xmean) ** 2 / SSxx)
            upper: Callable[[NDArray[float], NDArray[float]], NDArray[float]] = \
                lambda x, y: y + t.ppf(q=q, df=df) * sqrt(s2) * sqrt(1 + 1 / n + (x - xmean) ** 2 / SSxx)

        else:
            f: Callable[[NDArray[float]], NDArray[float]] = \
                lambda x: beta0 * x
            lower: Callable[[NDArray[float], NDArray[float]], NDArray[float]] = \
                lambda x, y: y - t.ppf(q=q, df=df) * sqrt(s2) * sqrt(1 + x ** 2 / SSxx)
            upper: Callable[[NDArray[float], NDArray[float]], NDArray[float]] = \
                lambda x, y: y + t.ppf(q=q, df=df) * sqrt(s2) * sqrt(1 + x ** 2 / SSxx)

        def wrapped(
                extreme: Series
        ) -> Tuple[NDArray[int], NDArray[float], NDArray[float], NDArray[float], NDArray[int], NDArray[float]]:
            years: List[str] = sorted(set(ey) & set(extreme.index))
            x: NDArray[float] = extreme[years].values.astype(float)

            y: NDArray[float] = f(x)
            l: NDArray[float] = lower(x, y)
            u: NDArray[float] = upper(x, y)

            y, l, u = inverse(y) * -1, inverse(l) * -1, inverse(u) * -1

            rx: List[str] = years[window // 2:len(years) - window // 2]
            ry: NDArray[float] = convolve(y, ones(window), 'valid') / window

            return array(years, dtype=int), y, l, u, array(rx, dtype=int), ry

        return wrapped

    with progressbar(list(functions.iterrows()), label='Plotting...', file=stderr) as bar:
        for _, row in bar:
            nuts: str = row[geoid]
            crop: str = row[cropid]

            func = damages(row)
            hx, hy, hl, hu, *_ = func(historic.loc[historic[geoid] == nuts].iloc[0])
            ys: List[NDArray[float]] = [hy]

            fig: Figure = plt.figure()
            grid: GridSpec = GridSpec(len(projections), 2, figure=fig)
            dx: float = 5 / fig.dpi
            dy: float = -15 / fig.dpi
            offset: ScaledTranslation = ScaledTranslation(dx, dy, fig.dpi_scale_trans)

            ax: Union[Axes, None] = None
            axes: List[Axes] = list()
            for i, (opt, p) in enumerate(zip(opts, projections)):
                px, py, pl, pu, prx, pry = func(p.loc[p[geoid] == nuts].iloc[0])
                ys.append(py)

                ax: Axes = fig.add_subplot(grid[i, 0], ylim=(0, 1), xlim=(1981, 2100), sharex=ax)
                axes.append(ax)

                ax.plot(hx, hy, color=base.color, zorder=base.zorder, solid_capstyle='round', linewidth=.9)
                ax.fill_between(hx, hl, hu, color=base.color, alpha=base.alpha, ec='none')
                ax.plot(px, py, color=opt.color, zorder=opt.zorder, solid_capstyle='round', linewidth=.9)
                ax.fill_between(px, pl, pu, color=opt.color, alpha=opt.alpha, ec='none')
                ax.plot(prx, pry, color='red', zorder=1, solid_capstyle='round', linewidth=.6, alpha=.9)
                ax.xaxis.set_tick_params(which='major', labelbottom=False)

                if i == 1:
                    ax.set_ylabel('Damage')
                    ax.yaxis.set_tick_params(which='major', labelleft=True)

            ax.set_xlabel('Year')
            ax.xaxis.set_tick_params(which='major', labelbottom=True)

            ax = fig.add_subplot(grid[:, 1], ylim=(0, 1))
            axes.append(ax)

            boxes = ax.boxplot(ys, patch_artist=True,
                               labels=[base.label] + [opt.label for opt, _ in zip(opts, projections)])

            for opt, m in zip([base] + opts, boxes['medians']):
                m.set(color=opt.median)

            for opt, b in zip([base] + opts, boxes['boxes']):
                b.set(fc=opt.color)

            ax.yaxis.tick_right()

            for ax, label in zip(axes, ['A', 'B', 'C', 'D']):
                ax.text(0, 1, label, transform=ax.transAxes + offset)

            plt.tight_layout()
            fig.savefig(join(path, f'{nuts}_{crop}.svg'))
            plt.close(fig=fig)


BASE: dict = {
    'ec': 'black',
    'fc': '#bdbdbd',
    'linewidth': .4
}

CRS: PlateCarree = PlateCarree()

NCOLORS: int = 12
START: float
END: float
INC: float
START, END, INC = 0, .55, .05
CMAP = get_cmap('Reds', NCOLORS)
TICKS: NDArray[float] = arange(START, END, INC, dtype=float).round(2)
TICKS = append(TICKS, 1.)
NORM: BoundaryNorm = BoundaryNorm(TICKS, ncolors=NCOLORS)
BGCOL: str = '#9ecae1'


def styler(geoid: str, layer: GeoDataFrame, means: DataFrame) -> Callable[[Polygon | MultiPolygon], dict]:
    """Styler for the map drawn in panel_three

    Args:
        geoid: Used to match rows of layer and means.
        layer: Geometries that are drawn.
        means: Mean damage values that are used to color the geometries.

    Returns:
        A style function corresponding the cartopy interface
    """

    def wrapped(geo: Polygon | MultiPolygon) -> dict:
        # Select data of the geometry to be drawn/styled
        point: Point = geo.geoms[0].representative_point() if isinstance(geo, MultiPolygon) \
            else geo.representative_point()
        idx: Series = layer.intersection(point).apply(lambda r: not r.is_empty)
        polygons: GeoDataFrame = layer[idx]

        # Get the mean damage data for the geometry
        if len(polygons) > 0 and len(means.loc[means[geoid] == polygons.iloc[0][geoid]]) > 0:
            row: Series = means.loc[means[geoid] == polygons.iloc[0][geoid]].iloc[0]

            style = BASE.copy()
            style.update({'fc': CMAP(NORM(row[MEAN]))})

            return style

        return BASE

    return wrapped


@command()
@option(
    '--geoid',
    type=str,
    default='NUTS_ID',
    show_default=True,
    is_eager=True,
    help='Column used to match LAYER geometries and mena damage data'
)
@option(
    '--outlines',
    type=Path(exists=True, dir_okay=False),
    help='A vector file containing extra geometries to be drawn on top of each plot'
)
@option(
    '--means',
    type=File(),
    required=True,
    multiple=True,
    callback=validate_labels(['geoid'], [MEAN]),
    help='One or multiple CSV-files containing mean damage per geoid'
)
@argument(
    'layer',
    type=Path(exists=True, dir_okay=False),
    required=True
)
@argument(
    'plot',
    type=Path(dir_okay=False, writable=True),
    required=True
)
def panel_three(
        geoid: str,
        outlines: str,
        means: Union[DataFrame, List[DataFrame]],
        layer: str,
        plot: str,
) -> None:
    """Plots a panel of damage maps for each mean damage CSV-file

    Plots per geometry in **LAYER** the corresponding mean damage value provided via the option **--means**. The option
    **--geoid** identifies matching rows, and the number of panels equals the number of CSV files via the option
    **--means**. The option **--outlines** can be used to draw geometries as outlines on top of each plot.

    Default colouring is based on the Red colour map with 12 colours. The range of values is from 0 to 0.55 with an
    increment of 0.05, while 1.0 is the upper limit. Additionally, a land mask is drawn in the background.
    """
    geometries: GeoDataFrame = read_file(layer)

    if outlines:
        extra: GeoDataFrame = read_file(outlines)
        style: dict = BASE.copy()
        style.update({'fc': 'none', 'linewidth': .7})
    else:
        extra: GeoDataFrame = geometries
        style: dict = BASE.copy()
        style.update({'fc': 'none'})

    mns, mxs = geometries.bounds.min(), geometries.bounds.max()
    xmn, xmx, ymn, ymx = mns['minx'], mxs['maxx'], mns['miny'], mxs['maxy']

    fig: Figure = Figure()
    if isinstance(means, DataFrame):
        grid: GridSpec = GridSpec(1, 1, figure=fig)
        means = [means]
    else:
        grid: GridSpec = GridSpec(len(means) // 2 + len(means) % 2, 2, figure=fig)

    ax: Union[Axes, None] = None
    for g, m, l in zip(grid, means, ['A', 'B', 'C', 'D']):
        ax = fig.add_subplot(g, projection=CRS)
        ax.add_feature(LAND, **BASE)
        ax.set_extent([xmn - 1, xmx + 1, ymx + 1, ymn - 1], CRS)
        ax.add_geometries(geometries.geometry, CRS, styler=styler(geoid, geometries, m))
        ax.add_geometries(extra.geometry, CRS, **style)
        ax.text(xmn, ymx - 2, l, transform=CRS)
        ax.set_facecolor(BGCOL)

    if ax:
        cbar = ax.figure.colorbar(ScalarMappable(norm=NORM, cmap=CMAP), ax=ax)
        cbar.ax.set_ylabel('Damage', rotation=-90, va="bottom")
        cbar.set_ticks(TICKS)

    fig.tight_layout()
    fig.savefig(plot)
    plt.close(fig=fig)

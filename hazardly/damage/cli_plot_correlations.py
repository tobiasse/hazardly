from collections import namedtuple
from typing import List
from typing import Tuple

import matplotlib.pyplot as plt
from click import File
from click import Path
from click import argument
from click import command
from click import option
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import FuncFormatter
from numpy import arange
from numpy import empty
from numpy import isnan
from numpy import logical_and as AND
from numpy import logical_or as OR
from numpy import nan
from numpy.typing import NDArray
from pandas import DataFrame

from hazardly.damage.cli_correlations import CORR_COL_NAME
from hazardly.damage.cli_correlations import ZSCORE_COL_NAME
from hazardly.eurostat.plot import annotate
from hazardly.eurostat.plot import heatmap
from hazardly.utils.parse import validate_labels

CI = namedtuple('CI', ['mn', 'mx', 'sym'])
CIS: List[CI] = [
    CI(mn=-1.96, mx=1.96, sym='*'),  # > 95%
    CI(mn=-2.58, mx=2.57, sym='**'),  # > 99%
    CI(mn=-3.32, mx=3.32, sym='***'),  # > 99.9%
]

NCOLORS: int = 11
START: float
END: float
INC: float
START, END, INC = -1, 1.1, .2
CMAP = plt.get_cmap('PuOr', NCOLORS)
TICKS: NDArray[float] = arange(START, END, INC).round(1)
NORM: BoundaryNorm = BoundaryNorm(TICKS, ncolors=NCOLORS)


@command()
@option('--geoid', default='NUTS_ID', show_default=True, type=str, is_eager=True)
@option('--cropid', default='CROP_ID', show_default=True, type=str, is_eager=True)
@option('--significant', is_flag=True, help='Depict only significant correlations')
@option('--size', nargs=2, type=int, default=(15., 15.), show_default=True,
        help='Size of the figure')
@option('--title', type=str, default='Correlations', show_default=True,
        help='Title text')
@option('--legend', type=str, default='Correlation coefficient', show_default=True,
        help='Legend text')
@option('--filter_nan', is_flag=True, help='Remove empty rows and columns')
@argument('correlations', type=File(lazy=False), required=True,
          callback=validate_labels(['geoid', 'cropid'], [CORR_COL_NAME]))
@argument('plot', type=Path(dir_okay=False), required=True)
def plot_correlations(
        geoid: str,
        cropid: str,
        significant: bool,
        size: Tuple[float, float],
        title: str,
        legend: str,
        filter_nan: bool,
        correlations: DataFrame,
        plot: str,
) -> None:
    """Plots a correlation heatmap

    Input data (CORRELATIONS) should be a correlation table as produced by the correlations tool. The plot highlights
    significant correlations if the zscore column is available. The following symbol confidence interval relations are
    used: '*' > 95%, '**' > 99%, '***' > 99.9%.

    Data is pivoted along the geoid and cropid columns, and the geoid is declared as the index and cropid as the new
    column labels. Therefore, the geoid becomes the y-axis and cropid the plot's x-axis.
    """
    if ZSCORE_COL_NAME in correlations.columns:
        df_coeffs: DataFrame = correlations.drop(labels=ZSCORE_COL_NAME, axis=1)
        df_coeffs = df_coeffs.pivot(index=geoid, columns=cropid, values=CORR_COL_NAME)
        df_scores: DataFrame = correlations.drop(labels=CORR_COL_NAME, axis=1)
        df_scores = df_scores.pivot(index=geoid, columns=cropid, values=ZSCORE_COL_NAME)
    else:
        df_coeffs: DataFrame = correlations.pivot(index=geoid, columns=cropid, values=CORR_COL_NAME)
        df_scores: DataFrame = correlations.copy()
        df_scores[CORR_COL_NAME] = nan
        df_scores = df_scores.pivot(index=geoid, columns=cropid, values=CORR_COL_NAME)

    coeffs: NDArray[float] = df_coeffs.values.astype(float)
    scores: NDArray[float] = df_scores.values.astype(float)
    if significant:
        ci: CI = CIS[0]
        coeffs[AND(scores > ci.mn, scores < ci.mx)] = nan
        coeffs[isnan(scores)] = nan
        scores[AND(scores > ci.mn, scores < ci.mx)] = nan

    if filter_nan:
        if significant:
            df_coeffs[:] = coeffs
            df_coeffs = df_coeffs.dropna(how='all').dropna(axis='columns', how='all')
            coeffs = df_coeffs.values.astype(float)
            df_scores[:] = scores
            df_scores = df_scores.dropna(how='all').dropna(axis='columns', how='all')
            scores = df_scores.values.astype(float)
        else:
            df_coeffs[:] = coeffs
            oidx, ocols = df_coeffs.index.tolist(), df_coeffs.columns.tolist()
            df_coeffs = df_coeffs.dropna(how='all').dropna(axis='columns', how='all')
            nidx, ncols = df_coeffs.index.tolist(), df_coeffs.columns.tolist()
            didx, dcols = set(oidx) ^ set(nidx), set(ocols) ^ set(ncols)
            coeffs = df_coeffs.values.astype(float)
            df_scores[:] = scores
            df_scores = df_scores.drop(index=didx, columns=dcols)
            scores = df_scores.values.astype(float)

    nums: NDArray[str] = coeffs.round(2).astype(str)
    nums[nums == 'nan'] = ''
    stars: NDArray[object] = empty(scores.shape, dtype=object)
    stars[stars == None] = ''
    for ci in CIS:
        stars[OR(scores <= ci.mn, scores >= ci.mx)] = ci.sym
    labels: NDArray[str] = nums + stars

    xticks: List[str] = df_coeffs.columns.tolist()
    yticks: List[str] = df_coeffs.index.tolist()

    fig, ax = plt.subplots(figsize=size)
    ax.set_title(title)

    im, _ = heatmap(coeffs, yticks, xticks, ax, cmap=CMAP, norm=NORM, cbarlabel=legend, cbar_kw={'ticks': TICKS})
    annotate(im, labels, size=6, formatter=FuncFormatter(lambda x, pos: x))

    plt.tight_layout()
    fig.savefig(plot)

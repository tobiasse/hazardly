from matplotlib import pyplot as plt
from numpy import full
from numpy import percentile
from numpy import zeros

from hazardly.thresher.spa import ar1
from hazardly.thresher.spa import spa

dmin = 0
time_gaps = False
days, lats, lons = 210, 10, 10

qt = ar1((days, lats, lons), phi=.9, mu=60., sigma=20., seed=42).round(2)
q0 = full(shape=(days, lats, lons), fill_value=percentile(qt, q=20., axis=0))
events = zeros(shape=(days, lats, lons), dtype=int)
time = list(range(days))

if time_gaps:
    gaps = list(range(0, 50)) + list(range(100, 150)) + list(range(175, days))
else:
    gaps = time

storage = spa(q0, qt, events, gaps, lats, lons, dmin=dmin)

x, y = 1, 1

fig, ax1 = plt.subplots()

ax1.plot(time, qt[:, y, x], color='#252525', label='Daily precipitation (Qₜ)')
ax1.hlines(q0[0, y, x], 0, days, colors='#f03b20', label='Target volume (Q₀)', linestyles='dotted')

if time_gaps:
    ax1.plot(gaps[:50], storage[1:51, y, x], color='#3182bd', label='Deficit volume (Sₜ)')
    ax1.plot(gaps[:50], events[0:50, y, x] * storage[1:51, y, x], color='#f03b20', label='Duration (dᵢ)',
             linestyle='dashed')
    ax1.plot(gaps[50:100], storage[52:102, y, x], color='#3182bd')
    ax1.plot(gaps[50:100], events[100:150, y, x] * storage[52:102, y, x], color='#f03b20', linestyle='dashed')
    ax1.plot(gaps[100:], storage[103:-1, y, x], color='#3182bd')
    ax1.plot(gaps[100:], events[175:, y, x] * storage[103:-1, y, x], color='#f03b20', linestyle='dashed')
else:
    ax1.plot(time, storage[1:-1, y, x], color='#3182bd', label='Deficit volume (Sₜ)')
    ax1.plot(time, events[:, y, x] * storage[1:-1, y, x], color='#f03b20', label='Duration (dᵢ)',
             linestyle='dashed')

ax1.set_title(f'Sequent peak algorithm (dmin={dmin}, gaps={time_gaps})')
ax1.set_xlabel('Days')
ax1.set_ylabel('Precipitation [mm]')

ax1.legend(frameon=False, loc='upper left', bbox_to_anchor=(1.04, 1))

fig.show()
# plt.savefig(f'spa_gaps{time_gaps}_dmin{dmin}.svg', bbox_inches='tight')

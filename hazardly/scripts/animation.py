"""
Plot weather-extreme events animations.
"""
from typing import Dict

import geopandas as gpd
import matplotlib.pyplot as plt
import pandas as pd
from cartopy.crs import CRS
from cartopy.crs import PlateCarree
from matplotlib.animation import FFMpegFileWriter
from matplotlib.animation import FuncAnimation
from matplotlib.axes import Axes

from hazardly.extremes.geofeature import GeoFeature
from hazardly.extremes.runner import AnimationRunner


def styler(ax: Axes, crs: CRS, col: str, responses: pd.DataFrame, geofeatures: Dict[str, GeoFeature]):
    selection = responses[(responses[col] != 0) & (pd.notna(responses[col]))]
    artists = list()

    for _, row in selection.iterrows():
        geo = geofeatures[row['NUTS_ID']].geometry
        x, y = geo.centroid.x, geo.centroid.y
        artist = ax.text(x, y, str(round(row[col])), transform=crs, fontsize='xx-small', ha='left', va='center')
        artists.append(artist)

    return artists


nuts = gpd.read_file(f'../../data/interim/nuts1.shp')
countries = gpd.read_file('../../data/interim/nuts0.shp')

extremes = pd.read_csv(f'../../data/proc/era5-land_heat-waves/yearly_heat-waves_max_CD5_1981-2021_nuts1.csv')

mns, mxs = nuts.bounds.min(), nuts.bounds.max()
xmn, xmx, ymn, ymx = mns['minx'], mxs['maxx'], mns['miny'], mxs['maxy']

crs = PlateCarree()

fig = plt.figure(figsize=(5, 5), dpi=200, tight_layout=True, facecolor='#9ecae1')
grid = fig.add_gridspec(nrows=1, ncols=1)

ax = fig.add_subplot(grid[0, 0], projection=crs, frameon=False)
ax.set_extent([xmn, xmx + 1, ymx + 1, ymn - 1], crs)

on = 'NUTS_ID'
cols = [str(i) for i in range(1981, 2021)]

runner = AnimationRunner(
    ax=ax, crs=crs, on=on, cols=cols, responses=extremes, frames=30, mode='col', styler=styler,
    geofeatures={
        row[on]: GeoFeature(
            idx=row[on],
            feature=ax.add_geometries([row.geometry], crs, ec='#404040', fc='white', linewidth=.5),
            geometry=row.geometry
        )
        for _, row in nuts.iterrows()
    },
    title='Heat-waves',
)

animation = FuncAnimation(
    fig,
    runner.update,
    frames=1200,
    init_func=lambda: list(),
    interval=33,
    blit=False,
    cache_frame_data=False
)

ax.add_geometries(countries.geometry.values, crs, facecolor='none', linewidth=.8, ec='black')

animation.save(
    f'/home/tobiasse/Documents/heat-waves.mp4',
    writer=FFMpegFileWriter(fps=24)
)
# plt.show()

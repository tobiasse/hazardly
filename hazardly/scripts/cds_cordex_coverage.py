"""
Check if CORDEX datasets with selected parameter configurations are available in der CDS.
"""
from itertools import product
from multiprocessing.pool import ThreadPool
from typing import Tuple

import pandas as pd
from cdsapi import Client

from hazardly.cds.cfg import cds_ensemble_members
from hazardly.cds.cfg import cds_experiments
from hazardly.cds.cfg import cds_gcms
from hazardly.cds.cfg import cds_rcms
from hazardly.cds.cfg import cds_temporal_resolution
from hazardly.cds.cfg import cds_variables


def worker(idx: int, row: pd.Series) -> Tuple[int, bool]:
    """Check if a EURO-CORDEX dataset with the requested parameter configuration exists in the CDS.

    Args:
        idx: Row index
        row: Row must contain experiment, temporal_resolution, variable, gcm, rcm, ensemble, start, and end.

    Returns:
        The row index and true if the requested dataset is available for download.
    """
    req = {
        'domain': 'europe',
        'experiment': row.experiment,
        'horizontal_resolution': '0_11_degree_x_0_11_degree',
        'temporal_resolution': row.temporal_resolution,
        'variable': row.variable,
        'gcm_model': row.gcm,
        'rcm_model': row.rcm,
        'ensemble_member': row.ensemble,
        'start_year': row.start,
        'end_year': row.end,
        'format': 'zip',
    }
    client = Client()

    # noinspection PyBroadException
    try:
        _ = client.retrieve('projections-cordex-domains-single-levels', req)  # Ignore res (result is a download URL)
        result = True

    except Exception:
        result = False

    return idx, result


options = pd.DataFrame.from_records(
    list(product(
        cds_experiments, cds_temporal_resolution, cds_variables, cds_gcms, cds_rcms, cds_ensemble_members
    )),
    columns=['experiment', 'temporal_resolution', 'variable', 'gcm', 'rcm', 'ensemble']
)

options.loc[options.experiment == 'historical', 'start'] = '1970'
options.loc[options.experiment == 'historical', 'end'] = '1970'
options.loc[options.experiment != 'historical', 'start'] = '2006'
options.loc[options.experiment != 'historical', 'end'] = '2006'
options['available'] = False
options['visited'] = False

with ThreadPool(processes=10) as pool:
    threads = [pool.apply_async(worker, args=(i, r)) for i, r in options.iterrows()]

    for t in threads:
        i, a = t.get()
        options.loc[i, 'visited'] = True
        options.loc[i, 'available'] = a

        if i % 100 == 0:
            options.to_csv('total_run_off_flux.csv', index=False)

options.to_csv('total_run_off_flux.csv', index=False)

from matplotlib import pyplot as plt
from netCDF4 import Dataset
from numpy import zeros

from hazardly.thresher.spa import spa

events_calculated = Dataset('../../data/proc/spa_dmin5.nc')
thresholds = Dataset('../../data/proc/thresholds.nc')
prc = Dataset('../../data/interim/prc_mm_1981-2021.nc')

qt = prc.variables['tp'][59:269, :, :]
q0 = thresholds.variables['tp'][0, :210, :, :]
droughts = events_calculated['exceedance'][0, :210, :, :]

dmin = 5
x, y = 205, 155
days, lats, lons = qt.shape
time = list(range(days))
events = zeros(shape=(days, lats, lons), dtype=int)

storage = spa(q0, qt, events, time, lats, lons, dmin=dmin)

fig, (ax1, ax2) = plt.subplots(nrows=2, sharex='all')
fig.suptitle(f'Sequent peak algorithm (dmin={dmin})')

ax1.plot(time, qt[:, y, x], color='#252525', label='Daily precipitation (Qₜ)')

ax1.legend(frameon=False, loc='upper left')
ax1.set_ylabel('Precipitation [mm]')

ax2.plot(time, q0[:, y, x], color='#f03b20', label='Target volume (Q₀)', linestyle='dotted')
ax2.plot(time, storage[1:-1, y, x], color='#3182bd', label='Deficit volume (Sₜ)')
ax2.plot(time, events[:, y, x] * storage[1:-1, y, x], color='#f03b20', label='Duration (dᵢ)', linestyle='dashed')
ax2.step(time, -1 * droughts[:, y, x].data, color='#252525', linestyle='dashed')

ax2.legend(frameon=False, loc='upper left')
ax2.set_xlabel('Days')
ax2.set_ylabel('Precipitation [mm]')

# fig.show()
plt.savefig('/home/tobiasse/Documents/test.svg')

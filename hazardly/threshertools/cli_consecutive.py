from typing import Tuple

import click
from netCDF4 import Dataset

from hazardly.threshertools.consecutive import cc
from hazardly.threshertools.consecutive import condition
from hazardly.threshertools.consecutive import cumulative
from hazardly.threshertools.consecutive import disaggregated
from hazardly.threshertools.consecutive import parse_condition
from hazardly.thresher.netcdf import get_attr

# noinspection SpellCheckingInspection
_modes = {
    'cum': cumulative,
    'disagg': disaggregated
}


# noinspection SpellCheckingInspection
@click.command()
@click.option('-v', '--var', required=True,
              help='Counting number of consecutive events for the selected variables, e.g., "var1,var2,...".')
@click.option('-s', '--start_condition', required=True, type=str, callback=parse_condition,
              help='The counting start condition, e.g., OPERATION (<, >, ==, !=, <=, >=) NUMBER.')
@click.option('-e', '--end_condition', required=True, type=str, callback=parse_condition,
              help='The counting end condition, e.g., OPERATION (<, >, ==, !=, <=, >=) NUMBER.')
@click.option('-m', '--mode', default='cum', show_default=True, type=click.Choice(list(_modes.keys())),
              help='The counting mode.')
@click.option('--mn', default=0, type=int, show_default=True, help='The minimum number of consecutive events.')
@click.option('--set_to', default=1, type=int, show_default=True,
              help='Set consecutive count to this value, only applied if counting mode is disagg.')
@click.argument('netcdf', required=True, type=str)
def consecutive(
        var: str,
        start_condition: Tuple[str, float],
        end_condition: Tuple[str, float],
        mode: str,
        mn: int,
        set_to: int,
        netcdf: str
) -> None:
    """Counts for the selected variables in a NetCDF the number of consecutive events.

    Count for one or more selected variables (-v, --var) the number of events where the give start condition (-s,
    --start_condition) is fulfilled till the end condition (-e, --end_condition) at least for the minimum (--mn) number
    of steps. The mode (-m, --mode) determines if the number of consecutive events is stored cumulative or disaggregated
    , see examples. The number of consecutive events is stored in the given NETCDF by adding the suffix consecutive to
    the selected variables.

    Examples:

    consecutive -v exceedance -s "==1" -e "!=1" -m cum --mn 3 foo.nc

    0 1 1 0 1 1 1 1 0 0 0 1 0 1 1 1 < input

    0 0 0 0 0 0 0 4 0 0 0 0 0 0 0 3 < output

    consecutive -v exceedance -s "==1" -e "!=1" -m disagg --mn 3 foo.nc

    0 1 1 0 1 1 1 1 0 0 0 1 0 1 1 1 < input

    0 0 0 0 1 1 1 1 0 0 0 0 0 1 1 1 < output
    """
    sc = condition(*start_condition)
    ec = condition(*end_condition)

    m = _modes[mode]

    # TODO REFACTOR create a new netcdf and dump results
    with Dataset(netcdf, mode='a') as n:
        var = var.split(',')
        for v in var:
            assert v in n.variables
            assert f'{v}_consecutive' not in n.variables

            src = n.variables[v]

            dst = n.createVariable(f'{v}_consecutive', 'i2', src.dimensions, fill_value=0)
            dst.units = get_attr('units', src, '')
            dst.long_name = get_attr('long_name', src, '')
            dst.standard_name = get_attr('standard_name', src, '')
            dst.start_condition = start_condition
            dst.end_condition = end_condition
            dst.mode = mode
            dst.mn = mn

            cc(src, dst, start_condition=sc, end_condition=ec, mode=m, mn=mn, set_to=set_to)

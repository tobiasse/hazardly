from typing import Callable
from typing import Union

from click import progressbar
from netCDF4 import Variable
from numpy import any
from numpy import copy
# noinspection PyPep8Naming
from numpy import logical_and as AND
# noinspection PyPep8Naming
from numpy import logical_not as NOT
# noinspection PyPep8Naming
from numpy import logical_or as OR
from numpy import ndarray
from numpy import where
from numpy import zeros

from hazardly.threshertools.consecutive import disaggregated


# noinspection SpellCheckingInspection
# TODO Refactor counts to extra functions
def pooling(
        src: Union[Variable, ndarray],
        dst: Union[Variable, ndarray],
        real: Union[Variable, ndarray],
        full: Union[Variable, ndarray],
        events: Union[Variable, ndarray],
        event_condition: Callable[[ndarray], ndarray],
        betweenness_condition: Callable[[ndarray], ndarray],
        mx: int,
        dmin: int,
        set_to: int,
) -> None:
    """Pools adjacent events together, counts real and full duration, and the event occurrence dependent on dmin.

    Real duration is the cumulative number of events where the event condition applies within mx. Full duration is the
    cumulative number of events where the event and betweenness condition applies within mx. An event is counted when
    the real duration is above dmin (inclusive).

    Args:
        src: Pool events of this object.
        dst: Writes pooling results to this object.
        real: The cumulative number of events is stored at the end of the sequence.
        full: The cumulative number of events is stored at the end of the sequence.
        events: The event flag is stored at the end of the sequence.
        event_condition: Values evaluating to true by this condition will be detected as events.
        betweenness_condition: Values between events and evaluating to true by this condition will be counted for
            pooling.
        mx: Only events below this distance are pooled (exclusive: distance < mx).
        dmin: An event sequence where the real duration is greater then dmin is counted as an event.
        set_to: Set all values between pooled events to this value.
    """
    shape = src.shape[1:]
    nullify = zeros(shape)
    preceding = zeros(shape)
    betweenness = zeros(shape)
    real_duration = zeros(shape)
    full_duration = zeros(shape)

    with progressbar(range(len(src) + mx), label='Pooling events') as idxs:
        for i in idxs:
            if i >= len(src):
                data = nullify
            else:
                data = src[i]

            # Independent evaluated conditions
            is_event = event_condition(data)
            is_between = betweenness_condition(data)

            preceding[is_event] = 1
            betweenness[AND(is_between, preceding == 1)] += 1

            # Dependent evaluated conditions
            is_between = betweenness > 0
            below_mx = betweenness < mx
            exceeds_mx = betweenness >= mx

            # Counting durations
            real_duration[is_event] += 1
            full_duration[OR(is_event, is_between)] += 1

            if any(exceeds_mx):
                exceeds_dmin = real_duration >= dmin
                # Removes betweenness overflow from full duration
                full_duration[exceeds_mx] -= mx
                # Copy values to source datasets
                real[i - mx] = where(AND(exceeds_mx, exceeds_dmin), real_duration, real[i - mx])
                full[i - mx] = where(AND(exceeds_mx, exceeds_dmin), full_duration, full[i - mx])
                events[i - mx] = where(AND(exceeds_mx, exceeds_dmin), 1, events[i - mx])
                # Reset durations
                preceding[exceeds_mx] = 0
                real_duration[exceeds_mx] = 0
                full_duration[exceeds_mx] = 0
                betweenness[exceeds_mx] = 0

            if any(AND(is_event, AND(is_between, below_mx))):
                # Copy pooled events to source dataset
                cp = copy(betweenness)
                cp[NOT(is_event)] = 0
                disagg = disaggregated(cp, set_to)
                dst[i - len(disagg):i] = where(disagg > 0, disagg, dst[i - len(disagg):i])
                betweenness[cp > 0] = 0

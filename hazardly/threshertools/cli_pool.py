from typing import Tuple

import click
from netCDF4 import Dataset

from hazardly.threshertools.consecutive import condition
from hazardly.threshertools.consecutive import parse_condition
from hazardly.thresher.netcdf import get_attr
from hazardly.threshertools.pooling import pooling


# noinspection SpellCheckingInspection
@click.command()
@click.option('-v', '--variable', type=str, required=True,
              help='Run event pooling for the selected variable.')
@click.option('-e', '--event_condition', type=str, required=True, callback=parse_condition,
              help='If condition applies an event occurs, e.g., OPERATION (<, >, ==, !=, <=, >=) NUMBER; ">=2".')
@click.option('-b', '--betweenness_condition', type=str, required=True, callback=parse_condition,
              help='If condition applies between events distance is counted (see event_condition).')
@click.option('--mx', default=10, type=int, show_default=True,
              help='The maximum pool distance between two adjacent events.')
@click.option('--dmin', default=15, type=int, show_default=True,
              help='If real duration of an event sequence is greater than dmin it is counted as an event.')
@click.option('--set_to', default=1, type=int, show_default=True, help='Set pool distance to this value.')
@click.argument('netcdf', type=str, required=True)
def pool(
        variable: str,
        event_condition: Tuple[str, float],
        betweenness_condition: Tuple[str, float],
        mx: int,
        dmin: int,
        set_to: int,
        netcdf: str
) -> None:
    """Pools two adjacent events together, counts real and full duration, and the event occurrence dependent on dmin.

    Pools two adjacent events together for a selected netcdf (NETCDF) variable (-v, --variable). The result of the
    pooling is written into the selected variable. Optimally, the variable contains disaggregated values obtained from
    the consecutive tool. An event is detected by defining the event condition (-e, --event_condition). Events are
    pooled by counting the distance between (-b, --betweenness_condition) them. If the the distance is smaller than max
    distance (--mx) the events get pooled (see example pooled). While pooling the events the real and full duration is
    counted. Real duration is the cumulative number of events where the event condition applies within mx (see example
    real duration). Full duration is the cumulative number of events where the event and betweenness condition applies
    within mx (see example full duration). An event is counted when the real duration is greater dmin (inclusive).

    The implemented algorithm builds upon the inter-event time method of Zelenhasić, E., & Salvai, A. (1987). A method
    of streamflow drought analysis. Water Resources Research, 23(1), 156–168. https://doi.org/10.1029/WR023i001p00156.

    Examples:

    pooling -v consecutive -e "==1" -b "==0" --mx 3 --dmin 4 foo.nc

    0 1 0 0 1 1 1 0 0 0 1 1 0 0 0 0 < input

    0 1 1 1 1 1 1 0 0 0 1 1 0 0 0 0 < pooled

    0 0 0 0 0 0 4 0 0 0 0 0 0 0 0 0 < real duration

    0 0 0 0 0 0 6 0 0 0 0 0 0 0 0 0 < full duration

    0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 < events

    """
    ec = condition(*event_condition)
    bc = condition(*betweenness_condition)

    with Dataset(netcdf, mode='a') as nc:
        assert variable in nc.variables
        src = nc.variables[variable]

        durations = list()
        for n in ['real', 'full', 'events']:
            assert f'{variable}_{n}' not in nc.variables
            v = nc.createVariable(f'{variable}_{n}', 'i2', src.dimensions, fill_value=0)
            v.units = get_attr('units', src, '')
            v.long_name = get_attr('long_name', src, '')
            v.standard_name = get_attr('standard_name', src, '')
            durations.append(v)

        pooling(src, src, *durations, event_condition=ec, betweenness_condition=bc, mx=mx, dmin=dmin, set_to=set_to)

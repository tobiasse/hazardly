import re
from typing import Callable
from typing import Tuple
from typing import Union

import click
from click import progressbar
from netCDF4 import Variable
from numpy import any
from numpy import array
from numpy import copy
from numpy import logical_and as AND
from numpy import max
from numpy import ndarray
from numpy import where
from numpy import zeros


def cc(
        src: Union[Variable, ndarray],
        dst: Union[Variable, ndarray],
        start_condition: Callable[[ndarray], ndarray],
        end_condition: Callable[[ndarray], ndarray],
        mode: Callable[[ndarray, int], ndarray],
        mn: int,
        set_to: int
) -> None:
    """Counts the number of consecutive time units per cell if the provided start_condition and mn requirement are true.

    Args:
        src: Count consecutive time units for this dataset.
        dst: The consecutive time unit count is stored in this dataset.
        start_condition: Start consecutive time units counting for values that fulfill the provided condition.
        end_condition: End consecutive time units counting for values that fulfill the provided condition.
        mode: Counting mode
        mn: The minimum number of consecutive time units, e.g., when mn equals 3 than only cells will be counted where
            number of consecutive time units is > mn.
        set_to: Set the consecutive counting to this value. Please note this value is only used if the counting mode is
            disaggregated.
    """
    # In case src has a sequence break, e.g., src contains monthly data from January till March for several years, the
    # consecutive counting will count overlapping consecutive time units, e.g. last day of March and the first days of
    # January.
    count = zeros(src.shape[1:])

    with progressbar(range(len(src) + 1), label='Counting consecutive time units') as idxs:
        for i in idxs:
            # At idx overflow of src data: add a zero matrix to execute the algorithm another time
            if i == len(src):
                data = zeros(src.shape[1:])
            else:
                data = src[i]

            count[start_condition(data)] += 1
            count[AND(end_condition(data), count < mn)] = 0

            if any(count[AND(end_condition(data), count >= mn)]):
                cp = copy(count)
                cp[start_condition(data)] = 0
                counts = mode(cp, set_to)
                dst[i - len(counts):i] = where(counts > 0, counts, dst[i - len(counts):i])
                count[cp > 0] = 0


def cumulative(counts: ndarray, set_to: int) -> ndarray:
    return array([counts])


def disaggregated(counts: ndarray, set_to: int) -> ndarray:
    """Disaggregates the consecutive counts.

    Example:
        counts =  [0, 1, 2]
        result = [[0, 0, 1],
                  [0, 1, 1]]

    Args:
        counts: Consecutive counts
        set_to: Set disaggregated values to this value.

    Returns:
        Disaggregated consecutive counts in shape of (max(counts), rows(counts), cols(counts))
    """
    counts = copy(counts)  # Avoid inplace modification
    sink = zeros((int(max(counts)), *counts.shape))
    for z in sink[-1::-1]:
        z[counts > 0] = set_to
        counts[counts > 0] -= 1
    return sink


def condition(op: str, threshold: float) -> Callable[[ndarray], ndarray]:
    """Creates a anonymous comparison function which compares a comparable object with a fixed threshold and that
    returns the result of that comparison to its caller.

    Args:
        op: One of the comparison operators, e.g., >, <, ==, >=, <=, !=.
        threshold: A number

    Returns:
        A anonymous function (lambda) that may be used to compare a comparable object with a fixed threshold by using
        the selected comparison operator. The result of this comparison is returned to the caller.
    """
    return {
        '<': lambda x: x < threshold,
        '>': lambda x: x > threshold,
        '==': lambda x: x == threshold,
        '!=': lambda x: x != threshold,
        '>=': lambda x: x >= threshold,
        '<=': lambda x: x <= threshold,
    }[op]


def parse_condition(ctx: click.Context, param: str, value: str) -> Tuple[str, float]:
    regex = re.compile(r' ?(?P<op>>|<|<=|>=|==|!=) ?(?P<threshold>\d+(\.\d+)?) ?')

    if match := regex.match(value):
        return match.group('op'), float(match.group('threshold'))

    else:
        raise click.BadParameter(f'Error in start condition formatting {value}!')

import click
import geopandas as gpd
from netCDF4 import Dataset
from netCDF4 import num2date
from numpy.ma import masked

from hazardly.thresher.cfg import LAT
from hazardly.thresher.cfg import LON
from hazardly.thresher.cfg import TIME
from hazardly.threshertools.rescales import Masking
from hazardly.threshertools.rescales import md
from hazardly.threshertools.rescales import me
from hazardly.threshertools.rescales import mn
from hazardly.threshertools.rescales import mx
from hazardly.threshertools.rescales import output_factory
from hazardly.threshertools.rescales import sm

_aggregations = {
    'mn': mn,
    'mx': mx,
    'me': me,
    'md': md,
    'sm': sm,
}


@click.command()
@click.option('-a', '--agg', required=True, type=click.Choice(list(_aggregations.keys())),
              help='The rescaling operation')
@click.option('-v', '--var', required=True, type=str, help='The target variable')
@click.option('--all_touched', is_flag=True, help='If True, all cells touched by geometries will be burned in')
@click.option('-e', '--encoding', type=str, help='Data encoding of the mask')
@click.argument('netcdf', required=True, type=click.Path(exists=True))
@click.argument('mask', required=True, type=click.Path(exists=True))
@click.argument('rescaled', required=True, type=click.Path())
def rescale(agg: str, var: str, all_touched: bool, encoding: str, netcdf: str, mask: str, rescaled: str) -> None:
    """Rescales the cell values of netCDF target variable for all time steps by applying the selected aggregation.
    Rescaling units are the geometries provided by a vector file like a shapefile or geojson.

    Please note, uses the statistical mean when mean rescaling is selected. Therefore, you should not use this
    tool for rescaling large spatial units!
    """
    gdf = gpd.read_file(mask, encoding=encoding)
    rescaling = _aggregations[agg]
    with Dataset(netcdf) as src:
        ext = rescaled.split('.')[-1]
        times = num2date(src.variables[TIME][:], units=src.variables[TIME].units, calendar=src.variables[TIME].calendar)

        output = output_factory(ext, table=gdf, variable=var, times=times)
        masker = Masking(src.variables[LAT][:], src.variables[LON][:], all_touched=all_touched)

        with click.progressbar(gdf.iterrows(), label='Rescaling') as bar:
            for i, series in bar:
                ma = masker.get_mask(series.geometry)
                row = list()
                for data in src.variables[var][:, :, :]:
                    data[ma] = masked
                    if data.mask.all():
                        row.append(0)
                    else:
                        row.append(rescaling(data))
                output.write(i, row)

        output.save(rescaled)

from abc import abstractmethod
from typing import List

from affine import Affine
from cftime import datetime
from geopandas import GeoDataFrame
from numpy import logical_and
from numpy import ndarray
from numpy.ma import masked_array
from numpy.ma import max
from numpy.ma import mean
from numpy.ma import median
from numpy.ma import min
from numpy.ma import sum
from rasterio.features import geometry_mask
from shapely.geometry import Polygon


def mn(data: masked_array) -> float:
    return min(data)


def mx(data: masked_array) -> float:
    return max(data)


def me(data: masked_array) -> float:
    return mean(data)


def md(data: masked_array) -> float:
    return median(data)


def sm(data: masked_array) -> float:
    return sum(data)


def output_factory(ext: str, *args, **kwargs) -> '_Base':
    """Get an output abstraction for the supported outputs formats.

    Args:
        ext:
        args: Class arguments, see doc of _Base.
        kwargs: Named class arguments, see doc of _Base.

    Returns:
        The required abstraction.
    """
    ext = ext.lower()
    if ext == 'csv':
        return CSV(*args, **kwargs)
    elif ext == 'shp':
        return SHP(*args, **kwargs)
    elif ext == 'geojson' or ext == 'json':
        return GeoJSON(*args, **kwargs)
    else:
        raise AttributeError(f'Unsupported output format: {ext}')


class _Base:
    """Base class please use SHP, CSV, or GeoJSON.

    Wraps a GeoDataFrame to write rescaling results conveniently. Creates a copy of the rescaling mask and
    adds the required number of columns to store the rescaling results.

    Attributes:
        table: The rescaling mask.
        variable: The name of the variable that will be rescaled.
        times: Time dimension of the variable that will be rescaled.
    """

    def __init__(self, table: GeoDataFrame, variable: str, times: List[datetime]) -> None:
        self.table = table
        self.cols = list()
        for t in times:
            cname = f'{variable}_{t.year}-{t.day:02d}-{t.month:02d}_{t.hour:02d}:{t.minute:02d}'
            self.table[cname] = 0
            self.cols.append(cname)

    def write(self, row_id: int, row_data: List[float]) -> None:
        """Writes

        Args:
            row_id: The id of the row to write.
            row_data: The cell data to write.
        """
        for col, cell in zip(self.cols, row_data):
            self.table.loc[row_id, col] = cell

    @abstractmethod
    def save(self, filepath: str) -> None:
        pass


class SHP(_Base):
    def __init__(self, table: GeoDataFrame, variable: str, times: List[datetime]) -> None:
        t = table.copy(deep=True)
        super().__init__(t, variable, times)

    def save(self, filepath: str) -> None:
        """Write to disk as a shapefile."""
        # Convert new field names to numeric ids bc shapefiles have a 10char field name size limitation
        self.table.columns = [str(self.cols.index(c)) if c in self.cols else c for c in self.table.columns]
        self.table.to_file(filepath)


class CSV(_Base):
    def __init__(self, table: GeoDataFrame, variable: str, times: List[datetime]) -> None:
        t = table.copy(deep=True)
        t.drop('geometry', axis=1, inplace=True)
        super().__init__(t, variable, times)

    def save(self, filepath: str) -> None:
        """Write to disk as a CSV."""
        self.table.to_csv(filepath)


class GeoJSON(_Base):
    def __init__(self, table: GeoDataFrame, variable: str, times: List[datetime]) -> None:
        t = table.copy(deep=True)
        super().__init__(t, variable, times)

    def save(self, filepath: str) -> None:
        """Write to disk as a GeoJSON."""
        text = self.table.to_json()
        with open(filepath, 'w') as dst:
            dst.write(text)


class Masking:
    """Create geometry masks for predefined extent.

    Attributes:
        lats: A list of latitude coordinates that represent the source area.
        lons: A list of longitude coordinates tat represent the source area.
        all_touched: If True, all pixels touched by geometries will be burned in.
            If false, only pixels whose center is within the polygon or that
            are selected by Bresenham's line algorithm will be burned in. (rasterio documentation)
        invert: If True, mask will be True for pixels that overlap shapes.
    """

    def __init__(self, lats: List[float], lons: List[float], all_touched: bool = False, invert: bool = False) -> None:
        self.invert = invert
        self.all_touched = all_touched

        self._lons = lons
        self._lats = lats
        self._out_shape = (len(self._lats), len(self._lons))

        x, y = abs(self._lons[1] - self._lons[0]), abs(self._lats[1] - self._lats[0])
        self._transform = Affine(x, 0, self._lons[0], 0, -y, self._lats[0])

    def masked_lons(self, geometry: Polygon) -> List[float]:
        bounds = geometry.bounds
        return self._lons[logical_and(self._lons >= bounds[0], self._lons <= bounds[2])]

    def masked_lats(self, geometry: Polygon) -> List[float]:
        bounds = geometry.bounds
        return self._lats[logical_and(self._lats >= bounds[1], self._lats <= bounds[3])]

    def get_mask(self, geometry: Polygon) -> ndarray:
        """Get a geometry mask for the provided polygon object.

        Args:
            geometry: A shapely polygon object.

        Returns:
            A geometry mask replicating the extent that was used for the initialization.
        """
        return geometry_mask(
            [geometry], self._out_shape, self._transform, all_touched=self.all_touched, invert=self.invert
        )

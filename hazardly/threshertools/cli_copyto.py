import click
from hazardly.thresher.netcdf import copy_to


@click.command()
@click.option('-v', '--variable', type=str, required=True, help='')
@click.argument('src', type=str, required=True)
@click.argument('dst', type=str, required=True)
def copyto(src: str, dst: str, variable: str) -> None:
    """Copies the content of a NetCDF dataset (SRC) variable (-v, --variable) to another NetCDF dataset (DST).

    Both dataset should have the variable and the variable should have the same dimensionality. Values of SRC take
    precedence over DST.
    """
    copy_to(src, dst, variable)

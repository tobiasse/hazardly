```bash
# On your machine - Install
$ cd /home/<user>/Documents/hazardly
$ source venv/bin/activate
$ make install

# Fetch extremes
# On cluster
$ ssh <user>@cluster.pik-potsdam.de
$ cd /p/tmp/tobiasse/hazardly/data/proc/
$ tar -xvf cluster  # Kill cmd (<ctrl><c>) after required data is already extracted!
$ rm -r cluster  # Exec after required data is copied

# On your machine
$ rsync -avhz \
>   <user>@cluster.pik-potsdam.de:/p/tmp/tobiasse/hazardly/data/proc/cluster/era5-land_heat-waves/yearly_heat-waves_CD5*.nc \
>   /home/<user>/Documents/hazardly/data/interim

# Rescale extremes
$ rescale -a "mx" -v "exceedance_consecutive" \
> data/interim/yearly_heat-waves_CD5_bs_1981-2021.nc \
> data/interim/nuts0.shp \
> data/proc/hw_bs.geojson

$ rescale -a "mx" -v "exceedance_consecutive" \
> data/interim/yearly_heat-waves_CD5_1981-2021.nc \
> data/interim/nuts0.shp \
> data/proc/hw.geojson

$ merge-extremes data/proc/hw_bs.geojson data/proc/hw.geojson data/proc/hw.csv

# Yield data
$ source yield.sh; run
$ python <<< "
> import pandas as pd
> df = pd.read_csv(\"data/proc/country_YI.csv\")
> df.columns = [\"NUTS_ID\" if c == \"geo_id\" else c for c in df.columns]
> df.columns = [\"CROP_ID\" if c == \"crops_id\" else c for c in df.columns]
> df.to_csv(\"data/proc/country_YI.csv\", index=False)
> "
$ python <<< "
> import pandas as pd
> df = pd.read_csv(\"data/proc/country_YI_coverage.csv\")
> df.columns = [\"NUTS_ID\" if c == \"Unnamed: 0\" else c for c in df.columns]
> df.to_csv(\"data/proc/country_YI_coverage.csv\", index=False)
> "

# Correlations
$ correlations --zscore data/proc/country_YI.csv data/proc/hw.csv data/proc/correlations.csv
$ corr-plot --size 30 30 data/proc/correlations.csv data/proc/correlations.svg

# Parameters
$ functions data/proc/country_YI.csv data/proc/hw.csv data/proc/parameters.csv

# Decision
$ decisions data/proc/country_YI_coverage.csv data/proc/correlations.csv data/proc/parameters.csv data/proc/decisions.csv

# Damages
$ damages data/proc/hw.csv data/proc/parameters.csv data/proc/hw_damages.csv
```

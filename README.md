# Hazardly

The content of this repository is on the topic of impacts on cropy yield in response to weather extremes. You may find
all code in this package to reproduce our results or to drive your analysis. The flowchart below shows a simple overview
of the processing steps involved in this project.

![A simple overview of the processing steps involved in this project](article/figures/flowchart.svg "Flowchart")

**Fig 1:** A simple overview of the processing steps involved in this project

![Yearly heat waves from 1971-2020](article/figures/hw.gif "Weather extremes timeseries")

**Fig 2:** Yearly heat waves from 1971-2020

![For potatoes in Ostösterreich (A) Upper segments of the convex hull and yearly yield (B) the parametrized damage function](article/figures/ch.svg "Convex-hull and damage function")

**Fig 3:** For potatoes in Ostösterreich (A) upper segments of the [convex hull](hazardly/damage/dea.py) and yearly
yield from 1975-2020 and (B) the parametrized damage function

![For potatoes in Ostösterreich (A-C) yearly yield loss from 1981-2100 for RCP 2.6, 4.5, and 8.5 (D) boxplot of the damages](article/figures/func.svg "Damage time series and damage distribution")

**Fig 4:** For potatoes in Ostösterreich (A-C) yearly yield loss for past (black) and future from 1981-2100 for RCP 2.6
(blue), 4.5 (green), 8.5 (orange), and (D) boxplot of the damages

![Means damages for (A) past, (B) RCP 2.6, (C) 4.5, and (D) 8.5](article/figures/hw_damages_map.svg "Mean damages per NUTS 1 unit")

**Fig 5:** Mean damages for (A) past, (B) RCP 2.6, (C) 4.5, and (D) 8.5

![Means damages for (A) past, (B) RCP 2.6, (C) 4.5, and (D) 8.5](article/figures/hw_damages_map_significant.svg "Mean damages per NUTS 1 unit")

**Fig 6:** Mean damages for (A) past, (B) RCP 2.6, (C) 4.5, and (D) 8.5 for preselected damage functions

| Crop ID    |            Historic |              RCP2.6 |              RCP4.5 |              RCP8.5 |
|:-----------|--------------------:|--------------------:|--------------------:|--------------------:|
| C0000      | 0.06 (-0.09,  0.20) | 0.10 (-0.05,  0.24) | 0.20 ( 0.05,  0.32) | 0.35 ( 0.20,  0.45) |
| C1000      | 0.06 (-0.07,  0.18) | 0.11 (-0.03,  0.23) | 0.21 ( 0.05,  0.32) | 0.34 ( 0.17,  0.44) |
| C1100      | 0.05 (-0.06,  0.15) | 0.09 (-0.04,  0.18) | 0.16 ( 0.01,  0.25) | 0.26 ( 0.06,  0.35) |
| C1110      | 0.04 (-0.08,  0.16) | 0.08 (-0.05,  0.19) | 0.15 ( 0.01,  0.26) | 0.28 ( 0.13,  0.38) |
| C1111      | 0.03 (-0.07,  0.12) | 0.05 (-0.06,  0.14) | 0.09 (-0.02,  0.19) | 0.18 ( 0.02,  0.28) |
| C1120      | 0.07 (-0.12,  0.23) | 0.12 (-0.07,  0.27) | 0.25 ( 0.07,  0.36) | 0.35 ( 0.19,  0.43) |
| C1200      | 0.05 (-0.12,  0.22) | 0.09 (-0.09,  0.26) | 0.18 (-0.02,  0.35) | 0.34 ( 0.10,  0.50) |
| C1210      | 0.03 (-0.10,  0.14) | 0.05 (-0.10,  0.16) | 0.10 (-0.12,  0.22) | 0.17 (-0.16,  0.28) |
| C1300      | 0.07 (-0.11,  0.23) | 0.12 (-0.07,  0.27) | 0.23 ( 0.04,  0.36) | 0.38 ( 0.20,  0.48) |
| C1310      | 0.03 (-0.08,  0.13) | 0.05 (-0.06,  0.16) | 0.10 (-0.03,  0.21) | 0.19 ( 0.03,  0.30) |
| C1410      | 0.05 (-0.12,  0.21) | 0.10 (-0.08,  0.27) | 0.20 (-0.01,  0.36) | 0.32 ( 0.07,  0.48) |
| C1420      | 0.03 (-0.07,  0.12) | 0.06 (-0.07,  0.16) | 0.12 (-0.07,  0.23) | 0.18 (-0.08,  0.30) |
| C1500      | 0.07 (-0.11,  0.23) | 0.12 (-0.06,  0.27) | 0.22 ( 0.04,  0.36) | 0.36 ( 0.18,  0.48) |
| C1600      | 0.04 (-0.09,  0.17) | 0.07 (-0.07,  0.20) | 0.14 (-0.02,  0.28) | 0.27 ( 0.06,  0.40) |
| C1700      | 0.06 (-0.13,  0.24) | 0.11 (-0.08,  0.29) | 0.26 ( 0.03,  0.42) | 0.39 ( 0.15,  0.53) |
| C1900      | 0.02 (-0.03,  0.07) | 0.04 (-0.02,  0.09) | 0.09 ( 0.00,  0.14) | 0.14 ( 0.03,  0.17) |
| C2000      | 0.01 (-0.02,  0.04) | 0.03 (-0.00,  0.05) | 0.05 ( 0.02,  0.07) | 0.07 ( 0.05,  0.09) |
| C2100      | 0.02 (-0.06,  0.08) | 0.03 (-0.05,  0.10) | 0.07 (-0.03,  0.16) | 0.11 (-0.03,  0.22) |
| C2200      | 0.01 (-0.02,  0.04) | 0.01 (-0.02,  0.04) | 0.03 (-0.01,  0.07) | 0.05 (-0.00,  0.09) |
| F0000      | 0.08 (-0.19,  0.28) | 0.14 (-0.25,  0.38) | 0.28 (-0.34,  0.46) | 0.39 (-0.34,  0.49) |
| G0000      | 0.03 (-0.08,  0.14) | 0.06 (-0.06,  0.17) | 0.12 (-0.03,  0.24) | 0.20 ( 0.01,  0.33) |
| G1000      | 0.10 (-0.23,  0.29) | 0.16 (-0.20,  0.32) | 0.26 (-0.15,  0.39) | 0.38 (-0.09,  0.48) |
| G2000      | 0.03 (-0.09,  0.15) | 0.06 (-0.07,  0.18) | 0.13 (-0.03,  0.27) | 0.21 ( 0.01,  0.35) |
| G3000      | 0.08 (-0.09,  0.22) | 0.12 (-0.04,  0.25) | 0.21 ( 0.05,  0.33) | 0.36 ( 0.23,  0.46) |
| G9100      | 0.06 (-0.12,  0.22) | 0.11 (-0.08,  0.27) | 0.21 ( 0.01,  0.35) | 0.32 ( 0.10,  0.45) |
| I0000      | 0.02 (-0.02,  0.05) | 0.03 (-0.02,  0.07) | 0.05 (-0.01,  0.10) | 0.09 ( 0.00,  0.13) |
| I1100      | 0.06 (-0.03,  0.13) | 0.09 (-0.01,  0.16) | 0.17 ( 0.05,  0.23) | 0.23 ( 0.10,  0.27) |
| I1110      | 0.07 (-0.12,  0.24) | 0.11 (-0.08,  0.28) | 0.20 ( 0.00,  0.35) | 0.37 ( 0.18,  0.48) |
| I1110-1130 | 0.09 (-0.10,  0.26) | 0.15 (-0.05,  0.31) | 0.27 ( 0.08,  0.40) | 0.43 ( 0.25,  0.53) |
| I1111      | 0.04 (-0.08,  0.15) | 0.06 (-0.06,  0.18) | 0.11 (-0.02,  0.23) | 0.22 ( 0.06,  0.34) |
| I1120      | 0.07 (-0.12,  0.24) | 0.12 (-0.07,  0.28) | 0.24 ( 0.05,  0.37) | 0.38 ( 0.22,  0.47) |
| I1130      | 0.07 (-0.10,  0.23) | 0.13 (-0.05,  0.28) | 0.28 ( 0.09,  0.41) | 0.40 ( 0.23,  0.51) |
| I1140      | 0.08 (-0.21,  0.30) | 0.17 (-0.13,  0.33) | 0.25 (-0.01,  0.36) | 0.34 ( 0.15,  0.39) |
| I1150      | 0.05 (-0.18,  0.22) | 0.09 (-0.16,  0.23) | 0.19 (-0.11,  0.25) | 0.23 (-0.08,  0.25) |
| I2100      | 0.04 (-0.07,  0.14) | 0.09 (-0.02,  0.20) | 0.17 ( 0.04,  0.26) | 0.32 ( 0.18,  0.39) |
| I2200      | 0.04 (-0.04,  0.10) | 0.06 (-0.03,  0.13) | 0.11 (-0.02,  0.18) | 0.18 (-0.01,  0.25) |
| I2300      | 0.02 (-0.02,  0.05) | 0.03 (-0.01,  0.06) | 0.07 ( 0.03,  0.09) | 0.09 ( 0.06,  0.10) |
| I3000      | 0.05 (-0.09,  0.18) | 0.09 (-0.06,  0.21) | 0.18 ( 0.04,  0.28) | 0.26 ( 0.13,  0.33) |
| I4000      | 0.07 (-0.16,  0.27) | 0.10 (-0.14,  0.31) | 0.18 (-0.11,  0.40) | 0.33 (-0.07,  0.56) |
| I5000      | 0.02 (-0.05,  0.07) | 0.03 (-0.04,  0.08) | 0.05 (-0.03,  0.11) | 0.08 (-0.02,  0.13) |
| I6000      | 0.11 (-0.32,  0.44) | 0.18 (-0.27,  0.47) | 0.32 (-0.16,  0.55) | 0.48 (-0.02,  0.62) |
| O1000      | 0.06 (-0.17,  0.23) | 0.12 (-0.18,  0.29) | 0.22 (-0.22,  0.39) | 0.28 (-0.26,  0.46) |
| P0000      | 0.07 (-0.10,  0.22) | 0.13 (-0.05,  0.26) | 0.23 ( 0.07,  0.33) | 0.36 ( 0.24,  0.41) |
| P1100      | 0.06 (-0.11,  0.21) | 0.10 (-0.07,  0.24) | 0.19 ( 0.01,  0.32) | 0.32 ( 0.13,  0.41) |
| P1200      | 0.06 (-0.12,  0.21) | 0.11 (-0.08,  0.24) | 0.19 (-0.01,  0.30) | 0.30 ( 0.09,  0.38) |
| P1300      | 0.13 (-0.22,  0.41) | 0.19 (-0.17,  0.45) | 0.34 (-0.01,  0.55) | 0.55 ( 0.24,  0.66) |
| P9000      | 0.03 (-0.08,  0.12) | 0.06 (-0.07,  0.13) | 0.11 (-0.07,  0.16) | 0.15 (-0.07,  0.18) |
| R0000      | 0.02 (-0.05,  0.08) | 0.03 (-0.04,  0.10) | 0.06 (-0.04,  0.15) | 0.12 (-0.05,  0.21) |
| R1000      | 0.06 (-0.08,  0.19) | 0.10 (-0.04,  0.23) | 0.18 ( 0.04,  0.30) | 0.35 ( 0.20,  0.45) |
| R2000      | 0.04 (-0.06,  0.13) | 0.07 (-0.03,  0.16) | 0.13 ( 0.03,  0.21) | 0.23 ( 0.13,  0.30) |
| R9000      | 0.02 (-0.02,  0.06) | 0.04 (-0.01,  0.08) | 0.07 ( 0.01,  0.11) | 0.10 ( 0.02,  0.16) |
| W1000      | 0.06 (-0.20,  0.26) | 0.10 (-0.22,  0.31) | 0.19 (-0.26,  0.41) | 0.27 (-0.29,  0.47) |

**Table 1:** Mean damage per crop group across Europe

| Crop ID    |            Historic |              RCP2.6 |              RCP4.5 |              RCP8.5 |
|:-----------|--------------------:|--------------------:|--------------------:|--------------------:|
| C0000      | 0.08 (-0.09,  0.25) | 0.14 (-0.03,  0.31) | 0.27 ( 0.09,  0.43) | 0.48 ( 0.29,  0.63) |
| C1100      | 0.07 (-0.05,  0.18) | 0.14 ( 0.02,  0.26) | 0.27 ( 0.12,  0.39) | 0.59 ( 0.41,  0.73) |
| C1110      | 0.08 (-0.12,  0.27) | 0.12 (-0.08,  0.31) | 0.21 ( 0.01,  0.40) | 0.42 ( 0.20,  0.60) |
| C1111      | 0.09 (-0.10,  0.27) | 0.17 (-0.03,  0.35) | 0.37 ( 0.16,  0.54) | 0.61 ( 0.39,  0.76) |
| C1120      | 0.23 (-0.26,  0.59) | 0.44 (-0.02,  0.69) | 0.71 ( 0.49,  0.83) | 0.84 ( 0.70,  0.92) |
| C1300      | 0.07 (-0.06,  0.20) | 0.16 ( 0.03,  0.29) | 0.29 ( 0.15,  0.41) | 0.55 ( 0.41,  0.66) |
| C1600      | 0.32 (-0.28,  0.74) | 0.52 (-0.04,  0.84) | 0.90 ( 0.65,  0.98) | 0.99 ( 0.92,  1.00) |
| G3000      | 0.10 (-0.08,  0.28) | 0.16 (-0.03,  0.33) | 0.27 ( 0.09,  0.44) | 0.51 ( 0.33,  0.65) |
| I1100      | 0.10 (-0.04,  0.24) | 0.32 ( 0.16,  0.46) | 0.54 ( 0.38,  0.67) | 0.89 ( 0.77,  0.95) |
| I1110      | 0.08 (-0.18,  0.33) | 0.11 (-0.15,  0.35) | 0.18 (-0.09,  0.42) | 0.40 ( 0.12,  0.62) |
| I1110-1130 | 0.17 (-0.14,  0.43) | 0.30 (-0.00,  0.54) | 0.52 ( 0.23,  0.70) | 0.79 ( 0.59,  0.88) |
| I1120      | 0.12 (-0.10,  0.32) | 0.22 ( 0.00,  0.41) | 0.42 ( 0.22,  0.59) | 0.66 ( 0.49,  0.77) |
| I1130      | 0.12 (-0.08,  0.31) | 0.20 ( 0.00,  0.39) | 0.49 ( 0.30,  0.65) | 0.67 ( 0.49,  0.80) |
| I1140      | 0.10 (-0.10,  0.29) | 0.17 (-0.04,  0.36) | 0.32 ( 0.11,  0.50) | 0.64 ( 0.43,  0.78) |
| I3000      | 0.23 (-0.23,  0.59) | 0.40 (-0.04,  0.71) | 0.73 ( 0.41,  0.89) | 0.94 ( 0.80,  0.98) |
| P0000      | 0.17 (-0.23,  0.52) | 0.32 (-0.07,  0.62) | 0.57 ( 0.23,  0.78) | 0.86 ( 0.67,  0.94) |
| R1000      | 0.08 (-0.08,  0.23) | 0.13 (-0.03,  0.28) | 0.22 ( 0.06,  0.37) | 0.48 ( 0.31,  0.61) |
| R2000      | 0.11 (-0.09,  0.30) | 0.17 (-0.03,  0.35) | 0.29 ( 0.09,  0.46) | 0.53 ( 0.35,  0.67) |

**Table 2:** Mean damage per crop group across Europe for selected damage functions

## Weather extremes

| Type          | Variable      | Percentile | Window | Min duration | Rescaling | Months              |
|:--------------|:--------------|-----------:|-------:|-------------:|:----------|:--------------------|
| Heat wave     | tmin and tmax |         90 |      5 |            3 | max       | 6, 7, 8             |
| Cold wave     | tmin and tmax |         10 |      5 |            3 | max       | 1, 2, 10, 11, 12    |
| Flash drought | swvl 0-28 cm  |         30 |      5 |            5 | mean      | 6, 7, 8, 9, 10      |
| Fire risk     | FWI           |         90 |      5 |            1 | mean      | 3, 4, 5, 6, 7, 8, 9 |

## Features

Hazardly is a collection of different command line tools used for the described analysis. The following list presents a
broad overview of the available tools, while detailed documentation may be called by ``<tool> --help``.

**Data**

- **[download-fwi](hazardly/cds/cli_download_fwi.py)** Downloader for fire weather index data from CDS
- **[download-era5](hazardly/cds/cli_download_era5.py)** Downloader for ERA5 data from CDS
- **[download-cordex](hazardly/cds/cli_download_cordex.py)** Downloader for CORDEX data from CDS
- **[download-crops](hazardly/eurostat/cli_download_crops.py)** Downloader for agricultural data from Eurostat
- **[reshape](hazardly/eurostat/cli_reshape_crops.py)** Reshape Eurostat data to timeseries
- **[yield](hazardly/eurostat/cli_crop_yield.py)** Calculate yield
- **[sum-nuts](hazardly/eurostat/cli_sum_nuts.py)** Aggregate data for selected NUTS units
- **[merge-nuts](hazardly/eurostat/cli_merge_nuts.py)** Merge timeseries for selected NUTS units
- **[filter](hazardly/eurostat/cli_filter_crops.py)** Remove rows

**Weather extremes**

- **[thresher](hazardly/thresher/cli_thresher.py)** Calculate daily thresholds
- **[apply](hazardly/thresher/cli_apply.py)** Apply thresholds for exceedance
- **[spa](hazardly/thresher/cli_spa.py)** Sequent peak analysis by using thresholds
- **[daily](hazardly/thresher/cli_daily.py)** Aggregate exceedance to daily exceedance
- **[monthly](hazardly/thresher/cli_monthly.py)** Aggregate exceedance to monthly exceedance
- **[yearly](hazardly/thresher/cli_yearly.py)** Aggregate exceedance to yearly exceedance
- **[pool](hazardly/threshertools/cli_pool.py)** Interevent pooling of daily exceedance
- **[consecutive](hazardly/threshertools/cli_consecutive.py)** Consecutive pooling of daily exceedance
- **[setto](hazardly/threshertools/cli_setto.py)** Set values to a constant
- **[rescale](hazardly/threshertools/cli_rescale.py)** Rescale grid data to polygons
- **[merge-extremes](hazardly/extremes/cli_merge.py)** Merge in-base and out-base exceedance

**Damages**

- **[means](hazardly/damage/cli_means.py)** Weighted aggregation of yield loss
- **[damages](hazardly/damage/cli_damages.py)** Calculates yield loss
- **[decisions](hazardly/damage/cli_decisions.py)** Flexibel model filtering
- **[functions](hazardly/damage/cli_functions.py)** Parametrize damage functions
- **[correlations](hazardly/damage/cli_correlations.py)** Correlate weather extremes with yield loss
- **[corr-plot](hazardly/damage/cli_plot_correlations.py)** Plot correlation heatmap
- **[panel-one](hazardly/damage/cli_plot_panels.py)** Plot convex hull and damage function
- **[panel-two](hazardly/damage/cli_plot_panels.py)** Plot damage timeseries and distribution
- **[panel-three](hazardly/damage/cli_plot_panels.py)** Plot damage map

## Authors

Main author of this package is [Tobias Seydewitz](https://www.pik-potsdam.de/members/tobiasse) with contributions from
[Diego Rybski](https://www.pik-potsdam.de/members/rybski), [Prajal Pradhan](https://www.pik-potsdam.de/members/pradhan),
[Christoph Müller](https://www.pik-potsdam.de/members/cmueller),
and [Jürgen Kropp](https://www.pik-potsdam.de/members/kropp/).

## Funding

Tobias Seydewitz acknowledges funding from the German Federal Ministry of Education and Research for the
[BIOCLIMAPATHS](https://www.pik-potsdam.de/en/output/projects/all/647) project (grant agreement No 01LS1906A) under the
Axis-ERANET call. The funders had no role in study design, data collection, analysis, decision to publish, or manuscript
preparation.
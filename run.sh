#!/bin/bash

#SBATCH --account=nsp
#SBATCH --job-name=extremes

#SBATCH --error=data/err_hdroughts_%j.log
#SBATCH --output=data/out_hdroughts_%j.log
#SBATCH --workdir=/p/tmp/tobiasse/hazardly

#SBATCH --qos=medium
#SBATCH --time=1-0

#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=16384

python -u hazardly/damage/cli_correlations.py
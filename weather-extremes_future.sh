#!/bin/bash

# Download daily historic and future projections of fire-risk and temperature from CDS.
# $1 Is the path where the downloaded files are stored, e.g., data/raw/.
function download {
  local path=${1:-}

  local oldIFS="$IFS"
  IFS=";"

  # Daily fire weather index data
  local prefix="fwi"
  local gcmRcms=("ichec_ec_earth" "mpi_m_mpi_esm_lr") # Downscaled by SMHI-RCA4
  local expPeriod=("historical;1981-2005" "rcp_2_6;2006-2098" "rcp_4_5;2006-2098" "rcp_8_5;2006-2098")

  for m in "${gcmRcms[@]}"; do
    for ep in "${expPeriod[@]}"; do
      read -r -a arr <<<"${ep}"
      local e=${arr[0]}
      local p=${arr[1]}
      echo "Downloading... ${prefix}_${m}_${e}.zip to ${path}"
      download-fwi -e "$e" --gcm "$m" -y "$p" -p "${path}${prefix}_${m}_${e}.zip"
    done
  done

  # Daily min and max temperature data
  local prefix="tmp"
  local gcmRcms=(
    "ichec_ec_earth;smhi_rca4;r1i1p1;r12i1p1;r12i1p1;r1i1p1"
    "mpi_m_mpi_esm_lr;smhi_rca4;r1i1p1;r1i1p1;r1i1p1;r1i1p1"
  )
  local sy="2006,2011,2016,2021,2026,2031,2036,2041,2046,2051,2056,2061,2066,2071,2076,2081,2086,2091,2096"
  local ey="2010,2015,2020,2025,2030,2035,2040,2045,2050,2055,2060,2065,2070,2075,2080,2085,2090,2095,2100"
  local expPeriod=(
    "historical;1981,1986,1991,1996,2001;1985,1990,1995,2000,2005"
    "rcp_2_6;${sy};${ey}"
    "rcp_4_5;${sy};${ey}"
    "rcp_8_5;${sy};${ey}"
  )
  local t="daily_mean"
  local v="maximum_2m_temperature_in_the_last_24_hours,minimum_2m_temperature_in_the_last_24_hours"

  for m in "${gcmRcms[@]}"; do
    read -r -a arr <<<"${m}"
    local g=${arr[0]}
    local r=${arr[1]}
    local ensemble=("${arr[@]:2:$((${#arr[@]} - 2))}")
    for i in {0..3}; do
      local e=${ensemble[$i]}
      read -r -a arr <<<"${expPeriod[$i]}"
      local s=${arr[0]}
      local sp=${arr[1]}
      local ep=${arr[2]}
      echo "Downloading... ${prefix}_${g}_${r}_${s}.zip to ${path}"
      download-cordex -s "$s" -t "$t" -v "$v" -g "$g" -r "$r" -e "$e" --start "$sp" --end "$ep" \
        "${path}${prefix}_${g}_${r}_${s}.zip"
    done
  done

  IFS="$oldIFS"
}

# Preprocess daily fire-risk and temperature data.
# $1 Is the location of the files to preprocess.
# $2 Is the path where the preprocessed files are stored.
# $3 Is the grid definition file used for remapping.
function preprocess {
  local in=${1:-}
  local out=${2:-}
  local grid=${3:-"grid"}
  local remap="remapbil"

  local oldIFS="$IFS"
  IFS=";"

  # Daily fire weather index data
  local prefix="fwi"
  local var="fwi-daily-proj"
  local gcmRcms=("ichec_ec_earth" "mpi_m_mpi_esm_lr")
  local exp=("historical" "rcp_2_6" "rcp_4_5" "rcp_8_5")

  for e in "${exp[@]}"; do
    local ncs=()
    for m in "${gcmRcms[@]}"; do
      echo "Merging... ${in}${prefix}_${m}_${e}.zip to ${out}${prefix}_${m}_${e}.nc"
      merge -a "${in}${prefix}_${m}_${e}.zip" -n "${out}${prefix}_${m}_${e}.nc" --variable "$var" -g "$grid" -m "$remap"
      ncs+=(--ncs)
      ncs+=("${out}${prefix}_${m}_${e}.nc")
    done
    echo "Aggregating..." "${ncs[@]}" "to ${out}${prefix}_${e}.nc"
    stats -p "${out}${prefix}_${e}.nc" -v "$var" --me "${ncs[@]}"
  done

  # Daily min and max temperature data
  local prefix="tmp"
  local vars=("tasmin" "tasmax")
  local gcmRcms=("ichec_ec_earth;smhi_rca4" "mpi_m_mpi_esm_lr;smhi_rca4")
  local exp=("historical" "rcp_2_6" "rcp_4_5" "rcp_8_5")

  for e in "${exp[@]}"; do
    local exps=()
    for m in "${gcmRcms[@]}"; do
      read -r -a arr <<<"${m}"
      local g=${arr[0]}
      local r=${arr[1]}
      local merges=()
      for v in "${vars[@]}"; do
        echo "Merging... ${in}${prefix}_${g}_${r}_${e}.zip to ${out}${prefix}_${g}_${r}_${e}_${v}.nc"
        merge --variable "$v" -g "$grid" -m "$remap" \
          --pattern "${v}_EUR-11_[0-9A-Za-z-]+_${e//_/}_r\d+i\d+p\d+_SMHI-RCA4_v\d+(?:[a-zA-Z]+)?_day_\d{8}-\d{8}\.nc" \
          -a "${in}${prefix}_${g}_${r}_${e}.zip" -n "${out}${prefix}_${g}_${r}_${e}_${v}.nc"
        merges+=("${out}${prefix}_${g}_${r}_${e}_${v}.nc")
      done
      echo "Merging..." "${merges[@]}" "to ${out}${prefix}_${g}_${r}_${e}.nc"
      cdo merge "${merges[@]}" "${out}${prefix}_${g}_${r}_${e}.nc"
      exps+=(--ncs)
      exps+=("${out}${prefix}_${g}_${r}_${e}.nc")
    done
    echo "Aggregating..." "${exps[@]}" "to ${out}${prefix}_${e}.nc"
    stats -p "${out}${prefix}_${e}.nc" -v "$(join "," "${vars[@]}")" --me "${exps[@]}"
  done

  IFS="$oldIFS"
}

# Preprocess daily fire-risk and temperature data.
# $1 Is the location of the files to preprocess.
# $2 Is the path where the preprocessed files are stored.
# $3 Threshold mask.
# $4 Rescaling mask.
function process {
  local in=${1:-}
  local out=${2:-}
  local tmask=${3:-"eu.shp"}
  local rmask=${4:-"nuts1.shp"}

  local window=5
  local start=1981
  local end=2005

  # Fire-risk extremes
  local exp=("rcp_2_6" "rcp_4_5" "rcp_8_5")
  local var="fwi-daily-proj_mean"
  local prefix="fwi"
  local season="6-10"
  local percentile="90"

  echo "Thresholds... ${in}${prefix}_historical.nc to ${out}${prefix}_thresholds.nc"
  thresher -p "${var}>${percentile}" -y $start $end -m "$season" -w "$window" -r "$tmask" \
    "${in}${prefix}_historical.nc" "${out}${prefix}_thresholds.nc"

  for e in "${exp[@]}"; do
    echo "Applying... ${out}${prefix}_thresholds.nc to ${out}${prefix}_${e}_daily_exceedance.nc"
    apply "${in}${prefix}_${e}.nc" "${out}${prefix}_thresholds.nc" "${out}${prefix}_${e}_daily_exceedance.nc"

    echo "Yearly... ${out}${prefix}_${e}_daily_exceedance.nc to ${out}${prefix}_${e}_yearly_exceedance.nc"
    yearly "${out}${prefix}_${e}_daily_exceedance.nc" "${out}${prefix}_${e}_yearly_exceedance.nc"

    echo "Rescaling... ${out}${prefix}_${e}_yearly_exceedance.nc to ${out}${prefix}_${e}.geojson"
    rescale -v "exceedance" -a "me" --encoding "utf-8" \
      "${out}${prefix}_${e}_yearly_exceedance.nc" "$rmask" "${out}${prefix}_${e}.geojson"
  done

  # Temperatur extremes
  local exp=("rcp_2_6" "rcp_4_5" "rcp_8_5")
  local prefix="tmp"
  local season=("6-9" "1-3,10-12")
  local month=("9" "3")
  local percentiles=("tasmin_mean>90,tasmax_mean>90" "tasmin_mean<10,tasmax_mean<10")

  for ((i = 0; i < "${#percentiles[@]}"; i++)); do
    p="${percentiles[$i]}"
    s="${season[$i]}"
    m="${month[$i]}"
    thresher -p "$p" -y $start $end -m "$s" -w "$window" -r "$tmask" \
      "${in}${prefix}_historical.nc" "${out}${prefix}_${p}_thresholds.nc"

    for e in "${exp[@]}"; do
      apply "${in}${prefix}_${e}.nc" "${out}${prefix}_${p}_thresholds.nc" "${out}${prefix}_${p}_${e}_exceedance.nc"

      daily "${out}${prefix}_${p}_${e}_exceedance.nc" "${out}${prefix}_${p}_${e}_daily_exceedance.nc"

      setto -v "exceedance" -m "$m" -c 0 "${out}${prefix}_${p}_${e}_daily_exceedance.nc"

      consecutive -v "exceedance" -s ">=1" -e "<1" --mn 3 "${out}${prefix}_${p}_${e}_daily_exceedance.nc"

      cdo --timestat_date "first" yearsum \
        "${out}${prefix}_${p}_${e}_daily_exceedance.nc" "${out}${prefix}_${p}_${e}_yearly_exceedance.nc"

      rescale -v "exceedance_consecutive" -a "mx" --encoding "utf-8" \
        "${out}${prefix}_${p}_${e}_yearly_exceedance.nc" "$rmask" "${out}${prefix}_${p}_${e}.geojson"
    done
  done
}

# Join list elements by a delimiter (stackoverflow.com/questions/1527049/how-can-i-join-elements-of-an-array-in-bash).
# $1 Is the delimiter.
# $2 Is the list to join.
function join {
  local d=${1-} f=${2-}
  if shift 2; then
    printf %s "$f" "${@/#/$d}"
  fi
}

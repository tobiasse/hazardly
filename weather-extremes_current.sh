#!/bin/bash

function download {
  local path=${1:-}
  local v=$2
  local e=$3

  local hours="0-23"
  local days="1-31"
  local months="1-12"

  for y in {1981..2020}; do
    echo "Downloading... ${v}_${y}.$e to $path"
    download-era5 -v "$v" -h "$hours" -d "$days" -m "$months" -y "$y" "$path${v}_${y}.$e"
  done
}

function fwi {
  true;
}

function temperature {
  true;
}

function precipitation {
    true;
}

function soil_water {
  true;
}

function run {
  local raw="data/raw/"
  local proc="data/proc/"
  local interim="data/interim/"

  local ext=("zip" "nc" "nc" "nc" "nc")
  local vars=("fire_weather_index" "2m_temperature" "total_precipitation" "volumetric_soil_water_layer_1" "volumetric_soil_water_layer_2")

  for ((i = 0; i < ${#vars[@]}; i++)); do
    download $raw "${vars[$i]}" "${ext[$i]}"
  done
}

# for temperature, precipitation -> find -> copy to float -> mergetime